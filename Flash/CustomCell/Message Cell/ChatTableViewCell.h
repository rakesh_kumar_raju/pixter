//
//  ChatTableViewCell.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 3/3/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatTableViewCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UILabel *friendNameLbl, *selfUserNAmeLbl, *timeLbl;

@property (nonatomic, retain) IBOutlet UILabel *friendMsgLbl, *selfUserMsgLbl;

@property (nonatomic, retain) IBOutlet UIImageView *friendImgView, *selfUserImg;

@end
