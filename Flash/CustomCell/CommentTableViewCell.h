//
//  CommentTableViewCell.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 2/17/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ResponsiveLabel.h"


@interface CommentTableViewCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UILabel *nameLbl, *timeLbl;

@property (nonatomic, retain) IBOutlet ResponsiveLabel *commentLbl;

@property (nonatomic, retain) IBOutlet UIImageView *userProfileImg;

@property (nonatomic, retain) IBOutlet UIButton *totalLikeBtn, *replyBtn, *likeBtn;

@end
