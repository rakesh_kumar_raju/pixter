//
//  MessageTableViewCell.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 1/2/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageTableViewCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UILabel *nameLbl, *messageLbl, *timeLbl;

@property (nonatomic, retain) IBOutlet UIImageView *profileImg;

@property (nonatomic, retain) IBOutlet NSLayoutConstraint *nameLblTop;

@end
