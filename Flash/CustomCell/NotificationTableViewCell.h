//
//  NotificationTableViewCell.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 1/4/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationTableViewCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UIImageView *userImg, *mediaImg;

@property (nonatomic, retain) IBOutlet UILabel *descLbl, *timeLbl;

@property (nonatomic, retain) IBOutlet UIButton *acceptBtn, *cancelBtn, *tapOnPost, *tapOnUserImg;

@end
