//
//  RewardTableViewCell.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 1/9/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RewardTableViewCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UIImageView *coponImg;

@property (nonatomic, retain) IBOutlet UILabel *titleLbl, *descLbl, *pointsLbl, *priceLbl;

@property (nonatomic, retain) IBOutlet UIButton *buyNowBtn;

@end
