//
//  SerachTableViewCell.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 2/24/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SerachTableViewCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UIImageView *userImg;

@property (nonatomic, retain) IBOutlet UILabel *nameLbl, *totalPostLbl;

@end
