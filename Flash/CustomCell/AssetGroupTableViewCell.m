//
//  AssetGroupTableViewCell.m
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 1/27/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import "AssetGroupTableViewCell.h"

@implementation AssetGroupTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
