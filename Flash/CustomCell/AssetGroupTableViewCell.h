//
//  AssetGroupTableViewCell.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 1/27/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AssetGroupTableViewCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UIImageView *posterImg;

@property (nonatomic, retain) IBOutlet UILabel *titleLbl, *detailLbl;

@end
