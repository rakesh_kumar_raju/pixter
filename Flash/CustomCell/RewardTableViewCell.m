//
//  RewardTableViewCell.m
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 1/9/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import "RewardTableViewCell.h"

@implementation RewardTableViewCell


- (void)awakeFromNib
{
    [super awakeFromNib];
   
    self.buyNowBtn.layer.borderWidth = 1.0;
    
    self.buyNowBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    
    self.buyNowBtn.layer.cornerRadius = 3.0;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
