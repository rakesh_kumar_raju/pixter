//
//  LikesTableViewCell.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 2/20/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface LikesTableViewCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UILabel *nameLbl;

@property (nonatomic, retain) IBOutlet UIImageView *userProfileImg;

@property (nonatomic, retain) IBOutlet UIButton *followBtn;


@end
