//
//  CouponTableViewCell.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 5/4/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CouponTableViewCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UIImageView *coponImg;

@property (nonatomic, retain) IBOutlet UILabel *titleLbl, *codeLbl, *timeLbl;


@end
