//
//  AppDelegate.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 12/30/16.
//  Copyright © 2016 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#define APPDEL ((AppDelegate *)[[UIApplication sharedApplication] delegate])

#define ADMob_UnitID_For_Reward_Video  @"ca-app-pub-9153505799578900/2529446074"

#define ADMob_APPID @"ca-app-pub-9153505799578900~4620444870"


#import <UIKit/UIKit.h>

#import <Fabric/Fabric.h>
#import <DigitsKit/DigitsKit.h>
#import <Crashlytics/Crashlytics.h>

#import <CoreLocation/CoreLocation.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    CLLocationManager *locationManager;
}


@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, assign) BOOL isVideo, isFrontCamera;

@property (nonatomic, retain) NSMutableArray *AvplayerObj, *tagPeopleArr;

@property (nonatomic, retain) NSDictionary *userDetailDict, *currentLocation;

@property (nonatomic, retain) NSString *locationStr;


@end

