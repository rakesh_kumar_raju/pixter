//
//  ShowAllUserViewController.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 3/3/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServiceHelper.h"

@interface ShowAllUserViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate>
{
    IBOutlet UITableView *userTbl;
    
    IBOutlet UILabel *titleLbl;
    
    IBOutlet UISearchBar *searchBar;
}

@property (nonatomic, retain) NSString *titleStr;

- (IBAction)backTap;


@end
