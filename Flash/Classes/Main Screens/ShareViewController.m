//
//  ShareViewController.m
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 2/9/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import "ShareViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "TabbarViewController.h"
#import "CameraViewController.h"
#import "TagViewController.h"
#import "AddLocationViewController.h"


@interface ShareViewController ()
{
    NSString *tagPeopleStr;
    
    NSString *locationStr;
    
    NSMutableArray *taggArr;
    
    BOOL isPressPost;
    
    int imageWidth, imageHeight;
}


@end


@implementation ShareViewController


#pragma mark - ViewCycle


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    imageWidth = 0;
    
    imageHeight = 0;
    
    
    [self setUpView];
    
    
    if (self.isVideo == YES)
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
        NSString *inputPath = [documentsDirectory stringByAppendingPathComponent:@"output.mov"];
        
        NSString *outputPath = [documentsDirectory stringByAppendingPathComponent:@"123.mov"];
        
        
        NSURL *videoURL = [NSURL fileURLWithPath:inputPath];
        NSURL *outputURL = [NSURL fileURLWithPath:outputPath];
        
        [[ServiceHelper sharedManager] showHud];
        
        [self convertVideoToLowQuailtyWithInputURL:videoURL outputURL:outputURL handler:^(AVAssetExportSession *exportSession)
         {
             if (exportSession.status == AVAssetExportSessionStatusCompleted)
             {
                 [[ServiceHelper sharedManager] dismisHud];
                 
                 printf("completed\n");
             }
             else
             {
                 printf("error\n");
                 
             }
         }];
    }
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    locationLbl.text = (APPDEL).locationStr;
    
    captionTv.placeholder = @"Add Description...";

    captionTv.placeholderTextColor = [UIColor whiteColor];


    shareBtn.layer.cornerRadius = 25.0;
    
    shareBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    
    shareBtn.layer.borderWidth = 1.0;
    
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)])
    {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    
    
    taggArr = [NSMutableArray new];
    
    for (NSDictionary *dict in (APPDEL).tagPeopleArr)
    {
        if ([dict allKeys].count)
        {
            [taggArr addObject:dict];
        }
    }
    
    if (taggArr.count)
    {
        totalTagPeopleLbl.text = [NSString stringWithFormat:@"%lu People", (unsigned long)taggArr.count];
    }
    else
    {
        totalTagPeopleLbl.text = @"";
    }
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
 
    
    if (self.isVideo == YES)
    {
        tagBTnHeight.constant = 0;
        
        tagPeopleHeight.constant = 0;
        
        lineTop.constant = 0;
        
        tagBTn.hidden = YES;
        
        titleLbl.text = @"Post";
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
       
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
        NSString *stringPath = [documentsDirectory stringByAppendingPathComponent:@"output.mov"];
        
        stringPath = [documentsDirectory stringByAppendingPathComponent:@"output.mov"];
       
        /*if(self.isFilter == YES)
        {
            stringPath = [documentsDirectory stringByAppendingPathComponent:@"filter.mov"];
        }
        else
        {
            stringPath = [documentsDirectory stringByAppendingPathComponent:@"output.mov"];
        }*/
       
        NSURL *videoURL = [NSURL fileURLWithPath:stringPath];
        
        
        AVURLAsset *asset1 = [[AVURLAsset alloc] initWithURL:videoURL options:nil];
        
        
        AVAssetImageGenerator *generate1 = [[AVAssetImageGenerator alloc] initWithAsset:asset1];
        
        generate1.appliesPreferredTrackTransform = YES;
        
        
        NSError *err = NULL;
        
        CMTime time = CMTimeMake(1, 2);
        
        CGImageRef oneRef = [generate1 copyCGImageAtTime:time actualTime:NULL error:&err];
        
        UIImage *one = [[UIImage alloc] initWithCGImage:oneRef];
        
        
        thumbNailImg.image = one;
        

        NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:@"filterImage.jpg"];
        
        NSData *imageData = UIImageJPEGRepresentation(one, 1);
        
        [imageData writeToFile:savedImagePath atomically:NO];

    }
    else
    {
        titleLbl.text = @"Post";
        
        NSString *documentDirectory=[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
        
        NSString *imagePath = [documentDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",@"filterImage.jpg"]];
        
        UIImage *image = [self imageWithImage:[UIImage imageWithContentsOfFile:imagePath] scaledToWidth:[[UIScreen mainScreen] bounds].size.width ];
        
        thumbNailImg.image = image;
    

        imageWidth = image.size.width;
        
        imageHeight = image.size.height;
    }
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    
   /* [UIView animateWithDuration:0.2 animations:^{
        
        self.tabBarController.tabBar.frame = CGRectMake(0, [[UIScreen mainScreen] bounds].size.height-self.tabBarController.tabBar.frame.size.height, self.tabBarController.tabBar.frame.size.width, self.tabBarController.tabBar.frame.size.height);
    }
                     completion:^(BOOL finished)
     {
         
     }];*/
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
}


#pragma mark - Void

- (void) setUpView
{
   taggArr = [NSMutableArray new];
}


#pragma mark - Video compress

- (void)convertVideoToLowQuailtyWithInputURL:(NSURL*)inputURL
                                   outputURL:(NSURL*)outputURL
                                     handler:(void (^)(AVAssetExportSession*))handler
{
    [[NSFileManager defaultManager] removeItemAtURL:outputURL error:nil];
   
    AVURLAsset *asset = [AVURLAsset URLAssetWithURL:inputURL options:nil];
  
    AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPresetMediumQuality];
   
    exportSession.outputURL = outputURL;
  
    exportSession.outputFileType = AVFileTypeQuickTimeMovie;
   
    [exportSession exportAsynchronouslyWithCompletionHandler:^(void)
     {
         handler(exportSession);
         
         ALAssetsLibrary *assetLibrary=[[ALAssetsLibrary alloc] init];
         
         __block NSData *data;
         
         [assetLibrary assetForURL: outputURL resultBlock:^(ALAsset *asset)
          {
              ALAssetRepresentation *rep = [asset defaultRepresentation];
              
              Byte *buffer = (Byte*)malloc(rep.size);
              
              NSUInteger buffered = [rep getBytes:buffer fromOffset:0.0 length:rep.size error:nil];
              
              data = [NSData dataWithBytesNoCopy:buffer length:buffered freeWhenDone:YES];
              
              NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
              
              NSString *documentsDirectory = [paths objectAtIndex:0];
              
              NSString *stringPath = [documentsDirectory stringByAppendingPathComponent:@"output.mov"];
              
              stringPath = [documentsDirectory stringByAppendingPathComponent:@"123123.mov"];
              
              [data writeToFile:stringPath atomically:YES];
          }
                      failureBlock:^(NSError *err)
          {
              NSLog(@"Error: %@",[err localizedDescription]);
              
          }];

     }];
}

#pragma mark - Get image size


- (UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width
{
    float oldWidth = sourceImage.size.width;
    
    float scaleFactor = i_width / oldWidth;
    
    
    float newHeight = sourceImage.size.height * scaleFactor;
    
    float newWidth = oldWidth * scaleFactor;
    
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return newImage;
}



#pragma mark - Action


- (IBAction)backTap
{
    for(UIViewController *controller in self.navigationController.viewControllers)
    {
        if ([controller isKindOfClass:[CameraViewController class]])
        {
            (APPDEL).locationStr = @"";
            
            [(APPDEL).tagPeopleArr removeAllObjects];
            
            [self.navigationController popToViewController:controller animated:NO];
        }
    }
    
}


- (IBAction)shareTap
{
    [self.view endEditing: YES];
    
    if (isPressPost == NO)
    {
        [self shareFileOnServer];
    }
}


- (IBAction)tagPeopleTap
{
    TagViewController *vc = [[TagViewController alloc] initWithNibName:@"TagViewController" bundle:nil];
    
    vc.image = thumbNailImg.image;
    
    vc.ifEditPost = NO;
    
    [self presentViewController:vc animated:YES completion:nil];
}


- (IBAction)addLocationTap
{
    AddLocationViewController *vc = [[AddLocationViewController alloc] initWithNibName:@"AddLocationViewController" bundle:nil];
    
    [self presentViewController:vc animated:YES completion:nil];
}



#pragma mark - WebService


- (void)shareFileOnServer
{
    isPressPost = YES;
    
    NSError * err;
  
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:taggArr options:0 error:&err];
    
    
    tagPeopleStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    
//    NSError *jsonError;
//    
//    NSData *data = [tagPeopleStr dataUsingEncoding:NSUTF8StringEncoding];;
//    
//    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data
//                                                         options:NSJSONReadingMutableContainers
//                                                           error:&jsonError];
//    
    
  
    locationStr = (APPDEL).locationStr;
    
    
    
    NSDictionary *params;
    
    NSString *userId = [(APPDEL).userDetailDict objectForKey:@"userId"];
    
    if (self.isVideo == YES)
    {
        params = [NSDictionary dictionaryWithObjectsAndKeys:
                  tagPeopleStr.length ? tagPeopleStr : @"", @"tagpeople",
                  locationStr.length ? locationStr : @"", @"location",
                  @"video", @"type",
                  userId, @"userId",
                  captionTv.text, @"caption",
                  enableCommentSwitch.on == YES ? @"1" : @"0", @"commentflag",  nil];
    }
    
    else
    {
        params = [NSDictionary dictionaryWithObjectsAndKeys:
                  tagPeopleStr.length ? tagPeopleStr : @"", @"tagpeople",
                  locationStr.length ? locationStr : @"", @"location",
                  @"image", @"type",
                  userId, @"userId",
                  captionTv.text, @"caption",
                  enableCommentSwitch.on == YES ? @"1" : @"0", @"commentflag",
                  [NSString stringWithFormat:@"%i", imageHeight],@"height",
                  [NSString stringWithFormat:@"%i", imageWidth],@"width",nil];
    }
    
    
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:@"http://52.43.13.44/flashws/ws/addPosts" parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
                                    {
                                        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                                        
                                       
                                        if (self.isVideo)
                                        {
                                            NSString *documentsDirectoryForMovie;
                                            
                                             documentsDirectoryForMovie = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"123.mov"];
                                            /*if(self.isFilter == YES)
                                            {
                                                documentsDirectoryForMovie = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"filter.mov"];
                                            }
                                            else
                                            {
                                                documentsDirectoryForMovie = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"output.mov"];
                                            }*/
                                            
                                            NSString *thumbnail = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"filterImage.jpg"];
                                            
                                            
                                            [formData appendPartWithFileURL:[NSURL fileURLWithPath:documentsDirectoryForMovie] name:@"file" fileName:@"123.mov" mimeType:@"video/quicktime" error:nil];
                                            
                                            [formData appendPartWithFileURL:[NSURL fileURLWithPath:thumbnail] name:@"thumbnail" fileName:@"filterImage.jpg" mimeType:@"image/jpeg" error:nil];
                                        }
                                        else
                                        {
                                            NSString *documentsDirectoryForImage = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"filterImage.jpg"];
                                           
                                            [formData appendPartWithFileURL:[NSURL fileURLWithPath:documentsDirectoryForImage] name:@"file" fileName:@"filterImage.jpg" mimeType:@"image/jpeg" error:nil];
                                        }
                                        
                                    } error:nil];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
   
    NSURLSessionUploadTask *uploadTask;
    
    uploadTask = [manager uploadTaskWithStreamedRequest:request  progress:^(NSProgress * _Nonnull uploadProgress)
                  {
                      
                      dispatch_async(dispatch_get_main_queue(), ^{
                          //Update the progress view
                          //[progressView setProgress:uploadProgress.fractionCompleted];
                          
                          if((int)uploadProgress.fractionCompleted == 1)
                          {
                             [[ServiceHelper sharedManager] showHudWithProgress:uploadProgress.fractionCompleted withStatus:@"Complete"];
                          }
                          else
                          {
                               [[ServiceHelper sharedManager] showHudWithProgress:uploadProgress.fractionCompleted withStatus:@"Progress.."];
                          }
                        
                      });
                  }
                                      completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error)
                  {
                      [[ServiceHelper sharedManager] dismisHud];
                      
                     
                      
                      if (error)
                      {
                          NSLog(@"Error: %@", error);
                          
                          //[[UIAlertView alloc] in];
                      }
                      
                      else
                      {
                          isPressPost = NO;
                          
                          NSLog(@"Complete");
                          
                          [(APPDEL).tagPeopleArr removeAllObjects];
                          
                          (APPDEL).locationStr = @"";
                          
                          //TabbarViewController *tvc = [[TabbarViewController alloc] initWithNibName:@"TabbarViewController" bundle:nil];
                          
                          self.tabBarController.selectedIndex = 0;
                    
                          for (UIViewController *controller in self.navigationController.viewControllers)
                          {
                              if ([controller isKindOfClass:[CameraViewController class]])
                              {
                                  [self.navigationController popToViewController:controller animated:YES];
                              }
                          }
                          
                          [[NSNotificationCenter defaultCenter] postNotificationName:@"imagePostAndScrolToTop" object:nil];
                          
                          //[self.navigationController pushViewController:tvc animated:YES];
                          
                          
                      }
                  }];
    
    [uploadTask resume];
    
}



#pragma mark - TextView

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (textView == captionTv)
    {        
        return [[textView text] length] - range.length + text.length <= 1000;
    }
    return NO;
}

@end
