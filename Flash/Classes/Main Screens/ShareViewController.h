//
//  ShareViewController.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 2/9/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "ServiceHelper.h"
#import "AppDelegate.h"
#import "SZTextView.h"


@interface ShareViewController : UIViewController <UITextViewDelegate>
{
    IBOutlet UIImageView *thumbNailImg;
    
    IBOutlet UISwitch *enableCommentSwitch;
    
    IBOutlet SZTextView *captionTv;
    
    IBOutlet UIButton *tagBTn, *shareBtn;
    
    IBOutlet UILabel *titleLbl, *locationLbl, *totalTagPeopleLbl;
    
    IBOutlet NSLayoutConstraint *tagBTnHeight, *tagPeopleHeight, *lineTop;
}

@property (nonatomic, assign) BOOL isVideo, isFilter, *isEdit;


- (IBAction)backTap;

- (IBAction)shareTap;

- (IBAction)tagPeopleTap;

- (IBAction)addLocationTap;

@end
