//
//  SettingViewController.m
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 3/28/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import "SettingViewController.h"
#import "EditProfileViewController.h"
#import "CreateBusinessViewController.h"
#import "TermOfServiceViewController.h"
#import "WelcomeViewController.h"
#import "PushnotificationViewController.h"
#import "ZOWVideoCache.h"

#import <MessageUI/MessageUI.h> 

@interface SettingViewController ()<UIAlertViewDelegate, UIActionSheetDelegate, MFMailComposeViewControllerDelegate>
{
    NSMutableDictionary *settingDict;
}


@end


@implementation SettingViewController


#pragma mark - ViewCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [self setUpView];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
}


#pragma mark - Void


- (void)setUpView
{
    NSLog(@"%@", (APPDEL).userDetailDict);
    
    settingDict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
  
                   @[@{@"title": @"Edit Profile"} ,@{@"title": @"Push Notifications", @"isOn":[[(APPDEL).userDetailDict objectForKey:@"notification_flag"] intValue] == 0 ? @"0" : @"1"}, @{@"title" : @"Clear Cache"}, @{@"title" : @"Private Account", @"isOn":[[(APPDEL).userDetailDict objectForKey:@"privacy"] intValue] == 0 ? @"0" : @"1"}], @"Account",
                   
                   @[@{@"title": @"Report Problem"}, @{@"title": @"Terms of Service"},  @{@"title": @"Privacy Policy"}, @{@"title": @"Logout"}, @{@"title": @"Delete Account"}], @"Support", nil];
    
    
    [privacySwitch setOn: [[(APPDEL).userDetailDict objectForKey:@"privacy"] intValue] == 0 ? NO : YES];
    
    [enablePushNotification setOn: [[(APPDEL).userDetailDict objectForKey:@"notification_flag"] intValue] == 0 ? YES : NO];
}


- (void)deleteAccount
{
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"listAccount" setHTTPMethod:@"POST" params:[NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userId",   nil] completion:^(NSDictionary *result)
     {
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 UIAlertController *alertController = [UIAlertController
                                                       alertControllerWithTitle:nil
                                                       message:nil
                                                       preferredStyle:UIAlertControllerStyleActionSheet];
                 
                 NSArray *accArr = [result objectForKey:@"detail"];
                 
                 if (accArr.count == 1)
                 {
                     NSString *primary = [[[result objectForKey:@"detail"] objectAtIndex:0] objectForKey:@"name"];
                     
                     
                    
                     UIAlertAction *primaryAcc = [UIAlertAction
                                                  actionWithTitle:[NSString stringWithFormat:@"Delete(%@)", primary]
                                                  style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction *action)
                                                  {
                                                      NSPredicate *predicate =[NSPredicate predicateWithFormat:@"name ==[c] %@", primary];
                                                      
                                                      NSArray *arr = [[result objectForKey:@"detail"] filteredArrayUsingPredicate:predicate ];
                                                      
                                                      NSLog(@"%@", arr);
                                                      
                                                      [self deleteAccountWebService : [[arr objectAtIndex:0] objectForKey:@"id"]];
                                                  }];
                     
                     UIAlertAction *cancel = [UIAlertAction
                                              actionWithTitle:@"Cancel"
                                              style:UIAlertActionStyleCancel
                                              handler:^(UIAlertAction *action)
                                              {
                                                  NSLog(@"Cancel, %li", (long)alertController.view.tag);
                                              }];
                     
                     
                     [alertController addAction:primaryAcc];
                   
                     [alertController addAction:cancel];
                 }
                 else
                 {
                     NSString *primary = [[[result objectForKey:@"detail"] objectAtIndex:0] objectForKey:@"name"];
                     
                     
                     NSString *secondry = [[[result objectForKey:@"detail"] objectAtIndex:1] objectForKey:@"name"];
                     
                     UIAlertAction *primaryAcc = [UIAlertAction
                                                  actionWithTitle:[NSString stringWithFormat:@"Delete(%@)", primary]
                                                  style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction *action)
                                                  {
                                                      NSPredicate *predicate =[NSPredicate predicateWithFormat:@"name ==[c] %@", primary];
                                                      
                                                      NSArray *arr = [[result objectForKey:@"detail"] filteredArrayUsingPredicate:predicate ];
                                                      
                                                      NSLog(@"%@", arr);
                                                      
                                                      [self deleteAccountWebService : [[arr objectAtIndex:0] objectForKey:@"id"]];
                                                  }];
                     
                     UIAlertAction *secondryAcc = [UIAlertAction
                                                   actionWithTitle:[NSString stringWithFormat:@"Delete(%@)", secondry]
                                                   style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction *action)
                                                   {
                                                       NSPredicate *predicate =[NSPredicate predicateWithFormat:@"name ==[c] %@", secondry];
                                                       
                                                       NSArray *arr = [[result objectForKey:@"detail"] filteredArrayUsingPredicate:predicate ];
                                                       
                                                       NSLog(@"%@", arr);
                                                       
                                                       [self deleteAccountWebService : [[arr objectAtIndex:0] objectForKey:@"id"]];
                                                   }];
                     
                     UIAlertAction *cancel = [UIAlertAction
                                              actionWithTitle:@"Cancel"
                                              style:UIAlertActionStyleCancel
                                              handler:^(UIAlertAction *action)
                                              {
                                                  NSLog(@"Cancel, %li", (long)alertController.view.tag);
                                              }];
                     
                     
                     [alertController addAction:primaryAcc];
                     
                     [alertController addAction:secondryAcc];
                     
                     [alertController addAction:cancel];
                 }
                 
                 
                 
                     
                     
                     [self presentViewController:alertController animated:YES completion:nil];
                 
             }
         }
         
     }];
    
}



#pragma mark - Send mail for Report Problem

- (void)sendMailToServerForProblem
{
    NSString *emailTitle = @"Report Problem";
    
    NSString *messageBody = @"<Write your Message>";
    
    NSArray *toRecipents = [NSArray arrayWithObject:@"report@pixster.us"];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
  
    mc.mailComposeDelegate = self;
    
    [mc setSubject:emailTitle];
    
    [mc setMessageBody:messageBody isHTML:NO];
    
    [mc setToRecipients:toRecipents];

    [self presentViewController:mc animated:YES completion:nil];
}



#pragma mark - Webservice

- (void)deleteAccountWebService:(NSString *)userId
{
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"deleteAccount" setHTTPMethod:@"POST" params:[NSDictionary dictionaryWithObjectsAndKeys:userId, @"userId", nil] completion:^(NSDictionary *result)
     {
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isUserLogin"];
                 
                 [[NSUserDefaults standardUserDefaults] synchronize];
                 
                 
                 WelcomeViewController *vc = [[WelcomeViewController alloc] initWithNibName:@"WelcomeViewController" bundle:nil];
                 
                 
                 UINavigationController *nVc = [[UINavigationController alloc] initWithRootViewController:vc];
                 
                 nVc.navigationBar.hidden = YES;
                 
                 [self presentViewController:nVc animated:YES completion:nil];
             }
             
         }
         
     }];
}


#pragma mark - Action

- (IBAction)backTap
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)multipleSelections:(UIButton *)sender
{
    if (sender.tag == 10)
    {
        EditProfileViewController *vc = [[EditProfileViewController alloc] initWithNibName:@"EditProfileViewController" bundle:nil];
        
        [self presentViewController:vc animated:YES completion:nil];
    }
    
    else if (sender.tag == 20)
    {
        CreateBusinessViewController *vc = [[CreateBusinessViewController alloc] initWithNibName:@"CreateBusinessViewController" bundle:nil];
        
        [self presentViewController:vc animated:YES completion:nil];
    }
  
    else if (sender.tag == 30)
    {
        [[[UIAlertView alloc] initWithTitle:nil message:@"Are you sure?" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Cancel", @"Yes, I'm sure", nil] show];
        
    }
    
    else if (sender.tag == 40)
    {
        
    }
    
    else if (sender.tag == 50)
    {
        
    }
    
    else if (sender.tag == 60)
    {
        TermOfServiceViewController *vc = [[TermOfServiceViewController alloc] initWithNibName:@"TermOfServiceViewController" bundle:nil];
        
        vc.isTermOfService = YES;
        
        [self presentViewController:vc animated:YES completion:nil];
    }
    
    else if (sender.tag == 70)
    {
        TermOfServiceViewController *vc = [[TermOfServiceViewController alloc] initWithNibName:@"TermOfServiceViewController" bundle:nil];
        
        vc.isTermOfService = NO;
        
        [self presentViewController:vc animated:YES completion:nil];
    }
    
    else if (sender.tag == 80)
    {
        UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:@"Are you sure?" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Logout" otherButtonTitles:nil];
        
        action.tag = 888;
        
        [action showInView:self.view];
    }
    
    else if (sender.tag == 90)
    {
        if ([[(APPDEL).userDetailDict objectForKey:@"multipleAccount"] intValue] == 1)
        {
            [[ServiceHelper sharedManager] setHudShow:YES];
            
            [[ServiceHelper sharedManager] sendRequestWithMethodName:@"listAccount" setHTTPMethod:@"POST" params:[NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userId",   nil] completion:^(NSDictionary *result)
             {
                 if (result)
                 {
                     if ([[result objectForKey:@"status"] intValue] == 1)
                     {
                         UIAlertController *alertController = [UIAlertController
                                                               alertControllerWithTitle:nil
                                                               message:nil
                                                               preferredStyle:UIAlertControllerStyleActionSheet];
                        
                         NSString *primary = [[[result objectForKey:@"detail"] objectAtIndex:0] objectForKey:@"name"];
                         
                         
                          NSString *secondry = [[[result objectForKey:@"detail"] objectAtIndex:1] objectForKey:@"name"];
                         
                         UIAlertAction *primaryAcc = [UIAlertAction
                                                               actionWithTitle:[NSString stringWithFormat:@"Delete(%@)", primary]
                                                               style:UIAlertActionStyleDefault
                                                               handler:^(UIAlertAction *action)
                                                               {
                                                                   NSPredicate *predicate =[NSPredicate predicateWithFormat:@"name ==[c] %@", primary];
                                                               
                                                                   NSArray *arr = [[result objectForKey:@"detail"] filteredArrayUsingPredicate:predicate ];
                                                                   
                                                                   NSLog(@"%@", arr);
                                                                   
                                                                   [self deleteAccountWebService : [[arr objectAtIndex:0] objectForKey:@"id"]];
                                                               }];
                        
                         UIAlertAction *secondryAcc = [UIAlertAction
                                                      actionWithTitle:[NSString stringWithFormat:@"Delete(%@)", secondry]
                                                      style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action)
                                                      {
                                                          NSPredicate *predicate =[NSPredicate predicateWithFormat:@"name ==[c] %@", secondry];
                                                          
                                                          NSArray *arr = [[result objectForKey:@"detail"] filteredArrayUsingPredicate:predicate ];
                                                          
                                                          NSLog(@"%@", arr);
                                                          
                                                          [self deleteAccountWebService : [[arr objectAtIndex:0] objectForKey:@"id"]];
                                                      }];
                         
                         UIAlertAction *cancel = [UIAlertAction
                                                       actionWithTitle:@"Cancel"
                                                       style:UIAlertActionStyleCancel
                                                       handler:^(UIAlertAction *action)
                                                       {
                                                           NSLog(@"Cancel, %li", (long)alertController.view.tag);
                                                       }];
                         
                         
                         [alertController addAction:primaryAcc];
                         
                         [alertController addAction:secondryAcc];
                         
                         [alertController addAction:cancel];
                         
                         
                          [self presentViewController:alertController animated:YES completion:nil];
                     }
                }
                 
             }];
         }
        else
        {
            UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:@"Are you sure?" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Delete Account" otherButtonTitles:nil];
            
            action.tag = 999;
            
            [action showInView:self.view];
        }
    }
}


- (IBAction)privateActionSwitch:(UISwitch *)sender
{
    NSDictionary *params;
    
    if (sender.isOn)
    {
        NSString *flagStr = @"1";
        
        params = [NSDictionary dictionaryWithObjectsAndKeys:
                                [(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", flagStr, @"flag",  nil];
        
        
    }
    else
    {
        NSString *flagStr = @"0";
        
        params = [NSDictionary dictionaryWithObjectsAndKeys:
                  [(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", flagStr, @"flag",  nil];
    }
    
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"setAccountPrivacy" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         
         
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:[result objectForKey:@"detail"]] forKey:@"login_dict"];
                 
                 [[NSUserDefaults standardUserDefaults] synchronize];
                 
                 
                 (APPDEL).userDetailDict = [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"login_dict"]];
                 
                 [privacySwitch setOn: [[(APPDEL).userDetailDict objectForKey:@"privacy"] intValue] == 0 ? NO : YES];
             }
             
         }
         
     }];
}


- (IBAction)enablePushNotificationSwitch:(UISwitch *)sender
{
    NSDictionary *params;
    
    if (sender.isOn)
    {
        NSString *flagStr = @"1";
        
        params = [NSDictionary dictionaryWithObjectsAndKeys:
                  [(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", flagStr, @"flag",  nil];
        
        
    }
    else
    {
        NSString *flagStr = @"0";
        
        params = [NSDictionary dictionaryWithObjectsAndKeys:
                  [(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", flagStr, @"flag",  nil];
    }
    
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"settingNotification" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         
         
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:[result objectForKey:@"detail"]] forKey:@"login_dict"];
                 
                 [[NSUserDefaults standardUserDefaults] synchronize];
                 
                 
                 (APPDEL).userDetailDict = [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"login_dict"]];
                 
                  [enablePushNotification setOn: [[(APPDEL).userDetailDict objectForKey:@"notification_flag"] intValue] == 0 ? NO : YES];
             }
             
         }
         
     }];
}


#pragma mark - UitableView

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [[settingDict allKeys] objectAtIndex:section];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [settingDict allKeys].count;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *keyStr = [[settingDict allKeys] objectAtIndex:section];
    
    NSArray *arr = [settingDict objectForKey:keyStr];
    
    return arr.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID = @"CELLID";
    
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    cell.backgroundColor = [UIColor blackColor];
    
    
    
    NSString *keyStr = [[settingDict allKeys] objectAtIndex:indexPath.section];
    
    NSArray *arr = [settingDict objectForKey:keyStr];
    
    cell.textLabel.font = [UIFont systemFontOfSize:16];
    
    cell.textLabel.text = [[arr objectAtIndex:indexPath.row] objectForKey:@"title"];
    
    cell.textLabel.textColor = [UIColor whiteColor];
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (indexPath.section == 0 && indexPath.row == 3)
    {
        UISwitch *switchBtn = [[UISwitch alloc] initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width - 70, 5, 51, 31)];
        
        [switchBtn setOn: [[(APPDEL).userDetailDict objectForKey:@"privacy"] intValue] == 0 ? NO : YES];
        
        switchBtn.tag = indexPath.row;
        
        [switchBtn addTarget:self action:@selector(privateActionSwitch:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.contentView addSubview:switchBtn];
        
         cell.accessoryType = UITableViewCellAccessoryNone;
    }
    else
    {
        if (indexPath.row == 3)
        {
            cell.textLabel.textAlignment = NSTextAlignmentCenter;
            
            cell.textLabel.textColor = [UIColor colorWithRed:77.0/255.0 green:144.0/255.0 blue:254.0/255.0 alpha:1.0];
            
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        
        else if (indexPath.row == 4)
        {
            cell.textLabel.textAlignment = NSTextAlignmentCenter;
            
            cell.textLabel.textColor = [UIColor redColor];
            
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        else
        {
             cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
    }
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        if (indexPath.row == 0)
        {
            EditProfileViewController *vc = [[EditProfileViewController alloc] initWithNibName:@"EditProfileViewController" bundle:nil];
            
            [self presentViewController:vc animated:YES completion:nil];
        }
        else if (indexPath.row == 1)
        {
            PushnotificationViewController *pVc = [[PushnotificationViewController alloc] initWithNibName:@"PushnotificationViewController" bundle:nil];
            
            [self presentViewController:pVc animated:YES completion:nil];
        }
        else if (indexPath.row == 2)
        {
             [[[UIAlertView alloc] initWithTitle:nil message:@"Are you sure?" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Cancel", @"Yes, I'm sure", nil] show];
        }
        else if (indexPath.row == 3)
        {
            
        }
    }
    else
    {
        if (indexPath.row == 0)
        {
            if ([MFMailComposeViewController canSendMail])
            {
                [self sendMailToServerForProblem];

            }
            else
            {
                UIAlertView *anAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"No mail account setup on device" delegate:self cancelButtonTitle:nil otherButtonTitles:nil];
              
                [anAlert addButtonWithTitle:@"Cancel"];
               
                [anAlert show];
            }

    }
        else if (indexPath.row == 1)
        {
            TermOfServiceViewController *vc = [[TermOfServiceViewController alloc] initWithNibName:@"TermOfServiceViewController" bundle:nil];
            
            vc.isTermOfService = YES;
            
            [self presentViewController:vc animated:YES completion:nil];
        }
        else if (indexPath.row == 2)
        {
            TermOfServiceViewController *vc = [[TermOfServiceViewController alloc] initWithNibName:@"TermOfServiceViewController" bundle:nil];
            
            vc.isTermOfService = NO;
            
            [self presentViewController:vc animated:YES completion:nil];
        }
        else if (indexPath.row == 3)
        {
            UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:@"Are you sure?" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Logout" otherButtonTitles:nil];
            
            action.tag = 888;
            
            [action showInView:self.view];
        }
        else if (indexPath.row == 4)
        {
            [self deleteAccount];
        }
    }
}


#pragma mark - UialertView Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        //[[ZOWVideoCache sharedVideoCache] clearAllCache];
        
        [[SDImageCache sharedImageCache] clearMemory];
        
        [[SDImageCache sharedImageCache] cleanDisk];
        
        [[SDImageCache sharedImageCache] clearDisk];
    }
}


#pragma mark - Action sheet


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 888)
    {
        if (buttonIndex == actionSheet.destructiveButtonIndex)
        {
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isUserLogin"];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            
            WelcomeViewController *vc = [[WelcomeViewController alloc] initWithNibName:@"WelcomeViewController" bundle:nil];
            
            
            UINavigationController *nVc = [[UINavigationController alloc] initWithRootViewController:vc];
    
            nVc.navigationBar.hidden = YES;
            
            [self presentViewController:nVc animated:YES completion:nil];
        }
    }
    
   else if (actionSheet.tag == 999)
    {
        if (buttonIndex == actionSheet.destructiveButtonIndex)
        {
            [self deleteAccountWebService: [(APPDEL).userDetailDict objectForKey:@"userId"]];
        }
    }
}


#pragma mark - Mail composer Delegate

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

@end

