//
//  FilterViewController.m
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 1/30/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#define SCREEN_Width [[UIScreen mainScreen] bounds].size.width

#import "FilterViewController.h"
#import "LiveViewCollectionViewCell.h"
#import "ShareViewController.h"
#import "IFImageFilter.h"
#import "AppDelegate.h"
#import "AvplayerLibrary.h"
#import "LoadingView.h"
#import "GPUImageBeautifyFilter.h"
#import "Normal.h"

@interface FilterViewController ()
{
    NSMutableArray<Class>* filterArr;
    
    NSMutableArray *editOptionArr;
    
    GPUImageMovie *movieFile;
    
    GPUImageOutput<GPUImageInput> *selectfilter;
    
    GPUImageMovieWriter *movieWriter;
    
    NSTimer * timer;
    
    BOOL video, isFileFilter;
    
    AvplayerLibrary *playerLibObj;
    
    LoadingView *loadingView;
    
    //
    AVAsset *firstAsset;
    
    NSArray *tracks;
    
    AVAssetTrack *track;
    
    int indexOfEditFilters;
    
    BOOL isEdit, isFileEdit;
    
    UIImage *editedImage;
    
    UIScrollView *scrolView;
}

@property (nonatomic, strong) GPUImagePicture *picture1;

@end


@implementation FilterViewController


#pragma mark - ViewCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
   
    isFileFilter = NO;
    
    isEdit = YES;
    
    isFileEdit = NO;
   
    filterArr = [[IFImageFilter allFilterClasses] mutableCopy];
  
  
    for (int i = 0; i < filterArr.count; i++) //Loop for normal filter on first index
    {
        if ([filterArr objectAtIndex:i] == [Normal class])
        {
            if (i != 0)
            {
               [filterArr insertObject:[filterArr objectAtIndex:i] atIndex:0];
                
                [filterArr removeObjectAtIndex:i+1];
                
                break;

            }
            else
            {
                break;
            }
        }
    }
    
    [self setUpView];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    firstAsset = [AVAsset assetWithURL:self.videoUrl];
    
    tracks = [firstAsset tracksWithMediaType:AVMediaTypeVideo];
    
    track = [tracks objectAtIndex:0];
    
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)])
    {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
//    NSString *documentDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
// 
//    NSString *imagePath = [documentDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",@"filterImage.jpg"]];
//    
//    self.captureImg = [UIImage imageWithContentsOfFile:imagePath];
//    
    
    CGRect size = CGRectMake(0, 0, 0, 0);
    
    if (SCREEN_Width == 320)
    {
        size = CGRectMake(0, 0, 320, [[UIScreen mainScreen] bounds].size.height-230-40);
    }
    
    else if (SCREEN_Width == 375)
    {
        size = CGRectMake(0, 0, 375, [[UIScreen mainScreen] bounds].size.height - 230-40);
    }
    
    else if (SCREEN_Width == 414)
    {
        size = CGRectMake(0, 0, 414, [[UIScreen mainScreen] bounds].size.height - 230-40);
    }
    
    
    if ((APPDEL).isVideo == YES)
    {
        video = YES;
        
        playerLibObj = [[[NSBundle mainBundle] loadNibNamed:@"AvplayerLibrary" owner:self options:nil] lastObject];
        
        playerLibObj.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, contentView.frame.size.height);
        
        [contentView addSubview:playerLibObj];
        
      
        [self playVideoWithUrl:self.videoUrl];
 
    }

    else
    {
        video = NO;
        
        //filteredImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, contentView.frame.size.height)];
        
        scrolView = [[UIScrollView alloc] init];
        
        scrolView.frame = size;
        
        scrolView.backgroundColor = [UIColor whiteColor];
        
        scrolView.minimumZoomScale = 1;
        
        scrolView.maximumZoomScale = 5;
        
        scrolView.scrollEnabled = YES;
        
        scrolView.delegate = self;
        
        
        filteredImg = [[UIImageView alloc] init];
        
        filteredImg.frame = size;
        
        filteredImg.contentMode = UIViewContentModeScaleAspectFit;
        
        //filteredImg.clipsToBounds = YES;
        
        filteredImg.backgroundColor = [UIColor blackColor];
        
        
        //[scrolView addSubview: filteredImg];
        
        [contentView addSubview: filteredImg];
        
        filteredImg.image = self.captureImg;/*[self setImageSize:self.captureImg];*/
     }
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    (APPDEL).isVideo = NO;
    
    [playerLibObj removeVideoPreviewLayer];
    
    [playerLibObj removeObserver];
}


#pragma mark - Void

- (void) setUpView
{
    if ((APPDEL).isVideo == YES)
    {
        filterBtn.hidden = YES;
        
        editBtn.hidden = YES;
    }
    
    [filterCollectionView registerNib:[UINib nibWithNibName:@"LiveViewCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"liveVideoCell"];
    
    editOptionArr = [[NSMutableArray alloc] initWithObjects:
                     
                 @{@"0" :NSStringFromClass([GPUImageBrightnessFilter class]), @"title" :@"Brightness"},
                     
                 @{@"1" :NSStringFromClass([GPUImageContrastFilter class]), @"title" :@"Contrast"},
                     
                 @{@"2" :NSStringFromClass([GPUImageSharpenFilter class]), @"title" :@"Sharpness"},
                 
                 @{@"3" :NSStringFromClass([GPUImageSaturationFilter class]), @"title" :@"Saturation"},
                     
                 @{@"4" : NSStringFromClass([GPUImageRGBFilter class]), @"title" :@"Color"},
                   
                 @{@"5" : NSStringFromClass([GPUImageOpacityFilter class]), @"title" :@"Fade"},
                 
                 @{@"6" : NSStringFromClass([GPUImageExposureFilter class]), @"title" :@"Highlight"},
                
                 @{@"7" :NSStringFromClass([GPUImageSketchFilter class]), @"title" :@"Sketch"},
                 
                 @{@"8" : NSStringFromClass([GPUImageVignetteFilter class]), @"title" :@"Vignette"}/*,

                 @{@"9" :NSStringFromClass([GPUImageSharpenFilter class]), @"title" :@"Sharpness"}*/,nil];
}


- (void)playVideoWithUrl:(NSURL *)url
{
    CGRect cameraFrame;
    
    if (SCREEN_Width == 320)
    {
        cameraFrame = CGRectMake(0, 0, 320, [[UIScreen mainScreen] bounds].size.height-230);
    }
    
    else if (SCREEN_Width == 375)
    {
        cameraFrame = CGRectMake(0, 0, 375, [[UIScreen mainScreen] bounds].size.height - 230);
    }
    
    else if (SCREEN_Width == 414)
    {
        cameraFrame = CGRectMake(0, 0, 414, [[UIScreen mainScreen] bounds].size.height - 230);
    }
    

    
    [playerLibObj videoUrl:url frame:cameraFrame];
}



// This method will play your video in correct orientation
- (void)setRotationForFilter:(GPUImageOutput<GPUImageInput> *)filterRef
{
    UIInterfaceOrientation orientation = [self orientationForTrack:firstAsset];
    
    if (orientation == UIInterfaceOrientationPortrait)
    {
        [filterRef setInputRotation:kGPUImageRotateRight atIndex:0];
    }
    
    else if (orientation == UIInterfaceOrientationLandscapeRight)
    {
        [filterRef setInputRotation:kGPUImageRotate180 atIndex:0];
    }
    else if (orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        [filterRef setInputRotation:kGPUImageRotateLeft atIndex:0];
    }
}

// A method to identify current video orientation
- (UIInterfaceOrientation)orientationForTrack:(AVAsset *)asset
{
    AVAssetTrack *videoTrack = [[asset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
 
    CGSize size = [videoTrack naturalSize];
   
    CGAffineTransform txf = [videoTrack preferredTransform];
   
    if (size.width == txf.tx && size.height == txf.ty)
    {
        return UIInterfaceOrientationLandscapeRight;
    }
    
    else if (txf.tx == 0 && txf.ty == 0)
    {
        return UIInterfaceOrientationLandscapeLeft;
    }
    
    else if (txf.tx == 0 && txf.ty == size.width)
    {
        return UIInterfaceOrientationPortraitUpsideDown;
    }
    else
    {
        return UIInterfaceOrientationPortrait;
    }
}


#pragma mark - Set Image Size

- (UIImage *)setImageSize:(UIImage *)image
{
    UIImage *originalImage = image;
    
    // Create rect with height and width equal to originalImages height
    CGSize size = CGSizeMake(originalImage.size.height, originalImage.size.height);
    
    UIGraphicsBeginImageContextWithOptions(size, YES, 0);
    
    // Fill the background with some color
    [[UIColor lightGrayColor] setFill];
    
    UIRectFill(CGRectMake(0, 0, size.width, size.height));
    
    // Create image for what we just did
    UIImage *bgImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    UIGraphicsBeginImageContext(bgImage.size);
    // Take originalImage and center it in bgImage
    // We get the center of bgImage by (bgImage.size.width / 2)
    // But the coords of the originalImage start at the top left (0,0)
    // So we need to subtract half of our orginalImages width so that it centers (bgImage.size.width / 2) - (originalImage.size.width / 2)
    [originalImage drawInRect:CGRectMake((bgImage.size.width / 2) - (originalImage.size.width / 2), 0, originalImage.size.width, originalImage.size.height)];
    // Create image for what we just did
    
    UIImage *finalImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return finalImage;
}



#pragma mark - Loading view

- (void)addLoadingView
{
    loadingView = [[[NSBundle mainBundle] loadNibNamed:@"LoadingView" owner:self options:nil] lastObject];
    
    [loadingView startAnimating];
    
    loadingView.frame = self.tabBarController.view.frame;
    
    [self.tabBarController.view addSubview:loadingView];
}


- (void)removeLoadingView
{
    [loadingView stopAnimating];
    
    [loadingView removeFromSuperview];
}


#pragma mark - Add filter on video

- (void)videoConvertingWithSelectedFilter:(IFImageFilter *)filter indexPath:(NSIndexPath *)index
{
    [self addLoadingView];
    
    [playerLibObj.player pause];
    
    
   
    movieFile = [[GPUImageMovie alloc] initWithURL:self.videoUrl];

    
    
    
    CGAffineTransform fileTransform = track.preferredTransform;
    
    CGSize mediaSize = track.naturalSize;   
    

    
   // AVAssetReaderTrackOutput *readerVideoTrackOutput = [AVAssetReaderTrackOutput assetReaderTrackOutputWithTrack:track outputSettings:videoCleanApertureSettings1];
    
    
    
    
    AVPlayerItem *playerItem = [AVPlayerItem playerItemWithAsset:firstAsset];
    
    UIInterfaceOrientation orientation = [self orientationForTrack:playerItem.asset];
    
    AVAssetTrack *videoTrack = [[firstAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    CGSize size = [videoTrack naturalSize];
 
//    
//    CGAffineTransform txf = [videoTrack preferredTransform];
//    
//    if (size.width == txf.tx && size.height == txf.ty)
//    {
//        orientation = UIInterfaceOrientationLandscapeRight;
//    }
//    else if (txf.tx == 0 && txf.ty == 0)
//    {
//         orientation = UIInterfaceOrientationLandscapeLeft;
//    }
//    else if (txf.tx == 0 && txf.ty == size.width)
//    {
//        orientation = UIInterfaceOrientationPortraitUpsideDown;
//    }
//    else
//    {
//        orientation = UIInterfaceOrientationLandscapeRight;
//    }
    
//    if (orientation == 1)
//    {
//        if (self.isGallaryVideo == YES)
//        {
//            [filter setInputRotation:kGPUImageRotateLeft atIndex:0];
//        }
//        
//        else
//        {
//            [filter setInputRotation:kGPUImageRotateRightFlipVertical atIndex:0];
//        }
//        
//    }
//    
//    else if (orientation == 2)
//    {
//        NSLog(@"2");
//    }
//    
//    else if (orientation == 3)
//    {
//        NSLog(@"3");
//        
//        [filter setInputRotation:kGPUImageRotateRightFlipHorizontal atIndex:0];
//    }
//    
//    else if (orientation == 4)
//    {
//        NSLog(@"4");
//        
//       // [filter setInputRotation:kGPUImageRotateRight atIndex:0];
//    }
    
   //[filter setInputRotation:kGPUImageRotateRightFlipVertical atIndex:0];//mirroed image
    
   
    
    movieFile.runBenchmark = YES;
    
    movieFile.playAtActualSpeed = NO;

  
    
    NSString *pathToMovie = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/filter.mov"];
   
    unlink([pathToMovie UTF8String]); // If a file already exists, AVAssetWriter won't let you record new frames, so delete the old movie
   
    NSURL *outputMovieURL = [NSURL fileURLWithPath:pathToMovie];
    
    
    NSDictionary *videoCleanApertureSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                                                [NSNumber numberWithInt:mediaSize.width], AVVideoWidthKey,
                                                [NSNumber numberWithInt:mediaSize.height], AVVideoHeightKey,AVVideoCodecH264, AVVideoCodecKey,
                                                nil];
    
    // movieWriter = [[GPUImageMovieWriter alloc] initWithMovieURL:outputMovieURL size:mediaSize fileType:AVFileTypeQuickTimeMovie outputSettings:videoCleanApertureSettings];
    
    if (index.row == 18)
    {
        GPUImageBeautifyFilter *beautyFilter = [[GPUImageBeautifyFilter alloc] init];
        
        [movieFile addTarget: beautyFilter];
    }
    else
    {
        [movieFile addTarget:filter];
    }
    
    
    if( orientation == 1)
    {
        
        if (self.isGallaryVideo == YES)
        {
             movieWriter = [[GPUImageMovieWriter alloc] initWithMovieURL:outputMovieURL size:size];
            [filter setInputRotation:kGPUImageRotateLeft atIndex:0];
        }
        
        else
        {
            if ((APPDEL).isFrontCamera == YES)
            {
                movieWriter = [[GPUImageMovieWriter alloc] initWithMovieURL:outputMovieURL size:CGSizeMake(300 , 520)];
                
                [filter setInputRotation:kGPUImageRotateRightFlipVertical atIndex:0];
            }
           else
           {
               movieWriter = [[GPUImageMovieWriter alloc] initWithMovieURL:outputMovieURL size:CGSizeMake(300 , 520)];
               
               [filter setInputRotation:kGPUImageRotateRight atIndex:0];
           }
        }
        
        //[movieFile addTarget:filter];
        
        [filter useNextFrameForImageCapture];
        
    }
    else if( orientation == 2 )
    {
       //movieWriter = [[GPUImageMovieWriter alloc] initWithMovieURL:outputMovieURL size:CGSizeMake(mediaSize.width , 640)];
        
        
        //[movieFile addTarget:filter];
        
        [filter useNextFrameForImageCapture];
        
    }
    else if( orientation == 3 )
    {
        
      // movieWriter = [[GPUImageMovieWriter alloc] initWithMovieURL:outputMovieURL size:mediaSize];
        
        
        
        movieWriter = [[GPUImageMovieWriter alloc] initWithMovieURL:outputMovieURL size:mediaSize fileType:AVFileTypeQuickTimeMovie outputSettings:videoCleanApertureSettings];
        
        [filter setInputRotation:kGPUImageRotate180 atIndex:0];
        
       // [movieFile addTarget:filter];
        
        [filter useNextFrameForImageCapture];
    }
    else
    {
        movieWriter = [[GPUImageMovieWriter alloc] initWithMovieURL:outputMovieURL size:mediaSize fileType:AVFileTypeQuickTimeMovie outputSettings:videoCleanApertureSettings];
        //movieWriter = [[GPUImageMovieWriter alloc] initWithMovieURL:outputMovieURL size:CGSizeMake(300 , 520)];
        
        //[movieFile addTarget:filter];
        
        //[filter setInputRotation:kGPUImageRotateLeft atIndex:0];
        
        [filter useNextFrameForImageCapture];
        
    }
    
    
//    mediaSize.width = [[UIScreen mainScreen] bounds].size.width;
//    
//    mediaSize.height = 480;
//    
//    movieWriter = [[GPUImageMovieWriter alloc] initWithMovieURL:outputMovieURL size: mediaSize];
   
    [filter addTarget:movieWriter];
    
    
    // Configure this for video from the movie file, where we want to preserve all video frames and audio samples
    movieWriter.shouldPassthroughAudio = YES;
   
    
    movieFile.audioEncodingTarget = movieWriter;
    
    
    [movieFile enableSynchronizedEncodingUsingMovieWriter:movieWriter];
    
    
    if (UIInterfaceOrientationIsPortrait(orientation))
    {
        [movieWriter startRecording/*InOrientation:CGAffineTransformMakeRotation (M_PI*90 / 180.0f)*/];
    }
    else
    {
        [movieWriter startRecording/*InOrientation:CGAffineTransformMakeRotation (M_PI*90 / 180.0f)*/];
    }
    
    [movieFile startProcessing];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:0.3f
                                             target:self
                                           selector:@selector(retrievingProgress)
                                           userInfo:nil
                                            repeats:YES];
    
    [movieWriter setCompletionBlock:
     ^{
        [filter removeTarget:movieWriter];
       
         [movieWriter finishRecording];
        
        dispatch_async(dispatch_get_main_queue(), ^{
           
            [timer invalidate];
         
            [self removeLoadingView];
            
            NSLog(@"%@", [NSString stringWithFormat:@"%d%%", (int)(movieFile.progress * 100)]);
            
            NSString *filename = @"filter.mov";
            
            NSArray *pathArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES);
            
            NSString *documentsDirectory = [pathArray objectAtIndex:0];
            
            NSString *videoPath = [documentsDirectory stringByAppendingPathComponent:filename];
            
            if ([[NSFileManager defaultManager] fileExistsAtPath:videoPath])
            {
                
                [self performSelector:@selector(playVideoWithUrl:) withObject:[NSURL fileURLWithPath:videoPath isDirectory:NO] afterDelay:0.5];
                //[playerLibObj videoUrl:[NSURL fileURLWithPath:videoPath isDirectory:NO]];
            }
        });
    }];
    
}


- (void)retrievingProgress
{
    NSLog(@"%@", [NSString stringWithFormat:@"%d%%", (int)(movieFile.progress * 100)]);
}



#pragma mark - GPUImageFilter

- (GPUImageFilter *)getFilterNameOnIndexPath:(NSIndexPath *)index
{
    controlView.frame = CGRectMake(0, 0, bottomView.frame.size.width, bottomView.frame.size.height);
    
    
    GPUImageFilter *selectedFilter;;
    
    indexOfEditFilters = (int)index.row;
    
    
    UIImage *inputImage;
    
    inputImage = self.captureImg;
    
    
    
    if (index.row == 0)
    {
        GPUImageBrightnessFilter *brightObj = [[GPUImageBrightnessFilter alloc] init];
        
        selectedFilter = brightObj;
        
        filteredImg.image = [brightObj imageByFilteringImage:inputImage];
        
        
        slider1.hidden = NO;
        
        slider2.hidden = YES;
        
        slider3.hidden = YES;
        
        
        sliderLbl1.text = @"Value";
        
        sliderLbl2.text = @"";
        
        sliderLbl3.text = @"";
        
        
        [bottomView addSubview:controlView];
    }
    
    else if (index.row == 1)
    {
        GPUImageContrastFilter *contrastObj = [[GPUImageContrastFilter alloc] init];
        
        selectedFilter = contrastObj;
        
        filteredImg.image = [selectedFilter imageByFilteringImage:inputImage];
        
        
        slider1.hidden = NO;
        
        slider2.hidden = YES;
        
        slider3.hidden = YES;
      
       
        sliderLbl1.text = @"Value";
        
        sliderLbl2.text = @"";
        
        sliderLbl3.text = @"";
        
        
        [bottomView addSubview:controlView];
    }
    
    else if (index.row == 2)
    {
        GPUImageSharpenFilter *sharpnessFilterObj = [[GPUImageSharpenFilter alloc] init];
        
        selectedFilter = sharpnessFilterObj;
        
      
        filteredImg.image = [sharpnessFilterObj imageByFilteringImage:inputImage];
        
        
        slider1.hidden = NO;
        
        slider2.hidden = YES;
      
        slider3.hidden = YES;
        
        
        sliderLbl1.text = @"Value";
        
        sliderLbl2.text = @"";
        
        sliderLbl3.text = @"";
       
        
        [bottomView addSubview:controlView];
    }
    
    else if (index.row == 3)
    {
        GPUImageSaturationFilter *saturationFilterObj = [[GPUImageSaturationFilter alloc] init];
        
        selectedFilter = saturationFilterObj;
        
        slider1.hidden = NO;
        
        slider2.hidden = YES;
        
        slider3.hidden = YES;
        
        
        sliderLbl1.text = @"Value";
        
        sliderLbl2.text = @"";
        
        sliderLbl3.text = @"";
        
        
        [bottomView addSubview:controlView];
        
    }
    
    else if (index.row == 4)
    {
        GPUImageRGBFilter *rgbObj = [[GPUImageRGBFilter alloc] init];
        
        [rgbObj setRed:1];
        
        [rgbObj setBlue:0.7];
        
        [rgbObj setGreen:0.3];
        
        
        slider1.hidden = NO;
        
        slider2.hidden = NO;
        
        slider3.hidden = NO;
        
        
        sliderLbl1.text = @"Red";
        
        sliderLbl2.text = @"Blue";
        
        sliderLbl3.text = @"Green";
        
        
        selectedFilter = rgbObj;
        
        [bottomView addSubview:controlView];
        
    }
    else if (index.row == 5)
    {
        GPUImageOpacityFilter *opacityObj = [[GPUImageOpacityFilter alloc] init];
        
        selectedFilter = opacityObj;
        

        slider1.hidden = NO;
        
        slider2.hidden = YES;
        
        slider3.hidden = YES;
        
        
        sliderLbl1.text = @"Fade";
        
        sliderLbl2.text = @"";
        
        sliderLbl3.text = @"";
        
        
        [bottomView addSubview:controlView];
    }
    else if (index.row == 6)
    {
        GPUImageExposureFilter *exposeObj = [[GPUImageExposureFilter alloc] init];
        
        selectedFilter = exposeObj;
        
        
        slider1.hidden = NO;
       
        slider2.hidden = YES;
       
        slider3.hidden = YES;
        
        
        sliderLbl1.text = @"Value";
        
        sliderLbl2.text = @"";
        
        sliderLbl3.text = @"";
       
        
        [bottomView addSubview:controlView];
    }
    
    else if (index.row == 7)
    {
        GPUImageSketchFilter *sketchFilter = [[GPUImageSketchFilter alloc] init];
        
        selectedFilter = sketchFilter;
        
        UIImage *outputImage;
        
        if (isFileEdit == YES)
        {
            outputImage = editedImage;
        }
        else
        {
            outputImage = self.captureImg;
        }
        
        UIImage *filteredImage = [selectedFilter imageByFilteringImage:outputImage];
        
        filteredImg.image = filteredImage;

        
        slider1.hidden = YES;
        
        slider2.hidden = YES;
        
        slider3.hidden = YES;
        
        
        sliderLbl1.text = @"";
        
        sliderLbl2.text = @"";
        
        sliderLbl3.text = @"";
        
        [bottomView addSubview:controlView];
    }

    else if (index.row == 8)
    {
        GPUImageVignetteFilter *vigenteFilter = [[GPUImageVignetteFilter alloc] init];
        
        selectedFilter = vigenteFilter;
        
        
        slider1.hidden = NO;
        
        slider2.hidden = NO;
        
        slider3.hidden = NO;
        
        
        sliderLbl1.text = @"Centre";
        
        sliderLbl2.text = @"Start";
        
        sliderLbl3.text = @"End";
        
        
        [bottomView addSubview:controlView];
    }
    
    else if (index.row == 9)
    {
        GPUImageSaturationFilter *saturObj = [[GPUImageSaturationFilter alloc] init];
        
        selectedFilter = saturObj;
        
        
        slider1.hidden = NO;
        
        slider2.hidden = YES;
        
        slider3.hidden = YES;
        
        
        sliderLbl1.text = @"Value";
        
        sliderLbl2.text = @"";
        
        sliderLbl3.text = @"";
        
        
        [bottomView addSubview:controlView];
    }
    
    [slider1 setValue:0];
    
    [slider2 setValue:0];
    
    [slider3 setValue:0];
    
    
    return selectedFilter;
}


#pragma mark - IFFilter

- (IFImageFilter *)addIfImageFilterOnImage:(NSIndexPath *)index
{
    self.picture1 = [[GPUImagePicture alloc] initWithImage:self.captureImg];
    
    [self.picture1 removeAllTargets];
    
    
    IFImageFilter *imageFilter = [[[filterArr objectAtIndex: index.row] alloc] init];
    
    [self.picture1 addTarget:imageFilter];
    
    [imageFilter useNextFrameForImageCapture];
    
    [self.picture1 processImage];
    
    
    return imageFilter;
}


#pragma mark - ScrolViewDelegate

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return filteredImg;
}


#pragma mark - CollectionView Delegate

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[UIScreen mainScreen] bounds].size.width == 320)
    {
        return  CGSizeMake(67, 67);
    }
    
    else if ([[UIScreen mainScreen] bounds].size.width == 375)
    {
        return  CGSizeMake(67, 67);
    }
    
    else
    {
        return  CGSizeMake(97, 97);
    }
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (isEdit == NO)
    {
        return filterArr.count;
    }
    
    return editOptionArr.count;
}



- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView1 cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    //filteredImg.image = [imageFilter imageFromCurrentFramebufferWithOrientation:self.captureImg.imageOrientation];
    
    if (isEdit == NO)
    {
        static NSString *gridCellIdentifier = @"liveVideoCell";
        
        LiveViewCollectionViewCell *cell = (LiveViewCollectionViewCell*)[collectionView1 dequeueReusableCellWithReuseIdentifier:gridCellIdentifier forIndexPath:indexPath];
        
        cell.contentView.autoresizingMask  = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;

        cell.userImg.image = self.captureImg;
        
        cell.titleLbl.text = NSStringFromClass(filterArr[indexPath.row]);
        
       /* dispatch_async(dispatch_get_main_queue(),
                       ^{
                           if (indexPath.row == 18)
                           {
                               GPUImagePicture *stillImageSource = [[GPUImagePicture alloc] initWithImage:self.captureImg];
                               
                               if (!cell.userImg.image)
                               {
                                   GPUImageBeautifyFilter *beautyFilter = [[GPUImageBeautifyFilter alloc] init];
                                   
                                   [stillImageSource addTarget: beautyFilter];
                                   
                                   [beautyFilter useNextFrameForImageCapture];
                                   
                                   [stillImageSource processImage];
                                  
                                   cell.userImg.image = [beautyFilter imageFromCurrentFramebuffer];
                               }
                               
                               
                               
                               cell.userImg.layer.cornerRadius = 2.0;
                               
                               cell.titleLbl.text = @"Beauty";
                           }
                           else
                           {
                               dispatch_async(dispatch_get_main_queue(),
                                              ^{
                                                  
                                                  if (!cell.userImg.image)
                                                  {
                                                      cell.userImg.image = [[self addIfImageFilterOnImage:indexPath] imageFromCurrentFramebufferWithOrientation:self.captureImg.imageOrientation];
                                                  }
                                              });
                               
                               
                               
                               cell.titleLbl.text = [NSString stringWithFormat:@"%@", [self addIfImageFilterOnImage:indexPath].name];
                           }
                       });*/
        
        
        
        return cell;
    }
    
    else
    {
        static NSString *gridCellIdentifier = @"liveVideoCell";
        
        LiveViewCollectionViewCell *cell = (LiveViewCollectionViewCell*)[collectionView1 dequeueReusableCellWithReuseIdentifier:gridCellIdentifier forIndexPath:indexPath];
        
        cell.contentView.autoresizingMask  = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;

        cell.titleLbl.text = [[editOptionArr objectAtIndex:indexPath.row] valueForKey:@"title"];
        
        cell.userImg.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@_%ld", [[editOptionArr objectAtIndex:indexPath.row] valueForKey:@"title"], indexPath.row]];
        
        cell.userImg.contentMode = UIViewContentModeScaleAspectFit;
        
        return cell;
    }
    
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (isEdit == NO)
    {
        isFileFilter = YES;
        
        if (video == YES)
        {
            [self videoConvertingWithSelectedFilter:[[[filterArr objectAtIndex: indexPath.row] alloc] init] indexPath:indexPath];
        }
        
        else
        {
            /*if (indexPath.row == 18)
            {
                GPUImagePicture *stillImageSource = [[GPUImagePicture alloc] initWithImage:self.captureImg];
                
                GPUImageBeautifyFilter *beautyFilter = [[GPUImageBeautifyFilter alloc] init];
                
                
                [stillImageSource addTarget: beautyFilter];
                
                [beautyFilter useNextFrameForImageCapture];
                
                [stillImageSource processImage];
                
                filteredImg.image = [beautyFilter imageFromCurrentFramebuffer];
            }
            else
            {
                filteredImg.image = [[self addIfImageFilterOnImage:indexPath] imageFromCurrentFramebufferWithOrientation:self.captureImg.imageOrientation];
            }*/
            
            dispatch_async(dispatch_get_main_queue(),
                           ^{
                             
                                   filteredImg.image = [[self addIfImageFilterOnImage:indexPath] imageFromCurrentFramebufferWithOrientation:self.captureImg.imageOrientation];
                             
                           });
        }
        
        [filterCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:filterArr.count - 1 inSection:0] atScrollPosition:UICollectionViewScrollPositionRight animated:YES];
        
        
        [filterCollectionView scrollToItemAtIndexPath:indexPath
                                     atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
                                             animated:YES];

    }
    
    else
    {
//        UIImage *filteredImage = [[self getFilterNameOnIndexPath:indexPath] imageByFilteringImage:self.captureImg];
//        
//        filteredImg.image = filteredImage;
        
        selectfilter = [self getFilterNameOnIndexPath:indexPath];
        
        [filterCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:editOptionArr.count - 1 inSection:0] atScrollPosition:UICollectionViewScrollPositionRight animated:YES];
        
        
        [filterCollectionView scrollToItemAtIndexPath:indexPath
                                     atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
                                             animated:YES];

   }
    
}


#pragma mark - Action

- (IBAction)backTap
{
    [self.navigationController popViewControllerAnimated:NO];
}


- (IBAction)nextTap
{
    if ((APPDEL).isVideo == NO)
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
        NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:@"filterImage.jpg"];
        
        //NSData *imageData = UIImagePNGRepresentation(filteredImg.image);
        
        
       /* UIGraphicsBeginImageContextWithOptions(filteredImg.bounds.size, filteredImg.opaque, 0.0);
      
        [filteredImg.layer renderInContext:UIGraphicsGetCurrentContext()];
        
        UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
        
        NSData *imageData = UIImageJPEGRepresentation(img, 1);
       
        [imageData writeToFile:savedImagePath atomically:NO];*/
        
       
       /* UIGraphicsBeginImageContextWithOptions(scrolView.bounds.size,
                                               YES,
                                               scrolView.window.screen.scale);
        
        CGPoint offset = scrolView.contentOffset;
        
        CGContextTranslateCTM(UIGraphicsGetCurrentContext(), -offset.x, -offset.y);
        
        [scrolView.layer renderInContext:UIGraphicsGetCurrentContext()];
        
        UIImage *visibleScrollViewImage = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
        
        NSData *imageData = UIImageJPEGRepresentation(visibleScrollViewImage, 1);*/
       
        NSData *imageData = UIImageJPEGRepresentation(filteredImg.image, 0.5);
        

        [imageData writeToFile:savedImagePath atomically:NO];
    }
    
    
    ShareViewController *vc = [[ShareViewController alloc] initWithNibName:@"ShareViewController" bundle:nil];
  
    vc.isVideo = video;
    
    vc.isFilter = isFileFilter;
    
    [self.navigationController pushViewController:vc animated:YES];
}


- (IBAction)updatePixelWidth:(UISlider *)sender
{
    NSLog(@"%f", sender.value);
    
//    [(GPUImagePixellateFilter *)selectfilter setFractionalWidthOfAPixel:[sender value]];
//    
//    UIImage *filteredImage = [selectfilter imageByFilteringImage:self.captureImg];
//    
//    filteredImg.image = filteredImage;
}

- (IBAction)doneTap
{
    isFileEdit = YES;
    
    editedImage = filteredImg.image;
    
    [controlView removeFromSuperview];
}

- (IBAction)cancelTap
{
    isFileEdit = NO;
    
    [controlView removeFromSuperview];
    
    filteredImg.image = self.captureImg;
}


- (IBAction)switchBtn:(UIButton *)sender
{
    if (sender == filterBtn)
    {
        if (sender.selected == NO)
        {
            sender.selected = YES;
            
            editBtn.selected = NO;
            
            isEdit = NO;
          
            [filterBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            
            [editBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            
            [filterCollectionView reloadData];
        }
        
    }
    else
    {
        if (sender.selected == NO)
        {
            sender.selected = YES;
            
            filterBtn.selected = NO;
            
            isEdit = YES;
            
            [filterBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            
            [editBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            
            [filterCollectionView reloadData];
        }
    }
    
    [filterCollectionView setContentOffset:CGPointMake(0, 0) animated:YES];
}


- (IBAction)addFilterValue:(UISlider *)sender
{
    if (indexOfEditFilters == 0)
    {
        [(GPUImageBrightnessFilter *)selectfilter setBrightness:[sender value]/5];
    }
    
    else if (indexOfEditFilters == 1)
    {
        if (slider1 == sender)
        {
            [(GPUImageContrastFilter *)selectfilter setContrast:sender.value *5];
        }
    }
    else if (indexOfEditFilters == 2)
    {
        if (slider1 == sender)
        {
            [(GPUImageSharpenFilter *)selectfilter setSharpness:sender.value];
        }
    }
    
    else if (indexOfEditFilters == 3)
    {
        if (slider1 == sender)
        {
            [(GPUImageSaturationFilter *)selectfilter setSaturation:sender.value];
        }
    }
    
    else if (indexOfEditFilters== 4)
    {
        if (slider1 == sender)
        {
            [(GPUImageRGBFilter *)selectfilter setRed:sender.value];
        }
        
        else if (slider2 == sender)
        {
            [(GPUImageRGBFilter *)selectfilter setBlue:sender.value];
        }
        
        else
        {
            [(GPUImageRGBFilter *)selectfilter setGreen:sender.value];
        }
    }
    
    else if (indexOfEditFilters== 5)
    {
        if (slider1 == sender)
        {
            [(GPUImageOpacityFilter *)selectfilter setOpacity:1.5-sender.value];
        }
    }
    
    else if (indexOfEditFilters == 6)
    {
        if (slider1 == sender)
        {
            [(GPUImageExposureFilter *)selectfilter setExposure:sender.value];
        }
    }
   
    else if (indexOfEditFilters == 8)
    {
        if (slider1 == sender)
        {
            [(GPUImageVignetteFilter *)selectfilter setVignetteCenter:CGPointMake(sender.value, sender.value)];
        }
        
        else if (slider2 == sender)
        {
            [(GPUImageVignetteFilter *)selectfilter setVignetteStart:sender.value];
        }
        
        else
        {
            [(GPUImageVignetteFilter *)selectfilter setVignetteEnd:sender.value];
        }
    }
    
    else if (indexOfEditFilters == 9)
    {
        if (slider1 == sender)
        {
            [(GPUImageSaturationFilter *)selectfilter setSaturation:sender.value];
        }
    }
    
    
    
  /*  else if (indexOfEditFilters == 1)
    {
        [(GPUImagePixellateFilter *)selectfilter setFractionalWidthOfAPixel:[sender value]/60];
        
    }
    
    else if (indexOfEditFilters == 2)
    {
        if (slider1 == sender)
        {
            [(GPUImageToonFilter *)selectfilter setThreshold:sender.value];
        }
        else
        {
            [(GPUImageToonFilter *)selectfilter setQuantizationLevels:sender.value];
        }
    }
    else if (indexOfEditFilters == 3)
    {
        if (slider1 == sender)
        {
            
            [(GPUImagePinchDistortionFilter *)selectfilter setCenter:CGPointMake(sender.value, sender.value)];
        }
        else if (slider2 == sender)
        {
            [(GPUImagePinchDistortionFilter *)selectfilter setRadius:sender.value];
        }
        
        else
        {
            [(GPUImagePinchDistortionFilter *)selectfilter setScale:sender.value];
        }
    }
    
    else if (indexOfEditFilters == 4)
    {
        if (slider1 == sender)
        {
            [(GPUImageExposureFilter *)selectfilter setExposure:sender.value];
        }
    }
    
    else if (indexOfEditFilters == 5)
    {
        if (slider1 == sender)
        {
            [(GPUImageContrastFilter *)selectfilter setContrast:sender.value *5];
        }
    }
    
    else if (indexOfEditFilters == 6)
    {
        if (slider1 == sender)
        {
            [(GPUImageSaturationFilter *)selectfilter setSaturation:sender.value];
        }
    }
    
  
    else if (indexOfEditFilters== 7)
    {
        if (slider1 == sender)
        {
            [(GPUImageRGBFilter *)selectfilter setRed:sender.value];
        }
        
        else if (slider2 == sender)
        {
            [(GPUImageRGBFilter *)selectfilter setBlue:sender.value];
        }
        
        else
        {
            [(GPUImageRGBFilter *)selectfilter setGreen:sender.value];
        }
    }
    
    else if (indexOfEditFilters == 8)
    {
        if (slider1 == sender)
        {
            [(GPUImageVignetteFilter *)selectfilter setVignetteCenter:CGPointMake(sender.value, sender.value)];
        }
        
        else if (slider2 == sender)
        {
            [(GPUImageVignetteFilter *)selectfilter setVignetteStart:sender.value];
        }
        
        else
        {
            [(GPUImageVignetteFilter *)selectfilter setVignetteEnd:sender.value];
        }
    }*/

    
    UIImage *outputImage;
    
    if (isFileEdit == YES)
    {
        outputImage = editedImage;
    }
    else
    {
        outputImage = self.captureImg;
    }
    
    
    UIImage *filteredImage = [selectfilter imageByFilteringImage:outputImage];
    
    filteredImg.image = filteredImage;

}

@end
