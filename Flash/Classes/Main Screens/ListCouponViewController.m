//
//  ListCouponViewController.m
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 5/4/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import "ListCouponViewController.h"
#import "CouponTableViewCell.h"
#import "ServiceHelper.h"

@interface ListCouponViewController ()
{
    NSArray *couponArr;
}


@end

@implementation ListCouponViewController



#pragma mark - ViewCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.hidden = YES;
    
    couponArr = [NSArray new];
    
    [self setUpView];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    [self getAllCouponWebSevice];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
}


#pragma mark - Void

- (void) setUpView
{
    
}


#pragma mark - Action

- (IBAction)backTap
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Webservice

- (void)getAllCouponWebSevice
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            [(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", nil];
    
    
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"listUsedCoupons" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 couponArr = [result objectForKey:@"usedcoupon"];
                 
                 if (!couponArr.count)
                 {
                     [[[UIAlertView alloc] initWithTitle:nil message:@"No Rewards Puchased" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
                 }
                 
                 [listCouponTbl reloadData];
             }
         }
     }];
}


#pragma mark - TableView Delegate


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return couponArr.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"CELL";
    
    CouponTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"CouponTableViewCell" owner:self options:nil] lastObject];
    }
    
   
    [cell.coponImg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", [[[couponArr objectAtIndex:indexPath.row] objectForKey:@"coupon_image"] stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]]] placeholderImage:nil options:SDWebImageRefreshCached];
    

    cell.codeLbl.text = [NSString stringWithFormat:@"Code - %@", [[couponArr objectAtIndex:indexPath.row] objectForKey:@"coupon_code"] ];
    
    cell.timeLbl.text = [NSString stringWithFormat:@"Expiry date - %@", [[couponArr objectAtIndex:indexPath.row] objectForKey:@"expiry_date"] ];
    
    return cell;
}


@end
