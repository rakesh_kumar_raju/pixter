//
//  SettingViewController.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 3/28/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>
{
   IBOutlet UISwitch *privacySwitch, *enablePushNotification;
    
    IBOutlet UITableView *settingTbl;
}

- (IBAction)backTap;

- (IBAction)multipleSelections:(UIButton *)sender;

- (IBAction)privateActionSwitch:(UISwitch *)sender;

- (IBAction)enablePushNotificationSwitch:(UISwitch *)sender;

@end
