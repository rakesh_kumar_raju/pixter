//
//  BuyCouponViewController.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 3/8/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BuyCouponViewController : UIViewController <UITextFieldDelegate>
{
    IBOutlet UITextField *userNameTf, *emailTf, *phoneTf;
    
    IBOutlet UILabel *titleLbl;
}

@property (nonatomic, retain) NSString *couponId, *couponName;;


- (IBAction)backTap;

- (IBAction)doneTap;


@end
