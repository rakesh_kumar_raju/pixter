//
//  SendMessageViewController.m
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 3/3/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import "SendMessageViewController.h"
#import "ChatTableViewCell.h"


@interface SendMessageViewController ()
{
    NSMutableArray *mesageArr;
}

@end

@implementation SendMessageViewController


#pragma mark - ViewCycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.navigationController.navigationBar.hidden = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadTblWhenGotMessage:) name:@"new_chat_message" object:nil];

    //self.tableView.allowsMultipleSelectionDuringEditing = YES;
    
    [self setUpView];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    
    self.navigationController.navigationBar.hidden = YES;
    
    [[NSUserDefaults standardUserDefaults] setObject: @"0" forKey:@"message_count"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
}


#pragma mark - Void

- (void) setUpView
{
    mesageArr = [NSMutableArray new];
 
    self.tableView.backgroundColor = [UIColor blackColor];
    
    self.textInputbar.tintColor = [UIColor blackColor];
    
    self.textInputbar.backgroundColor = [UIColor blackColor];
    
    self.textInputbar.translucent = NO;
    
    self.textView.keyboardAppearance = UIKeyboardAppearanceDark;
    
    self.textView.backgroundColor = [UIColor grayColor];
    
    self.textView.textColor = [UIColor whiteColor];
    
    
    [self viewMessageBetweenUsers];
    
    
    self.bounces = YES;
    
    self.inverted = NO;
    
    self.shouldScrollToBottomAfterKeyboardShows = YES;
    
    self.textView.placeholder = @"Write a message...";
    
    [self.rightButton setTitle:NSLocalizedString(@"Send", nil) forState:UIControlStateNormal];
    
    
    self.rightButton.tintColor = [UIColor whiteColor];
    
    self.textInputbar.autoHideRightButton = NO;
    
    self.textInputbar.maxCharCount = 256;
    
    self.textInputbar.counterStyle = SLKCounterStyleSplit;
    
    self.textInputbar.counterPosition = SLKCounterPositionTop;
    
    self.textView.keyboardType = UIKeyboardTypeDefault;
    
    
    self.tableView.estimatedRowHeight = 80;
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    
    self.title = self.userNameStr;
    
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    
    self.navigationController.navigationBar.translucent = NO;
    
    self.navigationController.navigationBar.titleTextAttributes =  @{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName: [UIFont systemFontOfSize:18]};
    
    
    
   
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];

    [backBtn setBackgroundImage:[UIImage imageNamed:@"phone_back"] forState:UIControlStateNormal];
    
    backBtn.frame = CGRectMake(0, 25.0, 40, 40);
    
    backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, -40, 0, 0);
   
    [backBtn addTarget:self action:@selector(backTap) forControlEvents:UIControlEventTouchUpInside];
    
    

    UIBarButtonItem *leftBtn = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    
    self.navigationItem.leftBarButtonItem = leftBtn;
}


#pragma mark - SetDateFormat

- (NSString *)setDateFormat:(NSString *)str
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    dateFormatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    
    [dateFormatter setDateFormat: @"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *myDate = [dateFormatter dateFromString:str];
    
    
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    
    dateFormatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    
    [dateFormatter2 setDateFormat:@"d MMM YYYY, hh:mm"];
    
    return  [dateFormatter2 stringFromDate:myDate];
}



#pragma mark - Notification

- (void)reloadTblWhenGotMessage:(NSNotification *)message
{
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"message_count"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    NSDictionary *msgObject = (NSDictionary *)message.object;
    
    
    [mesageArr addObject:msgObject];
    
    [self.tableView reloadData];
    
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:mesageArr.count - 1 inSection:0];
    
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}


#pragma mark - webService

- (void)sendMessage: (NSString *)text
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"primaryId",self.userIdStr, @"secondaryId", text, @"message", nil];
    
    
    [[ServiceHelper sharedManager] setHudShow:NO];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"sendMessage" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         [self viewMessageBetweenUsers];
     }];

}


- (void)viewMessageBetweenUsers
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"primaryId",self.userIdStr, @"secondaryId", nil];
    
    NSLog(@"%@", (APPDEL).userDetailDict);
    
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"viewMessage" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                  mesageArr = [[result objectForKey:@"allmessage"] mutableCopy];
                 
                 [self.tableView reloadData];
             }
         }
        
     }];

}


#pragma mark - Action

- (IBAction)backTap
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)editTap
{
    if (self.tableView.isEditing == YES)
    {
        [self.tableView setEditing:NO animated:YES];
    }
    else
    {
        [self.tableView setEditing:YES animated:YES];
    }
}


#pragma mark - SLKTEXT Send Button

- (void)didPressRightButton:(id)sender
{
    [self.view endEditing:YES];
    
    
    [self sendMessage: self.textView.text];
    
    
    /*NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:self.textView.text, @"message", [(APPDEL).userDetailDict objectForKey:@"userId"], @"primaryid", @"1", @"yousend", nil];
    
    
    [mesageArr addObject:dict];
    
    [self.tableView reloadData];
    
   
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:mesageArr.count - 1 inSection:0];
   
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];*/
    
    
    [super didPressRightButton:sender];
}


#pragma mark - TableView Delegate


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return mesageArr.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[[mesageArr objectAtIndex:indexPath.row] valueForKey:@"yousend"] intValue] == 1)
    {
        static NSString *cellIdentifier = @"CELL";
        
        ChatTableViewCell *cell1 = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if (cell1 == nil)
        {
            cell1 = [[[NSBundle mainBundle] loadNibNamed:@"ChatTableViewCell" owner:self options:nil] objectAtIndex:1];
        }
        
        cell1.selfUserNAmeLbl.text = [(APPDEL).userDetailDict objectForKey:@"fullName"];
        
        
        cell1.selfUserMsgLbl.text  = [[mesageArr objectAtIndex:indexPath.row] objectForKey:@"message"];
        
        cell1.timeLbl.text = [self setDateFormat:[[mesageArr objectAtIndex:indexPath.row] objectForKey:@"datetime"]];
        
        
        [cell1.selfUserImg sd_setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [(APPDEL).userDetailDict objectForKey:@"profilePic"]] ] placeholderImage:[UIImage imageNamed:@"profile_demo"]];
        
        return cell1;
    }
    
    else
    {
        static NSString *cellIdentifier = @"CELL2";
       
        ChatTableViewCell *cell2 = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if (cell2 == nil)
        {
            
            cell2 = [[[NSBundle mainBundle] loadNibNamed:@"ChatTableViewCell" owner:self options:nil] objectAtIndex:0];
        }
        
        cell2.friendNameLbl.text = self.userNameStr;
   
        cell2.friendMsgLbl.text  = [[mesageArr objectAtIndex:indexPath.row] objectForKey:@"message"];
        
        cell2.timeLbl.text = [self setDateFormat:[[mesageArr objectAtIndex:indexPath.row] objectForKey:@"datetime"]];
        
        [cell2.friendImgView sd_setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, self.profileUrlStr] ] placeholderImage:nil];
      
        return cell2;
    }
}


@end
