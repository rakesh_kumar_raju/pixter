//
//  SendMessageViewController.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 3/3/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import "SLKTextViewController.h"
#import "ServiceHelper.h"


@interface SendMessageViewController : SLKTextViewController
{
    IBOutlet UILabel *titleLbl;
}

@property (nonatomic, retain) NSString *userNameStr, *userIdStr;

@property (nonatomic, retain) NSString *profileUrlStr;

- (IBAction)backTap;

@end
