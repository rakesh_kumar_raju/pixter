//
//  ViewSinglePostViewController.m
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 3/10/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import "ViewPostBySearchViewController.h"
#import "AddCommentViewController.h"
#import "ViewLikesViewController.h"
#import "UserCollectionViewCell.h"
#import "ShowPostsViewController.h"
#import "UIImageView+WebCache.h"

@interface ViewPostBySearchViewController ()
{
    NSMutableArray *postArr;
}


@end


@implementation ViewPostBySearchViewController



#pragma mark - ViewCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)])
    {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    
    self.navigationController.navigationBar.hidden = YES;
    
    [self setUpView];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    self.navigationController.navigationBar.hidden = YES;
    
    //NSLog(@"%@", (APPDEL).userDetailDict);
    
    [self getPostWebServiceBySearchString];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
}


#pragma mark - Void

- (void) setUpView
{
    refreshControl = [[UIRefreshControl alloc] init];
    
    refreshControl.tintColor = [UIColor whiteColor];
    
    [refreshControl addTarget:self action:@selector(getPostWebServiceBySearchString) forControlEvents:UIControlEventValueChanged];
    
    [postColectionView addSubview:refreshControl];
    
    postColectionView.alwaysBounceVertical = YES;
    
  
    postArr = [NSMutableArray new];
    
    [postColectionView registerNib:[UINib nibWithNibName:@"UserCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"userCell"];
  
    
    titleLbl.text = self.searchStr;
}


#pragma mark - Webservice


- (void)getPostWebServiceBySearchString
{
    NSString *methodName = @"";
    
    NSDictionary *params;
    
    if (self.isSeacrhTagPeople == YES)
    {
        methodName = @"getHashTagPost";
        
        params = [NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", self.searchStr, @"hashTag", nil];
    }
    
    else
    {
        methodName = @"getLocationPost";
        
         params = [NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", self.searchStr, @"location", nil];
    }
    

    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:methodName setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         
         [refreshControl endRefreshing];
         
         if (self.isSeacrhTagPeople == YES)
         {
              postArr = [NSMutableArray arrayWithArray:[result objectForKey:@"posts"]];
         }
         
         else
         {
             postArr = [[result objectForKey:@"location"] mutableCopy];
         }
         
         [postColectionView reloadData];
     }];
}




#pragma mark - CollectionView Delegate

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView.frame.size.width == 320)
    {
        return CGSizeMake(106, 106);
        
    }
    else if (collectionView.frame.size.width == 375)
    {
        return CGSizeMake(124, 124);
    }
    
    return  CGSizeMake(137, 137);
    
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return postArr.count;
}



- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView1 cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *gridCellIdentifier = @"userCell";
    
    UserCollectionViewCell *cell = (UserCollectionViewCell*)[collectionView1 dequeueReusableCellWithReuseIdentifier:gridCellIdentifier forIndexPath:indexPath];
    
    cell.contentView.autoresizingMask  = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    if ([[[postArr objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"image"])
    {
        [cell.userImg sd_setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [[postArr objectAtIndex:indexPath.row] valueForKey:@"media"]]] placeholderImage:[UIImage imageNamed:@""] options:SDWebImageRefreshCached];
        
        cell.videoIconImg.hidden = YES;
        
    }
    else
    {
        [cell.userImg sd_setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [[postArr objectAtIndex:indexPath.row] valueForKey:@"thumbnail"]]] placeholderImage:[UIImage imageNamed:@""] options:SDWebImageRefreshCached];
        
        
        cell.videoIconImg.hidden = NO;
    }
    
    return cell;
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    ShowPostsViewController *vc = [[ShowPostsViewController alloc] initWithNibName:@"ShowPostsViewController" bundle:nil];
    
    vc.hidesBottomBarWhenPushed = YES;
    
    
    vc.postArr = postArr;
    
    vc.indexPath = indexPath;
    
    [self.navigationController pushViewController:vc animated:YES];
}


#pragma mark - Action

- (IBAction)backTap
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
