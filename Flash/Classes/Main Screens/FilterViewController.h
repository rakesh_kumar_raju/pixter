//
//  FilterViewController.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 1/30/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GPUImage.h"
#import "CameraViewController.h"

@interface FilterViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource>
{
    IBOutlet UICollectionView *filterCollectionView;
    
    IBOutlet UIButton *filterBtn, *editBtn;
    
    UIImageView *filteredImg;
    
    //IBOutlet GPUImageView *filterView;
    
    IBOutlet UIView *contentView;
    
    IBOutlet UIView *bottomView, *controlView;
    
    IBOutlet UISlider *slider1, *slider2, *slider3;
    
    IBOutlet UILabel *sliderLbl1, *sliderLbl2, *sliderLbl3;
}

@property (nonatomic, retain) UIImage *captureImg;

@property (nonatomic, assign) BOOL isGallaryVideo;

@property (nonatomic, retain) NSURL *videoUrl;


- (IBAction)updatePixelWidth:(UISlider *)sender;

- (IBAction)switchBtn:(UIButton *)sender;

- (IBAction)backTap;

- (IBAction)nextTap;

- (IBAction)doneTap;

- (IBAction)cancelTap;

- (IBAction)addFilterValue:(UISlider *)sender;

@end
