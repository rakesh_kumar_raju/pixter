//
//  EditProfileViewController.m
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 2/14/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import "EditProfileViewController.h"
#import "KeyboardManager.h"
#import "QuartzCore/CALayer.h"

#define ACCEPTABLE_CHARACTERS @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_."


@interface EditProfileViewController ()
{
    NSArray *dataArr;
    
    NSString *base64ImgStr;
    
    NSDictionary *userDetail;
    
    BOOL isEditProfileDone;
}

@end


@implementation EditProfileViewController


#pragma mark - ViewCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    base64ImgStr = @"";
    
    [self setUpView];
    
    bioTv.inputAccessoryView = accessoryView;

    [birthdayTf setCustomDoneTarget:self action:@selector(birthDayPickerDoneTap:)];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    

    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)])
    {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    [self addBaseLineBelowTextField:@"Name" textfield:nameTf leftViewImageName:@"edit_name"];
    
    [self addBaseLineBelowTextField:@"Username" textfield:userNameTf leftViewImageName:@"edit_username"];
    
    /* [self addBaseLineBelowTextField:@"Heading" textfield:headingTf leftViewImageName:@"edit_heading"];*/
    
    [self addBaseLineBelowTextField:@"Mobile" textfield:phoneTf leftViewImageName:@"edit_mobile"];
    
    [self addBaseLineBelowTextField:@"Email" textfield:emailTf leftViewImageName:@"edit_email"];
    
    [self addBaseLineBelowTextField:@"Gender" textfield:genderTf leftViewImageName:@"edit_gender"];
    
    [self addBaseLineBelowTextField:@"City" textfield:cityTf leftViewImageName:@"edit_city"];
    
    [self addBaseLineBelowTextField:@"State" textfield:stateTf leftViewImageName:@"edit_state"];
    
    [self addBaseLineBelowTextField:@"Birthday" textfield:birthdayTf leftViewImageName:@"edit_bdy"];
    
    [self addBaseLineBelowTextField:@"Enter referral username" textfield:refferalNameTf leftViewImageName:@"refferal"];

}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    
    [UIView animateWithDuration:0.2 animations:^{
        
        self.tabBarController.tabBar.frame = CGRectMake(0, [[UIScreen mainScreen] bounds].size.height-self.tabBarController.tabBar.frame.size.height, self.tabBarController.tabBar.frame.size.width, self.tabBarController.tabBar.frame.size.height);
    }
                     completion:^(BOOL finished)
     {
         
     }];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
}


#pragma mark - Void

- (void) setUpView
{
    
    [profileImg.layer setCornerRadius:50.0f];
    
    genderTf.inputView = picker;
    
    birthdayTf.inputView = datePicker;
    
   
    dataArr = [[NSArray alloc] initWithObjects:@"Male", @"Female", nil];
    
    
    [self getUserDetailWebservice];
    
//    userDetail = [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"user_detail"]];
//    
//    
//    if ([userDetail allKeys].count)
//    {
//        NSLog(@"%@", userDetail);
//        
//        nameTf.text = [[userDetail objectForKey:@"full_name"] isEqual:[NSNull null]] ? @"" : [userDetail objectForKey:@"full_name"];
//        
//        userNameTf.text = [[userDetail objectForKey:@"username"] isEqual:[NSNull null]] ? @"" :[userDetail  objectForKey:@"username"];
//        
//        phoneTf.text = [[userDetail objectForKey:@"phonenumber"] isEqual:[NSNull null]] ? @"" :[userDetail objectForKey:@"phonenumber"];
//        
//        emailTf.text = [[userDetail objectForKey:@"email"] isEqual:[NSNull null]] ? @"" :[userDetail objectForKey:@"email"];
//        
//        stateTf.text = [[userDetail objectForKey:@"state"] isEqual:[NSNull null]] ? @"" :[userDetail objectForKey:@"state"];
//        
//        cityTf.text = [[userDetail objectForKey:@"city"] isEqual:[NSNull null]] ? @"" :[userDetail objectForKey:@"city"];
//        
//        birthdayTf.text = [[userDetail objectForKey:@"birthday"] isEqual:[NSNull null]] ? @"" :[userDetail objectForKey:@"birthday"];
//        
//        
//        genderTf.text = [[userDetail objectForKey:@"gender"] isEqual:[NSNull null]] ? @"" :[userDetail objectForKey:@"gender"];
//        
//      /*  headingTf.text = [[userDetail objectForKey:@"quote"] isEqual:[NSNull null]] ? @"" :[userDetail objectForKey:@"quote"];*/
//        
//        bioTv.text = [[userDetail objectForKey:@"quote"] isEqual:[NSNull null]] ? @"" : [userDetail  objectForKey:@"quote"];
//        
//        accessoryLbl.text = [NSString stringWithFormat:@"%lu/300", (unsigned long)bioTv.text.length];
//        
//        [profileImg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", ServiceURL, [userDetail objectForKey:@"profile_pic"]]] placeholderImage:[UIImage imageNamed:@"cameraIcon"] options:SDWebImageRefreshCached];
//        
//        base64ImgStr = [self base64ImageConversion:profileImg.image isCompress:NO];
//        
//    }
//    else
//    {
//        [self getUserDetailWebservice];
//    }
}


- (NSString *)base64ImageConversion:(UIImage *)conversionImage isCompress:(BOOL)compress
{
    UIImage *image;
    
    if (compress == YES)
    {
        image = [self resizeImage:conversionImage];
    }
    else
    {
        image = conversionImage;
    }
    
    NSData *data = UIImagePNGRepresentation(image);
    
    
    return  [data base64EncodedStringWithOptions:
                    NSDataBase64Encoding64CharacterLineLength];
}


#pragma mark - Webservice

- (void)getUserDetailWebservice
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", nil];
    
    
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"getProfile" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         
        
         if (result)
         {
            
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
//                 [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:[result objectForKey:@"detail"]] forKey:@"user_detail"];
//                 
//                 [[NSUserDefaults standardUserDefaults] synchronize];

                 nameTf.text = [[[result objectForKey:@"detail"] objectForKey:@"full_name"] isEqual:[NSNull null]] ? @"" : [[result objectForKey:@"detail"] objectForKey:@"full_name"];
                 
                 userNameTf.text = [[[result objectForKey:@"detail"] objectForKey:@"username"] isEqual:[NSNull null]] ? @"" : [[result objectForKey:@"detail"] objectForKey:@"username"];
                 
                 phoneTf.text = [[[result objectForKey:@"detail"] objectForKey:@"phonenumber"] isEqual:[NSNull null]] ? @"" : [[result objectForKey:@"detail"] objectForKey:@"phonenumber"];
                 
                 emailTf.text = [[[result objectForKey:@"detail"] objectForKey:@"email"] isEqual:[NSNull null]] ? @"" : [[result objectForKey:@"detail"] objectForKey:@"email"];
                 
                 genderTf.text = [[[result objectForKey:@"detail"] objectForKey:@"gender"] isEqual:[NSNull null]] ? @"" : [[result objectForKey:@"detail"] objectForKey:@"gender"];
                 
                 stateTf.text = [[[result objectForKey:@"detail"] objectForKey:@"state"] isEqual:[NSNull null]] ? @"" : [[result objectForKey:@"detail"] objectForKey:@"state"];;
                 
                 cityTf.text = [[[result objectForKey:@"detail"] objectForKey:@"city"] isEqual:[NSNull null]] ? @"" : [[result objectForKey:@"detail"] objectForKey:@"city"];;
                 
                 birthdayTf.text = [[[result objectForKey:@"detail"] objectForKey:@"birthday"] isEqual:[NSNull null]] ? @"" : [[result objectForKey:@"detail"] objectForKey:@"birthday"];
                 
                 refferalNameTf.text = [[[result objectForKey:@"detail"] objectForKey:@"refUser"] isEqual:[NSNull null]] ? @"" : [[result objectForKey:@"detail"] objectForKey:@"refUser"];;
                 
                 
                 if (refferalNameTf.text.length)
                 {
                     refferalNameTf.userInteractionEnabled = NO;
                 }
                 
                 
                 
                /* headingTf.text = [[[result objectForKey:@"detail"] objectForKey:@"quote"] isEqual:[NSNull null]] ? @"" : [[result objectForKey:@"detail"] objectForKey:@"quote"];*/
                 
                 bioTv.text = [[[result objectForKey:@"detail"] objectForKey:@"quote"] isEqual:[NSNull null]] ? @"" : [[result objectForKey:@"detail"] objectForKey:@"quote"];
    
                 [profileImg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", ServiceURL, [[result objectForKey:@"detail"] objectForKey:@"profile_pic"]]] placeholderImage:[UIImage imageNamed:@"cameraIcon"] options:SDWebImageRefreshCached];
                 
//                [profileImg sd_setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [result objectForKey:@"profile_pic"]] ] placeholderImage:nil];
                 
                 if (isEditProfileDone == YES)
                 {
                     [self backTap];
                 }
                 
                 if (![[[result objectForKey:@"detail"] objectForKey:@"usertype"] isEqualToString:@"primary"])
                 {
                     privateViewHeight.constant = 0;
                  
//                     phoneTf.hidden = YES;
//                     
//                     emailTf.hidden = YES;
//                     
//                     cityTf.hidden = YES;
//                     
//                     stateTf.hidden = YES;
//                     
//                     birthdayTf.hidden = YES;
//                     
//                     genderTf.hidden = YES;
//                     
//                     privateInfoTitleLbl.hidden = YES;
//                     
//                     
//                     emailTf.userInteractionEnabled = NO;
//                     
//                     cityTf.userInteractionEnabled = NO;
//                     
//                     stateTf.userInteractionEnabled = NO;
//                     
//                     birthdayTf.userInteractionEnabled = NO;
//                     
//                     genderTf.userInteractionEnabled = NO;
//                     
//                     refferalNameTf.userInteractionEnabled = NO;
//                     
//                
                     
                     emailTf.textColor = [UIColor lightGrayColor];
                     
                     cityTf.textColor = [UIColor lightGrayColor];
                     
                     stateTf.textColor = [UIColor lightGrayColor];
                     
                     birthdayTf.textColor = [UIColor lightGrayColor];
                     
                     cityTf.textColor = [UIColor lightGrayColor];
                 }
                else
                {
                    privateViewHeight.constant = 400;
                    
                    
//                    phoneTf.hidden = NO;
//                    
//                    emailTf.hidden = NO;
//                    
//                    cityTf.hidden = NO;
//                    
//                    stateTf.hidden = NO;
//                    
//                    birthdayTf.hidden = NO;
//                    
//                    genderTf.hidden = NO;
//                    
//                    privateInfoTitleLbl.hidden = NO;
//                    
//                    
//                    emailTf.userInteractionEnabled = YES;
//                    
//                    cityTf.userInteractionEnabled = YES;
//                    
//                    stateTf.userInteractionEnabled = YES;
//                    
//                    birthdayTf.userInteractionEnabled = YES;
//                    
//                    genderTf.userInteractionEnabled = YES;
                    
                    
                    emailTf.textColor = [UIColor whiteColor];
                    
                    cityTf.textColor = [UIColor whiteColor];
                    
                    stateTf.textColor = [UIColor whiteColor];
                    
                    birthdayTf.textColor = [UIColor whiteColor];
                    
                    cityTf.textColor = [UIColor whiteColor];
                }
            }
             
         }
         
     }];
}


- (void)updateUserDetailWebservice
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            [(APPDEL).userDetailDict objectForKey:@"userId"], @"userId",
                            nameTf.text, @"fullName",
                            emailTf.text, @"emailId",
                            userNameTf.text, @"userName",
                           /* headingTf.text, @"quote",*/
                            bioTv.text, @"quote",
                            base64ImgStr, @"profilePic",
                            cityTf.text, @"city",
                            stateTf.text, @"state",
                            birthdayTf.text, @"birthday",
                            genderTf.text , @"gender",
                            refferalNameTf.text, @"refUser", nil];
    
     [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"updateProfile" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         
         
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 isEditProfileDone = YES;
                 
                  [self getUserDetailWebservice];
                 
             }
             else
             {
                  [[[UIAlertView alloc] initWithTitle:nil message:[result objectForKey:@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
             }
             
         }
         
     }];
}



#pragma mark - Reduce Image quality

- (UIImage *)resizeImage:(UIImage *)image
{
    float actualHeight = image.size.height;
    
    float actualWidth = image.size.width;
    
    float maxHeight = 300.0;
    
    float maxWidth = 400.0;
    
    float imgRatio = actualWidth/actualHeight;
    
    float maxRatio = maxWidth/maxHeight;
    
    float compressionQuality = 0.5;//50 percent compression
    
    
    if (actualHeight > maxHeight || actualWidth > maxWidth)
    {
        if(imgRatio < maxRatio)
        {
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            
            actualWidth = imgRatio * actualWidth;
            
            actualHeight = maxHeight;
        }
        
        else if(imgRatio > maxRatio)
        {
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            
            actualHeight = imgRatio * actualHeight;
            
            actualWidth = maxWidth;
        }
        
        else
        {
            actualHeight = maxHeight;
            
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    
    
    UIGraphicsBeginImageContext(rect.size);
    
    
    [image drawInRect:rect];
    
    
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    
    
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    
    
    UIGraphicsEndImageContext();
    
    
    return [UIImage imageWithData:imageData];
}


#pragma mark - Camera

- (void)takePhotoFromCamera
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imgPicker = [[UIImagePickerController alloc] init];
        
        imgPicker.delegate = self;
        
        imgPicker.allowsEditing = YES;
        
        imgPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        
        [self presentViewController:imgPicker animated:YES completion:NULL];
    }
}


- (void)takePotoFromGallary
{
    UIImagePickerController *imgPicker = [[UIImagePickerController alloc] init];
    
    imgPicker.delegate = self;
    
    imgPicker.allowsEditing = YES;
    
    imgPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:imgPicker animated:YES completion:NULL];
}


#pragma mark - Action

- (IBAction)backTap
{
    if (self.navigationController)
    {
        [self.navigationController popViewControllerAnimated:YES];
        
        return;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)doneTap
{
    if (nameTf.text.length == 0 || userNameTf.text.length == 0 || phoneTf.text.length == 0 || emailTf.text.length == 0)
    {
        [[[UIAlertView alloc] initWithTitle:nil message:[NSString allEmptyTxtFields] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        
        return;
    }
    else if(![NSString stringIsValidEmail:emailTf.text])
    {
        [[[UIAlertView alloc] initWithTitle:nil message:[NSString emailFormatNotCorrect] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        
        return;
    }
    else
    {
        [self updateUserDetailWebservice];
    }
}

- (IBAction)tapGestSender:(UITapGestureRecognizer *)sender
{
     [[[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take photo", @"Choose existing photo", nil] showInView:self.view];
}


- (IBAction)accessoryDoneTap
{
    [self.view endEditing:YES];
}


- (void)birthDayPickerDoneTap:(UITextField *)sender
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    
    [dateFormat setDateFormat:@"dd-MMMM-yyyy"];
    
    birthdayTf.text = [dateFormat stringFromDate: datePicker.date];
    
}


#pragma mark - Add base line 

- (void)addBaseLineBelowTextField :(NSString *)placeholder textfield:(UITextField *)textfield leftViewImageName:(NSString *)name
{
    UIColor *color = [UIColor lightGrayColor];
    
    textfield.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeholder attributes:@{NSForegroundColorAttributeName: color}];
    
    CALayer *bottomBorder = [CALayer layer];
    
    bottomBorder.frame = CGRectMake(0.0f, textfield.frame.size.height - 1, [[UIScreen mainScreen] bounds].size.width -10, 1.0f);
    
    bottomBorder.backgroundColor = [UIColor lightGrayColor].CGColor;
    
    //[textfield.layer addSublayer: bottomBorder];
    
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    
    UIImageView *fillImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    
    fillImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    fillImageView.image = [UIImage imageNamed:name];
    
    
    [leftView addSubview: fillImageView];
    
    
    textfield.leftViewMode = UITextFieldViewModeAlways;
    
    textfield.leftView = leftView;
}



#pragma mark - Picker Delegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView;
{
    return 1;
}


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component;
{
    return dataArr.count;
}


- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return dataArr[row];
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    genderTf.text = dataArr[row];
}


- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *title = dataArr[row];
    
    NSAttributedString *attString = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor blackColor]}];
    
    return attString;
}


#pragma mark - ActionSheet

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0)
    {
        [self takePhotoFromCamera];
    }
    
    else if(buttonIndex == 1)
    {
        [self takePotoFromGallary];
    }
}


#pragma mark - ImgaePicker Delegate

- (void)imagePickerController:(UIImagePickerController *)picker1 didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    
    
    profileImg.image = chosenImage;
    
    
    base64ImgStr = [self base64ImageConversion:chosenImage isCompress:YES];
    
    
    [picker1 dismissViewControllerAnimated:YES completion:NULL];
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker1
{
    [picker1 dismissViewControllerAnimated:YES completion:NULL];
}


#pragma mark - TextviewDelegate


- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{

    
    return YES;
}



- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (textView == bioTv)
    {
        accessoryLbl.text = [NSString stringWithFormat:@"%lu/300", (unsigned long)textView.text.length];
        
        return [[textView text] length] - range.length + text.length <= 300;
    }
      return NO;
}



#pragma mark - TextFieldDelegate


/*- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if ([nameTf canBecomeFirstResponder])
    {
        [nameTf becomeFirstResponder];
    }
    NSLog(@"212");
    
   // [[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
    
    return YES;
}*/


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == nameTf)
    {
        [userNameTf becomeFirstResponder];
    }
    
    else if (textField == userNameTf)
    {
        [bioTv becomeFirstResponder];
    }
    
    else if (textField == phoneTf)
    {
        [emailTf becomeFirstResponder];
    }
    
    else if (textField == emailTf)
    {
        [genderTf becomeFirstResponder];
    }
    
    else if (textField == genderTf)
    {
        [genderTf resignFirstResponder];
    }
    
    return YES;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == userNameTf)
    {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        if ([string isEqualToString:filtered])
        {
            if(range.length + range.location > textField.text.length)
            {
                return NO;
            }
            
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            
            return newLength <= 30;
         }
        else
        {
            return NO;
        }
    }
   else
   {
       return YES;
   }
}


@end
