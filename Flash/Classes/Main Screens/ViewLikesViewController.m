//
//  ViewLikesViewController.m
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 2/20/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import "ViewLikesViewController.h"
#import "LikesTableViewCell.h"
#import "ServiceHelper.h"
#import "ProfileViewController.h"


@interface ViewLikesViewController ()
{
    NSMutableArray *likesArr;
    
    NSString *webServiceName, *paramName;
    
    NSIndexPath *indexpath;
}

@end


@implementation ViewLikesViewController

#pragma mark - ViewCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.hidden = YES;
    
    [self setUpView];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [self viewAllUserWhoLikesYourPost];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
}


#pragma mark - Void

- (void) setUpView
{
    refreshControl = [[UIRefreshControl alloc] init];
    
    refreshControl.tintColor = [UIColor blackColor];
    
    [refreshControl addTarget:self action:@selector(viewAllUserWhoLikesYourPost) forControlEvents:UIControlEventValueChanged];
    
    [likesTbl addSubview:refreshControl];
    
    likesTbl.alwaysBounceVertical = YES;
}



#pragma mark - table cell button Action


- (void)followTap:(UIButton *)sender
{
    indexpath = [likesTbl indexPathForCell: (LikesTableViewCell *)sender.superview.superview];
    
    
    if ([[[likesArr objectAtIndex:sender.tag] objectForKey:@"youfollowing"] intValue] == 0)
    {
        [self changeFollowStatus];
    }
    else if ([[[likesArr objectAtIndex:sender.tag] objectForKey:@"youfollowing"] intValue] == 1)
    {
        unfollowView.frame = self.view.frame;
        
        [unfollowUserImg sd_setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [[likesArr objectAtIndex:sender.tag] valueForKey:@"profile_pic"]]] placeholderImage:nil];
        
        unfollowUserNameLbl.text = [NSString stringWithFormat:@"Unfollow %@?", [[likesArr objectAtIndex:sender.tag] valueForKey:@"full_name"]];
        
        
        [self.view addSubview:unfollowView];
        
        
        [UIView animateWithDuration:0.1 animations:^
         {
             popupView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
         }
                         completion:^(BOOL finished)
         {
             [UIView animateWithDuration:0.1 animations:^
              {
                  popupView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
              }
                              completion:^(BOOL finished)
              {
                  [UIView animateWithDuration:0.1 animations:^
                   {
                       popupView.transform = CGAffineTransformIdentity;
                   }];
              }];
         }];
    }
    else
    {
        [self changeFollowStatus];
    }
}


#pragma mark - Action

- (IBAction)backTap
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)unfollowCancelTap
{
    [unfollowView removeFromSuperview];
}


- (IBAction)unfollowTap
{
    [self changeFollowStatus];
    
    [unfollowView removeFromSuperview];
}


#pragma mark - Webservice

- (void)viewAllUserWhoLikesYourPost
{
    NSDictionary *params;
    
    if ([self.switchTitleStr isEqualToString:@"like"])
    {
        titleLbl.text = @"Likes";
        
        webServiceName = @"viewTotalLike";
        
        paramName = @"postId";
        
        params = [NSDictionary dictionaryWithObjectsAndKeys:self.postId, paramName, [(APPDEL).userDetailDict objectForKey:@"userId"], @"mainId", nil];
    }
    
    else if ([self.switchTitleStr isEqualToString:@"FOLLOWERS"])
    {
        titleLbl.text = @"Followers";
        
        webServiceName = @"getYouFollow";
        
        paramName = @"userId";
        
        params = [NSDictionary dictionaryWithObjectsAndKeys:self.postId, paramName, [(APPDEL).userDetailDict objectForKey:@"userId"], @"mainId", nil];
    }
    else
    {
        titleLbl.text = @"Following";
        
        webServiceName = @"getYourFollowing";
        
        paramName = @"userId";
        
        params = [NSDictionary dictionaryWithObjectsAndKeys:self.postId, paramName, [(APPDEL).userDetailDict objectForKey:@"userId"], @"mainId", nil];
    }
    
    
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:webServiceName setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
   
         [refreshControl endRefreshing];
         
         if ([self.switchTitleStr isEqualToString:@"like"])
         {
            likesArr = [[result objectForKey:@"alllike"] mutableCopy];
         }
         
         else if ([self.switchTitleStr isEqualToString:@"followers"])
         {
            likesArr = [[result objectForKey:@"list"] mutableCopy];
         }
         else
         {
           likesArr = [[result objectForKey:@"list"] mutableCopy];
         }
         
         
         [likesTbl reloadData];
     }];
}



- (void)changeFollowStatus
{
    NSMutableDictionary *dict = [[likesArr objectAtIndex:indexpath.row] mutableCopy];
    
    [likesArr removeObjectAtIndex:indexpath.row];
    
    
    if ([[dict objectForKey:@"youfollowing"] intValue] == 0)
    {
        [dict setObject:@"1" forKey:@"youfollowing"];
        
        [likesArr insertObject:dict atIndex:indexpath.row];
        
        [self followUser:[[likesArr objectAtIndex:indexpath.row] valueForKey:@"userId"]];
    }
    else if ([[dict objectForKey:@"youfollowing"] intValue] == 1)
    {
        [dict setObject:@"0" forKey:@"youfollowing"];
        
        [likesArr insertObject:dict atIndex:indexpath.row];
        
        [self unfollowUser:[[likesArr objectAtIndex:indexpath.row] valueForKey:@"userId"]];
    }
    else
    {
        [dict setObject:@"0" forKey:@"youfollowing"];
        
        [likesArr insertObject:dict atIndex:indexpath.row];
        
        
        [self cancelFollowRequest:[[likesArr objectAtIndex:indexpath.row] valueForKey:@"userId"]];
        
    }
}


- (void)followUser:(NSString *)userId
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"primary_userid", userId, @"secondary_userid", nil];
    
    
    [[ServiceHelper sharedManager] setHudShow:NO];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"sendFollowingRequest" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 [self viewAllUserWhoLikesYourPost];
             }
             
             else
             {
                 
             }
         }
         else
         {
             NSMutableDictionary *dict = [[likesArr objectAtIndex:indexpath.row] mutableCopy];
             
             [likesArr removeObjectAtIndex:indexpath.row];
             
             
             if ([[dict objectForKey:@"youfollowing"] intValue] == 0)
             {
                 [dict setObject:@"1" forKey:@"youfollowing"];
             }
             else
             {
                 [dict setObject:@"0" forKey:@"youfollowing"];
             }
             
             [likesArr insertObject:dict atIndex:indexpath.row];
             
             
             [likesTbl reloadRowsAtIndexPaths:@[indexpath] withRowAnimation:UITableViewRowAnimationFade];
         }
     }];
    
    
}


- (void)unfollowUser:(NSString *)userId
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userid", userId, @"followerid", nil];
    
    
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"unFollowUser" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
             [self viewAllUserWhoLikesYourPost];
             
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 
             }
             
             else
             {
                 
             }
         }
         else
         {
             NSMutableDictionary *dict = [[likesArr objectAtIndex:indexpath.row] mutableCopy];
             
             [likesArr removeObjectAtIndex:indexpath.row];
             
             
             if ([[dict objectForKey:@"youfollowing"] intValue] == 0)
             {
                 [dict setObject:@"1" forKey:@"youfollowing"];
             }
             else
             {
                 [dict setObject:@"0" forKey:@"youfollowing"];
             }
             
             [likesArr insertObject:dict atIndex:indexpath.row];
             
             
             [likesTbl reloadRowsAtIndexPaths:@[indexpath] withRowAnimation:UITableViewRowAnimationFade];
         }
     }];
    
}



- (void)cancelFollowRequest:(NSString *)userId
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", userId, @"secondaryId", nil];
    
    
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"cancelFollowRequest" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
             [self viewAllUserWhoLikesYourPost];
             
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 
             }
             
             else
             {
                 
             }
         }
         else
         {
           
         }
     }];
    
}



#pragma mark - TableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return likesArr.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"CELL";
    
    LikesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"LikesTableViewCell" owner:self options:nil] lastObject];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [cell.userProfileImg sd_setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [[likesArr objectAtIndex:indexPath.row] valueForKey:@"profile_pic"] ] ] placeholderImage:[UIImage imageNamed:@"profile_demo"]];
    
    cell.nameLbl.text = [[[likesArr objectAtIndex:indexPath.row] valueForKey:@"full_name"] lowercaseString];
    
    
    if ([[(APPDEL).userDetailDict objectForKey:@"userId"] intValue] != [[[likesArr objectAtIndex:indexPath.row] valueForKey:@"userId"] intValue])
    {
        if ([[[likesArr objectAtIndex:indexPath.row] objectForKey:@"youfollowing"] intValue] == 0)
        {
            [cell.followBtn setTitle:@"Follow" forState:UIControlStateNormal];
            
            cell.followBtn.backgroundColor = [UIColor whiteColor];
            
            [cell.followBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        }
        else if ([[[likesArr objectAtIndex:indexPath.row] objectForKey:@"youfollowing"] intValue] == 1)
        {
            [cell.followBtn setTitle:@"Following" forState:UIControlStateNormal];
            
            cell.followBtn.backgroundColor = [UIColor blackColor];
            
            [cell.followBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            
            
            cell.followBtn.layer.cornerRadius = 3.0;
            
            cell.followBtn.layer.borderWidth = 1.0;
            
            cell.followBtn.layer.borderColor = [UIColor whiteColor].CGColor;
        }
        else
        {
            [cell.followBtn setTitle:@"Requested" forState:UIControlStateNormal];
            
            cell.followBtn.backgroundColor = [UIColor blackColor];
            
            [cell.followBtn setTitleColor: [UIColor whiteColor] forState:UIControlStateNormal];
            
            
            cell.followBtn.layer.cornerRadius = 3.0;
            
            cell.followBtn.layer.borderWidth = 1.0;
            
            cell.followBtn.layer.borderColor = [UIColor whiteColor].CGColor;
        }
        

        cell.followBtn.tag = indexPath.row;
        
        [cell.followBtn addTarget:self action:@selector(followTap:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ProfileViewController *vc = [[ProfileViewController alloc] initWithNibName:@"ProfileViewController" bundle:nil];
    
    vc.differentUserId = [[likesArr objectAtIndex: indexPath.row] objectForKey:@"userId"] ;
    
    [self.navigationController pushViewController:vc animated:YES];
}


 

@end
