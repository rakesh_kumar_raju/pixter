//
//  HelpViewViewController.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 7/26/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpViewViewController : UIViewController<UIWebViewDelegate>
{
    IBOutlet UIWebView *helpWebView;
}

- (IBAction)backTap;

@end
