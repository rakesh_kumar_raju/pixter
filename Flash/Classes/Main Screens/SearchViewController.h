//
//  SearchViewController.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 2/24/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"


@interface SearchViewController : UIViewController <UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UISearchBar *searchBar;
    
    IBOutlet UITableView *searchUserTbl;
    
    UIRefreshControl *refreshControl;
    
    IBOutlet UIScrollView *contentSv;
    
    IBOutlet UIImageView *animatImage;
    
    IBOutlet UIButton *topBtn, *peopleBtn, *tagBtn, *locationBtn;
    
    IBOutlet UIActivityIndicatorView *indicator;
}

- (IBAction)backTap;

- (IBAction)tabbesButtonAction:(UIButton *)sender;


@end
