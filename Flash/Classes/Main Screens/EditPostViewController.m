//
//  EditPostViewController.m
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 4/6/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import "EditPostViewController.h"
#import "TagViewController.h"
#import "AddLocationViewController.h"
#import "TabbarViewController.h"
#import "HomeViewController.h"
#import "ProfileViewController.h"
#import "ServiceHelper.h"


@interface EditPostViewController ()
{
    NSMutableArray *tagArr;
}

@end


@implementation EditPostViewController


#pragma mark - ViewCycle


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    //[captionTv performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:0.2];
    
    (APPDEL).locationStr = [self.postDict objectForKey:@"location"];
    
  
    if ([[self.postDict objectForKey:@"location"] isEqualToString:@""])
    {
         [locationBtn setTitle:@"Add your location.." forState:UIControlStateNormal];
    }
    else
    {
         [locationBtn setTitle:(APPDEL).locationStr forState:UIControlStateNormal];
    }
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getNewLocation:) name:@"SetNewLocation" object:nil];
    
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getNewTagPeople:) name:@"GetNewTagPeople" object:nil];
    
   
    
    [(APPDEL).tagPeopleArr removeAllObjects];
    
    [self setUpView];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    NSLog(@"%@", (APPDEL).tagPeopleArr);
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
}


#pragma mark - Void


- (void)setUpView
{
    if ([[self.postDict objectForKey:@"type"] isEqualToString:@"image"])
    {
        [postImg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", ServiceURL, [self.postDict objectForKey:@"media"]]] placeholderImage:nil options:SDWebImageRefreshCached];
        
        NSArray *arr = [self.postDict objectForKey:@"tagpeople"];
        
        if (arr.count)
        {
            tagPeopleLbl.text = [NSString stringWithFormat:@"%lu People", (unsigned long)arr.count];
            
            (APPDEL).tagPeopleArr = [[self.postDict objectForKey:@"tagpeople"] mutableCopy];
        }
        else
        {
            tagPeopleLbl.text = @"";
        }
        
        
        tagBtn.hidden = NO;
    }
    else
    {
        [postImg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", ServiceURL, [self.postDict objectForKey:@"thumbnail"]]] placeholderImage:nil options:SDWebImageRefreshCached];
        
        tagBtn.hidden = YES;
    }
    
    
    captionTv.placeholder = @"Add Description...";
    
    
    [profileImg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", ServiceURL, [self.postDict objectForKey:@"profilePic"]]] placeholderImage:nil options:SDWebImageRefreshCached];
    
    
    nameLbl.text = [self.postDict objectForKey:@"fullname"];
    
  
    locationBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;;
    
    locationBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    
    

    captionTv.text = [self.postDict objectForKey:@"caption"];
}


#pragma mark - Notification


- (void)getNewLocation:(NSNotification *)noti
{
     [locationBtn setTitle:noti.object forState:UIControlStateNormal];
}

- (void)getNewTagPeople:(NSNotification *)noti
{
    NSLog(@"%@", (APPDEL).tagPeopleArr);
    
    NSMutableArray *arr = [NSMutableArray new];
    
    for (NSDictionary *dict in noti.object)
    {
        if ([dict allKeys].count)
        {
            [arr addObject:dict];
        }
    }
    
    
    if (arr.count)
    {
        tagPeopleLbl.text = [NSString stringWithFormat:@"%lu People", (unsigned long)arr.count];
        
        (APPDEL).tagPeopleArr = arr;
    }
    else
    {
        tagPeopleLbl.text = @"";
    }

}


#pragma mark - Action

- (IBAction)backTap
{
   [self.navigationController popViewControllerAnimated:NO];
}


- (IBAction)doneTap
{
    [self editPostWebservice];
}


- (IBAction)tagBtnTap
{
    TagViewController *vc = [[TagViewController alloc] initWithNibName:@"TagViewController" bundle:nil];
    
    vc.image = postImg.image;
    
    vc.ifEditPost = YES;
    
    [self presentViewController:vc animated:NO completion:nil];
}


- (IBAction)locationBtnTap
{
    AddLocationViewController *vc = [[AddLocationViewController alloc] initWithNibName:@"AddLocationViewController" bundle:nil];
    
    [self presentViewController:vc animated:YES completion:nil];
}


- (IBAction)tapGestOnImage:(UITapGestureRecognizer *)sender
{
    [self tagBtnTap];
}


#pragma mark - Webservice


- (void)editPostWebservice
{
     NSLog(@"%@", (APPDEL).tagPeopleArr);
    
    NSError * err;
    
    NSMutableArray *taggArr = [NSMutableArray new];
    
    for (NSDictionary *dict in (APPDEL).tagPeopleArr)
    {
        if ([dict allKeys].count)
        {
            [taggArr addObject:dict];
        }
    }
    
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:taggArr options:0 error:&err];
    
    
    NSString *tagPeopleStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            [self.postDict objectForKey:@"userId"], @"userId",
                            [self.postDict objectForKey:@"postId"], @"postId",
                            captionTv.text, @"caption",
                            tagPeopleStr, @"tagpeople",
                            [locationBtn.titleLabel.text isEqualToString:@"Add your location.."] ? @"" : locationBtn.titleLabel.text , @"location",nil];
    
   
    [[ServiceHelper sharedManager] setHudShow:YES];
    
   
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"editPost" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if ([[result objectForKey:@"status"] intValue] == 1)
         {
             (APPDEL).locationStr = @"";
             
             [(APPDEL).tagPeopleArr removeAllObjects];
             
             
             for (UIViewController *controller in self.navigationController.viewControllers)
             {
                 if ([controller isKindOfClass:[HomeViewController class]] || [controller isKindOfClass:[ProfileViewController class]])
                 {
                     [self.navigationController popToViewController:controller animated:YES];
                 }
             }
         }
     }];
}


#pragma mark - TextView

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (textView == captionTv)
    {
        return [[textView text] length] - range.length + text.length <= 1000;
    }
    return NO;
}

@end
