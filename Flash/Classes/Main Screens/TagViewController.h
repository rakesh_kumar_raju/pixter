//
//  TagViewController.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 3/30/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface TagViewController : UIViewController <UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate>
{
    IBOutlet UIView *findPersonView;
    
    IBOutlet UISearchBar *searchBar;
    
    IBOutlet UIImageView *dataImg;
    
    IBOutlet UITableView *peopleTbl;
}

@property (nonatomic, retain) UIImage *image;

@property (nonatomic, assign) BOOL ifEditPost;


- (IBAction)backTap;

- (IBAction)doneTap;

@end
