//
//  MessageViewController.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 1/2/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HelpViewViewController.h"

@interface MessageViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UITableView *universalTable;
    
    IBOutlet UILabel *titleLbl;
    
    IBOutlet NSLayoutConstraint *tblBottom;
    
    IBOutlet UIButton *newMsgBtn;
    
    IBOutlet UIButton *editBtn, *plusBtn;
    
    UIRefreshControl *refreshControl;
}


@property (nonatomic, assign) BOOL isVCForMessage;

- (IBAction)editTap;

- (IBAction)backTap;

-(IBAction)newMsgTap;

@end
