//
//  AddCommentViewController.m
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 2/15/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#define hashtag_color [UIColor colorWithRed:18.0/255.0 green:86.0/255.0 blue:136.0/255.0 alpha:1.0]


#import "AddCommentViewController.h"
#import "CommentTableViewCell.h"
#import "ServiceHelper.h"
#import "ResponsiveLabel.h"



@interface AddCommentViewController ()
{
    //NSDictionary *userDict;
    
    NSMutableArray *commentArr;
}

@end


@implementation AddCommentViewController


#pragma mark - ViewCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.hidden = NO;
    
    [self setUpView];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    
    [self getAllComents];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
}


#pragma mark - Void

- (void) setUpView
{
    refreshControl = [[UIRefreshControl alloc] init];
    
    refreshControl.tintColor = [UIColor blackColor];
    
    [refreshControl addTarget:self action:@selector(getAllComents) forControlEvents:UIControlEventValueChanged];
    
    [self.tableView addSubview:refreshControl];
    
    self.tableView.alwaysBounceVertical = YES;
    
    self.tableView.backgroundColor = [UIColor blackColor];
    
    
    self.textView.keyboardAppearance = UIKeyboardAppearanceDark;
    
   
    self.textInputbar.tintColor = [UIColor blackColor];
    
    self.textInputbar.backgroundColor = [UIColor blackColor];
    
    self.textInputbar.translucent = NO;
    
    
    
    //userDict = [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"login_dict"]];
    
    
    self.bounces = YES;
 
    self.inverted = NO;
    
    self.shouldScrollToBottomAfterKeyboardShows = YES;
    
    self.textView.placeholder = @"Write a comment...";
    
    [self.rightButton setTitle:NSLocalizedString(@"Send", nil) forState:UIControlStateNormal];
    
   
    self.rightButton.tintColor = [UIColor whiteColor];
    
    self.textInputbar.autoHideRightButton = NO;
    
    self.textInputbar.maxCharCount = 256;
    
    self.textInputbar.counterStyle = SLKCounterStyleSplit;
    
    self.textInputbar.counterPosition = SLKCounterPositionTop;
    
    self.textView.keyboardType = UIKeyboardTypeDefault;
    
    self.textView.backgroundColor = [UIColor grayColor];
    
    self.textView.textColor = [UIColor whiteColor];
    
    
    self.tableView.estimatedRowHeight = 80;
   
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    
    self.title = @"Comments";
    
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    
    self.navigationController.navigationBar.translucent = NO;
    
    
    self.navigationController.navigationBar.titleTextAttributes =  @{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName: [UIFont fontWithName:@"MuseoSansCyrl-500" size:18]};
    
    
    
 
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [backBtn setBackgroundImage:[UIImage imageNamed:@"phone_back"] forState:UIControlStateNormal];
    
    backBtn.frame = CGRectMake(0, 25.0, 40, 40);
    
    backBtn.imageEdgeInsets = UIEdgeInsetsMake(0, -40, 0, 0);
    
    [backBtn addTarget:self action:@selector(backTap) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    UIBarButtonItem *leftBtn = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    
    self.navigationItem.leftBarButtonItem = leftBtn;
}


#pragma mark - Animation on button

- (void)animateButton:(UIButton *)sender
{
    sender.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    
    [UIView animateWithDuration:0.3/1.5 animations:^{
        
        sender.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
    }
                     completion:^(BOOL finished)
     {
         [UIView animateWithDuration:0.3/2 animations:^{
             
             sender.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
         }
                          completion:^(BOOL finished)
          {
              [UIView animateWithDuration:0.3/2 animations:^{
                  
                  sender.transform = CGAffineTransformIdentity;
              }];
          }];
     }];
}


#pragma mark - Tableview Action


- (void)replyBtnTap:(UIButton *)sender
{
    
    self.textView.text = [NSString stringWithFormat:@"@%@", [[commentArr objectAtIndex:sender.tag] valueForKey:@"full_name"]];
    
    [self.textView becomeFirstResponder];
}


- (void)commentLikeTap:(UIButton *)sender
{
    [self animateButton:sender];
    
    NSMutableDictionary *dict = [[commentArr objectAtIndex:sender.tag] mutableCopy];
    
    
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userId",[[commentArr objectAtIndex:sender.tag] objectForKey:@"commentid"], @"commentId", [[commentArr objectAtIndex:sender.tag] objectForKey:@"postId"], @"postId", [[commentArr objectAtIndex:sender.tag] objectForKey:@"userId"], @"secondaryId", nil];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"likeComment" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if ([[result objectForKey:@"status"] intValue] == 1)
         {
             if ([[[commentArr objectAtIndex:sender.tag] objectForKey:@"youlike"] intValue] == 0)
             {
                 [commentArr removeObjectAtIndex:sender.tag];
                 
               
                 [dict setObject:@"1" forKey:@"youlike"];
                 
                 int totlLike = [[dict objectForKey:@"totallike"] intValue]+1;
                 
                 [dict setObject:[NSString stringWithFormat:@"%i", totlLike] forKey:@"totallike"];
                 
                 
                 [commentArr insertObject:dict atIndex:sender.tag];
             }
             else
             {
                 [commentArr removeObjectAtIndex:sender.tag];
                 
                 
                 [dict setObject:@"0" forKey:@"youlike"];
                 
                 int totlLike = [[dict objectForKey:@"totallike"] intValue]-1;
                 
                 [dict setObject:[NSString stringWithFormat:@"%i", totlLike] forKey:@"totallike"];
                 
                 
                 [commentArr insertObject:dict atIndex:sender.tag];
             }
         }
         
         [self.tableView reloadData];
     }];
}



#pragma mark - Action


- (IBAction)backTap
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Webservice

- (void)getAllComents
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userId",self.postId, @"postId", nil];
    
   
    [[ServiceHelper sharedManager] setHudShow:NO];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"viewComment" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         [refreshControl endRefreshing];
         
         commentArr = [[result objectForKey:@"comments"] mutableCopy];
         
         [self.tableView reloadData];
     }];

}


- (void)postComment:(NSString *)text
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userId",self.postId, @"postId", self.secondryId, @"secondaryId", text, @"comment", nil];
    
    
    [[ServiceHelper sharedManager] setHudShow:NO];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"addComment" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         [self getAllComents];
     }];
}


#pragma mark - TableView Delegate


/*- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *commentStr = [[commentArr objectAtIndex:indexPath.row] valueForKey:@"comment"];
    
    
    CGRect textRect = [commentStr boundingRectWithSize:CGSizeMake([[UIScreen mainScreen] bounds].size.width-20, FLT_MAX)
                                                    options:NSStringDrawingUsesLineFragmentOrigin
                                                 attributes:@{NSFontAttributeName:[UIFont fontWithName:@"MuseoSansCyrl-500" size:14]}
                                                    context:nil];
    
    return textRect.size.height + 90;
}*/


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return commentArr.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"CELL";
    
    CommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"CommentTableViewCell" owner:self options:nil] lastObject];
    }
    
    [cell.userProfileImg sd_setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [[commentArr objectAtIndex:indexPath.row] valueForKey:@"profile_pic"] ] ] placeholderImage:[UIImage imageNamed:@"profile_demo"]];
    
    cell.nameLbl.text = [[[commentArr objectAtIndex:indexPath.row] valueForKey:@"full_name"] lowercaseString];
    
    cell.commentLbl.text = [[commentArr objectAtIndex:indexPath.row] valueForKey:@"comment"];
    
    cell.replyBtn.tag = indexPath.row;
    
    cell.likeBtn.tag = indexPath.row;
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    [cell.replyBtn addTarget:self action:@selector(replyBtnTap:) forControlEvents:UIControlEventTouchUpInside];

    [cell.likeBtn addTarget:self action:@selector(commentLikeTap:) forControlEvents:UIControlEventTouchUpInside];
    
    
    if ([[[commentArr objectAtIndex:indexPath.row] valueForKey:@"youlike"] intValue] == 0)
    {
        [cell.likeBtn setImage:[UIImage imageNamed:@"comment_like"] forState:UIControlStateNormal];
    }
    else
    {
        [cell.likeBtn setImage:[UIImage imageNamed:@"comment_like_h"] forState:UIControlStateNormal];
    }
    
    
    if ([[[commentArr objectAtIndex:indexPath.row] valueForKey:@"totallike"] intValue] != 0)
    {
        NSString *singularOrPlural = @"";
        
        if ([[[commentArr objectAtIndex:indexPath.row] valueForKey:@"totallike"] intValue] > 1)
        {
            singularOrPlural = @"Likes";
        }
        else
        {
            singularOrPlural = @"Like";
        }
        
        [cell.totalLikeBtn setTitle:[NSString stringWithFormat:@"%@ %@", [[commentArr objectAtIndex:indexPath.row] valueForKey:@"totallike"], singularOrPlural] forState:UIControlStateNormal];
    }
    
    [cell.replyBtn addTarget:self action:@selector(replyBtnTap:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    //Hastag detection
    
    cell.commentLbl.userInteractionEnabled = YES;
    PatternTapResponder hashTagTapAction = ^(NSString *tappedString)
    {
        NSLog(@"HashTag Tapped = %@",tappedString);
    };
    
    [cell.commentLbl enableHashTagDetectionWithAttributes:
     @{NSForegroundColorAttributeName:hashtag_color, RLTapResponderAttributeName:hashTagTapAction}];
    
    //
    
    
    //Username detection
    
    cell.commentLbl.userInteractionEnabled = YES;
    PatternTapResponder userNameDetection = ^(NSString *tappedString)
    {
        NSLog(@"HashTag Tapped = %@",tappedString);
    };
    
    [cell.commentLbl enableUserHandleDetectionWithAttributes:@{NSForegroundColorAttributeName:hashtag_color, RLTapResponderAttributeName:userNameDetection}];
    
    //
    
    cell.timeLbl.text = [[commentArr objectAtIndex:indexPath.row] valueForKey:@"commenttime"];
    
    /*static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
    }
    
    //Profile image
    
    UIImageView *profileImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 50, 50)];
    
    profileImage.layer.cornerRadius = 25.0;
    
    profileImage.clipsToBounds = YES;
    
    [profileImage sd_setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [[commentArr objectAtIndex:indexPath.row] valueForKey:@"profile_pic"] ] ] placeholderImage:nil];
    
    
    [cell.contentView addSubview:profileImage];
    
    
    //Profile Picture button
    
    UIButton *profilePicBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    profilePicBtn.tag = indexPath.row;
    
    //[profilePicBtn addTarget:self action:@selector(tapOnProfileImage:) forControlEvents:UIControlEventTouchUpInside];
    
    profilePicBtn.frame = profileImage.frame;
    
    [profilePicBtn setImage:[UIImage imageNamed:@"like"] forState:UIControlStateNormal];
    
    [cell.contentView addSubview:profilePicBtn];
    
    
    
    // Name;
    
    UILabel *nameLbl = [[UILabel alloc] initWithFrame:CGRectMake(profileImage.frame.origin.x + profileImage.frame.size.width+5, 20, 170, 20)];
    
    nameLbl.font = [UIFont fontWithName:@"MuseoSansCyrl-700" size:13];
    
    nameLbl.textColor = [UIColor blackColor];
    
    nameLbl.text = [[commentArr objectAtIndex:indexPath.row] valueForKey:@"full_name"];
    
    [cell.contentView addSubview:nameLbl];
    
    
    //Comment Label
    
    ResponsiveLabel *commentLbl = [[ResponsiveLabel alloc] init];
    
    commentLbl.numberOfLines = 999;
    
    commentLbl.font = [UIFont fontWithName:@"MuseoSansCyrl-500" size:14];
    
    commentLbl.textColor = [UIColor blackColor];
    
    commentLbl.text = [[commentArr objectAtIndex:indexPath.row] valueForKey:@"comment"];
    
    
    CGRect textRect = [commentLbl.text boundingRectWithSize:CGSizeMake([[UIScreen mainScreen] bounds].size.width-20, FLT_MAX)
                                            options:NSStringDrawingUsesLineFragmentOrigin
                                         attributes:@{NSFontAttributeName:[UIFont fontWithName:@"MuseoSansCyrl-500" size:14]}
                                            context:nil];
   
    commentLbl.frame = CGRectMake(60, nameLbl.frame.origin.y + nameLbl.frame.size.height+5, [[UIScreen mainScreen] bounds].size.width - 100, textRect.size.height);
    
    
    [cell.contentView addSubview:commentLbl];
    
    
    
    //Hastag detection
    
    commentLbl.userInteractionEnabled = YES;
    
    PatternTapResponder hashTagTapAction = ^(NSString *tappedString)
    {
        NSLog(@"HashTag Tapped = %@",tappedString);
    };
    
    [commentLbl enableHashTagDetectionWithAttributes:
     @{NSForegroundColorAttributeName:[UIColor blueColor], RLTapResponderAttributeName:hashTagTapAction}];
    
    //
    
    
    //Username detection
    
    commentLbl.userInteractionEnabled = YES;
    PatternTapResponder userNameDetection = ^(NSString *tappedString)
    {
        NSLog(@"HashTag Tapped = %@",tappedString);
    };
    
    [commentLbl enableUserHandleDetectionWithAttributes:@{NSForegroundColorAttributeName:[UIColor blueColor], RLTapResponderAttributeName:userNameDetection}];
    
    //
    
    
    //Comment like Button
    
    UIButton *commentLikeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    commentLikeBtn.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width - 60, 50, 50, 50);
    
    [commentLikeBtn setImage: [UIImage imageNamed:@""] forState:UIControlStateNormal];
    
    [cell.contentView addSubview:commentLikeBtn];
    
    //
    
    
    //time label
    
    UILabel *timeLbl = [[UILabel alloc] initWithFrame:CGRectMake(60, commentLbl.frame.origin.y + commentLbl.frame.size.height, 80, 20)];
    
    timeLbl.font = [UIFont fontWithName:@"MuseoSansCyrl-500" size:11];
    
    timeLbl.text = [NSString stringWithFormat:@"%@ Ago", [[commentArr objectAtIndex:indexPath.row] valueForKey:@"commenttime"] ];
    
    timeLbl.textColor = [UIColor darkGrayColor];
    
    nameLbl.text = [[commentArr objectAtIndex:indexPath.row] valueForKey:@"full_name"];
    
    [cell.contentView addSubview:timeLbl];
    
    
    //Reply Button
    
    UIButton *replyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [replyBtn setTitle:@"Reply" forState:UIControlStateNormal];
    
    replyBtn.titleLabel.font = [UIFont fontWithName:@"MuseoSansCyrl-500" size:13];
    
    [replyBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    
    replyBtn.tag = indexPath.row;
    
    [replyBtn addTarget:self action:@selector(replyBtnTap:) forControlEvents:UIControlEventTouchUpInside];
    
    replyBtn.frame = CGRectMake(timeLbl.frame.origin.x + timeLbl.frame.size.width, timeLbl.frame.origin.y, 80, 20);

    [cell.contentView addSubview:replyBtn];
    
    //
    
    
    //Total like Button
    
    UIButton *totalLikeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    totalLikeBtn.frame = CGRectMake(replyBtn.frame.origin.x + replyBtn.frame.size.width, timeLbl.frame.origin.y, 80, 20);
    
    [totalLikeBtn setImage: [UIImage imageNamed:@""] forState:UIControlStateNormal];
    
    [cell.contentView addSubview:totalLikeBtn];*/
    
    

    
    
    return cell;
}



#pragma mark - UITextViewDelegate Methods

- (BOOL)textView:(SLKTextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    return [super textView:textView shouldChangeTextInRange:range replacementText:text];
}


#pragma mark - SLKTEXT Send Button

- (void)didPressRightButton:(id)sender
{
    [self.view endEditing:YES];
    
    
    [self postComment:self.textView.text];
    
    [super didPressRightButton:sender];
}
@end
