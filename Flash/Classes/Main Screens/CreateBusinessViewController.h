//
//  CreateBusinessViewController.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 3/17/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SZTextView.h"

@interface CreateBusinessViewController : UIViewController
{
    IBOutlet UIImageView *profileImg;
    
    IBOutlet UITextField *fullNameTf, *emailTf, *userNameTf;
    
    IBOutlet SZTextView *descTv;
    
    IBOutlet UIView *contentView;
    
    IBOutlet UIButton *createAccBtn;
}

@property (nonatomic, retain) NSString *userId;

- (IBAction)createAccountTap;

- (IBAction)backTap;

- (IBAction)tapGestOnUserPic:(UITapGestureRecognizer *)sender;


@end
