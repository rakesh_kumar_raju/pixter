//
//  TagViewController.m
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 3/30/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//


#import "TagViewController.h"
#import "JDTagView.h"
#import "ServiceHelper.h"
#import "SerachTableViewCell.h"
#import <MapKit/MapKit.h>

@interface TagViewController ()<JDTagViewDelegate>
{
    JDTagView *tagview;
    
    NSMutableArray *peopleArr, *taggArr;
    
    NSString *xAxis, *yAxis;
    
    UIView *containerView;
    
    int tagId;
}

@end


@implementation TagViewController


#pragma mark - ViewCycle


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    peopleArr = [NSMutableArray new];
    
    taggArr = [NSMutableArray new];
    
    tagId = 1;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    
    UIImage *testImage = [[UIImage alloc] init];
    
    testImage = [self imageWithImage:self.image scaledToWidth:[[UIScreen mainScreen] bounds].size.width];
    
    
    containerView = [[UIView alloc] initWithFrame:CGRectMake(0, 70, testImage.size.width, testImage.size.height)];
    
    
    
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, containerView.frame.size.width, containerView.frame.size.height)];
    
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    
    imageView.userInteractionEnabled = YES;
    
    imageView.clipsToBounds = YES;
    
    imageView.image = self.image;
    
    [containerView addSubview:imageView];
    
    [self.view addSubview:containerView];
    
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped:)];
    
    [tapRecognizer setNumberOfTapsRequired:1];
    
    [containerView addGestureRecognizer:tapRecognizer];
    
    
    
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, (containerView.frame.origin.y + containerView.frame.size.height)+50, [[UIScreen mainScreen] bounds].size.width, 20)];
    
    //lbl.font = [UIFont fontWithName:@"MuseoSansCyrl-500" size:15];
    
    lbl.font = [UIFont systemFontOfSize:15];
    
    
    lbl.textColor = [UIColor lightGrayColor];
    
    lbl.textAlignment = NSTextAlignmentCenter;
    
    lbl.text = @"Tap the photo to tag people.";
    
    [self.view addSubview:lbl];
    
    
    [self setUpView];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
}


#pragma mark - Void


- (void)setUpView
{
    if ((APPDEL).tagPeopleArr.count)
    {
        for (int i = 0 ; i < (APPDEL).tagPeopleArr.count; i++)
        {
            int backEndHeight = containerView.frame.size.height;
            
            int backEndscreenWidth = containerView.frame.size.width;
            
            
            float scaleFator = [[UIScreen mainScreen] bounds].size.width/backEndscreenWidth;
            
            float actualHeight = scaleFator * backEndHeight;
            
            
            CGPoint location;
            
            location.x = [[[(APPDEL).tagPeopleArr objectAtIndex:i] objectForKey:@"tagX"] floatValue] *  [[UIScreen mainScreen]bounds].size.width;
            
            location.y = [[[(APPDEL).tagPeopleArr objectAtIndex:i] objectForKey:@"tagY"] floatValue] * actualHeight;
            
            
            tagview = [[JDTagView alloc] init];
            
            tagview.JDTagViewDelegate = self;
            
            tagview.tag = i+1;
            
            [tagview setUpTagViewForViewWithTagTitle:[[(APPDEL).tagPeopleArr objectAtIndex:i] objectForKey:@"userName"] WithViewPoints:location];
            
            [self.view addSubview: tagview];
        }
    }
}


- (void)sendTagPeople
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GetNewTagPeople" object:(APPDEL).tagPeopleArr];
}


#pragma mark - Get image size


- (UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width
{
    float oldWidth = sourceImage.size.width;
    
    float scaleFactor = i_width / oldWidth;
    
    
    float newHeight = sourceImage.size.height * scaleFactor;
    
    float newWidth = oldWidth * scaleFactor;
    
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return newImage;
}


#pragma mark - Action

- (IBAction)backTap
{
    if (self.ifEditPost == NO)
    {
         [(APPDEL).tagPeopleArr removeAllObjects];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)doneTap
{
     NSLog(@"%@", (APPDEL).tagPeopleArr);
    
    if (self.ifEditPost == YES)
    {
        [self performSelector:@selector(sendTagPeople) withObject:nil afterDelay:1];
    }
    
     [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma marh get touch point

- (void)tapped:(UITapGestureRecognizer *)touch
{
    CGPoint location = [touch locationInView:containerView];
    
    location.y += 70;
    
   
    xAxis = [NSString stringWithFormat:@"%f", location.x/[[UIScreen mainScreen] bounds].size.width];
    
    yAxis = [NSString stringWithFormat:@"%f", location.y/containerView.frame.size.height];
    
    
    tagview = [[JDTagView alloc] init];
    
    tagview.JDTagViewDelegate = self;
    
    tagview.tag = tagId;
    
    tagId ++;
    
    [tagview setUpTagViewForViewWithTagTitle:@"who's this?" WithViewPoints:location];
    
    [self.view addSubview: tagview];
    
    
    findPersonView.frame = CGRectMake(0, [[UIScreen mainScreen] bounds].size.height, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height-70);
    
    [self.view addSubview: findPersonView];
    
    [UIView animateWithDuration:0.2 animations:^
    {
        findPersonView.frame = CGRectMake(0, 70, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height-70);
    }];
}


#pragma mark - Webservice


- (void)findPeople:(NSString *)searchStr
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:searchStr, @"userName", [(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", @"0", @"typeFlag", nil];
    
    [[ServiceHelper sharedManager] setHudShow:NO];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"searchUser" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         peopleArr = [[result objectForKey:@"user"] mutableCopy];
         
         [peopleTbl reloadData];
     }];
}


#pragma mark - Searchbar delegate


- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self.view endEditing:YES];
    
    [tagview removeFromSuperview];
    
    [UIView animateWithDuration:0.2 animations:^{
        findPersonView.frame = CGRectMake(0,  [[UIScreen mainScreen] bounds].size.height, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height-70);
    }];
}


- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self findPeople:searchText];
}


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar1
{
    [self.view endEditing:YES];
    
    [self findPeople:searchBar1.text];
}

#pragma mark - TableView


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return peopleArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"CELL";
    
    SerachTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"SerachTableViewCell" owner:self options:nil] lastObject];
    }
    
    [cell.userImg sd_setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [[peopleArr objectAtIndex:indexPath.row] valueForKey:@"profile_pic"] ] ] placeholderImage:nil];
    
    cell.nameLbl.text = [[[peopleArr objectAtIndex:indexPath.row] valueForKey:@"full_name"] lowercaseString];
    
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    
    
    [tagview setTagActionTitle:[[peopleArr objectAtIndex:indexPath.row] valueForKey:@"full_name"]];
    
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:xAxis, @"tagX", yAxis, @"tagY", [[peopleArr objectAtIndex:indexPath.row] valueForKey:@"id"], @"userId", [[peopleArr objectAtIndex:indexPath.row] valueForKey:@"full_name"], @"userName", nil];
    
   
    [(APPDEL).tagPeopleArr addObject:dict];
    
    [UIView animateWithDuration:0.2 animations:^{
        findPersonView.frame = CGRectMake(0,  [[UIScreen mainScreen] bounds].size.height, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height-70);
    }];
}


#pragma mark - JDTagview Delegate


- (void)deleteTagFromView:(JDTagView *)JDTagView
{
    NSLog(@"%li", (long)JDTagView.tag);
    
    tagId --;

    [(APPDEL).tagPeopleArr removeObjectAtIndex:JDTagView.tag - 1];
    
    [(APPDEL).tagPeopleArr insertObject:[NSDictionary new] atIndex:JDTagView.tag - 1];//just for Logic
    
    if (tagId == 1)
    {
        [(APPDEL).tagPeopleArr removeAllObjects];
    }
    
    NSLog(@"%@", (APPDEL).tagPeopleArr);
}


- (void)selectedTag:(JDTagView *)View
{
    
}

@end


