//
//  EditPostViewController.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 4/6/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SZTextView.h"

@interface EditPostViewController : UIViewController
{
    IBOutlet UIImageView *profileImg, *postImg;
    
    IBOutlet UILabel *nameLbl, *locationLbl, *tagPeopleLbl;;
    
    IBOutlet SZTextView *captionTv;
    
    IBOutlet UIButton *tagBtn, *locationBtn;
}

@property (nonatomic, retain) NSDictionary *postDict;

@property (nonatomic, assign) BOOL isEdit;


- (IBAction)backTap;

- (IBAction)doneTap;

- (IBAction)tagBtnTap;

- (IBAction)locationBtnTap;

- (IBAction)tapGestOnImage:(UITapGestureRecognizer *)sender;

@end
