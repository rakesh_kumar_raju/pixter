//
//  AddCommentViewController.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 2/15/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SLKTextViewController.h"
#import "AppDelegate.h"


@interface AddCommentViewController : SLKTextViewController
{
    //IBOutlet UITableView *commentTbl;
    
     UIRefreshControl *refreshControl;
}

@property (nonatomic, retain) NSString *postId, *secondryId;


- (IBAction)backTap;

@end
