//
//  TermOfServiceViewController.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 4/4/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TermOfServiceViewController : UIViewController
{
    IBOutlet UILabel *titleLbl;
    
    IBOutlet UITextView *textView;
}

@property(nonatomic, assign) BOOL isTermOfService;

- (IBAction)backTap;

@end
