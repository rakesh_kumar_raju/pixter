//
//  PushnotificationViewController.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 5/4/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PushnotificationViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UITableView *notificationTbl;
}

- (IBAction)backTap;

@end
