//
//  ShowPostsViewController.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 3/9/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowPostsViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource>
{
    IBOutlet UICollectionView *postCollectinView;
    
    IBOutlet UIView *unfollowView, *popupView;
    
    IBOutlet UIImageView *unfollowUserImg;
    
    IBOutlet UILabel *unfollowUserNameLbl;
}

@property (nonatomic, retain) NSMutableArray *postArr;

@property (nonatomic, retain) NSIndexPath *indexPath;

@property (nonatomic, retain) NSString *differentUserId;


- (IBAction)backTap;

- (IBAction)unfollowCancelTap;

- (IBAction)unfollowTap;

@end
