//
//  TermOfServiceViewController.m
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 4/4/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import "TermOfServiceViewController.h"

@interface TermOfServiceViewController ()

@end


@implementation TermOfServiceViewController


#pragma mark - ViewCycle


-(void)viewDidLoad
{
    [super viewDidLoad];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    [self setUpView];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
}


#pragma mark - Void


- (void)setUpView
{
    NSString *contentStr = @"";
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Strings" ofType:@"plist"];
    
    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:path];
   
    if (self.isTermOfService == YES)
    {
        contentStr = [dict objectForKey:@"Terms"];
        
        titleLbl.text = @"Terms Of Service";
    }
    else
    {
        contentStr = [dict objectForKey:@"Privacy"];
        
        titleLbl.text = @"Privacy Policy";
    }
   
    textView.text = contentStr;
    
    textView.textColor = [UIColor whiteColor];
    
     [textView setContentOffset:CGPointZero animated:NO];
}


#pragma mark - Action

- (IBAction)backTap
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

@end
