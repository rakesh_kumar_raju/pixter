//
//  ViewLikesViewController.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 2/20/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewLikesViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UITableView *likesTbl;
    
     UIRefreshControl *refreshControl;
    
    IBOutlet UILabel *titleLbl;
    
    
    IBOutlet UIView *unfollowView, *popupView;
    
    IBOutlet UIImageView *unfollowUserImg;
    
    IBOutlet UILabel *unfollowUserNameLbl;
}

@property (nonatomic, retain) NSString *postId;

@property (nonatomic, retain) NSString *switchTitleStr;


- (IBAction)backTap;

- (IBAction)unfollowCancelTap;

- (IBAction)unfollowTap;


@end
