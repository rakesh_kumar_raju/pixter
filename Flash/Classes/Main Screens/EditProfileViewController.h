//
//  EditProfileViewController.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 2/14/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServiceHelper.h"
#import "UIImageView+WebCache.h"

@interface EditProfileViewController : UIViewController <UIPickerViewDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate, UITextViewDelegate, UITextFieldDelegate>
{
    IBOutlet UITextField *nameTf, *userNameTf, *phoneTf, *emailTf, *cityTf, *stateTf, *birthdayTf, *genderTf, *refferalNameTf;
    
    IBOutlet UITextView *bioTv;
    
    
    IBOutlet UIPickerView *picker;
    
    IBOutlet UIDatePicker *datePicker;
    
    IBOutlet UITapGestureRecognizer *tapGest;
    
    IBOutlet UIImageView *profileImg, *descriptionImg;
    
    IBOutlet UIView *accessoryView;
    
    IBOutlet UILabel *accessoryLbl, *changePhotoLbl, *privateInfoTitleLbl;
    
    IBOutlet UIButton *accessoryDoneBtn;
    
    IBOutlet NSLayoutConstraint *privateViewHeight, *biotvHeight;
}

- (IBAction)tapGestSender:(UITapGestureRecognizer *)sender;

- (IBAction)doneTap;

- (IBAction)backTap;

- (IBAction)accessoryDoneTap;

@end
