//
//  ShowAllUserViewController.m
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 3/3/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import "ShowAllUserViewController.h"
#import "SendMessageViewController.h"
#import "SerachTableViewCell.h"


@interface ShowAllUserViewController ()
{
    NSMutableArray *userArr, *helperArr;
    
    BOOL isFilter;
}

@end


@implementation ShowAllUserViewController


#pragma mark - ViewCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    self.navigationController.navigationBar.hidden = YES;
    
    
    helperArr = [NSMutableArray new];
    
    userArr = [NSMutableArray new];
    
    
    [self setUpView];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
}


#pragma mark - Void

- (void) setUpView
{
    isFilter = NO;
    
    if (!self.titleStr.length)
    {
        titleLbl.text = @"New Message";
        
    }
    
    [searchBar performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:1];
    
    UIView *view = [searchBar.subviews objectAtIndex:0];
    
    for(UIView *subView in view.subviews)
    {
        if([subView isKindOfClass: [UITextField class]])
        {
            [(UITextField *)subView setKeyboardAppearance: UIKeyboardAppearanceDark];
            
            
            UITextField *searchField = (UITextField *)subView;
            
            searchField.backgroundColor = [UIColor grayColor];
            
            searchField.textColor = [UIColor whiteColor];
            
            searchField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Search"];
            
            UILabel *placeholderLabel = [searchField valueForKey:@"placeholderLabel"];
            
            placeholderLabel.textColor = [UIColor darkGrayColor];
        }
    }
}


#pragma mark - Webservice


- (void)fetchFolowerOrFolowing:(NSString *)searchKey
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:searchKey, @"userName", [(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", @"0", @"typeFlag", nil];
    
    [[ServiceHelper sharedManager] setHudShow:NO];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"searchUser" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {

         
         userArr = [[result objectForKey:@"user"] mutableCopy];
         
         helperArr = [[result objectForKey:@"user"] mutableCopy];
         
         [userTbl reloadData];
     }];
}



#pragma mark - Action

- (IBAction)backTap
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - TableView Delegate


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return userArr.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"CELL";
    
    SerachTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"SerachTableViewCell" owner:self options:nil] lastObject];
    }

    
    cell.selectionStyle = UITableViewCellEditingStyleNone;
    
    
    [cell.userImg sd_setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [[userArr objectAtIndex:indexPath.row] valueForKey:@"profile_pic"] ] ] placeholderImage:[UIImage imageNamed:@"profile_demo"]];
    
    cell.nameLbl.text = [[[userArr objectAtIndex:indexPath.row] valueForKey:@"full_name"] lowercaseString];
    
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SendMessageViewController *vc = [[SendMessageViewController alloc] initWithNibName:@"SendMessageViewController" bundle:nil];
    
    vc.userNameStr = [[[userArr objectAtIndex:indexPath.row] valueForKey:@"full_name"] lowercaseString];
    
    vc.userIdStr = [[userArr objectAtIndex:indexPath.row] valueForKey:@"id"];
    
    vc.profileUrlStr = [[userArr objectAtIndex:indexPath.row] valueForKey:@"profile_pic"];
    
    [self.navigationController pushViewController:vc animated:YES];
}


#pragma mark - Searchbar delegate


- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self.view endEditing:YES];
}


- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    /*NSPredicate *predicateString = [NSPredicate predicateWithFormat:@"full_name contains[cd] %@", searchText];
    
    
    NSArray *filteredArray = [NSMutableArray arrayWithArray:[userArr filteredArrayUsingPredicate:predicateString]];
 
    if (filteredArray.count)
    {
        userArr = [[NSArray arrayWithArray:filteredArray] mutableCopy];
    }
    else
    {
        userArr = [[NSArray arrayWithArray:helperArr] mutableCopy];
    }
    
    [userTbl reloadData];*/
    
    [self fetchFolowerOrFolowing:searchText];
}


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar1
{
    [self.view endEditing:YES];
}

@end
