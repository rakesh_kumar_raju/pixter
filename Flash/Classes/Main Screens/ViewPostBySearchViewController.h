//
//  ViewSinglePostViewController.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 3/10/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "ServiceHelper.h"


@interface ViewPostBySearchViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource>
{
    IBOutlet UILabel *titleLbl;
    
    IBOutlet UICollectionView *postColectionView;
    
    UIRefreshControl *refreshControl;
}

@property (nonatomic, assign) BOOL isSeacrhTagPeople;

@property (nonatomic, retain) NSString *searchStr;


- (IBAction)backTap;


@end
