//
//  HelpViewViewController.m
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 7/26/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import "HelpViewViewController.h"
#import "ServiceHelper.h"


@interface HelpViewViewController ()

@end

@implementation HelpViewViewController


#pragma mark - ViewCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setUpView];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
  
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
}


#pragma mark - Void

- (void)setUpView
{
    [helpWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://52.43.13.44/welcome"]]];
}


#pragma mark - Action

- (IBAction)backTap
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - WebView Delegate


- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [[ServiceHelper sharedManager] showHud];
}


- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [[ServiceHelper sharedManager] dismisHud];
}


- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [[ServiceHelper sharedManager] showHud];
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Error!"
                                          message:@"Something went wrong on server. Please try again later."
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction *oKBtn = [UIAlertAction
                            actionWithTitle:@"OK"
                            style:UIAlertActionStyleDefault
                            handler:^(UIAlertAction *action)
                            {
                              
                            }];
    
   
    [alertController addAction:oKBtn];
    
    
    
    [self presentViewController:alertController animated:YES completion:nil];
}


@end
