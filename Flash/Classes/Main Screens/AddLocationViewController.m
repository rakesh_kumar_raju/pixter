//
//  PeopleViewController.m
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 3/31/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#define APIKEY @"AIzaSyDf7058ZhztKEbda1br_fpW5k4P_FNRCsA"

#import "AddLocationViewController.h"
#import <MapKit/MapKit.h>
#import "ServiceHelper.h"

@interface AddLocationViewController ()
{
    NSMutableArray *locationarr;
}

@end

@implementation AddLocationViewController


#pragma mark - ViewCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    locationarr = [NSMutableArray new];
    
    indicator.hidden = YES;
    
    [self setUpView];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES]; 
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
}


#pragma mark - Void


- (void)setUpView
{
   UIView *view = [searchBar.subviews objectAtIndex:0];
    
    for(UIView *subView in view.subviews)
    {
        if([subView isKindOfClass: [UITextField class]])
        {
            [(UITextField *)subView setKeyboardAppearance: UIKeyboardAppearanceDark];
            
            
            UITextField *searchField = (UITextField *)subView;
            
            searchField.backgroundColor = [UIColor grayColor];
            
            searchField.textColor = [UIColor whiteColor];
            
            searchField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Search Location"];
            
            UILabel *placeholderLabel = [searchField valueForKey:@"placeholderLabel"];
            
            placeholderLabel.textColor = [UIColor darkGrayColor];
        }
    }
}


#pragma mark - Action

- (IBAction)backTap
{
    (APPDEL).locationStr = @"";
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Searchbar delegate


- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    indicator.hidden = NO;
    
    [indicator startAnimating];
    
    CLLocationDegrees lat = [[(APPDEL).currentLocation objectForKey:@"lattitude"] floatValue];
    CLLocationDegrees lon = [[(APPDEL).currentLocation objectForKey:@"longitude"] floatValue];
    
    /*MKLocalSearchRequest *request = [[MKLocalSearchRequest alloc] init];
    
    request.naturalLanguageQuery = searchText;

    request.region = MKCoordinateRegionMake(CLLocationCoordinate2DMake(lat, lon), MKCoordinateSpanMake(.1, .1));
    
    MKLocalSearch *search = [[MKLocalSearch alloc] initWithRequest:request];
    
    [search startWithCompletionHandler:^(MKLocalSearchResponse *response, NSError *error)
     {
         indicator.hidden = YES;
         
         [indicator stopAnimating];
         
         NSLog(@"Map Items: %@", response.mapItems);
         
         locationarr = [[NSMutableArray alloc] initWithArray:response.mapItems];
         
         [locationTbl reloadData];
     }];*/
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    //AIzaSyCIXhFeFjCNZIybAyyDj7cVY_wCUYxEddo Xpressor
    //AIzaSyC3eWGBuN-1wwsUftV0CiMtvK09umlcABw Pixster
    //AIzaSyBoycrTcChgElPwCqCGophElkNjEqr3tn8 client pixster
    
    //https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&key=%@&location=%f,%f&radius=1000
    
    //https://maps.googleapis.com/maps/api/place/textsearch/json?query=%@&location=%f,%f&radius=%i&key=AIzaSyBoycrTcChgElPwCqCGophElkNjEqr3tn8
    
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&location=%f,%f&radius=1000&key=AIzaSyBoycrTcChgElPwCqCGophElkNjEqr3tn8", [searchBar.text stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding], lat, lon]]];
    
    
   //maps.googleapis.com/maps/api/place/textsearch/json?query=%@&key=APIKEY
    [request setHTTPMethod:@"POST"];
    
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError)
     {
         NSError *error1;
         
         indicator.hidden = YES;
         
         [indicator stopAnimating];
         
         
         if (data)
         {
             NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error1];
           
             //locationarr = [[NSMutableArray alloc] initWithArray:[dict objectForKey:@"results"]];
             
             locationarr = [[NSMutableArray alloc] initWithArray:[dict objectForKey:@"predictions"]];
             
             [locationTbl reloadData];
         }
     }];
    

}



#pragma mark - TableView
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return locationarr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"CELLID";
    
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.backgroundColor = [UIColor blackColor];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
    
    view.layer.cornerRadius = 30;
    
    view.clipsToBounds = YES;
    
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
    
    imgView.image = [UIImage imageNamed:@""];
    
    [view addSubview: imgView];
    
    
    [cell.contentView addSubview:view];
    
    /*MKMapItem *mapItem = [locationarr objectAtIndex:indexPath.row];
    
    
    cell.textLabel.text = mapItem.name;
    
    cell.textLabel.textColor = [UIColor whiteColor];
    
    NSDictionary *address = mapItem.placemark.addressDictionary;
    
    
    NSString *street = [address objectForKey:@"Street"];
    
    NSString *city = [address objectForKey:@"City"];
    
    NSString *state = [address objectForKey:@"State"];
    
    NSString *country = [address objectForKey:@"Country"];
    
    NSString *countryCode = [address objectForKey:@"CountryCode"];
    
    
    street = [address objectForKey:@"Street"] ? [address objectForKey:@"Street"] : @"";
    
    state = [address objectForKey:@"State"] ? [address objectForKey:@"State"] : @"";
    
    city = [address objectForKey:@"City"] ? [address objectForKey:@"City"] : @"";
    
    country = [address objectForKey:@"Country"] ? [address objectForKey:@"Country"] : @"";
    
    countryCode = [address objectForKey:@"CountryCode"] ? [address objectForKey:@"CountryCode"] : @"";
    
    
    cell.detailTextLabel.textColor = [UIColor whiteColor];
     
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ %@ %@ %@ %@", street, state, city, country, countryCode];*/
    
    //cell.textLabel.text = [[locationarr objectAtIndex:indexPath.row] objectForKey:@"name"];
    
    
    cell.textLabel.text = [[[locationarr objectAtIndex:indexPath.row] objectForKey:@"structured_formatting"] objectForKey:@"main_text"];
    
    
    cell.textLabel.textColor = [UIColor whiteColor];
    
    
    //cell.detailTextLabel.text = [[locationarr objectAtIndex:indexPath.row] objectForKey:@"formatted_address"];
    
    
    cell.detailTextLabel.text = [[locationarr objectAtIndex:indexPath.row] objectForKey:@"description"];
    
    
    cell.detailTextLabel.textColor = [UIColor whiteColor];
    
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    MKMapItem *mapItem = [locationarr objectAtIndex:indexPath.row];
//    
//    (APPDEL).locationStr = mapItem.name;
// 
//    [self dismissViewControllerAnimated:YES completion:nil];
    
    (APPDEL).locationStr = [[[locationarr objectAtIndex:indexPath.row] objectForKey:@"structured_formatting"] objectForKey:@"main_text"];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SetNewLocation" object: (APPDEL).locationStr];
}

@end
