//
//  ShowPostsViewController.m
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 3/9/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#define RestOfElementSize_insted_of_imageSize  140

#define hashtag_color [UIColor colorWithRed:18.0/255.0 green:86.0/255.0 blue:136.0/255.0 alpha:1.0]


#import <MessageUI/MessageUI.h>


#import "ShowPostsViewController.h"
#import "ListCollectionViewCell.h"
#import "ListCollectionViewVideoCell.h"
#import "AddCommentViewController.h"
#import "ViewLikesViewController.h"
#import "ProfileViewController.h"
#import "EditPostViewController.h"
#import "ViewPostBySearchViewController.h"
#import "ServiceHelper.h"
#import "JDTagView.h"

#import "ZOWVideoView.h"
#import "InstagramVideoView.h"


@interface ShowPostsViewController () <UIActionSheetDelegate, MFMailComposeViewControllerDelegate, JDTagViewDelegate>
{
    NSIndexPath *index, *playVideoAtIndex;
    
    BOOL isScrolling;
  
    NSMutableArray *videoPlayerObj;
}

@end


@implementation ShowPostsViewController


#pragma mark - ViewCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)])
//    {
//        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
//    }
    
    self.navigationController.navigationBar.hidden = YES;
    
    [self setUpView];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
     self.navigationController.navigationBar.hidden = YES;
    
    NSLog(@"%@", (APPDEL).userDetailDict);
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    
    if ((APPDEL).AvplayerObj.count)
    {
        for (AVPlayer *player in (APPDEL).AvplayerObj)
        {
            NSLog(@"%@", player);
            
            [player pause];
        }
    }
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
}


#pragma mark - Void

- (void) setUpView
{
    videoPlayerObj = [NSMutableArray new];
    
    [postCollectinView  registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"imageCell"];

//    [postCollectinView registerNib:[UINib nibWithNibName:@"ListCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"listCollection"];
//    
//    [postCollectinView registerNib:[UINib nibWithNibName:@"ListCollectionViewVideoCell" bundle:nil] forCellWithReuseIdentifier:@"listCollectionVideo"];
    
    [self performSelector:@selector(itemScrollATIndex) withObject:nil afterDelay:0];
}


- (void)changeFollowStatus
{
    NSMutableDictionary *dict = [[self.postArr objectAtIndex:index.row] mutableCopy];
    
    [self.postArr removeObjectAtIndex:index.row];
    
    
    if ([[dict objectForKey:@"youfollowing"] intValue] == 0)
    {
        [dict setObject:@"1" forKey:@"youfollowing"];
        
        [self.postArr insertObject:dict atIndex:index.row];
        
        [self followUser:[dict valueForKey:@"userId"]];
    }
    else
    {
        [dict setObject:@"0" forKey:@"youfollowing"];
        
        [self.postArr insertObject:dict atIndex:index.row];
        
         [self unfollowUser:[dict valueForKey:@"userId"]];
    }
    
    
    
    
   // [postCollectinView performSelector:@selector(reloadItemsAtIndexPaths:) withObject:@[index] afterDelay:0.2];
}


#pragma mark - Clear all video viewFrom Cell

- (void)clearvideoViews
{
    for (InstagramVideoView *videoView in videoPlayerObj)
    {
        [[NSNotificationCenter defaultCenter] removeObserver:videoView];
        
        [videoView pause];
    }
}

#pragma mark - Get image size


- (UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width
{
    float oldWidth = sourceImage.size.width;
    
    float scaleFactor = i_width / oldWidth;
    
    
    float newHeight = sourceImage.size.height * scaleFactor;
    
    float newWidth = oldWidth * scaleFactor;
    
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return newImage;
}



#pragma mark - Add tag button on Cell


/*- (void)addTagBtnOnCell: (ListCollectionViewCell *)cell personTagArr:(NSArray *)personTagArr
{
    if (personTagArr.count)
    {
        cell.tagBtn.hidden = NO;
        
        cell.tagBtn.alpha = 1;
        
        [self performSelector:@selector(hideTagBtn:) withObject:cell afterDelay:2];
        
        if (cell.subviews.count > 1)
        {
            for (UIView *view in cell.subviews)
            {
                if ([view isKindOfClass:[JDTagView class]])
                {
                    [view removeFromSuperview];
                }
            }
        }
        else
        {
            for (int i = 0 ; i < personTagArr.count; i++)
            {
                CGPoint location;
                
                location.x = [[[personTagArr objectAtIndex:i] objectForKey:@"tagX"] floatValue];
                
                location.y = [[[personTagArr objectAtIndex:i] objectForKey:@"tagY"] floatValue];
                
                
                JDTagView *tagview = [[JDTagView alloc] init];
             
                tagview.JDTagViewDelegate = self;
                
                tagview.tag = i+1;
                
                [tagview setUpTagViewForViewWithTagTitle:[[personTagArr objectAtIndex:i] objectForKey:@"userName"] WithViewPoints:location];
                
                [cell addSubview: tagview];
                
                tagview.delBtn.hidden = YES;
            }
        }
    }
    else
    {
        cell.tagBtn.hidden = YES;
    }
}*/


- (void)addTagBtnOnCell: (UICollectionViewCell *)cell sender:(UIButton *)sender personTagArr:(NSArray *)personTagArr
{
    if (personTagArr.count)
    {
        sender.hidden = NO;
        
        sender.alpha = 1;
        
        [self performSelector:@selector(hideTagBtn:) withObject:sender afterDelay:2];
        
        if (cell.subviews.count > 1)
        {
            for (UIView *view in cell.subviews)
            {
                if ([view isKindOfClass:[JDTagView class]])
                {
                    [view removeFromSuperview];
                }
            }
        }
        else
        {
            for (int i = 0 ; i < personTagArr.count; i++)
            {
                int backEndHeight = [[[self.postArr objectAtIndex:sender.tag] valueForKey:@"height"] intValue];
                
                int backEndscreenWidth = [[[self.postArr objectAtIndex:sender.tag] valueForKey:@"width"] intValue];
                
                
                float scaleFator = [[UIScreen mainScreen] bounds].size.width/backEndscreenWidth;
                
                float actualHeight = scaleFator * backEndHeight;
                
                
                CGPoint location;
                
                location.x =  [[[personTagArr objectAtIndex:i] objectForKey:@"tagX"] floatValue] * [[UIScreen mainScreen]bounds].size.width;
                
                location.y = [[[personTagArr objectAtIndex:i] objectForKey:@"tagY"] floatValue] * actualHeight;
                
                
                JDTagView *tagview = [[JDTagView alloc] init];
                
                tagview.JDTagViewDelegate = self;
                
                tagview.tag = i+1;
                
                [tagview setUpTagViewForViewWithTagTitle:[[personTagArr objectAtIndex:i] objectForKey:@"userName"] WithViewPoints:location];
                
                [cell addSubview: tagview];
                
                tagview.delBtn.hidden = YES;
            }
        }
    }
    else
    {
        sender.hidden = YES;
    }
}


#pragma mark - Webservice

- (void)viewUsersPostsWebserviceService:(BOOL)showHud
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userId",[[NSUserDefaults standardUserDefaults] objectForKey:@"device_token"], @"device_token", nil];
    
    
    [[ServiceHelper sharedManager] setHudShow:showHud];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"viewPost" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
       
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 self.postArr = [[NSMutableArray alloc] initWithArray:[result objectForKey:@"posts"]];
                 
                 [postCollectinView reloadData];
             }
             
         }
         
     }];
}



- (void)likePost:(NSDictionary *)params index:(NSInteger)tag
{
    [[ServiceHelper sharedManager] setHudShow:NO];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"likePost" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 
                 
             }
             
             else
             {
                 NSMutableDictionary *dict = [[self.postArr objectAtIndex:tag] mutableCopy];
                 
                 [self.postArr removeObjectAtIndex:tag];
                 
                 
                 [dict setObject:[NSString stringWithFormat:@"%i", [[result objectForKey:@"totallike"] intValue]-1] forKey:@"totallike"];
                 
                 
                 if ([[dict objectForKey:@"youlike"] intValue] == 0)
                 {
                     [dict setObject:@"1" forKey:@"youlike"];
                 }
                 else
                 {
                     [dict setObject:@"0" forKey:@"youlike"];
                 }
                 
                 [self.postArr insertObject:dict atIndex:(int)tag];
                 
                 [postCollectinView reloadData];
             }
         }
         
     }];
    
}


- (void)favPost:(NSString *)postId index:(NSInteger)tag
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", postId, @"postId", nil];
    
    
    [[ServiceHelper sharedManager] setHudShow:NO];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"addFavourite" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 
                 
             }
             
             else
             {
                 NSMutableDictionary *dict = [[self.postArr objectAtIndex:tag] mutableCopy];
                 
                 [self.postArr removeObjectAtIndex:tag];
                 
                 
                 if ([[dict objectForKey:@"favourite"] intValue] == 0)
                 {
                     [dict setObject:@"1" forKey:@"favourite"];
                 }
                 else
                 {
                     [dict setObject:@"0" forKey:@"favourite"];
                 }
                 
                 [self.postArr insertObject:dict atIndex:(int)tag];
                 
                 [postCollectinView reloadData];
             }
         }
         
     }];
    
}


- (void)getUserIdFromUsername:(NSDictionary *)params
{
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"getByUser" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 ProfileViewController *vc = [[ProfileViewController alloc] initWithNibName:@"ProfileViewController" bundle:nil];
                 
                 vc.differentUserId = [result objectForKey:@"id"] ;
                 
                 [self.navigationController pushViewController:vc animated:YES];
             }
             
             else
             {
                 
             }
         }
     }];
    
}


#pragma mark - Collection scroll at index path


- (void)itemScrollATIndex
{
    [postCollectinView scrollToItemAtIndexPath:self.indexPath atScrollPosition:UICollectionViewScrollPositionTop animated:NO];
}


#pragma mark - Set collectinView height

- (CGSize)setCollectionViewHeight:(NSDictionary *)dict indexPath:(NSIndexPath *)indexPath
{
    int backEndHeight = [[[self.postArr objectAtIndex:indexPath.row] valueForKey:@"height"] intValue];
    
    int backEndscreenWidth = [[[self.postArr objectAtIndex:indexPath.row] valueForKey:@"width"] intValue];
    
    
    float scaleFator = [[UIScreen mainScreen] bounds].size.width/backEndscreenWidth;
    
    
    float actualHeight = scaleFator * backEndHeight;
    
    
    
    int imageCellHeight = actualHeight + RestOfElementSize_insted_of_imageSize;
    
    
    
    if ([[[self.postArr objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"image"])
    {
        imageCellHeight = actualHeight + RestOfElementSize_insted_of_imageSize;
    }
    
    else
    {
        // 350 is video view height
        
        imageCellHeight = 350 + RestOfElementSize_insted_of_imageSize;
    }

    
    
    NSString *dataStr;
    
    CGSize cellHeight;
    
    CGSize maximumLabelSize;
    
    
    if (![[dict objectForKey:@"caption"] isEqualToString:@""])
    {
        dataStr = [NSString stringWithFormat:@"%@ %@", [dict objectForKey:@"fullname"], [dict objectForKey:@"caption"]];
        
    }
    

    /*NSArray *commentArr = [[self.postArr objectAtIndex:indexPath.row] objectForKey:@"allComment"];
    
    if ([commentArr count])
    {
        NSMutableString *commentStr = [@"" mutableCopy];
        
        for (int i = 0; i < commentArr.count; i++)
        {
            [commentStr appendFormat:@"%@\n", [NSString stringWithFormat:@"%@ %@", [[commentArr objectAtIndex:i] objectForKey:@"fullname"], [[commentArr objectAtIndex:i] objectForKey:@"comment"]]];
        }
        
        dataStr = [NSString stringWithFormat:@"%@\nView all comments\n\n%@", dataStr, commentStr];
    }*/
    
    
    //Set label width & cellHeight
   
    if ([[UIScreen mainScreen] bounds].size.width == 320)
    {
        maximumLabelSize = CGSizeMake(300, FLT_MAX);
        
        if ([[dict valueForKey:@"type"] isEqualToString:@"image"])
        {
            cellHeight = CGSizeMake(320, imageCellHeight);
        }
        else
        {
            cellHeight = CGSizeMake(320, imageCellHeight);
        }
    }
    else if ([[UIScreen mainScreen] bounds].size.width == 375)
    {
        maximumLabelSize = CGSizeMake(355, FLT_MAX);
        
        if ([[dict valueForKey:@"type"] isEqualToString:@"image"])
        {
            cellHeight = CGSizeMake(375, imageCellHeight);
        }
        
        else
        {
            cellHeight = CGSizeMake(375, imageCellHeight);
        }
    }
    else
    {
        maximumLabelSize = CGSizeMake(394, FLT_MAX);
        
        if ([[dict valueForKey:@"type"] isEqualToString:@"image"])
        {
            cellHeight = CGSizeMake(414, imageCellHeight);
        }
        else
        {
            cellHeight = CGSizeMake(414, imageCellHeight);
        }
    }
    
    
    CGRect textRect = [dataStr boundingRectWithSize:maximumLabelSize
                                            options:NSStringDrawingUsesLineFragmentOrigin
                                         attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]}
                                            context:nil];

   // NSLog(@"%f %@", textRect.size.height, dict);
    
    cellHeight = CGSizeMake(cellHeight.width, cellHeight.height + textRect.size.height+25);
    
    return cellHeight;
}


#pragma mark - Bold perticular string

- (NSMutableAttributedString *)setBoldString:(NSString *)fullstring boldString:(NSString *)boldString
{
    NSMutableAttributedString *yourAttributedString = [[NSMutableAttributedString alloc] initWithString:fullstring];
   
    NSRange boldRange = [fullstring rangeOfString:boldString];
    
    [yourAttributedString addAttribute: NSFontAttributeName value:[UIFont fontWithName:@"MuseoSansCyrl-500" size:14] range:boldRange];
   
    [yourAttributedString addAttribute: NSForegroundColorAttributeName value:[UIColor blackColor] range:boldRange];
    
    return  yourAttributedString;
}


#pragma mark - Animation on button

- (void)animateButton:(UIButton *)sender
{
    sender.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    
    [UIView animateWithDuration:0.3/1.5 animations:^{
        
        sender.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
    }
                     completion:^(BOOL finished)
     {
         [UIView animateWithDuration:0.3/2 animations:^{
             
             sender.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
         }
                          completion:^(BOOL finished)
          {
              [UIView animateWithDuration:0.3/2 animations:^{
                  
                  sender.transform = CGAffineTransformIdentity;
              }];
          }];
     }];
}



#pragma mark - Action


- (IBAction)backTap
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)unfollowCancelTap
{
    [unfollowView removeFromSuperview];
}


- (IBAction)unfollowTap
{
    [self changeFollowStatus];
    
    [unfollowView removeFromSuperview];
}


#pragma mark - Dot button action


- (void)postShareOnFacebookShare:(UIImage *)image
{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
        SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        [mySLComposerSheet addImage:image];
        
    
        [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result)
        {
            
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    NSLog(@"Post Canceled");
                    break;
                case SLComposeViewControllerResultDone:
                    NSLog(@"Post Sucessful");
                    break;
                    
                default:
                    break;
            }
        }];
        
        [self presentViewController:mySLComposerSheet animated:YES completion:nil];
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:@"Oops!" message:@"Please login your Facebook account in your device" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
}


- (void)postShareOnMailShare:(NSString *)imagePath
{
    if ([MFMailComposeViewController canSendMail])
    {
        NSString *mimeType = @"image/jpeg";
       
        
        
        MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
       
        mail.mailComposeDelegate = self;
        
        [mail setSubject:@"Check out this"];
        
        [mail setMessageBody:@"Please download the Pister app for more activity." isHTML:NO];
        
        [mail addAttachmentData:[NSData dataWithContentsOfFile:imagePath] mimeType:mimeType fileName:@"Pixster"];
        
        
       
        [self presentViewController:mail animated:YES completion:NULL];
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:@"Oops!" message:@"This device cannot send email" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];

    }
}


- (void)reportAbuse:(NSString *)postId secondryId:(NSString *)secondryId isInappropiate: (NSString *)type
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", postId, @"postId", type, @"flag", secondryId, @"secondaryId", nil];
    
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"postReportAbuse" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 
                 
             }
             
             else
             {
                 
             }
         }
         
     }];
}


- (void)deletePost:(NSInteger)path postId:(NSString *)postId
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", postId, @"postId", nil];
    
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"deletePost" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 [self.postArr removeObjectAtIndex:path];
                 
                 if (self.postArr.count)
                 {
                     [postCollectinView reloadData];
                 }
                 
                 else
                 {
                     [self.navigationController popViewControllerAnimated:YES];
                    
                     [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadTableAfterDeletePost" object:nil];
                 }
             }
             
             else
             {
                 
             }
         }
     }];
}


- (void)turnOffCommenting:(NSInteger) path params:(NSDictionary *)params
{
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"setCommenting" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 NSMutableDictionary *dict = [[self.postArr objectAtIndex:path] mutableCopy];
              
                 //NSSingleObjectArray is just a special case of NSArray for arrays with a single element; happens quite a lot and having a special, smaller object for that case saves space.
                 
                 //For handling __NSSingleObjectArrayI (if array has one object)
                 
                 if (self.postArr.count == 1)
                 {
                     [dict setObject:[result objectForKey:@"commentflag"] forKey:@"commentflag"];
                     
                     self.postArr = [[NSMutableArray alloc] initWithObjects:dict, nil];
                 }
                 else
                 {
                     [self.postArr removeObjectAtIndex:path];
                     
                     [dict setObject:[result objectForKey:@"commentflag"] forKey:@"commentflag"];
                     
                     [self.postArr insertObject:dict atIndex:path];
                 }
                 
                 [postCollectinView reloadData];
             }
             
             else
             {
                 
             }
         }
     }];
}


#pragma mark - Collection Action

- (void)tapOnProfileImage:(UIButton *)sender
{
    ProfileViewController *vc = [[ProfileViewController alloc] initWithNibName:@"ProfileViewController" bundle:nil];
    
    vc.differentUserId = [[self.postArr objectAtIndex: sender.tag] objectForKey:@"userId"] ;
    
    [self.navigationController pushViewController:vc animated:YES];
}


- (void)likeTap:(UIButton *)sender
{
    [self animateButton:sender];
    
    
    NSMutableDictionary *dict = [[self.postArr objectAtIndex:sender.tag] mutableCopy];
    
    [self.postArr removeObjectAtIndex:sender.tag];
    
    
    if ([[dict objectForKey:@"youlike"] intValue] == 0)
    {
        [dict setObject:@"1" forKey:@"youlike"];
        
        [dict setObject:[NSString stringWithFormat:@"%i", [[dict objectForKey:@"totallike"] intValue]+1 ] forKey:@"totallike"];
    }
    else
    {
        [dict setObject:@"0" forKey:@"youlike"];
        
        [dict setObject:[NSString stringWithFormat:@"%i", [[dict objectForKey:@"totallike"] intValue]-1 ] forKey:@"totallike"];
    }
    
    [self.postArr insertObject:dict atIndex:sender.tag];
    
    
    NSIndexPath *path = [postCollectinView indexPathForCell: (ListCollectionViewCell *)sender.superview.superview];
    
    
    [postCollectinView performSelector:@selector(reloadItemsAtIndexPaths:) withObject:@[path] afterDelay:0.2];
   
    
    [self likePost:[NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", [dict valueForKey:@"postId"], @"postId", [dict valueForKey:@"userId"], @"secondaryId", nil] index:sender.tag];
    
}


- (void)favTap:(UIButton *)sender
{
    [self animateButton:sender];
    
    NSMutableDictionary *dict = [[self.postArr objectAtIndex:sender.tag] mutableCopy];
    
    [self.postArr removeObjectAtIndex:sender.tag];
    
    
    if ([[dict objectForKey:@"favourite"] intValue] == 0)
    {
        [dict setObject:@"1" forKey:@"favourite"];
    }
    else
    {
        [dict setObject:@"0" forKey:@"favourite"];
    }
    
    [self.postArr insertObject:dict atIndex:sender.tag];
    
    
    NSIndexPath *path = [postCollectinView indexPathForCell: (ListCollectionViewCell *)sender.superview.superview];
    
    
    [postCollectinView performSelector:@selector(reloadItemsAtIndexPaths:) withObject:@[path] afterDelay:0.2];
    
    
    [self favPost:[[self.postArr objectAtIndex:sender.tag] valueForKey:@"postId"] index:sender.tag];
}


- (void)commentTap:(UIButton *)sender
{
    if ([[[self.postArr objectAtIndex:sender.tag] objectForKey:@"commentflag"] intValue] == 1)
    {
        AddCommentViewController *vc = [[AddCommentViewController alloc] initWithNibName:@"AddCommentViewController" bundle:nil];
        
        vc.hidesBottomBarWhenPushed = YES;
        
        vc.postId = [[self.postArr objectAtIndex:sender.tag] objectForKey:@"postId"];
        
        vc.secondryId = [[self.postArr objectAtIndex:sender.tag] objectForKey:@"userId"];

        [self.navigationController pushViewController:vc animated:YES];
    }
}


- (void)followTap:(UIButton *)sender
{
   index = [postCollectinView indexPathForCell: (ListCollectionViewCell *)sender.superview.superview];
   
    NSLog(@"%ld", sender.tag);
    
    if ([[[self.postArr objectAtIndex:sender.tag] objectForKey:@"youfollowing"] intValue] == 0)
    {
        [self changeFollowStatus];
    }
    else if ([[[self.postArr objectAtIndex:sender.tag] objectForKey:@"youfollowing"] intValue] == 1)
    {
        unfollowView.frame = self.view.frame;
        
        [unfollowUserImg sd_setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [[self.postArr objectAtIndex:sender.tag] valueForKey:@"profilePic"]]] placeholderImage:nil];
        
        unfollowUserNameLbl.text = [NSString stringWithFormat:@"Unfollow %@?", [[self.postArr objectAtIndex:sender.tag] valueForKey:@"fullname"]];
        
        
        [self.view addSubview:unfollowView];
        
        
        [UIView animateWithDuration:0.1 animations:^
         {
             popupView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
         }
                         completion:^(BOOL finished)
         {
             [UIView animateWithDuration:0.1 animations:^
              {
                  popupView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
              }
                              completion:^(BOOL finished)
              {
                  [UIView animateWithDuration:0.1 animations:^
                   {
                       popupView.transform = CGAffineTransformIdentity;
                   }];
              }];
         }];
    }
    else
    {
        
        NSMutableDictionary *dict = [[self.postArr objectAtIndex:sender.tag] mutableCopy];
        
        [self.postArr removeObjectAtIndex:index.row];
        
        
        if ([[dict objectForKey:@"youfollowing"] intValue] == 2)
        {
            [dict setObject:@"0" forKey:@"youfollowing"];
            
            [self.postArr insertObject:dict atIndex:index.row];
            
            [self cancelFollowRequest:[[self.postArr objectAtIndex:index.row] valueForKey:@"userId"]];
        }
    
        [postCollectinView performSelector:@selector(reloadItemsAtIndexPaths:) withObject:@[index] afterDelay:0.2];
    }
}



- (void)followUser:(NSString *)userId
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"primary_userid", userId, @"secondary_userid", nil];
    
    
    [[ServiceHelper sharedManager] setHudShow:NO];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"sendFollowingRequest" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 [self viewUsersPostsWebserviceService:NO];
                 //                 if ([[result objectForKey:@"follow"] intValue] == 0)
                 //                 {
                 //                     [editProfileBtn setTitle:@"Follow" forState:UIControlStateNormal];
                 //                 }
                 //
                 //                 else
                 //                 {
                 //                     [editProfileBtn setTitle:@"Following" forState:UIControlStateNormal];
                 //                 }
             }
             
             else
             {
                 
             }
         }
         else
         {
             NSMutableDictionary *dict = [[self.postArr objectAtIndex:index.row] mutableCopy];
             
             [self.postArr removeObjectAtIndex:index.row];
             
             
             if ([[dict objectForKey:@"youfollowing"] intValue] == 0)
             {
                 [dict setObject:@"1" forKey:@"youfollowing"];
             }
             else
             {
                 [dict setObject:@"0" forKey:@"youfollowing"];
             }
             
             [self.postArr insertObject:dict atIndex:index.row];
             
             
             [postCollectinView performSelector:@selector(reloadItemsAtIndexPaths:) withObject:@[index] afterDelay:0.2];
         }
     }];
}



- (void)unfollowUser:(NSString *)userId
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userid", userId, @"followerid", nil];
    
    
    [[ServiceHelper sharedManager] setHudShow:NO];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"unFollowUser" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 [self viewUsersPostsWebserviceService:NO];
             }
             
             else
             {
                 
             }
         }
         else
         {
             NSMutableDictionary *dict = [[self.postArr objectAtIndex:index.row] mutableCopy];
             
             [self.postArr removeObjectAtIndex:index.row];
             
             
             if ([[dict objectForKey:@"youfollowing"] intValue] == 0)
             {
                 [dict setObject:@"1" forKey:@"youfollowing"];
             }
             else
             {
                 [dict setObject:@"0" forKey:@"youfollowing"];
             }
             
             [self.postArr insertObject:dict atIndex:index.row];
             
             
             [postCollectinView performSelector:@selector(reloadItemsAtIndexPaths:) withObject:@[index] afterDelay:0.2];
         }
     }];
    
}


- (void)cancelFollowRequest:(NSString *)userId
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", userId, @"secondaryId", nil];
    
    
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"cancelFollowRequest" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
             [self viewUsersPostsWebserviceService:YES];
             
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 
             }
             
             else
             {
                 
             }
         }
         else
         {
             /*NSMutableDictionary *dict = [[postArr objectAtIndex:indexpath.row] mutableCopy];
              
              [postArr removeObjectAtIndex:indexpath.row];
              
              
              if ([[dict objectForKey:@"youfollowing"] intValue] == 0)
              {
              [dict setObject:@"1" forKey:@"youfollowing"];
              }
              else
              {
              [dict setObject:@"0" forKey:@"youfollowing"];
              }
              
              [postArr insertObject:dict atIndex:indexpath.row];
              
              
              [userCollectionView performSelector:@selector(reloadItemsAtIndexPaths:) withObject:@[indexpath] afterDelay:0.2];*/
         }
     }];
    
}




- (void)shareTap:(UIButton *)sender
{
    
}


- (void)getTotalLike:(UIButton *)sender
{
    ViewLikesViewController *vc = [[ViewLikesViewController alloc] initWithNibName:@"ViewLikesViewController" bundle:nil];
    
    vc.hidesBottomBarWhenPushed = YES;
    
    vc.switchTitleStr = @"like";
    
    vc.postId = [[self.postArr objectAtIndex:sender.tag] objectForKey:@"postId"];
    
    [self.navigationController pushViewController:vc animated:YES];
}


- (void)dotsTap:(UIButton *)sender
{
    if ([[[self.postArr objectAtIndex:sender.tag] objectForKey:@"userId"] intValue] == [[(APPDEL).userDetailDict objectForKey:@"userId"] intValue])
    {
        NSString *swapStr;
        
        if ([[[self.postArr objectAtIndex:sender.tag] objectForKey:@"commentflag"] intValue] == 1)
        {
            swapStr = @"Turn Off Commenting";
        }
        
        else
        {
            swapStr = @"Turn On Commenting";
        }
        
        
        UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Delete" otherButtonTitles:swapStr, @"Edit", @"Share", nil];
        
        action.tag = sender.tag;
        
        [action showInView:self.view];
    }
    else
    {
        UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Report", nil];
        
        action.tag = sender.tag;
        
        [action showInView:self.view];
    }
}


- (void)locationTap:(UIButton *)sender
{
    ViewPostBySearchViewController *vc = [[ViewPostBySearchViewController alloc] initWithNibName:@"ViewPostBySearchViewController" bundle:nil];
    
    vc.isSeacrhTagPeople = NO;
    
    vc.searchStr = [[self.postArr objectAtIndex:sender.tag] valueForKey:@"location"];
    
    [self.navigationController pushViewController:vc animated:YES];
}


- (void)hideTagBtn:(UIButton *)sender
{
    [UIView animateWithDuration:0.2 animations:^
    {
        sender.alpha = 0;
    }];
}


- (void)showTagBtn:(ListCollectionViewCell *)cell
{
    [UIView animateWithDuration:0.2 animations:^
     {
         cell.tagBtn.alpha = 1;
     }];
}



- (void)showTagPeopleView:(UIButton *)sender
{
    UICollectionViewCell *cell = (UICollectionViewCell *)sender.superview.superview;
    
    [self addTagBtnOnCell:cell sender:sender personTagArr:[[self.postArr objectAtIndex:sender.tag] valueForKey:@"tagpeople"]];
}


#pragma mark - CollectionView Delegate

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{    
    return [self setCollectionViewHeight:[self.postArr objectAtIndex:indexPath.row] indexPath:indexPath];
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.postArr.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView1 cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [postCollectinView dequeueReusableCellWithReuseIdentifier:@"imageCell" forIndexPath:indexPath];
    
    if (cell != nil)
    {
        for (UIView* view in [cell.contentView subviews])
        {
            [view removeFromSuperview];
        }
    }
    //Profile image
    
    UIImageView *profileImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 50, 50)];
    
    profileImage.layer.cornerRadius = 25.0;
    
    profileImage.clipsToBounds = YES;
    
    [profileImage sd_setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [[self.postArr objectAtIndex:indexPath.row] valueForKey:@"profilePic"] ] ] placeholderImage:[UIImage imageNamed:@"profile_demo"]];
    
    
    [cell.contentView addSubview:profileImage];
    
    
    //Profile Picture button
    
    UIButton *profilePicBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    profilePicBtn.tag = indexPath.row;
    
    [profilePicBtn addTarget:self action:@selector(tapOnProfileImage:) forControlEvents:UIControlEventTouchUpInside];
    
    profilePicBtn.frame = profileImage.frame;
    
   // [profilePicBtn setImage:[UIImage imageNamed:@"like"] forState:UIControlStateNormal];
    
    [cell.contentView addSubview:profilePicBtn];
    
    
    
    // Name;
    
    UILabel *nameLbl = [[UILabel alloc] initWithFrame:CGRectMake(profileImage.frame.origin.x + profileImage.frame.size.width+5, 20, [[UIScreen mainScreen] bounds].size.width-5, 20)];
    
    //nameLbl.font = [UIFont fontWithName:@"MuseoSansCyrl-700" size:13];
    
    nameLbl.font = [UIFont boldSystemFontOfSize:15];
    
    nameLbl.textColor = [UIColor whiteColor];
    
    nameLbl.text = [[[self.postArr objectAtIndex:indexPath.row] objectForKey:@"fullname"] lowercaseString];
    
    [cell.contentView addSubview:nameLbl];
    
    
    
    //location
    
    UILabel *locationLbl = [[UILabel alloc] initWithFrame:CGRectMake(profileImage.frame.origin.x + profileImage.frame.size.width+5, nameLbl.frame.origin.y + nameLbl.frame.size.height, [[UIScreen mainScreen] bounds].size.width-5, 20)];
    
    //locationLbl.font = [UIFont fontWithName:@"MuseoSansCyrl-500" size:13];
    
    locationLbl.font = [UIFont systemFontOfSize:13];
    
    locationLbl.textColor = [UIColor whiteColor];
    
    locationLbl.text = [[self.postArr objectAtIndex:indexPath.row] objectForKey:@"location"];
    
    
    [cell.contentView addSubview:locationLbl];
    
    
    
    //LocationTap
    
    UIButton *locationBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    locationBtn.backgroundColor = [UIColor clearColor];
    
    locationBtn.tag = indexPath.row;
    
    locationBtn.frame = locationLbl.frame;
    
    [locationBtn addTarget:self action:@selector(locationTap:) forControlEvents:UIControlEventTouchUpInside];
    
    if (locationLbl.text.length)
    {
        [cell.contentView addSubview:locationBtn];
    }
    
    //Follow button
    
    if ([[[self.postArr objectAtIndex:indexPath.row] valueForKey:@"userId"] intValue] != [[(APPDEL).userDetailDict objectForKey:@"userId"] intValue])
    {
        UIButton *followBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        
        followBtn.tag = indexPath.row;
        
        [followBtn addTarget:self action:@selector(followTap:) forControlEvents:UIControlEventTouchUpInside];
        
        
        if ([[[self.postArr objectAtIndex:indexPath.row] objectForKey:@"youfollowing"] intValue] == 0)
        {
            [followBtn setTitle:@"Follow" forState:UIControlStateNormal];
            
            followBtn.backgroundColor = [UIColor whiteColor];
            
            [followBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        }
        else if ([[[self.postArr objectAtIndex:indexPath.row] objectForKey:@"youfollowing"] intValue] == 1)
        {
            [followBtn setTitle:@"Following" forState:UIControlStateNormal];
            
            followBtn.backgroundColor = [UIColor blackColor];
            
            [followBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }
        else
        {
            [followBtn setTitle:@"Requested" forState:UIControlStateNormal];
            
            followBtn.backgroundColor = [UIColor blackColor];
            
            [followBtn setTitleColor: [UIColor whiteColor] forState:UIControlStateNormal];
        }
        
        followBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    
        followBtn.frame = CGRectMake(([[UIScreen mainScreen] bounds].size.width - 65) -15, 23, 65, 25);
        
        followBtn.layer.cornerRadius = 2.0;
        
        followBtn.layer.borderWidth = 1.0;
        
        followBtn.layer.borderColor = [UIColor blackColor].CGColor;
        
       // [cell.contentView addSubview:followBtn];
    }
    
    
    //Media
    
    UIView *mediaView = [[UIView alloc] init];
    
    mediaView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    
    if ([[[self.postArr objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"image"])
    {
        int backEndHeight = [[[self.postArr objectAtIndex:indexPath.row] valueForKey:@"height"] intValue];
        
        int backEndscreenWidth = [[[self.postArr objectAtIndex:indexPath.row] valueForKey:@"width"] intValue];
        
        
        float scaleFator = [[UIScreen mainScreen] bounds].size.width/backEndscreenWidth;
        
        
        float actualHeight = scaleFator * backEndHeight;

        
        mediaView.frame = CGRectMake(0, locationLbl.frame.origin.y + locationLbl.frame.size.height+5, [[UIScreen mainScreen] bounds].size.width, actualHeight);
        
        
        UIImageView *mediaImageView = [[UIImageView alloc] init];
        
        mediaImageView.backgroundColor = [UIColor clearColor];
        
        //[mediaImageView sd_setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [[self.postArr objectAtIndex:indexPath.row] valueForKey:@"media"] ] ] placeholderImage:nil options:SDWebImageRefreshCached];
        
        mediaImageView.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, actualHeight);
        
        
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        
        [manager downloadImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [[self.postArr objectAtIndex:indexPath.row] valueForKey:@"media"] ] ] options:SDWebImageLowPriority progress:^(NSInteger receivedSize, NSInteger expectedSize)
         {
             NSLog(@"received - %li,  expect - %li", (long)receivedSize, (long)expectedSize);
             
         } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
             if (image)
             {
                 mediaImageView.image = image;
             }
         }];
        
        
        
        [mediaView addSubview:mediaImageView];
        
        [cell.contentView addSubview:mediaView];
        
        //Tag people Btn
        
        if ([(NSArray *)[[self.postArr objectAtIndex:indexPath.row] valueForKey:@"tagpeople"] count])
        {
            UIButton *tagPeopleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            
            tagPeopleBtn.tag = indexPath.row;
            
            tagPeopleBtn.frame = CGRectMake(10, (mediaView.frame.origin.y + mediaView.frame.size.height) - 30, 25, 25);
            
            [tagPeopleBtn addTarget:self action:@selector(showTagPeopleView:) forControlEvents:UIControlEventTouchUpInside];
            
            
            [tagPeopleBtn setImage:[UIImage imageNamed:@"tag_Pic"] forState:UIControlStateNormal];
            
           // [cell.contentView addSubview:tagPeopleBtn];
        }
    }
    else
    {
        
        mediaView.frame = CGRectMake(0, locationLbl.frame.origin.y + locationLbl.frame.size.height+5, [[UIScreen mainScreen] bounds].size.width, 350);
        
        InstagramVideoView *videoView = [[InstagramVideoView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 350)];
        
        
        videoView.backgroundColor = [UIColor clearColor];
        
        
        [mediaView addSubview:videoView];
        
        [cell.contentView addSubview:mediaView];
        
        dispatch_async(dispatch_get_main_queue(),
       ^{
           if(isScrolling == NO)
           {
               [videoView resume];
               
               [videoView playVideoWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", ServiceURL, [[self.postArr objectAtIndex:indexPath.row] valueForKey:@"media"]]] ];
               
               [videoPlayerObj addObject:videoView];
           }
           else
           {
               [videoView stopVideoPlay];
           }
       });
    }
    
    
    // like Buttom
    
    UIButton *likeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    likeBtn.tag = indexPath.row;
    
    [likeBtn addTarget:self action:@selector(likeTap:) forControlEvents:UIControlEventTouchUpInside];
    
    likeBtn.frame = CGRectMake(10, mediaView.frame.origin.y + mediaView.frame.size.height+10, 25, 25);
    
    
    if ([[[self.postArr objectAtIndex:indexPath.row] valueForKey:@"youlike"] intValue] == 0)
    {
        [likeBtn setImage:[UIImage imageNamed:@"like"] forState:UIControlStateNormal];
    }
    else
    {
        [likeBtn setImage:[UIImage imageNamed:@"like_h"] forState:UIControlStateNormal];
    }
    
    
    
    [cell.contentView addSubview:likeBtn];
    
    
    // Comment Buttom
    
    UIButton *commentBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    commentBtn.tag = indexPath.row;
    
    commentBtn.frame = CGRectMake(likeBtn.frame.origin.x + likeBtn.frame.size.width + 10, likeBtn.frame.origin.y, 25, 25);
    
    if ([[[self.postArr objectAtIndex:indexPath.row] objectForKey:@"commentflag"] intValue] == 0)
    {
        [commentBtn setImage:[UIImage imageNamed:@"comment_off"] forState: UIControlStateNormal];
    }
    else
    {
        [commentBtn setImage:[UIImage imageNamed:@"comment"] forState: UIControlStateNormal];
        
        [commentBtn addTarget:self action:@selector(commentTap:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    [cell.contentView addSubview:commentBtn];
    
    
    // Favourite Buttom
    
    UIButton *favBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    favBtn.tag = indexPath.row;
    
    [favBtn addTarget:self action:@selector(favTap:) forControlEvents:UIControlEventTouchUpInside];
    
    favBtn.frame = CGRectMake(commentBtn.frame.origin.x + commentBtn.frame.size.width + 10, likeBtn.frame.origin.y, 25, 25);
    
    if ([[[self.postArr objectAtIndex:indexPath.row] valueForKey:@"favourite"] intValue] == 0)
    {
        [favBtn setImage:[UIImage imageNamed:@"star"] forState:UIControlStateNormal];
    }
    else
    {
        [favBtn setImage:[UIImage imageNamed:@"star_h"] forState:UIControlStateNormal];
    }
    
    [cell.contentView addSubview:favBtn];
    
    
    
    // Three Dot Button
    
    UIButton *dotBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    dotBtn.tag = indexPath.row;
    
    [dotBtn addTarget:self action:@selector(dotsTap:) forControlEvents:UIControlEventTouchUpInside];
    
    dotBtn.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width - 50, likeBtn.frame.origin.y, 40, 30);
    
    [dotBtn setImage:[UIImage imageNamed:@"three_dots"] forState:UIControlStateNormal];
    
    [cell.contentView addSubview:dotBtn];
    
    
    
    //Total LikesLabel
    
    UILabel *totalLikeLbl = [[UILabel alloc] init];
    
    totalLikeLbl.backgroundColor = [UIColor clearColor];
    
    totalLikeLbl.font = [UIFont boldSystemFontOfSize:15];
    
    totalLikeLbl.textAlignment = NSTextAlignmentLeft;
    
    totalLikeLbl.textColor = [UIColor whiteColor];
    
    
    // Total like Button
    
    UIButton *getTotlLikeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    getTotlLikeBtn.backgroundColor = [UIColor clearColor];
    
    getTotlLikeBtn.tag = indexPath.row;
    
    [getTotlLikeBtn addTarget:self action:@selector(getTotalLike:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    //Caption Label
    
    
    ResponsiveLabel *captionLbl = [[ResponsiveLabel alloc] init];
    
    captionLbl.numberOfLines = 999;
    
    //captionLbl.font = [UIFont fontWithName:@"MuseoSansCyrl-500" size:14];
    
    captionLbl.font = [UIFont systemFontOfSize:14];
    
    captionLbl.textColor = [UIColor whiteColor];
    
    
    // Get the Label height
    
    NSString *dataStr = @"";
    
    if (![[[self.postArr objectAtIndex:indexPath.row] objectForKey:@"caption"] isEqualToString:@""])
    {
        dataStr = [NSString stringWithFormat:@"%@ %@", [[[self.postArr objectAtIndex:indexPath.row] objectForKey:@"fullname"] lowercaseString], [[self.postArr objectAtIndex:indexPath.row] objectForKey:@"caption"]];
        
    }
    
    /*NSArray *commentArr = [[self.postArr objectAtIndex:indexPath.row] objectForKey:@"allComment"];
    
    NSString *totalComment = @"";
    
    if ([commentArr count])
    {
        NSMutableString *commentStr = [@"" mutableCopy];
        
        for (int i = 0; i < commentArr.count; i++)
        {
            [commentStr appendFormat:@"%@\n", [NSString stringWithFormat:@"%@ %@", [[commentArr objectAtIndex:i] objectForKey:@"fullname"], [[commentArr objectAtIndex:i] objectForKey:@"comment"]]];
            
            
            //string detection
            PatternTapResponder commentUser = ^(NSString *string)
            {
                NSLog(@"tapped = %@",string);
            };
            
            [captionLbl enableStringDetection:[[commentArr objectAtIndex:i] objectForKey:@"fullname"] withAttributes:@{NSFontAttributeName : [UIFont fontWithName:@"MuseoSansCyrl-700" size:12],NSForegroundColorAttributeName:[UIColor blackColor],RLTapResponderAttributeName: commentUser}];
            //
        }
        
        if ([[[self.postArr objectAtIndex:indexPath.row] objectForKey:@"totalcomment"] intValue] > 1)
        {
            totalComment = [[self.postArr objectAtIndex:indexPath.row] objectForKey:@"totalcomment"];
            
            dataStr = [NSString stringWithFormat:@"%@\nView all %@ comments\n\n%@", dataStr, totalComment, commentStr];
            
        }
        else
        {
            dataStr = [NSString stringWithFormat:@"%@\n%@", dataStr, commentStr];
        }
    }
    
    if ([[[self.postArr objectAtIndex:indexPath.row] objectForKey:@"totalcomment"] intValue] > 1)
    {
        if (dataStr.length)
        {
            dataStr = [NSString stringWithFormat:@"%@\n\nView all %@ comments", dataStr, [[self.postArr objectAtIndex:indexPath.row] objectForKey:@"totalcomment"]];
        }
        else
        {
            dataStr = [NSString stringWithFormat:@"View all %@ comments", [[self.postArr objectAtIndex:indexPath.row] objectForKey:@"totalcomment"]];
        }
        
    }
    else if ([[[self.postArr objectAtIndex:indexPath.row] objectForKey:@"totalcomment"] intValue] == 1)
    {
        if (dataStr.length)
        {
            dataStr = [NSString stringWithFormat:@"%@\n\nView all %@ comments", dataStr, [[self.postArr objectAtIndex:indexPath.row] objectForKey:@"totalcomment"]];
        }
        else
        {
            dataStr = [NSString stringWithFormat:@"View all %@ comments", [[self.postArr objectAtIndex:indexPath.row] objectForKey:@"totalcomment"]];
        }
        
    }
    else
    {
        if (dataStr.length)
        {
            dataStr = [NSString stringWithFormat:@"%@\n\nView all comments", dataStr];
        }
        else
        {
            dataStr = @"View all comments";
        }
    }*/
    
    captionLbl.text = dataStr;
    
    
    
    //View all comment label
    
    ResponsiveLabel *viewAllCommentLbl = [[ResponsiveLabel alloc] init];
    
    //viewAllCommentLbl.font = [UIFont fontWithName:@"MuseoSansCyrl-500" size:14];
    
    viewAllCommentLbl.font = [UIFont systemFontOfSize:14];
    
    viewAllCommentLbl.textColor = [UIColor lightGrayColor];
    
    
    if ([[[self.postArr objectAtIndex:indexPath.row] objectForKey:@"totalcomment"] intValue] > 1)
    {
        viewAllCommentLbl.text = [NSString stringWithFormat:@"View all %@ comments", [[self.postArr objectAtIndex:indexPath.row] objectForKey:@"totalcomment"]];
        
    }
    else if ([[[self.postArr objectAtIndex:indexPath.row] objectForKey:@"totalcomment"] intValue] == 1)
    {
        viewAllCommentLbl.text = [NSString stringWithFormat:@"View all %@ comments", [[self.postArr objectAtIndex:indexPath.row] objectForKey:@"totalcomment"]];
    }
    else
    {
        viewAllCommentLbl.text = @"View all comments";
    }
    
    
    //string detection
    PatternTapResponder viewAllCountComment = ^(NSString *string)
    {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        
        btn.tag = indexPath.row;
        
        [self commentTap:btn];
    };
    
    [viewAllCommentLbl enableStringDetection:[NSString stringWithFormat:@"View all %@ comments", [[self.postArr objectAtIndex:indexPath.row] objectForKey:@"totalcomment"]] withAttributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor],RLTapResponderAttributeName: viewAllCountComment}];
    //
    
    
    //string detection
    PatternTapResponder viewAllcomment = ^(NSString *string)
    {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        
        btn.tag = indexPath.row;
        
        [self commentTap:btn];
    };
    
    [viewAllCommentLbl enableStringDetection:@"View all comments" withAttributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor],RLTapResponderAttributeName: viewAllcomment}];
    //

    
    
    //Hastag detection
    
    captionLbl.userInteractionEnabled = YES;
    PatternTapResponder hashTagTapAction = ^(NSString *tappedString)
    {
        NSLog(@"HashTag Tapped = %@",tappedString);
        
        ViewPostBySearchViewController *vc = [[ViewPostBySearchViewController alloc] initWithNibName:@"ViewPostBySearchViewController" bundle:nil];
        
        vc.isSeacrhTagPeople = YES;
        
        vc.searchStr = [tappedString stringByReplacingOccurrencesOfString:@"#" withString:@""];
        
        [self.navigationController pushViewController:vc animated:YES];
    };
    
    [captionLbl enableHashTagDetectionWithAttributes:
     @{NSForegroundColorAttributeName:hashtag_color, RLTapResponderAttributeName:hashTagTapAction}];
    
    //
    
    
    //string detection
    PatternTapResponder tapResponder = ^(NSString *string)
    {
        NSLog(@"tapped = %@",string);
    };
    
    [captionLbl enableStringDetection:nameLbl.text withAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName : [UIFont boldSystemFontOfSize:14], RLTapResponderAttributeName: tapResponder}];
    //
    
    
    
    //Username detection
    
    captionLbl.userInteractionEnabled = YES;
    PatternTapResponder userNameDetection = ^(NSString *tappedString)
    {
        NSLog(@"HashTag Tapped = %@",tappedString);
        
        [self getUserIdFromUsername:[NSDictionary dictionaryWithObjectsAndKeys:tappedString, @"otherusername", [(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", nil]];
    };
    
    [captionLbl enableUserHandleDetectionWithAttributes:@{NSForegroundColorAttributeName:hashtag_color, RLTapResponderAttributeName:userNameDetection}];
    
    //
    
    
    
    CGRect textRect = [dataStr boundingRectWithSize:CGSizeMake([[UIScreen mainScreen] bounds].size.width-20, FLT_MAX)
                                            options:NSStringDrawingUsesLineFragmentOrigin
                                         attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12]}
                                            context:nil];
    
    int captionLblHeigtht = textRect.size.height;
    
   
    //Time Label
    
    UILabel *timeLbl = [[UILabel alloc] init];

    //timeLbl.font = [UIFont fontWithName:@"MuseoSansCyrl-500" size:14];
    
    timeLbl.font = [UIFont systemFontOfSize:14];
    
    timeLbl.textColor = [UIColor lightGrayColor];
    
    
    if ([[[self.postArr objectAtIndex: indexPath.row] objectForKey:@"totallike"] intValue] != 0)
    {
        NSString *singularPluralStr;
        
        if ([[[self.postArr objectAtIndex: indexPath.row] objectForKey:@"totallike"] intValue] <= 1 )
        {
            singularPluralStr = @"like";
        }
        else
        {
            singularPluralStr = @"likes";
        }
        
        totalLikeLbl.frame = CGRectMake(10, likeBtn.frame.origin.y + likeBtn.frame.size.height + 5, [[UIScreen mainScreen] bounds].size.width - 20, 15);
        
        getTotlLikeBtn.frame = totalLikeLbl.frame;
        
        
        totalLikeLbl.text = [NSString stringWithFormat:@"%@ %@", [[self.postArr objectAtIndex: indexPath.row] objectForKey:@"totallike"], singularPluralStr];
        
        
        [cell.contentView addSubview:totalLikeLbl];
        
        [cell.contentView addSubview:getTotlLikeBtn];
        
        
        if (dataStr.length)
        {
            captionLbl.frame = CGRectMake(10, totalLikeLbl.frame.origin.y + totalLikeLbl.frame.size.height+5, [[UIScreen mainScreen] bounds].size.width - 20, captionLblHeigtht+10);
            
            [cell.contentView addSubview:captionLbl];
            
            
            viewAllCommentLbl.frame = CGRectMake(10, captionLbl.frame.origin.y + captionLbl.frame.size.height, 200, 15);
            
            timeLbl.frame = CGRectMake(10, viewAllCommentLbl.frame.origin.y + viewAllCommentLbl.frame.size.height+5, 200, 15);
            
            
            
            timeLbl.text = [NSString stringWithFormat:@"%@", [[self.postArr objectAtIndex:indexPath.row] objectForKey:@"posttime"]];
            
          
            [cell.contentView addSubview:viewAllCommentLbl];
            
            [cell.contentView addSubview:timeLbl];
        }
        else
        {
             viewAllCommentLbl.frame = CGRectMake(10, totalLikeLbl.frame.origin.y + totalLikeLbl.frame.size.height+5, 200, 15);
            
            timeLbl.frame = CGRectMake(10, viewAllCommentLbl.frame.origin.y + viewAllCommentLbl.frame.size.height+5, 100, 15);
            
            
            
            timeLbl.text = [NSString stringWithFormat:@"%@", [[self.postArr objectAtIndex:indexPath.row] objectForKey:@"posttime"]];
            
            [cell.contentView addSubview:viewAllCommentLbl];
            
            [cell.contentView addSubview:timeLbl];
        }
    }
    else
    {
        if (dataStr.length)
        {
            captionLbl.frame = CGRectMake(10, likeBtn.frame.origin.y + likeBtn.frame.size.height, [[UIScreen mainScreen] bounds].size.width-20, captionLblHeigtht+10);
            
            [cell.contentView addSubview:captionLbl];
            
            
            captionLbl.frame = CGRectMake(10, likeBtn.frame.origin.y + likeBtn.frame.size.height, [[UIScreen mainScreen] bounds].size.width-20, captionLblHeigtht+10);
            
            [cell.contentView addSubview:captionLbl];
            
            
            viewAllCommentLbl.frame = CGRectMake(10, captionLbl.frame.origin.y + captionLbl.frame.size.height, 200, 15);
            
            timeLbl.frame = CGRectMake(10, viewAllCommentLbl.frame.origin.y + viewAllCommentLbl.frame.size.height+5, 100, 15);
            
            
            
            timeLbl.text = [NSString stringWithFormat:@"%@", [[self.postArr objectAtIndex:indexPath.row] objectForKey:@"posttime"]];
            
            [cell.contentView addSubview:viewAllCommentLbl];
            
            [cell.contentView addSubview:timeLbl];
        }
        else
        {
            viewAllCommentLbl.frame = CGRectMake(10, likeBtn.frame.origin.y + likeBtn.frame.size.height, 200, 15);
            
            timeLbl.frame = CGRectMake(10, viewAllCommentLbl.frame.origin.y + viewAllCommentLbl.frame.size.height+5, 100, 15);
            
            
            
            timeLbl.text = [NSString stringWithFormat:@"%@", [[self.postArr objectAtIndex:indexPath.row] objectForKey:@"posttime"]];
            
            [cell.contentView addSubview:viewAllCommentLbl];
            
            [cell.contentView addSubview:timeLbl];
        }
    }
    
    return cell;
    
    
   /* static NSString *gridCellIdentifier;
    
    if ([[[self.postArr objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"image"])
    {
        NSLog(@"%ld", indexPath.row);
    
       gridCellIdentifier = @"listCollection";
    
    
    
        ListCollectionViewCell *cell = (ListCollectionViewCell*)[collectionView1 dequeueReusableCellWithReuseIdentifier:gridCellIdentifier forIndexPath:indexPath];
    
        cell.contentView.autoresizingMask  = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    
        //set button tag
        cell.likeBtn.tag = indexPath.row;
    
        cell.favBtn.tag = indexPath.row;
    
        cell.commentBtn.tag = indexPath.row;
    
        cell.shareBtn.tag = indexPath.row;
    
        cell.getTotlLike.tag = indexPath.row;
    
        cell.followBtn.tag = indexPath.row;
    
        cell.dotBtn.tag = indexPath.row;
    
        cell.tagBtn.tag = indexPath.row;
    
        cell.userImageTap.tag = indexPath.row;
    
    
        [self performSelector:@selector(hideTagBtn:) withObject:cell afterDelay:1];
 
    
        //set Action on buttons
        [cell.likeBtn addTarget:self action:@selector(likeTap:) forControlEvents:UIControlEventTouchUpInside];
    
        [cell.commentBtn addTarget:self action:@selector(commentTap:) forControlEvents:UIControlEventTouchUpInside];
    
        [cell.favBtn addTarget:self action:@selector(favTap:) forControlEvents:UIControlEventTouchUpInside];
    
        [cell.shareBtn addTarget:self action:@selector(shareTap:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.getTotlLike addTarget:self action:@selector(getTotalLike:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.followBtn addTarget:self action:@selector(followTap:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.dotBtn addTarget:self action:@selector(dotsTap:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.tagBtn addTarget:self action:@selector(showTagPeopleView:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.userImageTap addTarget:self action:@selector(tapOnProfileImage:) forControlEvents:UIControlEventTouchUpInside];
        
        
         [cell.profileImg sd_setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [[self.postArr objectAtIndex:indexPath.row] valueForKey:@"profilePic"] ] ] placeholderImage:nil];
        
      
        cell.userNameLbl.text = [[self.postArr objectAtIndex:indexPath.row] valueForKey:@"fullname"];
        
      
        
        if ([[[self.postArr objectAtIndex:indexPath.row] objectForKey:@"commentflag"] intValue] == 0)
        {
            [cell.commentBtn setImage:[UIImage imageNamed:@"comment_off"] forState: UIControlStateNormal];
        }
        else
        {
            [cell.commentBtn setImage:[UIImage imageNamed:@"comment"] forState: UIControlStateNormal];
        }
        
        
        if ([[[self.postArr objectAtIndex:indexPath.row] objectForKey:@"youfollowing"] intValue] == 0)
        {
            [cell.followBtn setTitle:@"Follow" forState:UIControlStateNormal];
            
            cell.followBtn.backgroundColor = [UIColor whiteColor];
            
            [cell.followBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        }
        else if ([[[self.postArr objectAtIndex:indexPath.row] objectForKey:@"youfollowing"] intValue] == 1)
        {
            [cell.followBtn setTitle:@"Following" forState:UIControlStateNormal];
            
            cell.followBtn.backgroundColor = [UIColor blackColor];
            
            [cell.followBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }
        else
        {
            [cell.followBtn setTitle:@"Requested" forState:UIControlStateNormal];
         
            cell.followBtn.backgroundColor = [UIColor blackColor];
         
            [cell.followBtn setTitleColor: [UIColor whiteColor] forState:UIControlStateNormal];
        }
        
        
        //Hastag detection
        
        cell.captionLbl.userInteractionEnabled = YES;
        PatternTapResponder hashTagTapAction = ^(NSString *tappedString)
        {
            NSLog(@"HashTag Tapped = %@",tappedString);
        };
        
        [cell.captionLbl enableHashTagDetectionWithAttributes:
         @{NSForegroundColorAttributeName:[UIColor blueColor], RLTapResponderAttributeName:hashTagTapAction}];
        
        //
        
        
        
        if (![[[self.postArr objectAtIndex:indexPath.row] objectForKey:@"caption"] isEqualToString:@""])
        {
            cell.captionLbl.attributedText = [self setBoldString:[NSString stringWithFormat:@"%@ %@", [[self.postArr objectAtIndex:indexPath.row] objectForKey:@"fullname"], [[self.postArr objectAtIndex:indexPath.row] objectForKey:@"caption"]] boldString:[[self.postArr objectAtIndex:indexPath.row] objectForKey:@"fullname"]];
        }
        else
        {
            cell.captionLbl.attributedText = [[NSMutableAttributedString alloc] initWithString:@""];
        }
        
        
        //string detection
        PatternTapResponder tapResponder = ^(NSString *string)
        {
            NSLog(@"tapped = %@",string);
        };
        
        [cell.captionLbl enableStringDetection:cell.userNameLbl.text withAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor],RLTapResponderAttributeName: tapResponder}];
        //
        
        
        NSArray *commentArr = [[self.postArr objectAtIndex:indexPath.row] objectForKey:@"allComment"];
        
        if ([commentArr count])
        {
            NSMutableString *commentStr = [@"" mutableCopy];
            
            for (int i = 0; i < commentArr.count; i++)
            {
                [commentStr appendFormat:@"%@\n", [NSString stringWithFormat:@"%@ %@", [[commentArr objectAtIndex:i] objectForKey:@"fullname"], [[commentArr objectAtIndex:i] objectForKey:@"comment"]]];
                
                //string detection
                PatternTapResponder tapResponder = ^(NSString *string)
                {
                    NSLog(@"tapped = %@",string);
                };
                
                [cell.captionLbl enableStringDetection:[[commentArr objectAtIndex:i] objectForKey:@"fullname"] withAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor],
                                                                                                                                RLTapResponderAttributeName: tapResponder}];
                //
            }
            
            //string detection
            PatternTapResponder tapResponder = ^(NSString *string)
            {
                UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
                
                btn.tag = indexPath.row;
                
                [self commentTap:btn];
            };
            
            [cell.captionLbl enableStringDetection:@"View all comments" withAttributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor],RLTapResponderAttributeName: tapResponder}];
            //
            
            NSMutableAttributedString *yourAttributedString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\nView all comments\n\n%@", cell.captionLbl.text, commentStr]];
            
            cell.captionLbl.attributedText = yourAttributedString;
        }
        
        
        if ([(NSArray *)[[self.postArr objectAtIndex:indexPath.row] valueForKey:@"tagpeople"] count])
        {
            [self showTagBtn:cell];
        }
        else
        {
            [self hideTagBtn:cell];
        }

        
        if ([[[self.postArr objectAtIndex:indexPath.row] valueForKey:@"userId"] intValue] == [[(APPDEL).userDetailDict objectForKey:@"userId"] intValue])
        {
            cell.followBtn.hidden = YES;
        }
        else
        {
            cell.followBtn.hidden = NO;
        }
        
        cell.timeLbl.text = [[self.postArr objectAtIndex:indexPath.row] valueForKey:@"posttime"];
        
        cell.locationLbl.text = [[self.postArr objectAtIndex:indexPath.row] valueForKey:@"location"];
        
        if ([[[self.postArr objectAtIndex:indexPath.row] valueForKey:@"totallike"] intValue] < 2)
        {
             cell.totalLikesLbl.text = [NSString stringWithFormat:@"%@ like", [[self.postArr objectAtIndex:indexPath.row] valueForKey:@"totallike"]];
        }
        else
        {
            cell.totalLikesLbl.text = [NSString stringWithFormat:@"%@ likes", [[self.postArr objectAtIndex:indexPath.row] valueForKey:@"totallike"]];
        }
        
        if ([[[self.postArr objectAtIndex:indexPath.row] valueForKey:@"totallike"] intValue] >= 1)
        {
            cell.totalLikesLbl.hidden = NO;
            
            cell.totalLikeImg.hidden = NO;
            
            cell.getTotlLike.hidden = NO;
        }
        else
        {
            cell.totalLikesLbl.hidden = YES;
            
            cell.totalLikeImg.hidden = YES;
            
            cell.getTotlLike.hidden = YES;
        }
        
        
        //like
        if ([[[self.postArr objectAtIndex:indexPath.row] valueForKey:@"youlike"] intValue] == 0)
        {
            [cell.likeBtn setImage:[UIImage imageNamed:@"like"] forState:UIControlStateNormal];
        }
        else
        {
            [cell.likeBtn setImage:[UIImage imageNamed:@"like_h"] forState:UIControlStateNormal];
        }
        
        //Favourite
        if ([[[self.postArr objectAtIndex:indexPath.row] valueForKey:@"favourite"] intValue] == 0)
        {
            [cell.favBtn setImage:[UIImage imageNamed:@"star"] forState:UIControlStateNormal];
        }
        else
        {
            [cell.favBtn setImage:[UIImage imageNamed:@"star_h"] forState:UIControlStateNormal];
        }
        
        [cell.userImg sd_setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [[self.postArr objectAtIndex:indexPath.row] valueForKey:@"media"] ] ] placeholderImage:nil];
     
       
        UIImageView *testImage = [[UIImageView alloc] init];
        
        [testImage sd_setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [[self.postArr objectAtIndex:indexPath.row] valueForKey:@"media"] ] ] placeholderImage:nil];
        
        UIImage *image = [self imageWithImage:testImage.image scaledToWidth:[[UIScreen mainScreen] bounds].size.width];
        
        
        cell.imageHeight.constant = image.size.height;

        
        return cell;
        
    }
    
    else
    {
        gridCellIdentifier = @"listCollectionVideo";
        
        ListCollectionViewVideoCell *cell = (ListCollectionViewVideoCell*)[collectionView1 dequeueReusableCellWithReuseIdentifier:gridCellIdentifier forIndexPath:indexPath];
        
        
        cell.contentView.autoresizingMask  = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        
        //set button tag
        cell.likeBtn.tag = indexPath.row;
        
        cell.favBtn.tag = indexPath.row;
        
        cell.commentBtn.tag = indexPath.row;
        
        cell.shareBtn.tag = indexPath.row;
        
        cell.followBtn.tag = indexPath.row;
        
        cell.dotBtn.tag = indexPath.row;
        
        cell.userImageTap.tag = indexPath.row;
        
        
        //set Action on buttons
        [cell.likeBtn addTarget:self action:@selector(likeTap:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.commentBtn addTarget:self action:@selector(commentTap:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.favBtn addTarget:self action:@selector(favTap:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.shareBtn addTarget:self action:@selector(shareTap:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.getTotlLike addTarget:self action:@selector(getTotalLike:) forControlEvents:UIControlEventTouchUpInside];
        
         [cell.followBtn addTarget:self action:@selector(followTap:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.dotBtn addTarget:self action:@selector(dotsTap:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.userImageTap addTarget:self action:@selector(tapOnProfileImage:) forControlEvents:UIControlEventTouchUpInside];
        
        
        
        [cell.profileImg sd_setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [[self.postArr objectAtIndex:indexPath.row] valueForKey:@"profilePic"]]] placeholderImage:nil];
        
        cell.userNameLbl.text = [[self.postArr objectAtIndex:indexPath.row] valueForKey:@"fullname"];
        
        
        //Hastag detection
        
        cell.captionLbl.userInteractionEnabled = YES;
        PatternTapResponder hashTagTapAction = ^(NSString *tappedString)
        {
            NSLog(@"HashTag Tapped = %@",tappedString);
        };
        
        [cell.captionLbl enableHashTagDetectionWithAttributes:
         @{NSForegroundColorAttributeName:[UIColor blueColor], RLTapResponderAttributeName:hashTagTapAction}];
        
        //
        
        
        if ([[[self.postArr objectAtIndex:indexPath.row] objectForKey:@"youfollowing"] intValue] == 0)
        {
            [cell.followBtn setTitle:@"Follow" forState:UIControlStateNormal];
            
            cell.followBtn.backgroundColor = [UIColor whiteColor];
            
            [cell.followBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        }
        else
        {
            [cell.followBtn setTitle:@"Following" forState:UIControlStateNormal];
            
            cell.followBtn.backgroundColor = [UIColor blackColor];
            
            [cell.followBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }
        
        if ([[[self.postArr objectAtIndex:indexPath.row] objectForKey:@"commentflag"] intValue] == 0)
        {
            [cell.commentBtn setImage:[UIImage imageNamed:@"comment_off"] forState: UIControlStateNormal];
        }
        else
        {
            [cell.commentBtn setImage:[UIImage imageNamed:@"comment"] forState: UIControlStateNormal];
        }

        
        NSArray *commentArr = [[self.postArr objectAtIndex:indexPath.row] objectForKey:@"allComment"];
        
        if ([commentArr count])
        {
            NSMutableString *commentStr = [@"" mutableCopy];
            
            for (int i = 0; i < commentArr.count; i++)
            {
                [commentStr appendFormat:@"%@\n", [NSString stringWithFormat:@"%@ %@", [[commentArr objectAtIndex:i] objectForKey:@"fullname"], [[commentArr objectAtIndex:i] objectForKey:@"comment"]]];
                
                //string detection
                PatternTapResponder tapResponder = ^(NSString *string)
                {
                    NSLog(@"tapped = %@",string);
                };
                
                [cell.captionLbl enableStringDetection:[[commentArr objectAtIndex:i] objectForKey:@"fullname"] withAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor],
                                                                                                                                RLTapResponderAttributeName: tapResponder}];
                //
            }
            
            //string detection
            PatternTapResponder tapResponder = ^(NSString *string)
            {
                UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
                
                btn.tag = indexPath.row;
                
                [self commentTap:btn];
            };
            
            [cell.captionLbl enableStringDetection:@"View all comments" withAttributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor],RLTapResponderAttributeName: tapResponder}];
            //
            
            NSMutableAttributedString *yourAttributedString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\nView all comments\n\n%@", cell.captionLbl.text, commentStr]];
            
            cell.captionLbl.attributedText = yourAttributedString;
            
        }
        
        
        
        if (![[[self.postArr objectAtIndex:indexPath.row] objectForKey:@"caption"] isEqualToString:@""])
        {
            cell.captionLbl.attributedText = [self setBoldString:[NSString stringWithFormat:@"%@ %@", [[self.postArr objectAtIndex:indexPath.row] objectForKey:@"fullname"], [[self.postArr objectAtIndex:indexPath.row] objectForKey:@"caption"]] boldString:[[self.postArr objectAtIndex:indexPath.row] objectForKey:@"fullname"]];
        }
        else
        {
            cell.captionLbl.attributedText = [[NSMutableAttributedString alloc] initWithString:@""];
        }
        
        
        if ([[[self.postArr objectAtIndex:indexPath.row] valueForKey:@"userId"] isEqualToString:[(APPDEL).userDetailDict objectForKey:@"userId"]])
        {
            cell.followBtn.hidden = YES;
        }
        else
        {
            cell.followBtn.hidden = NO;
        }
        
        cell.timeLbl.text = [[self.postArr objectAtIndex:indexPath.row] valueForKey:@"posttime"];
        
        cell.locationLbl.text = [[self.postArr objectAtIndex:indexPath.row] valueForKey:@"location"];
        
        cell.totalLikesLbl.text = [[self.postArr objectAtIndex:indexPath.row] valueForKey:@"totallike"];
        
        
        if ([[[self.postArr objectAtIndex:indexPath.row] valueForKey:@"totallike"] intValue] >= 1)
        {
            cell.totalLikesLbl.hidden = NO;
            
            cell.totalLikeImg.hidden = NO;
            
            cell.getTotlLike.hidden = NO;
        }
        else
        {
            cell.totalLikesLbl.hidden = YES;
            
            cell.totalLikeImg.hidden = YES;
            
            cell.getTotlLike.hidden = YES;
        }
        
        
        //like
        if ([[[self.postArr objectAtIndex:indexPath.row] valueForKey:@"youlike"] intValue] == 0)
        {
            [cell.likeBtn setImage:[UIImage imageNamed:@"like"] forState:UIControlStateNormal];
        }
        else
        {
            [cell.likeBtn setImage:[UIImage imageNamed:@"like_h"] forState:UIControlStateNormal];
        }
        
        //Favourite
        if ([[[self.postArr objectAtIndex:indexPath.row] valueForKey:@"favourite"] intValue] == 0)
        {
            [cell.favBtn setImage:[UIImage imageNamed:@"star"] forState:UIControlStateNormal];
        }
        else
        {
            [cell.favBtn setImage:[UIImage imageNamed:@"star_h"] forState:UIControlStateNormal];
        }
        
        cell.videoView = [[InstagramVideoView alloc] initWithFrame:cell.player.frame];
        
        cell.videoView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        
        [cell addSubview:cell.videoView];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
          
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [cell.videoView playVideoWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", ServiceURL, [[self.postArr objectAtIndex:indexPath.row] valueForKey:@"media"]]] ];
                
            });
        });
        
        if ([self isIndexPathVisible:indexPath])
        {
           // [cell.player videoUrl:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", ServiceURL, [[self.postArr objectAtIndex:indexPath.row] valueForKey:@"media"]]] frame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 300)];
            
            cell.videoView = [[InstagramVideoView alloc] initWithFrame:cell.player.frame];
            
            cell.videoView.backgroundColor = [UIColor lightTextColor];
            
            [cell addSubview:cell.videoView];
            
            [cell.videoView playVideoWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", ServiceURL, [[self.postArr objectAtIndex:indexPath.row] valueForKey:@"media"]]] ];
        }*/

    
    //}

    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = (UICollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    
    if ([[[self.postArr objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"image"])
    {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        
        button.tag = indexPath.row;
        
        [self addTagBtnOnCell:cell sender:button personTagArr:[[self.postArr objectAtIndex:indexPath.row] valueForKey:@"tagpeople"]];
    }
    
   /* ListCollectionViewCell *cell = (ListCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    
    if ([[[self.postArr objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"image"])
    {
        [self addTagBtnOnCell:cell personTagArr:[[self.postArr objectAtIndex:indexPath.row] valueForKey:@"tagpeople"]];
    }*/
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
 
}



- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    
   /* if ([[[self.postArr objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"video"])
    {
        NSLog(@"End - %ld", indexPath.row);
        
        ListCollectionViewVideoCell *videoCell = (ListCollectionViewVideoCell *)cell;
        
        if ([videoCell isKindOfClass:[ListCollectionViewVideoCell class]])
        {
            [videoCell.player.player pause];
            
            [videoCell.videoView stopVideoPlay];
            
            [videoCell.player removeObserver];
        }
    }
    else
    {
        ListCollectionViewCell *imageCell = (ListCollectionViewCell *)cell;
        
        if ([imageCell isKindOfClass:[ListCollectionViewCell class]])
        {
            for (UIView *view in cell.subviews)
            {
                if ([view isKindOfClass:[JDTagView class]])
                {
                    [view removeFromSuperview];
                }
            }
            
            imageCell.tagBtn.alpha = 1;
        }
    }*/
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    isScrolling = NO;
    
    [postCollectinView reloadData];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)aScrollView
{
    [self clearvideoViews];
    
    isScrolling = YES;
    
    [postCollectinView reloadData];
}


#pragma mark - ScrolView delegate


-  (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSIndexPath *path = [(UICollectionView *)scrollView indexPathForItemAtPoint:scrollView.contentOffset];
   
    NSLog(@"%ld", path.row);

//    postCollectinView = (UICollectionView *)scrollView;
//
//    CGRect rect = [tableview rectForRowAtIndexPath:[tableview indexPathForCell:self]];
//    
//    rect = [tableview convertRect:rect toView:tableview.superview];
//    BOOL completelyVisible = CGRectContainsRect(tableview.frame, rect);

//    ListCollectionViewVideoCell *cell = [(UICollectionView *)scrollView cellForItemAtIndexPath:[(UICollectionView *)scrollView indexPathForCell:[(UICollectionView *)scrollView cell]]];
//    
//    NSLog(@"%@", cell);
//  
//    if ([cell isKindOfClass:[ListCollectionViewVideoCell class]])
//    {
//        CGRect cellRect = [scrollView convertRect:cell.frame toView:scrollView.superview];
//        
//        NSLog(@"%f", cellRect.origin.y);
//        
//        if (cellRect.origin.y <= 60)
//        {
//            [cell.videoView resume];
//        }
//        else
//        {
//            [cell.videoView stopVideoPlay];
//        }
//    }
    
 
//    if (CGRectContainsRect(scrollView.frame, cellRect))
//    {
//        NSLog(@"Complete visible");
//
//    }
//    else
//    {
//        
//    }
    
    
}


- (BOOL)isIndexPathVisible:(NSIndexPath*)indexPath
{
    UICollectionViewLayoutAttributes *attributes = [postCollectinView layoutAttributesForItemAtIndexPath:indexPath];
    
    CGRect cellRect = attributes.frame;
    
    CGRect cellFrameInSuperview = [postCollectinView convertRect:cellRect toView:[postCollectinView superview]];
    
    NSLog(@"<><<><><<><%f",cellFrameInSuperview.origin.y);
    
    
    /*NSArray *visiblePaths = [postCollectinView indexPathsForVisibleItems];
    
    for (NSIndexPath *currentIndex in visiblePaths)
    {
        NSComparisonResult result = [currentIndex compare:currentIndex];
        
        if(result == NSOrderedSame)
        {
            NSLog(@"Visible");
            return YES;
        }
    }*/
    
    return YES;
}


#pragma mark - ActionSheet

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([[[self.postArr objectAtIndex:actionSheet.tag] objectForKey:@"userId"] intValue] == [[(APPDEL).userDetailDict objectForKey:@"userId"] intValue])
    {
        if(buttonIndex == 0)
        {
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:nil
                                                  message:@"Delete Post?"
                                                  preferredStyle:UIAlertControllerStyleAlert];
            
            alertController.view.tag = actionSheet.tag;
            
            UIAlertAction *yesAction = [UIAlertAction
                                        actionWithTitle:@"YES"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction *action)
                                        {
                                            
                                           [self deletePost:actionSheet.tag postId:[[self.postArr objectAtIndex:actionSheet.tag] valueForKey:@"postId"]];
                                        }];
            
            UIAlertAction *noAction = [UIAlertAction
                                       actionWithTitle:@"NO"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                           
                                       }];
            
            
            
            [alertController addAction:yesAction];
            
            [alertController addAction:noAction];
            
            
            [self presentViewController:alertController animated:YES completion:nil];
        }
        else if(buttonIndex == 1)
        {
            [self turnOffCommenting:actionSheet.tag params:[NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", [[self.postArr objectAtIndex:actionSheet.tag] objectForKey:@"postId"], @"postId", [[[self.postArr objectAtIndex:actionSheet.tag] objectForKey:@"commentflag"] intValue] == 0 ? @"1" : @"0", @"flag", nil]];
        }
        else if(buttonIndex == 2)
        {
            EditPostViewController *vc = [[EditPostViewController alloc] initWithNibName:@"EditPostViewController" bundle:nil];
            
            vc.postDict = [self.postArr objectAtIndex:actionSheet.tag];
            
            [self.navigationController pushViewController:vc animated:NO];
            
        }
        else if(buttonIndex == 3)
        {
            
        }
    }
    
    else
    {
        if (buttonIndex == 0)
        {
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:nil
                                                  message:nil
                                                  preferredStyle:UIAlertControllerStyleActionSheet];
            
            alertController.view.tag = actionSheet.tag;
            
            UIAlertAction *InappropriateAction = [UIAlertAction
                                                  actionWithTitle:@"Inappropriate Post"
                                                  style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction *action)
                                                  {
                                                      [self reportAbuse:[[self.postArr objectAtIndex:alertController.view.tag] valueForKey:@"postId"] secondryId:[[self.postArr objectAtIndex:alertController.view.tag] valueForKey:@"userId"] isInappropiate:@"1"];
                                                  }];
            
            UIAlertAction *SpamAction = [UIAlertAction
                                         actionWithTitle:@"Spam Account"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction *action)
                                         {
                                              [self reportAbuse:[[self.postArr objectAtIndex:alertController.view.tag] valueForKey:@"postId"] secondryId:[[self.postArr objectAtIndex:alertController.view.tag] valueForKey:@"userId"] isInappropiate:@"2"];
                                         }];
            
            UIAlertAction *resetAction = [UIAlertAction
                                          actionWithTitle:@"Cancel"
                                          style:UIAlertActionStyleCancel
                                          handler:^(UIAlertAction *action)
                                          {
                                              NSLog(@"Reset action");
                                          }];
            
            
            [alertController addAction:resetAction];
            
            [alertController addAction:InappropriateAction];
            
            [alertController addAction:SpamAction];
            
            
            [self presentViewController:alertController animated:YES completion:nil];
        }
        
        /*if ([[[self.postArr objectAtIndex:actionSheet.tag] valueForKey:@"type"] isEqualToString:@"image"])
        {
            UIImageView *localImg = [[UIImageView alloc] init];
            
            [localImg sd_setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [[self.postArr objectAtIndex:actionSheet.tag] valueForKey:@"media"] ] ] placeholderImage:nil];
           
            if(buttonIndex == 0)
            {
                [self postShareOnFacebookShare:localImg.image];
            }
            
            else if (buttonIndex == 1)
            {
                [self postShareOnMailShare:[NSString stringWithFormat:@"%@%@", ServiceURL, [[self.postArr objectAtIndex:actionSheet.tag] valueForKey:@"media"]]];
            }
            
            else if (buttonIndex == 2)
            {
                [self reportAbuse:[[self.postArr objectAtIndex:actionSheet.tag] valueForKey:@"postId"]];
            }
        }
        else
        {
            UIImageView *localImg = [[UIImageView alloc] init];
            
            [localImg sd_setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [[self.postArr objectAtIndex:actionSheet.tag] valueForKey:@"thumbnail"] ] ] placeholderImage:nil];
            
            
            if(buttonIndex == 0)
            {
                [self postShareOnFacebookShare:localImg.image];
            }
            
            else if (buttonIndex == 1)
            {
                 [self postShareOnMailShare:[NSString stringWithFormat:@"%@%@", ServiceURL, [[self.postArr objectAtIndex:actionSheet.tag] valueForKey:@"thumbnail"]]];
            }
            
            else if (buttonIndex == 2)
            {
                 [self reportAbuse:[[self.postArr objectAtIndex:actionSheet.tag] valueForKey:@"postId"]];
            }
        }*/
    }
}


#pragma mark - Mail Delegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}


#pragma mark - JDTagView Delegate

- (void)selectedTag:(JDTagView *)View
{
    ListCollectionViewCell *cell = (ListCollectionViewCell *)View.superview;
    
    UICollectionView *collectionView = (UICollectionView *)View.superview.superview;
    
    NSIndexPath *indexPath = [collectionView indexPathForCell:cell];
  
    NSArray *taggArr = [[self.postArr objectAtIndex:indexPath.row] objectForKey:@"tagpeople"];
    
    
    ProfileViewController *vc = [[ProfileViewController alloc] initWithNibName:@"ProfileViewController" bundle:nil];
    
    vc.differentUserId = [[taggArr objectAtIndex:View.tag-1] objectForKey:@"userId"];
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)deleteTagFromView:(JDTagView *)JDTagView
{
    
}


@end
