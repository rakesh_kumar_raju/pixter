//
//  CreateBusinessViewController.m
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 3/17/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#define ACCEPTABLE_CHARACTERS @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_."


#import "CreateBusinessViewController.h"
#import "ServiceHelper.h"
#import "KeyboardManager.h"



@interface CreateBusinessViewController ()<UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate, UITextFieldDelegate, UIAlertViewDelegate>
{
    NSString *base64ImgStr;
}


@end


@implementation CreateBusinessViewController

#pragma mark - ViewCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    base64ImgStr = @"";
    
    [[IQKeyboardManager sharedManager] setEnable:YES];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [self setUpView];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    
    createAccBtn.layer.cornerRadius = 25.0;
    
    createAccBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    
    createAccBtn.layer.borderWidth = 1.0;
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
}


#pragma mark - Void

- (void) setUpView
{
   
    userNameTf.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Username" attributes:@{NSForegroundColorAttributeName: [UIColor grayColor]}];
    
    fullNameTf.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Full Name or Business Name" attributes:@{NSForegroundColorAttributeName: [UIColor grayColor]}];
    

    
    
    for (UIView *view in contentView.subviews)
    {
        if ([view isKindOfClass:[UITextField class]])
        {
            UITextField *tf = (UITextField *)view;
            
            CALayer *bottomBorder = [CALayer layer];
            
            bottomBorder.frame = CGRectMake(0, view.frame.size.height - 5, view.frame.size.width-10, 1.0f);
            
            bottomBorder.backgroundColor = [UIColor whiteColor].CGColor;
            
            [tf.layer addSublayer: bottomBorder];
        }
    }
}


#pragma mark - myAccountWebservice

- (void)createBussinessWebService
{
    if(userNameTf.text.length == 0)
    {
        [[[UIAlertView alloc] initWithTitle:@"Error!" message:@"Please enter your username" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        
        return;
    }
    
    else
    {
        [self.view endEditing:YES];
        
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                [(APPDEL).userDetailDict objectForKey:@"userId"], @"userId",
                                userNameTf.text, @"userName",
                                fullNameTf.text, @"full_name",
                               [base64ImgStr stringByReplacingOccurrencesOfString:@"\n" withString:@""], @"profilePic",nil];
        
        
        [[ServiceHelper sharedManager] setHudShow:YES];
        
        [[ServiceHelper sharedManager] sendRequestWithMethodName:@"addanotherprofile" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
         {
             
             if (result)
             {
                 if ([[result objectForKey:@"status"] intValue] == 1)
                 {
                     [[[UIAlertView alloc] initWithTitle: nil message:[result objectForKey:@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
                 }
                 else
                 {
                     [[[UIAlertView alloc] initWithTitle: nil message:[result objectForKey:@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
                 }
                 
                 if (self.navigationController)
                 {
                        [self.navigationController popViewControllerAnimated:YES];
                 }
                 else
                 {
                     [self dismissViewControllerAnimated:YES completion:nil];
                 }
             }
         }];
    }
}



#pragma mark - Reduce Image quality

- (UIImage *)resizeImage:(UIImage *)image
{
    float actualHeight = image.size.height;
    
    float actualWidth = image.size.width;
    
    float maxHeight = 300.0;
    
    float maxWidth = 400.0;
    
    float imgRatio = actualWidth/actualHeight;
    
    float maxRatio = maxWidth/maxHeight;
    
    float compressionQuality = 0.5;//50 percent compression
    
    
    if (actualHeight > maxHeight || actualWidth > maxWidth)
    {
        if(imgRatio < maxRatio)
        {
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            
            actualWidth = imgRatio * actualWidth;
            
            actualHeight = maxHeight;
        }
        
        else if(imgRatio > maxRatio)
        {
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            
            actualHeight = imgRatio * actualHeight;
            
            actualWidth = maxWidth;
        }
        
        else
        {
            actualHeight = maxHeight;
            
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    
    
    UIGraphicsBeginImageContext(rect.size);
    
    
    [image drawInRect:rect];
    
    
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    
    
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    
    
    UIGraphicsEndImageContext();
    
    
    return [UIImage imageWithData:imageData];
}

#pragma mark - Camera

- (void)takePhotoFromCamera
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        
        picker.delegate = self;
        
        picker.allowsEditing = YES;
        
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        
        [self presentViewController:picker animated:YES completion:NULL];
    }
}


- (void)takePotoFromGallary
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    
    picker.delegate = self;
    
    picker.allowsEditing = YES;
    
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
}


#pragma mark - Action

- (IBAction)createAccountTap
{
    [self createBussinessWebService];
}


- (IBAction)backTap
{
    if (self.navigationController)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}


- (IBAction)tapGestOnUserPic:(UITapGestureRecognizer *)sender
{
    [[[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take photo", @"Choose existing photo", nil] showInView:self.view];
}


#pragma mark - ActionSheet

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0)
    {
        [self takePhotoFromCamera];
    }
    
    else if(buttonIndex == 1)
    {
        [self takePotoFromGallary];
    }
}


#pragma mark - Tetfield

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == userNameTf)
    {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        if ([string isEqualToString:filtered])
        {
            if(range.length + range.location > textField.text.length)
            {
                return NO;
            }
            
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            
            return newLength <= 30;
        }
        else
        {
            return NO;
        }
    }
    else
    {
        return YES;
    }
}



#pragma mark - ImgaePicker Delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    
    
    profileImg.image = [self resizeImage:chosenImage];
    
    
    NSData *data = UIImagePNGRepresentation(profileImg.image);
    
    
    base64ImgStr = [data base64EncodedStringWithOptions:
                    NSDataBase64Encoding64CharacterLineLength];
    
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}


#pragma mark - AlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == alertView.cancelButtonIndex)
    {
       
    }
}


@end
