//
//  ListCouponViewController.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 5/4/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface ListCouponViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UITableView *listCouponTbl;
}

- (IBAction)backTap;

@end
