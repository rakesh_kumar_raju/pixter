//
//  PeopleViewController.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 3/31/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddLocationViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate>
{
    IBOutlet UITableView *locationTbl;
    
    IBOutlet UISearchBar *searchBar;
    
    IBOutlet UIActivityIndicatorView *indicator;
}

- (IBAction)backTap;

@end
