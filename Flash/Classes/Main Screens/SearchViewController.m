//
//  SearchViewController.m
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 2/24/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#define Screen_width  [[UIScreen mainScreen] bounds].size.width

#define Screen_height  [[UIScreen mainScreen] bounds].size.height


typedef enum {
        SearchWithUser,
        SearchWithLocationAndUser,
} SearchType;


#import "SearchViewController.h"
#import "SerachTableViewCell.h"
#import "ProfileViewController.h"
#import "ServiceHelper.h"
#import "ViewPostBySearchViewController.h"
#import "SearchView.h"


@interface SearchViewController ()
{
    NSMutableArray *searchUserArr;

    NSString *searchStr;
    
    SearchView *topSearchView, *peopleSearchView, *tagSerchView, *locationSearchView;
    
    int searchType;
}

@end



@implementation SearchViewController


#pragma mark - ViewCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.hidden = YES;
    
    [self setUpView];
    
   
    searchType = 1;
    
    
    [self hideIndicator];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    
    [topBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [peopleBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    
    [tagBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    
    [locationBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    CGRect rect = searchBar.frame;
    
    UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, rect.size.width, 2)];
   
    lineView.backgroundColor = [UIColor blackColor];
    
    [searchBar addSubview:lineView];
    
    [searchBar performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:0];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
}


#pragma mark - Void

- (void) setUpView
{
//    refreshControl = [[UIRefreshControl alloc] init];
//    
//    refreshControl.tintColor = [UIColor redColor];
    
    //[refreshControl addTarget:self action:@selector(refreshTbl) forControlEvents:UIControlEventValueChanged];
    
    //searchUserTbl.alwaysBounceVertical = YES;
    
  
    searchUserArr = [NSMutableArray new];
    
    UIView *view = [searchBar.subviews objectAtIndex:0];
    
    for(UIView *subView in view.subviews)
    {
        if([subView isKindOfClass: [UITextField class]])
        {
            [(UITextField *)subView setKeyboardAppearance: UIKeyboardAppearanceDark];
            
            
            UITextField *searchField = (UITextField *)subView;
            
            searchField.backgroundColor = [UIColor grayColor];
            
            searchField.textColor = [UIColor whiteColor];
            
            searchField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Search"];
            
            UILabel *placeholderLabel = [searchField valueForKey:@"placeholderLabel"];
          
            placeholderLabel.textColor = [UIColor darkGrayColor];
        }
    }
}


- (void)addViewOnMainScreen
{
    topSearchView = [[[NSBundle mainBundle] loadNibNamed:@"SearchView" owner:self options:nil] lastObject];
    
    topSearchView.frame = CGRectMake(0, 0, Screen_width, Screen_height-149);
    
    
    peopleSearchView = [[[NSBundle mainBundle] loadNibNamed:@"SearchView" owner:self options:nil] lastObject];
    
    peopleSearchView.frame = CGRectMake(Screen_width, 0, Screen_width, Screen_height-149);
    
    
    
    tagSerchView = [[[NSBundle mainBundle] loadNibNamed:@"SearchView" owner:self options:nil] lastObject];
    
    tagSerchView.frame = CGRectMake(Screen_width+Screen_width, 0, Screen_width, Screen_height-149);
    
    
    locationSearchView = [[[NSBundle mainBundle] loadNibNamed:@"SearchView" owner:self options:nil] lastObject];
    
    locationSearchView.frame = CGRectMake(Screen_width+Screen_width+Screen_width, 0, Screen_width, Screen_height-149);
    
    
    
    [contentSv addSubview:topSearchView];
    
    [contentSv addSubview:peopleSearchView];
    
    [contentSv addSubview:tagSerchView];
    
    [contentSv addSubview:locationSearchView];
    
    
    contentSv.contentSize = CGSizeMake([[UIScreen mainScreen] bounds].size.width * 4, Screen_height-149);
}


#pragma mark - Indicator

- (void)showIndicator
{
    [indicator startAnimating];
    
    indicator.hidden = NO;
}


- (void)hideIndicator
{
    [indicator stopAnimating];
    
    indicator.hidden = YES;
}


#pragma mark - pull to refresh

- (void)refreshTbl
{
    //[self fetchUserByKeyWord:searchStr];
}


#pragma mark - Webservice


- (void)fetchUserByKeyWord:(NSString *)text
{
    //[refreshControl beginRefreshing];
    
   [self showIndicator];
    
    
    NSString *methodName = @"";
    
    NSDictionary *params;
    
    
    if (searchType == 1)
    {
        params = [NSDictionary dictionaryWithObjectsAndKeys:text, @"userName", [(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", [NSString stringWithFormat:@"%i", SearchWithLocationAndUser], @"typeFlag", nil];
        
        methodName = @"searchUser";
    }
    else if (searchType == 2)
    {
        params = [NSDictionary dictionaryWithObjectsAndKeys:text, @"userName", [(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", [NSString stringWithFormat:@"%i", SearchWithUser], @"typeFlag", nil];
        
        methodName = @"searchUser";
    }
    else if (searchType == 3)
    {
        params = [NSDictionary dictionaryWithObjectsAndKeys:text, @"hashTag", [(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", nil];
        
        methodName = @"searchHashtag";
    }
    else
    {
        params = [NSDictionary dictionaryWithObjectsAndKeys:text, @"location", [(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", nil];
        
        methodName = @"searchLocation";
    }
    
    
    //NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:text, @"userName", [(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", @"1", @"typeFlag", nil];
   
    
    [[ServiceHelper sharedManager] setHudShow:NO];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:methodName setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         [self hideIndicator];
         
         //[refreshControl endRefreshing];
         
         if (searchType == 1)
         {
             searchUserArr = [[result objectForKey:@"user"] mutableCopy];
         }
         else if (searchType == 2)
         {
             searchUserArr = [[result objectForKey:@"user"] mutableCopy];
         }
         else if (searchType == 3)
         {
             searchUserArr = [[result objectForKey:@"hashTag"] mutableCopy];
         }
         else
         {
            searchUserArr = [[result objectForKey:@"location"] mutableCopy];
         }
         
         
         
        /* if (contentSv.contentOffset.x == 0)
         {
             searchUserArr = [[result objectForKey:@"user"] mutableCopy];
             
             
             topSearchView.searchType = 1;
             
             [topSearchView setArr:searchUserArr];
             
             topSearchView.controller = self.navigationController;
             
             [topSearchView reloadTableView];
         }
         
         else if (contentSv.contentOffset.x == Screen_width)
         {
             searchUserArr = [[result objectForKey:@"user"] mutableCopy];
             
           
             peopleSearchView.searchType = 2;
             
             [peopleSearchView setArr:searchUserArr];
             
             peopleSearchView.controller = self.navigationController;
             
             [peopleSearchView reloadTableView];
         }
         
         else if (contentSv.contentOffset.x == Screen_width+Screen_width)
         {
             searchUserArr = [[result objectForKey:@"hashTag"] mutableCopy];
             
           
             tagSerchView.searchType = 3;
             
             [tagSerchView setArr:searchUserArr];
             
             tagSerchView.controller = self.navigationController;
             
             [tagSerchView reloadTableView];
         }
         else if (contentSv.contentOffset.x == Screen_width+Screen_width+Screen_width)
         {
             searchUserArr = [[result objectForKey:@"location"] mutableCopy];
            
             
             peopleSearchView.searchType = 4;
             
             [peopleSearchView setArr:searchUserArr];
             
             peopleSearchView.controller = self.navigationController;
             
             [peopleSearchView reloadTableView];
         }*/
         
         
         [searchUserTbl reloadData];
     }];
}


#pragma mark - Action

- (IBAction)backTap
{
     [self.navigationController popViewControllerAnimated:NO];
}


- (IBAction)tabbesButtonAction:(UIButton *)sender
{
    [searchUserArr removeAllObjects];
    
    [searchUserTbl reloadData];
    
    
    if (sender == topBtn && sender.selected == NO)
    {
        topBtn.selected = YES;
        
        peopleBtn.selected = NO;
        
        tagBtn.selected = NO;
        
        locationBtn.selected = NO;
        
        
        searchType = 1;
        
        [topBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [peopleBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        
        [tagBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        
        [locationBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        
        
        [self fetchUserByKeyWord:searchBar.text];
        
       // [contentSv setContentOffset:CGPointMake(0, 0) animated:YES];
    }
    
    else if (sender == peopleBtn && sender.selected == NO)
    {
        topBtn.selected = NO;
        
        peopleBtn.selected = YES;
        
        tagBtn.selected = NO;
        
        locationBtn.selected = NO;
        
        
        searchType = 2;
        
        
        [topBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        
        [peopleBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [tagBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        
        [locationBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        
        
        [self fetchUserByKeyWord:searchBar.text];
        
        //[contentSv setContentOffset:CGPointMake(Screen_width, 0) animated:YES];
    }
    
    else if (sender == tagBtn && sender.selected == NO)
    {
        topBtn.selected = NO;
        
        peopleBtn.selected = NO;
        
        tagBtn.selected = YES;
        
        locationBtn.selected = NO;
        
       
        searchType = 3;
        
        
        [topBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        
        [peopleBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        
        [tagBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [locationBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        
        
        [self fetchUserByKeyWord:searchBar.text];
        
        //[contentSv setContentOffset:CGPointMake(Screen_width + Screen_width, 0) animated:YES];
    }
    else if (sender == locationBtn && sender.selected == NO)
    {
        topBtn.selected = NO;
        
        peopleBtn.selected = NO;
        
        tagBtn.selected = NO;
        
        peopleBtn.selected = YES;
        
        
        searchType = 4;
        
      
        [topBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        
        [peopleBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        
        [tagBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        
        [locationBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        
        [self fetchUserByKeyWord:searchBar.text];
        
        //[contentSv setContentOffset:CGPointMake(Screen_width + Screen_width + Screen_width, 0) animated:YES];
    }
   
    animatImage.frame = CGRectMake(sender.frame.origin.x, animatImage.frame.origin.y, animatImage.frame.size.width, animatImage.frame.size.height);
}

#pragma mark - TableView Delegate


#pragma mark - TableView Delegate


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return searchUserArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"CELL";
    
    SerachTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    
    if (cell == nil)
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"SerachTableViewCell" owner:self options:nil] lastObject];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (searchType == 1)
    {
        if ([[[searchUserArr objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"location"])
        {
            cell.nameLbl.text = [NSString stringWithFormat:@"%@", [[[searchUserArr objectAtIndex: indexPath.row] objectForKey:@"full_name"] lowercaseString]];
            
            cell.totalPostLbl.text = [[searchUserArr objectAtIndex: indexPath.row] objectForKey:@"totalCount"];
            
            cell.totalPostLbl.textColor = [UIColor grayColor];
            
            cell.userImg.image = [UIImage imageNamed:@"search_location"];
        }
        else if ([[[searchUserArr objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"hashtag"])
        {
            cell.nameLbl.text = [NSString stringWithFormat:@"%@", [[[searchUserArr objectAtIndex: indexPath.row] objectForKey:@"full_name"] lowercaseString]];
            
            cell.totalPostLbl.text = [[searchUserArr objectAtIndex: indexPath.row] objectForKey:@"totalCount"];
            
            cell.totalPostLbl.textColor = [UIColor grayColor];
            
            cell.userImg.image = [UIImage imageNamed:@"hash"];
        }
        else
        {
            cell.nameLbl.text = [[[searchUserArr objectAtIndex: indexPath.row] objectForKey:@"full_name"] lowercaseString];
            
            cell.totalPostLbl.text = [[searchUserArr objectAtIndex: indexPath.row] objectForKey:@"fname"];
            
            cell.totalPostLbl.textColor = [UIColor grayColor];
            
            [cell.userImg sd_setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [[searchUserArr objectAtIndex:indexPath.row] valueForKey:@"profile_pic"] ] ] placeholderImage:[UIImage imageNamed:@"profile_demo"]];
        }
        
       
        return cell;
    }
   
    else if (searchType == 2)
    {
        if ([[[searchUserArr objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"location"])
        {
            cell.userImg.image = [UIImage imageNamed:@"search_location"];
        }
        else
        {
            [cell.userImg sd_setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [[searchUserArr objectAtIndex:indexPath.row] valueForKey:@"profile_pic"] ] ] placeholderImage:[UIImage imageNamed:@"profile_demo"]];
        }
        
        
        cell.nameLbl.text = [[[searchUserArr objectAtIndex:indexPath.row] valueForKey:@"full_name"] lowercaseString];
        
        cell.totalPostLbl.text = [[searchUserArr objectAtIndex: indexPath.row] objectForKey:@"fname"];

        cell.totalPostLbl.textColor = [UIColor grayColor];
        
        return cell;
    }
    
    else if (searchType == 3)
    {
        cell.userImg.image = [UIImage imageNamed:@"hash"];
        
        cell.nameLbl.text = [NSString stringWithFormat:@"%@", [[[searchUserArr objectAtIndex: indexPath.row] objectForKey:@"full_name"] lowercaseString]];
        
        cell.totalPostLbl.text = [[searchUserArr objectAtIndex: indexPath.row] objectForKey:@"totalCount"];
        
        cell.totalPostLbl.textColor = [UIColor grayColor];
        
        return cell;
    }
    
    else
    {
        cell.userImg.image = [UIImage imageNamed:@"search_location"];
        
        cell.nameLbl.text = [[[searchUserArr objectAtIndex:indexPath.row] valueForKey:@"full_name"] lowercaseString];
        
        cell.totalPostLbl.text = [[searchUserArr objectAtIndex: indexPath.row] objectForKey:@"totalCount"];
        
        cell.totalPostLbl.textColor = [UIColor grayColor];
        
        
        return cell;
        
    }
}


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (searchType == 1)
    {
        if ([[[searchUserArr objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"location"])
        {
            ViewPostBySearchViewController *vc = [[ViewPostBySearchViewController alloc] initWithNibName:@"ViewPostBySearchViewController" bundle:nil];
            
            vc.isSeacrhTagPeople = NO;
            
            vc.searchStr = [[searchUserArr objectAtIndex:indexPath.row] valueForKey:@"full_name"];
            
            [self.navigationController pushViewController:vc animated:YES];
        }
        else if ([[[searchUserArr objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"hashtag"])
        {
            ViewPostBySearchViewController *vc = [[ViewPostBySearchViewController alloc] initWithNibName:@"ViewPostBySearchViewController" bundle:nil];
            
            vc.isSeacrhTagPeople = YES;
            
            vc.searchStr = [[searchUserArr objectAtIndex:indexPath.row] valueForKey:@"full_name"];
            
            [self.navigationController pushViewController:vc animated:YES];
        }
        else
        {
            ProfileViewController *vc = [[ProfileViewController alloc] initWithNibName:@"ProfileViewController" bundle:nil];
            
            vc.differentUserId = [[searchUserArr objectAtIndex: indexPath.row] objectForKey:@"id"] ;
            
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
    
    else if (searchType == 2)
    {
        ProfileViewController *vc = [[ProfileViewController alloc] initWithNibName:@"ProfileViewController" bundle:nil];
        
        vc.differentUserId = [[searchUserArr objectAtIndex: indexPath.row] objectForKey:@"id"] ;
        
        [self.navigationController pushViewController:vc animated:YES];
    }
    
    else if (searchType == 3)
    {
        ViewPostBySearchViewController *vc = [[ViewPostBySearchViewController alloc] initWithNibName:@"ViewPostBySearchViewController" bundle:nil];
        
        vc.isSeacrhTagPeople = YES;
        
        vc.searchStr = [[searchUserArr objectAtIndex:indexPath.row] valueForKey:@"full_name"];
        
        [self.navigationController pushViewController:vc animated:YES];
    }
    
    else
    {
        ViewPostBySearchViewController *vc = [[ViewPostBySearchViewController alloc] initWithNibName:@"ViewPostBySearchViewController" bundle:nil];
        
        vc.isSeacrhTagPeople = NO;
        
        vc.searchStr = [[searchUserArr objectAtIndex:indexPath.row] valueForKey:@"full_name"];
        
        [self.navigationController pushViewController:vc animated:YES];

    }
}



#pragma mark - ScrolView

/*- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    animatImage.frame = CGRectMake(scrollView.contentOffset.x/4, animatImage.frame.origin.y, animatImage.frame.size.width, animatImage.frame.size.height);
    
    
    if (scrollView.contentOffset.x == 0)
    {
        searchType = 1;
        
        [topBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        
        [peopleBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        [tagBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        [locationBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
       
        [self fetchUserByKeyWord:searchBar.text];
        
        
        topSearchView.searchType = 1;
        
        [topSearchView setArr:searchUserArr];
        
        topSearchView.controller = self.navigationController;
        
        //[topSearchView reloadTableView];
    }
    
    else if (scrollView.contentOffset.x == Screen_width)
    {
        searchType = 2;
        
        
        [topBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        [peopleBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        
        [tagBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        [locationBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        
        [self fetchUserByKeyWord:searchBar.text];
        
       
        peopleSearchView.searchType = 2;
       
        [peopleSearchView setArr:searchUserArr];
        
        peopleSearchView.controller = self.navigationController;
        
        //[peopleSearchView reloadTableView];
    }
    
    else if (scrollView.contentOffset.x == Screen_width+Screen_width)
    {
        searchType = 3;
        
        
        [topBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        [peopleBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        [tagBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        
        [locationBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        
        [self fetchUserByKeyWord:searchBar.text];
        
       
        tagSerchView.searchType = 3;
        
        
        [tagSerchView setArr:searchUserArr];
        
        tagSerchView.controller = self.navigationController;
        
        //[tagSerchView reloadTableView];
    }
    
    else if (scrollView.contentOffset.x == Screen_width+Screen_width+Screen_width)
    {
        searchType = 4;
        
        [topBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        [peopleBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        [tagBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        [locationBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        
       
        [self fetchUserByKeyWord:searchBar.text];
        
        
        locationSearchView.searchType = 3;
        
        
        [locationSearchView setArr:searchUserArr];
        
        locationSearchView.controller = self.navigationController;
        
       // [locationSearchView reloadTableView];
    }
}*/



#pragma mark - SerachBar Delegate


- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self.navigationController popViewControllerAnimated:NO];
}


- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    NSLog(@"%@", searchText);
    
    if (!searchText.length)
    {
        searchStr = @"";
        
        //[refreshControl removeFromSuperview];
        
        [searchUserArr removeAllObjects];
        
        
        [topSearchView setArr:searchUserArr];
        
        [topSearchView reloadTableView];
        
        
//        [peopleSearchView setArr:searchUserArr];
//        
//        [peopleSearchView reloadTableView];
        
        [searchUserTbl reloadData];
    }
    else
    {
        searchStr = searchText;

        //[searchUserTbl addSubview:refreshControl];
        
        [self fetchUserByKeyWord:searchText];
    }
}



- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar1
{
    [self fetchUserByKeyWord:searchBar1.text];
    
    [self.view endEditing:YES];
}


@end
