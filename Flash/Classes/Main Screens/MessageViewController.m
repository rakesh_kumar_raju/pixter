//
//  MessageViewController.m
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 1/2/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//


#import "MessageViewController.h"
#import "MessageTableViewCell.h"
#import "ShowAllUserViewController.h"
#import "SendMessageViewController.h"
#import "ServiceHelper.h"


@interface MessageViewController ()
{
    NSMutableArray *messageArr, *deleteMessageArr;
    
    NSString *deletePeopleJsonStr;
}

@end



@implementation MessageViewController


#pragma mark - ViewCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setUpView];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
   
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)])
    {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    
    if (self.isVCForMessage)
    {
        titleLbl.text = @"Messages";
        
        //tblBottom.constant = 51;
        
        newMsgBtn.hidden = YES;
        
        tblBottom.constant = 0;
        
        messageArr = [NSMutableArray new];
        
        [self getMessagesWebServices];
    }
    else
    {
        titleLbl.text = @"ADVERTISING";
        
        newMsgBtn.hidden = YES;
        
        tblBottom.constant = 0;
    }
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    
    [deleteMessageArr removeAllObjects];
    
    [UIView animateWithDuration:0.2 animations:^{
        
        self.tabBarController.tabBar.frame = CGRectMake(0, [[UIScreen mainScreen] bounds].size.height-self.tabBarController.tabBar.frame.size.height, self.tabBarController.tabBar.frame.size.width, self.tabBarController.tabBar.frame.size.height);
    }
                     completion:^(BOOL finished)
     {
         
     }];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
}


#pragma mark - Void

- (void) setUpView
{
    refreshControl = [[UIRefreshControl alloc] init];
    
    refreshControl.tintColor = [UIColor whiteColor];
    
    [refreshControl addTarget:self action:@selector(getMessagesWebServices) forControlEvents:UIControlEventValueChanged];
    
    [universalTable addSubview:refreshControl];
    
    universalTable.alwaysBounceVertical = YES;
    
    
    deleteMessageArr = [NSMutableArray new];
    
    deletePeopleJsonStr = @"";
    
    
     universalTable.allowsMultipleSelectionDuringEditing = YES;
    
    
}


#pragma mark - SetDateFormat

- (NSString *)setDateFormat:(NSString *)str
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    dateFormatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    
    [dateFormatter setDateFormat: @"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *myDate = [dateFormatter dateFromString:str];
    
    
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    
    dateFormatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    
    [dateFormatter2 setDateFormat:@"d MMM YYYY, hh:mm"];
    
    return  [dateFormatter2 stringFromDate:myDate];
}



#pragma mark - WebServices

- (void)getMessagesWebServices
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", nil];
    
    
    [[ServiceHelper sharedManager] setHudShow:NO];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"viewAllMessage" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
             messageArr = [[result objectForKey:@"message"] mutableCopy];
             
             [messageArr addObject:[UIView new]];
             
             if (!messageArr.count)
             {
                 editBtn.hidden = YES;
             }
             else
             {
                 editBtn.hidden = NO;
             }
             
             [refreshControl endRefreshing];
             
             [universalTable reloadData];
         }
     }];
}


- (void)deleteMessages:(NSDictionary *)params
{
   [[ServiceHelper sharedManager] setHudShow:NO];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"deleteChat" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
             [self getMessagesWebServices];
             
             [editBtn setTitle:@"Edit" forState:UIControlStateNormal];
             
             [plusBtn setTitle:@"New" forState:UIControlStateNormal];
             
             [universalTable setEditing:NO animated:YES];
             
             [self getMessagesWebServices];
         }
     }];
}



#pragma mark - Action

- (IBAction)backTap
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)editTap
{
//    if ([editBtn.titleLabel.text isEqualToString:@"Edit"] || [editBtn.titleLabel.text isEqualToString:@"Cancel"])
//    {
        if (universalTable.isEditing == YES)
        {
            [universalTable setEditing:NO animated:YES];
            
            [editBtn setTitle:@"Edit" forState:UIControlStateNormal];
            
            [messageArr addObject:[UIView new]];
        }
        else
        {
            [universalTable setEditing:YES animated:YES];
            
            [editBtn setTitle:@"Cancel" forState:UIControlStateNormal];
            
            [messageArr removeLastObject];
        }
    //}
    /*else
    {
        
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:nil
                                              message:@"Delete Message?"
                                              preferredStyle:UIAlertControllerStyleAlert];
        
      
        UIAlertAction *yesAction = [UIAlertAction
                                    actionWithTitle:@"YES"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction *action)
                                    {
                                        
                                        NSError * err;
                                        
                                        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:deleteMessageArr options:0 error:&err];
                                        
                                        
                                        deletePeopleJsonStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                                        
                                        NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", deletePeopleJsonStr, @"secondaryId", nil];
                                        
                                        
                                        [self deleteMessages:dict];
                                    }];
        
        UIAlertAction *noAction = [UIAlertAction
                                   actionWithTitle:@"NO"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       
                                   }];
        
        
        
        [alertController addAction:yesAction];
        
        [alertController addAction:noAction];
        
        
        [self presentViewController:alertController animated:YES completion:nil];
    }*/
    
    [universalTable reloadData];
}


- (IBAction)newMsgTap
{
    if (![plusBtn.titleLabel.text isEqualToString:@"Delete"])
    {
        [universalTable setEditing:NO animated:YES];
        
        [editBtn setTitle:@"Edit" forState:UIControlStateNormal];
        
        ShowAllUserViewController *vc = [[ShowAllUserViewController alloc] initWithNibName:@"ShowAllUserViewController" bundle:nil];
        
        [self.navigationController pushViewController:vc animated:YES];
    }
    else
    {
        
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:nil
                                              message:@"Delete Message?"
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        
        UIAlertAction *yesAction = [UIAlertAction
                                    actionWithTitle:@"YES"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction *action)
                                    {
                                        
                                        NSError * err;
                                        
                                        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:deleteMessageArr options:0 error:&err];
                                        
                                        
                                        deletePeopleJsonStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                                        
                                        NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", deletePeopleJsonStr, @"secondaryId", nil];
                                        
                                        
                                        [self deleteMessages:dict];
                                    }];
        
        UIAlertAction *noAction = [UIAlertAction
                                   actionWithTitle:@"NO"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       
                                       [deleteMessageArr removeAllObjects];
                                       
                                       [plusBtn setTitle:@"New" forState:UIControlStateNormal];
                                      
                                       [editBtn setTitle:@"Edit" forState:UIControlStateNormal];
                                       
                                       [universalTable setEditing:NO animated:YES];
                                   }];
        
        
        
        [alertController addAction:yesAction];
        
        [alertController addAction:noAction];
        
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
    
}

#pragma mark - TableView Delegate


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.isVCForMessage)
    {
        return messageArr.count;
    }
    
    else
    {
        return 0;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 95;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"CELL";
    
    MessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
   
    if (cell == nil)
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"MessageTableViewCell" owner:self options:nil] lastObject];
    }

    if ([messageArr[indexPath.row] isKindOfClass:[UIView class]])
    {
        cell.nameLbl.text = @"Welcome to Paracle";
        
        cell.nameLbl.font = [UIFont boldSystemFontOfSize:18];
        
        cell.nameLblTop.constant = 20;
    }
    else
    {
         cell.nameLbl.font = [UIFont systemFontOfSize:15];
        
        if (universalTable.isEditing == YES)
        {
            cell.selectionStyle = UITableViewCellSelectionStyleDefault;
            
            UIView *backView = [[UIView alloc] initWithFrame:cell.frame];
            
            backView.backgroundColor = [UIColor blackColor];
            
            cell.selectedBackgroundView = backView;
        }
        else
        {
            cell.selectionStyle = UITableViewCellEditingStyleNone;
        }
        
        cell.nameLblTop.constant = 10;
        
        [cell.profileImg sd_setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [[messageArr objectAtIndex:indexPath.row] valueForKey:@"profilePic"] ] ] placeholderImage:[UIImage imageNamed:@"profile_demo"]];
        
        cell.nameLbl.text = [[[messageArr objectAtIndex:indexPath.row] valueForKey:@"full_name"] lowercaseString];
        
        cell.messageLbl.text = [[messageArr objectAtIndex:indexPath.row] valueForKey:@"message"];
        
        cell.timeLbl.text = [self setDateFormat:[[messageArr objectAtIndex:indexPath.row] objectForKey:@"datetime"]];
    }
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.isVCForMessage)
    {
        if (universalTable.isEditing == NO)
        {
            if (indexPath.row == messageArr.count-1)
            {
                [tableView deselectRowAtIndexPath:indexPath animated:YES];
                
                HelpViewViewController *vc = [[HelpViewViewController alloc] initWithNibName:@"HelpViewViewController" bundle:nil];
                
                [self presentViewController:vc animated:YES completion:nil];
            }
            else
            {
                SendMessageViewController *vc = [[SendMessageViewController alloc] initWithNibName:@"SendMessageViewController" bundle:nil];
                
                vc.userNameStr = [[[messageArr objectAtIndex:indexPath.row] valueForKey:@"full_name"] lowercaseString];
                
                vc.userIdStr = [[messageArr objectAtIndex:indexPath.row] valueForKey:@"userId"];
                
                vc.profileUrlStr = [[messageArr objectAtIndex:indexPath.row] valueForKey:@"profilePic"];
                
                [self.navigationController pushViewController:vc animated:YES];
            }
        }
        else
        {
            [deleteMessageArr addObject:[NSDictionary dictionaryWithObjectsAndKeys:[[messageArr objectAtIndex:indexPath.row] objectForKey:@"userId"], @"id", nil]];
            
            NSLog(@"%@", deleteMessageArr);
            
            //
            if (deleteMessageArr.count)
            {
                [plusBtn setTitle:@"Delete" forState:UIControlStateNormal];
                
            }
            else
            {
                [plusBtn setTitle:@"New" forState:UIControlStateNormal];
            }
        }
    }
}


- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
    
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:[[messageArr objectAtIndex:indexPath.row] objectForKey:@"userId"], @"id", nil];
    
    if ([deleteMessageArr containsObject:dict])
    {
        [deleteMessageArr removeObject:dict];
        
        NSLog(@"%@", deleteMessageArr);

    }
    
    
    //
    if (deleteMessageArr.count)
    {
        [plusBtn setTitle:@"Delete" forState:UIControlStateNormal];
    }
    else
    {
        [plusBtn setTitle:@"New" forState:UIControlStateNormal];
    }
}



@end
