//
//  PushnotificationViewController.m
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 5/4/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import "PushnotificationViewController.h"
#import "ServiceHelper.h"

@interface PushnotificationViewController ()
{
    NSMutableDictionary *notificationDict;
    
    NSDictionary *userNotificationDict;
}

@end


@implementation PushnotificationViewController


#pragma mark - ViewCycle


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    notificationDict = [[NSMutableDictionary alloc] init];
    
    [self getAllNotifications];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [self setUpView];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
}


#pragma mark - Void


- (void)setUpView
{
    NSLog(@"%@", (APPDEL).userDetailDict);
}


#pragma mark - Notification content


- (void)notificationContent:(NSDictionary *)dict
{
    NSString *forZero, *forOne, *forTwo;
    
    //Point
    
    forZero = @"0";
    forOne = @"0";
    forTwo = @"0";
    
    if ([[dict objectForKey:@"point_notification"] intValue] == 0)
    {
       forZero = @"1";
    }
    else if ([[dict objectForKey:@"point_notification"] intValue] == 1)
    {
        forOne = @"1";
    }
    else
    {
        forTwo = @"1";
    }
    
    [notificationDict setObject:@[@{@"title":@"Off", @"isSelect" :forTwo},
                                  @{@"title":@"On", @"isSelect" :forZero}] forKey:@"POINTS"];
    //Reward
    
    forZero = @"0";
    forOne = @"0";
    forTwo = @"0";
    
    if ([[dict objectForKey:@"reward_notification"] intValue] == 0)
    {
        forZero = @"1";
    }
    else if ([[dict objectForKey:@"reward_notification"] intValue] == 1)
    {
        forOne = @"1";
    }
    else
    {
        forTwo = @"1";
    }
    
    [notificationDict setObject:@[@{@"title":@"Off", @"isSelect" :forTwo},
                                  @{@"title":@"On", @"isSelect" :forZero}] forKey:@"REWARDS"];
    
    //Like
    
    forZero = @"0";
    forOne = @"0";
    forTwo = @"0";
    
    if ([[dict objectForKey:@"like_notification"] intValue] == 0)
    {
        forZero = @"1";
    }
    else if ([[dict objectForKey:@"like_notification"] intValue] == 1)
    {
        forOne = @"1";
    }
    else
    {
        forTwo = @"1";
    }

    [notificationDict setObject:@[@{@"title":@"Off", @"isSelect" :forTwo},
                                  @{@"title":@"From People I Follow", @"isSelect" :forOne},
                                  @{@"title":@"From Everyone", @"isSelect" :forZero}] forKey:@"LIKES"];
    //Comment
    
    forZero = @"0";
    forOne = @"0";
    forTwo = @"0";
    
    if ([[dict objectForKey:@"comment_notification"] intValue] == 0)
    {
        forZero = @"1";
    }
    else if ([[dict objectForKey:@"comment_notification"] intValue] == 1)
    {
        forOne = @"1";
    }
    else
    {
        forTwo = @"1";
    }
    
    [notificationDict setObject:@[@{@"title":@"Off", @"isSelect" :forTwo},
                                  @{@"title":@"From People I Follow", @"isSelect" :forOne},
                                  @{@"title":@"From Everyone", @"isSelect" :forZero}] forKey:@"COMMENT"];
    
    //comment_like
    
    forZero = @"0";
    forOne = @"0";
    forTwo = @"0";
    
    if ([[dict objectForKey:@"comment_like_notification"] intValue] == 0)
    {
        forZero = @"1";
    }
    else if ([[dict objectForKey:@"comment_like_notification"] intValue] == 1)
    {
        forOne = @"1";
    }
    else
    {
        forTwo = @"1";
    }
    
    [notificationDict setObject:@[@{@"title":@"Off", @"isSelect" :forTwo},
                                  @{@"title":@"From People I Follow", @"isSelect" :forOne},
                                  @{@"title":@"From Everyone", @"isSelect" :forZero}] forKey:@"COMMENT LIKE"];
    
    //follow_request
    
    forZero = @"0";
    forOne = @"0";
    forTwo = @"0";
    
    if ([[dict objectForKey:@"follow_request_notification"] intValue] == 0)
    {
        forZero = @"1";
    }
    else if ([[dict objectForKey:@"follow_request_notification"] intValue] == 1)
    {
        forOne = @"1";
    }
    else
    {
        forTwo = @"1";
    }
    
    [notificationDict setObject:@[@{@"title":@"Off", @"isSelect" :forTwo}, @{@"title":@"From Everyone", @"isSelect" :forZero}] forKey:@"FOLLOW REQUEST"];
    
    
    //follower_request
    
    forZero = @"0";
    forOne = @"0";
    forTwo = @"0";

    
    if ([[dict objectForKey:@"follower_request_notification"] intValue] == 0)
    {
        forZero = @"1";
    }
    else if ([[dict objectForKey:@"follower_request_notification"] intValue] == 1)
    {
        forOne = @"1";
    }
    else
    {
        forTwo = @"1";
    }
    
    
    [notificationDict setObject:@[@{@"title":@"Off", @"isSelect" :forTwo}, @{@"title":@"From Everyone", @"isSelect" :forZero}] forKey:@"FOLLOWER REQUEST"];
    
    
    /*notificationDict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                        @[@{@"title":@"Off", @"isSelect" :[dict objectForKey:@"point_notification"]},
                        @{@"title":@"On", @"isSelect" :[dict objectForKey:@"point_notification"]}], @"POINTS",
                       
                        @[@{@"title":@"Off", @"isSelect" :[dict objectForKey:@"reward_notification"]},
                        @{@"title":@"On", @"isSelect" :[dict objectForKey:@"reward_notification"]}], @"REWARDS",
                      
                        @[@{@"title":@"Off", @"isSelect" :[dict objectForKey:@"like_notification"]},
                        @{@"title":@"From People I Follow", @"isSelect" :[dict objectForKey:@"like_notification"]},
                        @{@"title":@"From Everyone", @"isSelect" :[dict objectForKey:@"like_notification"]}], @"LIKES",
                       
                        @[@{@"title":@"Off", @"isSelect" :[dict objectForKey:@"comment_notification"]},
                        @{@"title":@"From People I Follow", @"isSelect" :[dict objectForKey:@"comment_notification"]},
                        @{@"title":@"From Everyone", @"isSelect" :[dict objectForKey:@"comment_notification"]}], @"COMMENTS",
                       
                        @[@{@"title":@"Off", @"isSelect" :[dict objectForKey:@"comment_like_notification"]},
                        @{@"title":@"From People I Follow", @"isSelect" :[dict objectForKey:@"comment_like_notification"]},
                          @{@"title":@"From Everyone", @"isSelect" :[dict objectForKey:@"comment_like_notification"]}], @"COMMENT LIKES",
                       
                        @[@{@"title":@"Off", @"isSelect" :[dict objectForKey:@"follow_request_notification"]}, @{@"title":@"From Everyone", @"isSelect" :[dict objectForKey:@"follow_request_notification"]}], @"FOLLOW REQUESTS",
                      
                        @[@{@"title":@"Off", @"isSelect" :[dict objectForKey:@"follower_request_notification"]}, @{@"title":@"From Everyone", @"isSelect" :[dict objectForKey:@"follower_request_notification"]}], @"FOLLOWER REQUESTS", nil];*/
    
    
    
    
    [notificationTbl reloadData];
}


#pragma mark - Webservice


- (void)getAllNotifications
{
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"getNotificationFlag" setHTTPMethod:@"POST" params:[NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userId",   nil] completion:^(NSDictionary *result)
     {
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 [self notificationContent:[result objectForKey:@"notification"]];
                 
                 userNotificationDict = [result objectForKey:@"notification"];
             }
         }
         
     }];

}

- (void)updateNotification:(NSString *)methodName value:(NSString *)value
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userId",value, methodName , methodName, @"type", nil];
    
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"updateNotificationFlag" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 [self notificationContent:[result objectForKey:@"notification"]];
                 
                 userNotificationDict = [result objectForKey:@"notification"];
                 
                 [self getAllNotifications];
             }
         }
         
     }];
}


#pragma mark - Action

- (IBAction)backTap
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - UitableView


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [[notificationDict allKeys] objectAtIndex:section];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [notificationDict allKeys].count;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *keyStr = [[notificationDict allKeys] objectAtIndex:section];
    
    NSArray *arr = [notificationDict objectForKey:keyStr];
    
    return arr.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID = @"CELLID";
    
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    cell.backgroundColor = [UIColor blackColor];
    
    
    NSString *keyStr = [[notificationDict allKeys] objectAtIndex:indexPath.section];
    
    NSArray *arr = [notificationDict objectForKey:keyStr];
    
    
    if ([[[arr objectAtIndex:indexPath.row] objectForKey:@"isSelect"] intValue] == 1)
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
       cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    cell.textLabel.font = [UIFont systemFontOfSize:16];
    
    cell.textLabel.text = [[arr objectAtIndex:indexPath.row] objectForKey:@"title"];
    
    cell.textLabel.textColor = [UIColor whiteColor];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *keyStr = [[notificationDict allKeys] objectAtIndex:indexPath.section];
    
    
    NSString *methodName;
    
    NSString *value;
    
    if ([keyStr isEqualToString:@"POINTS"])
    {
        if (indexPath.row == 0)
        {
            value = @"2";
        }
        
        else
        {
            value = @"0";
        }
        
        methodName = @"point_notification";
     }
   
    else if ([keyStr isEqualToString:@"COMMENT LIKE"])
    {
        if (indexPath.row == 0)
        {
            value = @"2";
        }
        
        else if (indexPath.row == 1)
        {
            value = @"1";
        }
        else
        {
            value = @"0";
        }
        
        methodName = @"comment_like_notification";
    }
   
    else if ([keyStr isEqualToString:@"COMMENT"])
    {
        if (indexPath.row == 0)
        {
            value = @"2";
        }
        
        else if (indexPath.row == 1)
        {
            value = @"1";
        }
        else
        {
            value = @"0";
        }
        
        methodName = @"comment_notification";
    }
 
    else if ([keyStr isEqualToString:@"FOLLOWER REQUEST"])
    {
        if (indexPath.row == 0)
        {
            value = @"2";
        }
        
        else
        {
            value = @"0";
        }
        
         methodName = @"follower_request_notification";
    }
    else if ([keyStr isEqualToString:@"LIKES"])
    {
        if (indexPath.row == 0)
        {
            value = @"2";
        }
        
        else if (indexPath.row == 1)
        {
            value = @"1";
        }
        else
        {
            value = @"0";
        }
        
         methodName = @"like_notification";
    }
    else if ([keyStr isEqualToString:@"REWARDS"])
    {
        if (indexPath.row == 0)
        {
            value = @"2";
        }
        
        else
        {
            value = @"0";
        }
        
        methodName = @"reward_notification";
    }
    else if ([keyStr isEqualToString:@"FOLLOW REQUEST"])
    {
        if (indexPath.row == 0)
        {
            value = @"2";
        }
        
        else
        {
            value = @"0";
        }
        
        methodName = @"follow_request_notification";
    }

  
    NSLog(@"%@<><><>%@", value, methodName);
    
    [self updateNotification:methodName value:value];
}


@end
