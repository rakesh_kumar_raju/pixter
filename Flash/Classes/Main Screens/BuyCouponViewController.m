//
//  BuyCouponViewController.m
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 3/8/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//


#import "BuyCouponViewController.h"
#import "ServiceHelper.h"

@interface BuyCouponViewController ()
{
    
}
@end


@implementation BuyCouponViewController


#pragma mark - ViewCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.hidden = YES;
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)])
    {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }

    
    [self setUpView];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    NSLog(@"%@", (APPDEL).userDetailDict);
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    
    for (UIView *view in self.view.subviews)
    {
        if ([view isKindOfClass:[UITextField class]])
        {
            CALayer *bottomBorder = [CALayer layer];
            
            bottomBorder.frame = CGRectMake(0, view.frame.size.height - 8, view.frame.size.width-10, 1.0f);
            
            bottomBorder.backgroundColor = [UIColor darkGrayColor].CGColor;
            
            [view.layer addSublayer: bottomBorder];
        }
    }
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
}


#pragma mark - Void

- (void) setUpView
{
    titleLbl.text = self.couponName;
    
    userNameTf.text = [(APPDEL).userDetailDict objectForKey:@"fullName"];
    
    emailTf.text = [(APPDEL).userDetailDict objectForKey:@"emailId"];
    
    phoneTf.text = [(APPDEL).userDetailDict objectForKey:@"phonenumber"];
}


#pragma mark - Action

- (IBAction)backTap
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)doneTap
{
    if (!emailTf.text.length || !userNameTf.text.length || !phoneTf.text.length)
    {
        [[[UIAlertView alloc] initWithTitle:@"Oops!" message:[NSString allEmptyTxtFields] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
    
    else if (![NSString stringIsValidEmail:emailTf.text])
    {
        [[[UIAlertView alloc] initWithTitle:@"Oops!" message:[NSString emailFormatNotCorrect] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
    
    else
    {
        [self applyCouponWebService];
    }
}



#pragma mark - webService

- (void)applyCouponWebService
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userid", self.couponId, @"couponid", userNameTf.text, @"username", emailTf.text, @"email", phoneTf.text, @"phone", nil];
    
    
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"applyCoupon" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if ([[result objectForKey:@"status"] intValue] == 1)
         {
             [self.navigationController popViewControllerAnimated:YES];
         }
     }];
    
}


#pragma mark - TextField delegate


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (userNameTf == textField)
    {
        [emailTf becomeFirstResponder];
    }
    
    else if (emailTf == textField)
    {
        [phoneTf becomeFirstResponder];
    }
    else
    {
        [self.view endEditing:YES];
    }
    return YES;
}
@end
