//
//  HomeViewController.m
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 1/2/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#define RestOfElementSize_insted_of_imageSize  140

#define hashtag_color [UIColor colorWithRed:18.0/255.0 green:86.0/255.0 blue:136.0/255.0 alpha:1.0]

#import "UIScrollView+BottomRefreshControl.h"

#import "HomeViewController.h"
#import "MessageViewController.h"
#import "SearchViewController.h"
#import "UserCollectionViewCell.h"
#import "ShowPostsViewController.h"
#import "ListCollectionViewCell.h"
#import "ListCollectionImageCell.h"
#import "ListCollectionViewVideoCell.h"
#import "AddCommentViewController.h"
#import "ViewLikesViewController.h"
#import "ProfileViewController.h"
#import "EditPostViewController.h"
#import "EditProfileViewController.h"
#import "WelcomeViewController.h"
#import "ViewPostBySearchViewController.h"
#import "JDTagView.h"

#import "ZOWVideoView.h"
#import "InstagramVideoView.h"

#import "ResponsiveLabel.h"

#import "ServiceHelper.h"


//static NSString *const AdUnitID = @"ca-app-pub-8270141017539622/1576966794";

static NSString *const AdUnitID = @"ca-app-pub-9153505799578900/1872128070";


@interface HomeViewController ()<UIActionSheetDelegate, JDTagViewDelegate, GADRewardBasedVideoAdDelegate>
{
   // NSDictionary *userDict;
    
    NSMutableArray *postArr, *latestDataArr;
    
    BOOL isGridView, isScrolling, pointsDouble, scrolToTopWhenImagePost; /*, ifUserGetRewarded, wasPlayRewardVideo;*/
    
    NSIndexPath *indexpath;
    
    NSMutableArray *videoPlayerObj;
    
    int rewardPoint, totalAdds, userID;
    
    
    /// List of ads remaining to be preloaded.
    NSMutableArray<GADNativeExpressAdView *> *adsToLoad;
  
    /// Mapping of GADNativeExpressAdView ads to their load state.
    NSMutableDictionary<NSString *, NSNumber *> *loadStateForAds;
  
    /// A Native Express ad is placed in the UITableView once per adInterval.
    NSInteger adInterval;
    
    UIRefreshControl *refreshControlBottom;
    
    UIButton *floatButton;
    
    int doubleDouble;
    
    
}



@end


@implementation HomeViewController



#pragma mark - ViewCycle


- (void)viewDidLoad
{
    [super viewDidLoad];

    
    [GADRewardBasedVideoAd sharedInstance].delegate = self;
    
    [self loadAdd];
    
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"first_time_installedApp"] == NO)
    {
        [self showPopUpWhileFirstTimeInstalledApp];
    }
    
    
    
    adInterval = 10;
    
    rewardPoint = 0;
    
    totalAdds = 0;
    
    pointsDouble = NO;
    
//    ifUserGetRewarded = NO;
//    
//    wasPlayRewardVideo = NO;

    
    isGridView = NO;
    
    
    postArr = [NSMutableArray new];
    
    latestDataArr = [NSMutableArray new];
    
    videoPlayerObj = [NSMutableArray new];
    
    
    refreshControl = [[UIRefreshControl alloc] init];
   
    refreshControl.tintColor = [UIColor whiteColor];
    
    [refreshControl addTarget:self action:@selector(pullToRefrshView) forControlEvents:UIControlEventValueChanged];
   
    [userCollectionView addSubview:refreshControl];
    
    userCollectionView.alwaysBounceVertical = YES;
    
    
    refreshControlBottom = [UIRefreshControl new];
   
    refreshControlBottom.triggerVerticalOffset = 10;
   
    [refreshControlBottom addTarget:self action:@selector(pullToRefrshViewFromBottom) forControlEvents:UIControlEventValueChanged];
   
    userCollectionView.bottomRefreshControl = refreshControlBottom;
    
   
    [self setUpView];
}


- (void)viewWillAppear:(BOOL)animated
{
    scrolToTopWhenImagePost = NO;
    
    NSLog(@"%@", self.navigationController.viewControllers);
    
    NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:@"message_count"];
    
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"not_open_message_Box_first_time"] == NO)
    {
        messgeCountLbl.hidden = NO;
        
        messgeCountLbl.text = @"1";
    }
    else
    {
        if ([str isEqualToString:@"0"])
        {
            messgeCountLbl.hidden = YES;
        }
        else
        {
            messgeCountLbl.hidden = NO;
            
            messgeCountLbl.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"message_count"];
        }
    }
    
    self.navigationController.navigationBar.hidden = YES;
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    
    CGRect rect = searchBar.frame;
    
    UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, rect.size.width, 2)];
    
    lineView.backgroundColor = [UIColor blackColor];
    
    [searchBar addSubview:lineView];
    
    
    [[SDImageCache sharedImageCache] setShouldDecompressImages:NO];
    
    [[SDWebImageDownloader sharedDownloader] setShouldDecompressImages:NO];
    
    
    
    //Scheme when switch profile
    
    if (userID != [[(APPDEL).userDetailDict objectForKey:@"userId"] intValue])
    {
        [postArr removeAllObjects];
        
        [latestDataArr removeAllObjects];
        
        [self viewUsersPostsWebserviceService:YES count:[NSString stringWithFormat:@"%i", 0] totalCount:@""];
    }
    
    //    if (postArr.count)
    //    {
    //        [postArr removeAllObjects];
    //    }
    
    
    if (postArr.count)
    {
        /*NSMutableArray *testArr = [NSMutableArray new];
        
        for (id object in postArr)
        {
            if ([object isKindOfClass:[NSDictionary class]])
            {
                [testArr addObject:@"1"];
            }
        }
        
        
        NSLog(@"%i", (int)testArr.count);*/
    
        
        
        [self viewUsersPostsWebserviceService:YES count:[NSString stringWithFormat:@"%i", 0] totalCount:[NSString stringWithFormat:@"%i", [self getCount]]];
        
        
        
        
        /*if (postArr.count == latestDataArr.count+totalAdds) // comment for adding reward video
         {
         [postArr removeAllObjects];
         
         [self viewUsersPostsWebserviceService:YES count:[NSString stringWithFormat:@"%i", 0]];
         }
         else
         {
         int abc = (int)postArr.count - ((int)latestDataArr.count);
         
         for (int i = (int)postArr.count-1; i >= abc; i--)
         {
         [postArr removeObjectAtIndex:i];
         
         NSLog(@"%i", i);
         }
         
         [self viewUsersPostsWebserviceService:YES count:[NSString stringWithFormat:@"%d", [self getCount]]];
         }*/
    }
    //        else
    //        {
    //            [self viewUsersPostsWebserviceService:YES count:[NSString stringWithFormat:@"%i", 0] totalCount:@""];
    //        }
    
    
    
    //[self viewUsersPostsWebserviceService:YES count:[NSString stringWithFormat:@"%i", 0]];
    
}


- (void)viewWillDisappear:(BOOL)animated
{
    NSLog(@"%@", self.navigationController.viewControllers);
    
    [self clearvideoViews];
    
    
    [super viewWillDisappear:YES];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
}


#pragma mark - Void


- (int)getCount
{
    int totalCount;
    
    NSMutableArray *testArr = [NSMutableArray new];
    
    for (id object in postArr)
    {
        if ([object isKindOfClass:[NSDictionary class]])
        {
            [testArr addObject:@"1"];
        }
    }
    
    totalCount = (int)testArr.count;
    
    
    NSLog(@"%i", (int)testArr.count);
    
    
   /* if (postArr.count)
    {
        totalCount = (int)postArr.count;
    }
    else
    {
        totalCount = 0;
    }
    
    
    
    totalCount = totalCount -  totalCount/10;
    
    int tempCount;
    
    if (totalCount < 20)
    {
        tempCount = 0;
    }
    else
    {
    
        if (totalCount % 20 == 0)
        {
            tempCount = (totalCount / 20)  * 20;

//            if (totalCount/20 == 1)
//            {
//                tempCount = (totalCount / 20)  * 20;
//            }
//            else
//            {
//                tempCount = ((totalCount / 20) - 1) * 20;
//
//            }

        }
        else
        {
            tempCount = (totalCount/20) * 20;
        }
    }
    
    return tempCount;*/
    
    return totalCount;
}


- (void) setUpView
{
    userID =  [[(APPDEL).userDetailDict objectForKey:@"userId"] intValue];
    
    
    //[self viewUsersPostsWebserviceService:YES count:[NSString stringWithFormat:@"%i", 0]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(increaseMessageCounter:) name:@"messageCounter" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewScrollOnTop:) name:@"ScrollToTop" object:nil];
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(imagePostAndScrol:) name:@"imagePostAndScrolToTop" object:nil];
    
    //[self viewUsersPostsWebserviceService:YES count:[NSString stringWithFormat:@"%lu", (unsigned long)[postArr count]]];
    
    self.navigationController.navigationBar.hidden = YES;
    
    //[userCollectionView registerNib:[UINib nibWithNibName:@"ListCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"listCollection"];
   
    [userCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"imageCell"];
    
    [userCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"GoogleAdds"];
    
    [userCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"RewardAdCell"];
    
    
    //[userCollectionView registerNib:[UINib nibWithNibName:@"ListCollectionViewVideoCell" bundle:nil] forCellWithReuseIdentifier:@"listCollectionVideo"];
    
   
   // refreshControl.tintColor = [UIColor blackColor];
    
    
    adsToLoad = [[NSMutableArray alloc] init];
    
    loadStateForAds = [[NSMutableDictionary alloc] init];

    
    //[userCollectionView registerNib:[UINib nibWithNibName:@"UserCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"userCell"];
    
    [self viewUsersPostsWebserviceService:YES count:[NSString stringWithFormat:@"%i", 0] totalCount:@""];

 }


- (void)enableUserIntrationOnCollectionView
{
    userCollectionView.userInteractionEnabled = YES;
}


#pragma mark - Load Reward Video Ad

- (void)loadAdd
{
    if (![[GADRewardBasedVideoAd sharedInstance] isReady])
    {
        GADRequest *request = [GADRequest request];
        
        [[GADRewardBasedVideoAd sharedInstance] loadRequest:request withAdUnitID:ADMob_UnitID_For_Reward_Video];
    }
}


#pragma mark - Play Reward Video ad

- (void)playAddOnViewController
{
    if ([[GADRewardBasedVideoAd sharedInstance] isReady])
    {
        [floatButton removeFromSuperview];
        
        [[GADRewardBasedVideoAd sharedInstance] presentFromRootViewController:self];
    }
    else
    {
        [refreshControlBottom endRefreshing];
        
        //[self addLoadAddButton];
        
        floatButton.alpha = 0.7;
        
        floatButton.userInteractionEnabled = NO;
        
        [self loadAdd];
    }
}


/*#pragma mark - Add Floating button

- (void)addLoadAddButton
{
    floatButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    floatButton.frame = CGRectMake(([[UIScreen mainScreen] bounds].size.width/2) - 100, [[UIScreen mainScreen] bounds].size.height-80, 200, 30);
    
    [floatButton setTitle:@"Play video for load more posts" forState:UIControlStateNormal];
    
    floatButton.titleLabel.font = [UIFont systemFontOfSize:13];
    
    [floatButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    floatButton.backgroundColor = [UIColor blackColor];
    
    
    floatButton.layer.cornerRadius = 3.0;
    
    floatButton.layer.borderWidth = 1.0;
    
    floatButton.layer.borderColor = [UIColor whiteColor].CGColor;
    
    
    [floatButton addTarget:self action:@selector(playAddOnViewController) forControlEvents:UIControlEventTouchUpInside];
    
    [self.tabBarController.view addSubview:floatButton];
}


- (void)removeFloatButton
{
    [floatButton removeFromSuperview];
}*/


#pragma mark - First time installed App

- (void)showPopUpWhileFirstTimeInstalledApp
{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"first_time_installedApp"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];

    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Welcome to Paracle"
                                          message:@"Complete your profile now for 100 bonus reward points."
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction *okBtn = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action)
                                {
                                    
                                    EditProfileViewController *vc = [[EditProfileViewController alloc] initWithNibName:@"EditProfileViewController" bundle:nil];
                                    
                                    [self.navigationController pushViewController:vc animated:YES];
                                }];
    
    UIAlertAction *doItLaterBtn = [UIAlertAction
                               actionWithTitle:@"I'll do it later "
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   
                               }];
    
    
    
    [alertController addAction:okBtn];
    
    [alertController addAction:doItLaterBtn];
    
    
    [self presentViewController:alertController animated:YES completion:nil];
    

    
    
}


#pragma mark - Add load

- (void)addNativeExpressAds:(NSMutableArray *)array
{
    NSInteger index = adInterval;
   
    //[userCollectionView layoutIfNeeded];
    
    while (index <= array.count)
    {
        GADNativeExpressAdView *adView = [[GADNativeExpressAdView alloc]
                                          initWithAdSize:GADAdSizeFromCGSize(
                                                                             CGSizeMake(280, 250))];
        
        adView.adUnitID = AdUnitID;
        
        adView.rootViewController = self;
        
        [array insertObject:adView atIndex:index];
        
        [adsToLoad addObject:adView];
        
        loadStateForAds[[self referenceKeyForAdView:adView]] = @NO;
        
        totalAdds ++;
       
        index += adInterval+1;
        
    }
    
    
    if (doubleDouble == 1)
    {
        UIView *view = [[UIView alloc] init];
        
        [array addObject:view];
    }
  
    
    
    
    [latestDataArr addObjectsFromArray:array];
    
    [postArr addObjectsFromArray:array];
    
    
    NSLog(@"%ld", postArr.count);
    
     [userCollectionView reloadData];
    
    
    if (scrolToTopWhenImagePost == YES)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ScrollToTop" object:nil];
        
        scrolToTopWhenImagePost = NO;
    }
}


/// Preloads native express ads sequentially. Dequeues and loads next ad from adsToLoad list.
- (void)preloadNextAd
{
    if (!adsToLoad.count)
    {
        return;
    }
    GADNativeExpressAdView *adView = adsToLoad.firstObject;
    
    [adsToLoad removeObjectAtIndex:0];
    
    [adView loadRequest:[GADRequest request]];
}


// Return string containting memory address location of a GADNativeExpressAdView to be used to
// uniquely identify the the object.
- (NSString *)referenceKeyForAdView:(GADNativeExpressAdView *)adView
{
    return [[NSString alloc] initWithFormat:@"%p", adView];
}


- (void)changeFollowStatus
{
    NSMutableDictionary *dict = [[postArr objectAtIndex:indexpath.row] mutableCopy];
    
    [postArr removeObjectAtIndex:indexpath.row];
    
    
    if ([[dict objectForKey:@"youfollowing"] intValue] == 0)
    {
        [dict setObject:@"1" forKey:@"youfollowing"];
        
        [postArr insertObject:dict atIndex:indexpath.row];
        
        [self followUser:[[postArr objectAtIndex:indexpath.row] valueForKey:@"userId"]];
    }
    else if ([[dict objectForKey:@"youfollowing"] intValue] == 1)
    {
        [dict setObject:@"0" forKey:@"youfollowing"];
        
        [postArr insertObject:dict atIndex:indexpath.row];
        
        [self unfollowUser:[[postArr objectAtIndex:indexpath.row] valueForKey:@"userId"]];
    }
    else
    {
        [dict setObject:@"0" forKey:@"youfollowing"];
        
        [postArr insertObject:dict atIndex:indexpath.row];
        
        
        [self cancelFollowRequest:[[postArr objectAtIndex:indexpath.row] valueForKey:@"userId"]];
        
    }
    
    
    
    //[userCollectionView performSelector:@selector(reloadItemsAtIndexPaths:) withObject:@[indexpath] afterDelay:0.2];
}


#pragma mark - Notification

- (void)increaseMessageCounter:(NSNotification *)noti
{
    messgeCountLbl.hidden = NO;
    
    messgeCountLbl.text = noti.object;
}


- (void)viewScrollOnTop:(NSNotification *)noti
{
    [userCollectionView setContentOffset:CGPointMake(0, 0) animated:NO];
}

- (void)imagePostAndScrol:(NSNotification *)noti
{
    scrolToTopWhenImagePost = YES;
}

#pragma mark - Refresh Control Action


- (void)pullToRefrshView
{
    totalAdds = 0;
    
    [postArr removeAllObjects];
    
    [userCollectionView reloadData];
    
    [self viewUsersPostsWebserviceService:NO count:[NSString stringWithFormat:@"%i", 0] totalCount:@""];
}


- (void)pullToRefrshViewFromBottom
{
    if (latestDataArr.count > 20)  // No need to load more if less then 20 because more than 20 post fetch on refresh.
    {
//        if(ifUserGetRewarded == YES)
//        {
//            [self loadAdd]; // load more reward video we need to reinitialize
//            
//            
//            ifUserGetRewarded = NO;
//            
//            wasPlayRewardVideo = NO;
        
            
            int totalCount;
            
            /*if (postArr.count)
            {
                totalCount = (int)postArr.count;
            }
            else
            {
                totalCount = 0;
                
            }
            
            totalCount = totalCount -  totalCount/10;*/
        
//            totalCount = (int)postArr.count - totalAdds;  // -1 because one is ad and other is reward view cell
//            
//            NSLog(@"%i", totalCount);
        
        
            NSMutableArray *testArr = [NSMutableArray new];
            
            for (id object in postArr)
            {
                if ([object isKindOfClass:[NSDictionary class]])
                {
                    [testArr addObject:@"1"];
                }
            }
        
            totalCount = (int)testArr.count;
        
//            NSMutableArray *testArr = [NSMutableArray new];
//            
//            for (id object in postArr)
//            {
//                if ([object isKindOfClass:[NSDictionary class]])
//                {
//                    [testArr addObject:@"1"];
//                }
//            }

            //totalCount = (int)testArr.count;
            
            [self viewUsersPostsWebserviceService:NO count:[NSString stringWithFormat:@"%i", totalCount] totalCount:@""];
//        }
//        else
//        {
//            [refreshControlBottom endRefreshing];
//        }
    }
    else
    {
        [refreshControlBottom endRefreshing];
    }
}



#pragma mark - Clear all video viewFrom Cell

- (void)clearvideoViews
{
    for (InstagramVideoView *videoView in videoPlayerObj)
    {
        [[NSNotificationCenter defaultCenter] removeObserver:videoView];
        
        [videoView pause];
    }
}


#pragma mark - Get image size


- (UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width
{
    float oldWidth = sourceImage.size.width;
   
    float scaleFactor = i_width / oldWidth;
    
    
    float newHeight = sourceImage.size.height * scaleFactor;
    
    float newWidth = oldWidth * scaleFactor;
    
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return newImage;
}



#pragma mark - Animation on button

- (void)animateButton:(UIButton *)sender
{
    sender.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    
    [UIView animateWithDuration:0.3/1.5 animations:^{
        
        sender.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
    }
                     completion:^(BOOL finished)
     {
         [UIView animateWithDuration:0.3/2 animations:^{
             
             sender.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
         }
                          completion:^(BOOL finished)
          {
              [UIView animateWithDuration:0.3/2 animations:^{
                  
                  sender.transform = CGAffineTransformIdentity;
              }];
          }];
     }];
}


#pragma mark - Add tag button on Cell


/*- (void)addTagBtnOnCell: (ListCollectionViewCell *)cell personTagArr:(NSArray *)personTagArr
{
    if (personTagArr.count)
    {
        cell.tagBtn.hidden = NO;
        
        cell.tagBtn.alpha = 1;
        
        [self performSelector:@selector(hideTagBtn:) withObject:cell afterDelay:2];
        
        if (cell.subviews.count > 1)
        {
            for (UIView *view in cell.subviews)
            {
                if ([view isKindOfClass:[JDTagView class]])
                {
                    [view removeFromSuperview];
                }
            }
        }
        else
        {
            for (int i = 0 ; i < personTagArr.count; i++)
            {
                CGPoint location;
                
                location.x = [[[personTagArr objectAtIndex:i] objectForKey:@"tagX"] floatValue];
                
                location.y = [[[personTagArr objectAtIndex:i] objectForKey:@"tagY"] floatValue];
                
                
                JDTagView *tagview = [[JDTagView alloc] init];
                
                tagview.JDTagViewDelegate = self;
                
                tagview.tag = i+1;
                
                [tagview setUpTagViewForViewWithTagTitle:[[personTagArr objectAtIndex:i] objectForKey:@"userName"] WithViewPoints:location];
                
                [cell addSubview: tagview];
                
                tagview.delBtn.hidden = YES;
            }
        }
    }
    else
    {
        cell.tagBtn.hidden = YES;
    }
}*/


- (void)addTagBtnOnCell: (UICollectionViewCell *)cell sender:(UIButton *)sender personTagArr:(NSArray *)personTagArr
{
    if (personTagArr.count)
    {
        sender.hidden = NO;
        
        sender.alpha = 1;
        
        [self performSelector:@selector(hideTagBtn:) withObject:sender afterDelay:2];
        
        if (cell.subviews.count > 1)
        {
            for (UIView *view in cell.subviews)
            {
                if ([view isKindOfClass:[JDTagView class]])
                {
                    [view removeFromSuperview];
                }
            }
        }
        else
        {
            for (int i = 0 ; i < personTagArr.count; i++)
            {
                int backEndHeight = [[[postArr objectAtIndex:sender.tag] valueForKey:@"height"] intValue];
                
                int backEndscreenWidth = [[[postArr objectAtIndex:sender.tag] valueForKey:@"width"] intValue];
                
                
                float scaleFator = [[UIScreen mainScreen] bounds].size.width/backEndscreenWidth;
                
                float actualHeight = scaleFator * backEndHeight;
                
                
                CGPoint location;
                
                location.x =  [[[personTagArr objectAtIndex:i] objectForKey:@"tagX"] floatValue] * [[UIScreen mainScreen]bounds].size.width;
                
                location.y = [[[personTagArr objectAtIndex:i] objectForKey:@"tagY"] floatValue] * actualHeight;
                
                
                JDTagView *tagview = [[JDTagView alloc] init];
                
                tagview.JDTagViewDelegate = self;
                
                tagview.tag = i+1;
                
                [tagview setUpTagViewForViewWithTagTitle:[[personTagArr objectAtIndex:i] objectForKey:@"userName"] WithViewPoints:location];
                
                [cell addSubview: tagview];
                
                tagview.delBtn.hidden = YES;
            }
        }
    }
    else
    {
        sender.hidden = YES;
    }
}


#pragma mark - Webservice

- (void)viewUsersPostsWebserviceService:(BOOL)showHud count:(NSString *)count totalCount:(NSString *)total_count
{
    if (total_count.length)
    {
        [postArr removeAllObjects];
        
        [latestDataArr removeAllObjects];
    }
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userId",count,@"count", total_count, @"total_count", [[NSUserDefaults standardUserDefaults] objectForKey:@"device_token"], @"device_token", nil];
   
   // userCollectionView.userInteractionEnabled = NO;
    
    [[ServiceHelper sharedManager] setHudShow:showHud];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"viewPost" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         //[self performSelector:@selector(enableUserIntrationOnCollectionView) withObject:nil afterDelay:1];
         
         [refreshControl endRefreshing];
         
         [refreshControlBottom endRefreshing];
         
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                if ([[result objectForKey:@"deleteStatus"] intValue] == 0)
                 {
                     if ([(NSArray *)[result objectForKey:@"posts"] count])
                     {
                         [latestDataArr removeAllObjects];
                         
                        
                         doubleDouble = [[result objectForKey:@"sponser"] intValue];
                      
                         
                         [self addNativeExpressAds: [[result objectForKey:@"posts"] mutableCopy]];
                         
                         [self preloadNextAd];
                     }
                    
        
                     if ([[result objectForKey:@"points"] intValue] > 0)
                     {
                         rewardPoint = [[result objectForKey:@"points"] intValue];
                         
                         rewardLbl.text = [NSString stringWithFormat:@"Reward Points %@", [result objectForKey:@"points"]];
                     }
                     
                     else
                     {
                         rewardLbl.text = @"Reward Points 0";
                     }
                     
        
                     if ([[result objectForKey:@"activeDouble"] intValue] == 0)
                     {
                         pointsDouble = NO;
                     }
                     
                     else
                     {
                         pointsDouble = YES;
                     }
                 }
                 else
                 {
                     [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isUserLogin"];
                     
                     [[NSUserDefaults standardUserDefaults] synchronize];
                     
                     
                     WelcomeViewController *vc = [[WelcomeViewController alloc] initWithNibName:@"WelcomeViewController" bundle:nil];
                     
                     
                     UINavigationController *nVc = [[UINavigationController alloc] initWithRootViewController:vc];
                     
                     nVc.navigationBar.hidden = YES;
                     
                     [self presentViewController:nVc animated:YES completion:nil];
                 }
             }
             
         }
        
    }];
}


- (void)likePost:(NSString *)postId index:(NSInteger)tag
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", postId, @"postId", [[postArr objectAtIndex:tag] valueForKey:@"userId"], @"secondaryId", nil];
    
    
    [[ServiceHelper sharedManager] setHudShow:NO];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"likePost" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 
                 
             }
             
             else
             {
                 NSMutableDictionary *dict = [[postArr objectAtIndex:tag] mutableCopy];
                 
                 [postArr removeObjectAtIndex:tag];
                 
                 
                 [dict setObject:[NSString stringWithFormat:@"%i", [[result objectForKey:@"totallike"] intValue]-1] forKey:@"totallike"];
                 
                 
                 if ([[dict objectForKey:@"youlike"] intValue] == 0)
                 {
                     [dict setObject:@"1" forKey:@"youlike"];
                 }
                 else
                 {
                     [dict setObject:@"0" forKey:@"youlike"];
                 }
                 
                 [postArr insertObject:dict atIndex:(int)tag];
                 
                 [userCollectionView reloadData];
             }
         }
         
     }];
}


- (void)favPost:(NSString *)postId index:(NSInteger)tag
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", postId, @"postId", nil];
    
    
    [[ServiceHelper sharedManager] setHudShow:NO];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"addFavourite" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 
                 
             }
             
             else
             {
                 NSMutableDictionary *dict = [[postArr objectAtIndex:tag] mutableCopy];
                 
                 [postArr removeObjectAtIndex:tag];
                 
                 
                 if ([[dict objectForKey:@"favourite"] intValue] == 0)
                 {
                     [dict setObject:@"1" forKey:@"favourite"];
                 }
                 else
                 {
                     [dict setObject:@"0" forKey:@"favourite"];
                 }
                 
                 [postArr insertObject:dict atIndex:(int)tag];
                 
                 [userCollectionView reloadData];
             }
         }
     }];
}


- (void)getUserIdFromUsername:(NSDictionary *)params
{
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"getByUser" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 ProfileViewController *vc = [[ProfileViewController alloc] initWithNibName:@"ProfileViewController" bundle:nil];
                 
                 vc.differentUserId = [result objectForKey:@"id"] ;
                 
                 [self.navigationController pushViewController:vc animated:YES];
             }
             
             else
             {
                 
             }
         }
     }];

}



- (void)turnOffCommenting:(NSInteger) path params:(NSDictionary *)params
{
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"setCommenting" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 NSMutableDictionary *dict = [[postArr objectAtIndex:path] mutableCopy];
                 
                 [postArr removeObjectAtIndex:path];
                 
                 [dict setObject:[result objectForKey:@"commentflag"]  forKey:@"commentflag"];
                 
                 [postArr insertObject:dict atIndex:path];
                 
                 
                 [userCollectionView reloadData];
             }
             
             else
             {
                 
             }
         }
     }];
}


- (void)followUser:(NSString *)userId
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"primary_userid", userId, @"secondary_userid", nil];
    
    
    [[ServiceHelper sharedManager] setHudShow:NO];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"sendFollowingRequest" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 [self viewUsersPostsWebserviceService:NO count:@"" totalCount:[NSString stringWithFormat:@"%i", [self getCount]]];
                 
                 //                 if ([[result objectForKey:@"follow"] intValue] == 0)
                 //                 {
                 //                     [editProfileBtn setTitle:@"Follow" forState:UIControlStateNormal];
                 //                 }
                 //
                 //                 else
                 //                 {
                 //                     [editProfileBtn setTitle:@"Following" forState:UIControlStateNormal];
                 //                 }
             }
             
             else
             {
                 
             }
         }
         else
         {
             NSMutableDictionary *dict = [[postArr objectAtIndex:indexpath.row] mutableCopy];
             
             [postArr removeObjectAtIndex:indexpath.row];
             
             
             if ([[dict objectForKey:@"youfollowing"] intValue] == 0)
             {
                 [dict setObject:@"1" forKey:@"youfollowing"];
             }
             else
             {
                 [dict setObject:@"0" forKey:@"youfollowing"];
             }
             
             [postArr insertObject:dict atIndex:indexpath.row];
             
             
             [userCollectionView performSelector:@selector(reloadItemsAtIndexPaths:) withObject:@[indexpath] afterDelay:0.2];
         }
     }];
    
    
}

- (void)unfollowUser:(NSString *)userId
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userid", userId, @"followerid", nil];
    
    
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"unFollowUser" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
             [self viewUsersPostsWebserviceService:NO count:@"" totalCount:[NSString stringWithFormat:@"%i", [self getCount]]];
             
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
            
             }
             
             else
             {
                 
             }
         }
         else
         {
             NSMutableDictionary *dict = [[postArr objectAtIndex:indexpath.row] mutableCopy];
             
             [postArr removeObjectAtIndex:indexpath.row];
             
             
             if ([[dict objectForKey:@"youfollowing"] intValue] == 0)
             {
                 [dict setObject:@"1" forKey:@"youfollowing"];
             }
             else
             {
                 [dict setObject:@"0" forKey:@"youfollowing"];
             }
             
             [postArr insertObject:dict atIndex:indexpath.row];
             
             
             [userCollectionView performSelector:@selector(reloadItemsAtIndexPaths:) withObject:@[indexpath] afterDelay:0.2];
         }
     }];

}


- (void)cancelFollowRequest:(NSString *)userId
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", userId, @"secondaryId", nil];
    
    
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"cancelFollowRequest" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
              [self viewUsersPostsWebserviceService:NO count:@"" totalCount:[NSString stringWithFormat:@"%i", [self getCount]]] ;
             
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 
             }
             
             else
             {
                 
             }
         }
         else
         {
             /*NSMutableDictionary *dict = [[postArr objectAtIndex:indexpath.row] mutableCopy];
             
             [postArr removeObjectAtIndex:indexpath.row];
             
             
             if ([[dict objectForKey:@"youfollowing"] intValue] == 0)
             {
                 [dict setObject:@"1" forKey:@"youfollowing"];
             }
             else
             {
                 [dict setObject:@"0" forKey:@"youfollowing"];
             }
             
             [postArr insertObject:dict atIndex:indexpath.row];
             
             
             [userCollectionView performSelector:@selector(reloadItemsAtIndexPaths:) withObject:@[indexpath] afterDelay:0.2];*/
         }
     }];

}


- (void)activeDailyDouble:(NSInteger)tag
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", nil];
    
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"onDouble" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {

             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 [userCollectionView performBatchUpdates:^{
                     
                     
                     [postArr removeObjectAtIndex:tag];
                    
                     NSIndexPath *indexPath = [NSIndexPath indexPathForRow: tag inSection:0];
                     
                     [userCollectionView deleteItemsAtIndexPaths:[NSArray arrayWithObject:indexPath]];
                 }
                     completion:^(BOOL finished)
                 {
                     
                     [self viewUsersPostsWebserviceService:YES count:[NSString stringWithFormat:@"%i", 0] totalCount:[NSString stringWithFormat:@"%i", [self getCount]]];
                    }];
                 
//                 [postArr removeObjectAtIndex:tag];
//                 
//                 [userCollectionView deleteItemsAtIndexPaths:@[[NSIndexPath indexPathWithIndex:tag]]];
             }
             
             else
             {
                 
             }
         }
         else
         {
         
         }
     }];
    
}


#pragma mark - Action

- (IBAction)changeLayoutTap:(UIButton *)sender
{
    sender.selected = !sender.selected;
    
    if (sender.selected == YES)
    {
        
        [self clearvideoViews];
        
        
        isGridView = YES;
      
        userCollectionView.backgroundColor = [UIColor blackColor];
        
        refreshControl.tintColor = [UIColor whiteColor];
        
        [userCollectionView registerNib:[UINib nibWithNibName:@"UserCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"userCell"];
        
        
        for (int i = 0; i< postArr.count; i++)
        {
            if ([postArr[i] isKindOfClass:[GADNativeExpressAdView class]])
            {
                [postArr removeObjectAtIndex:i];
            }
        }
        //[userCollectionView registerNib:[UINib nibWithNibName:@"ListCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"listCollection"];
        
        //[userCollectionView registerClass:[ListCollectionImageCell class] forCellWithReuseIdentifier:@"imageCell"];
       
        //[userCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"ImageCell"];
        
        //[userCollectionView registerNib:[UINib nibWithNibName:@"ListCollectionViewVideoCell" bundle:nil] forCellWithReuseIdentifier:@"listCollectionVideo"];
        
    }
    
    else
    {
        isGridView = NO;
        
//        [self addNativeExpressAds];
//        
//        [self preloadNextAd];
        
        userCollectionView.backgroundColor = [UIColor whiteColor];
        
        //refreshControl.tintColor = [UIColor blackColor];
        
        [userCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"imageCell"];
    }
    
    [userCollectionView reloadData];
}


- (IBAction)messageTap
{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"not_open_message_Box_first_time"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    [[NSUserDefaults standardUserDefaults] setObject: @"0" forKey:@"message_count"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    MessageViewController *vc = [[MessageViewController alloc] initWithNibName:@"MessageViewController" bundle:nil];

    //vc.hidesBottomBarWhenPushed = YES;
    
    vc.isVCForMessage = YES;
    
    [(UINavigationController *)self.navigationController pushViewController:vc animated:YES];
}


- (IBAction)unfollowCancelTap
{
    [unfollowView removeFromSuperview];
}


- (IBAction)unfollowTap
{
    [self changeFollowStatus];
    
    [unfollowView removeFromSuperview];
}


- (IBAction)searchTap
{
    SearchViewController *vc = [[SearchViewController alloc] initWithNibName:@"SearchViewController" bundle:nil];
    
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:NO];
}


#pragma mark - Collection Action

- (void)likeTap:(UIButton *)sender
{
    [self animateButton:sender];
    
    
    NSMutableDictionary *dict = [[postArr objectAtIndex:sender.tag] mutableCopy];
    
    [postArr removeObjectAtIndex:sender.tag];
    
    
    if ([[dict objectForKey:@"youlike"] intValue] == 0)
    {
        if(![[dict objectForKey:@"userId"] isEqualToString:[(APPDEL).userDetailDict objectForKey:@"userId"]])
        {
            if (pointsDouble == YES)
            {
                rewardPoint = rewardPoint+2;
            }
            else
            {
                rewardPoint ++;
            }
            
        }
        
        [dict setObject:@"1" forKey:@"youlike"];
        
        [dict setObject:[NSString stringWithFormat:@"%i", [[dict objectForKey:@"totallike"] intValue]+1 ] forKey:@"totallike"];
    }
    else
    {
        if(![[dict objectForKey:@"userId"] isEqualToString:[(APPDEL).userDetailDict objectForKey:@"userId"]])
        {
            if(rewardPoint != 0)
            {
                if (pointsDouble == YES)
                {
                    if (rewardPoint == 1) //For prevent the point goes in (-)minus
                    {
                        rewardPoint --;
                    }
                    else
                    {
                        rewardPoint = rewardPoint-2;
                    }
                }
                else
                {
                    rewardPoint --;
                }
             }
        }
       
        [dict setObject:@"0" forKey:@"youlike"];
        
        [dict setObject:[NSString stringWithFormat:@"%i", [[dict objectForKey:@"totallike"] intValue]-1 ] forKey:@"totallike"];
    }
    
    rewardLbl.text = [NSString stringWithFormat:@"Reward Points %i", rewardPoint];
    
    [postArr insertObject:dict atIndex:sender.tag];
    
    
    NSIndexPath *path = [userCollectionView indexPathForCell: (UICollectionViewCell *)sender.superview.superview];
    
    
    [userCollectionView performSelector:@selector(reloadItemsAtIndexPaths:) withObject:@[path] afterDelay:0];
    

    [self likePost:[[postArr objectAtIndex:sender.tag] valueForKey:@"postId"] index:sender.tag];
    
}


- (void)favTap:(UIButton *)sender
{
    [self animateButton:sender];
    
    NSMutableDictionary *dict = [[postArr objectAtIndex:sender.tag] mutableCopy];
    
    [postArr removeObjectAtIndex:sender.tag];
    
    
    if ([[dict objectForKey:@"favourite"] intValue] == 0)
    {
        [dict setObject:@"1" forKey:@"favourite"];
    }
    else
    {
        [dict setObject:@"0" forKey:@"favourite"];
    }
    
    [postArr insertObject:dict atIndex:sender.tag];
    
    
    NSIndexPath *path = [userCollectionView indexPathForCell: (UICollectionViewCell *)sender.superview.superview];
    
    
    [userCollectionView performSelector:@selector(reloadItemsAtIndexPaths:) withObject:@[path] afterDelay:0.2];
    
    
    [self favPost:[[postArr objectAtIndex:sender.tag] valueForKey:@"postId"] index:sender.tag];
}


- (void)commentTap:(UIButton *)sender
{
    if ([[[postArr objectAtIndex:sender.tag] objectForKey:@"commentflag"] intValue] == 1)
    {
        AddCommentViewController *vc = [[AddCommentViewController alloc] initWithNibName:@"AddCommentViewController" bundle:nil];
        
        vc.hidesBottomBarWhenPushed = YES;
        
        vc.postId = [[postArr objectAtIndex:sender.tag] objectForKey:@"postId"];
        
        vc.secondryId = [[postArr objectAtIndex:sender.tag] objectForKey:@"userId"];
        
        [self.navigationController pushViewController:vc animated:YES];
    }
}


- (void)getTotalLike:(UIButton *)sender
{
    ViewLikesViewController *vc = [[ViewLikesViewController alloc] initWithNibName:@"ViewLikesViewController" bundle:nil];
    
    vc.hidesBottomBarWhenPushed = YES;
    
    vc.switchTitleStr = @"like";
    
    vc.postId = [[postArr objectAtIndex:sender.tag] objectForKey:@"postId"];
    
    [self.navigationController pushViewController:vc animated:YES];
    
}


- (void)followTap:(UIButton *)sender
{
    indexpath = [userCollectionView indexPathForCell: (ListCollectionViewCell *)sender.superview.superview];
    
    NSLog(@"%ld", sender.tag);
    
    if ([[[postArr objectAtIndex:sender.tag] objectForKey:@"youfollowing"] intValue] == 0)
    {
        [self changeFollowStatus];
    }
    else if ([[[postArr objectAtIndex:sender.tag] objectForKey:@"youfollowing"] intValue] == 1)
    {
        unfollowView.frame = self.view.frame;
        
        [unfollowUserImg sd_setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [[postArr objectAtIndex:sender.tag] valueForKey:@"profilePic"]]] placeholderImage:nil];
        
        unfollowUserNameLbl.text = [NSString stringWithFormat:@"Unfollow %@?", [[postArr objectAtIndex:sender.tag] valueForKey:@"fullname"]];
        
        
        [self.view addSubview:unfollowView];
        
        
        [UIView animateWithDuration:0.1 animations:^
         {
             popupView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
         }
                         completion:^(BOOL finished)
         {
             [UIView animateWithDuration:0.1 animations:^
              {
                  popupView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
              }
                              completion:^(BOOL finished)
              {
                  [UIView animateWithDuration:0.1 animations:^
                   {
                       popupView.transform = CGAffineTransformIdentity;
                   }];
              }];
         }];
    }
    else
    {
        [self changeFollowStatus];
    
        
        //[userCollectionView performSelector:@selector(reloadItemsAtIndexPaths:) withObject:@[indexpath] afterDelay:0.2];
    }
}


- (void)dotsTap:(UIButton *)sender
{
    if ([[[postArr objectAtIndex:sender.tag] objectForKey:@"userId"] intValue] == [[(APPDEL).userDetailDict objectForKey:@"userId"] intValue])
    {
        NSString *swapStr;
        
        if ([[[postArr objectAtIndex:sender.tag] objectForKey:@"commentflag"] intValue] == 1)
        {
            swapStr = @"Turn Off Commenting";
        }
        
        else
        {
            swapStr = @"Turn On Commenting";
        }
        
        UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Delete" otherButtonTitles:swapStr, @"Edit", nil];
        
        action.tag = sender.tag;
        
        [action showInView:self.view];
    }
    else
    {
        UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Report", nil];
        
        action.tag = sender.tag;
        
        [action showInView:self.view];
    
    }
}


- (void)tapOnProfileImage:(UIButton *)sender
{
    ProfileViewController *vc = [[ProfileViewController alloc] initWithNibName:@"ProfileViewController" bundle:nil];
    
    vc.differentUserId = [[postArr objectAtIndex: sender.tag] objectForKey:@"userId"] ;
    
    [self.navigationController pushViewController:vc animated:YES];
}


- (void)locationTap:(UIButton *)sender
{
    ViewPostBySearchViewController *vc = [[ViewPostBySearchViewController alloc] initWithNibName:@"ViewPostBySearchViewController" bundle:nil];
    
    vc.isSeacrhTagPeople = NO;
    
    vc.searchStr = [[postArr objectAtIndex:sender.tag] valueForKey:@"location"];
    
    [self.navigationController pushViewController:vc animated:YES];
}



- (void)hideTagBtn:(UIButton *)sender
{
    [UIView animateWithDuration:0.2 animations:^
     {
         sender.alpha = 0;
     }];
}


- (void)showTagBtn:(UIButton *)sender
{
    [UIView animateWithDuration:0.2 animations:^
     {
         sender.alpha = 1;
     }];
}

- (void)showTagPeopleView:(UIButton *)sender
{
    UICollectionViewCell *cell = (UICollectionViewCell *)sender.superview.superview;
    
    
    [self addTagBtnOnCell:cell sender:sender personTagArr:[[postArr objectAtIndex:sender.tag] valueForKey:@"tagpeople"]];
}


#pragma mark - Dot button action


- (void)reportAbuse:(NSString *)postId secondryId:(NSString *)secondryId isInappropiate: (NSString *)type
{
    NSDictionary *params;
    
    params = [NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", postId, @"postId", type, @"flag", secondryId, @"secondaryId", nil];
  
    
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"postReportAbuse" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 
                 
             }
             
             else
             {
                 
             }
         }
         
     }];
    
}


- (void)deletePost:(NSInteger)path postId:(NSString *)postId
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", postId, @"postId", nil];
    
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"deletePost" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 [postArr removeObjectAtIndex:path];
                 
                 [userCollectionView reloadData];
             }
             
             else
             {
                 
             }
         }
         
     }];
}


#pragma mark - Set collectinView height

- (CGSize)setCollectionViewHeight:(NSDictionary *)dict indexPath:(NSIndexPath *)indexPath
{
    int backEndHeight = [[[postArr objectAtIndex:indexPath.row] valueForKey:@"height"] intValue];
    
    int backEndscreenWidth = [[[postArr objectAtIndex:indexPath.row] valueForKey:@"width"] intValue];
    
    
    float scaleFator = [[UIScreen mainScreen] bounds].size.width/backEndscreenWidth;
    
    
    float actualHeight = scaleFator * backEndHeight;
    
    
    
    int imageCellHeight = actualHeight + RestOfElementSize_insted_of_imageSize;
    
   
    if ([[[postArr objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"image"])
    {
        imageCellHeight = actualHeight + RestOfElementSize_insted_of_imageSize;
    }
    
    else
    {
        // 350 is video view height
        
        imageCellHeight = 350 + RestOfElementSize_insted_of_imageSize;
    }
    
    
    NSString *dataStr;
    
    CGSize cellHeight;
    
    CGSize maximumLabelSize;
    

    if (![[dict objectForKey:@"caption"] isEqualToString:@""])
    {
        dataStr = [NSString stringWithFormat:@"%@ %@", [dict objectForKey:@"fullname"], [dict objectForKey:@"caption"]];
    }
   
    
   /* NSArray *commentArr = [[postArr objectAtIndex:indexPath.row] objectForKey:@"allComment"];
    
    if ([commentArr count])
    {
        NSMutableString *commentStr = [@"" mutableCopy];
        
        for (int i = 0; i < commentArr.count; i++)
        {
            [commentStr appendFormat:@"%@\n", [NSString stringWithFormat:@"%@ %@", [[commentArr objectAtIndex:i] objectForKey:@"fullname"], [[commentArr objectAtIndex:i] objectForKey:@"comment"]]];
        }
        
        dataStr = [NSString stringWithFormat:@"%@\nView all comments\n\n%@", dataStr, commentStr];
    }*/
    
//    if (dataStr.length)
//    {
//        dataStr = [NSString stringWithFormat:@"%@\n\nView all comments%@", dataStr, [dict objectForKey:@"totalcomment"]];
//    }
    
    //Set label width & cellHeight
    
    if ([[UIScreen mainScreen] bounds].size.width == 320)
    {
        maximumLabelSize = CGSizeMake(300, FLT_MAX);
        
        if ([[dict valueForKey:@"type"] isEqualToString:@"image"])
        {
            cellHeight = CGSizeMake(320, imageCellHeight);
        }
        else
        {
            cellHeight = CGSizeMake(320, imageCellHeight);
        }
    }
    else if ([[UIScreen mainScreen] bounds].size.width == 375)
    {
        maximumLabelSize = CGSizeMake(355, FLT_MAX);
        
        if ([[dict valueForKey:@"type"] isEqualToString:@"image"])
        {
            cellHeight = CGSizeMake(375, imageCellHeight);
        }
        
        else
        {
            cellHeight = CGSizeMake(375, imageCellHeight);
        }
    }
    else
    {
        maximumLabelSize = CGSizeMake(394, FLT_MAX);
        
        if ([[dict valueForKey:@"type"] isEqualToString:@"image"])
        {
            cellHeight = CGSizeMake(414, imageCellHeight);
        }
        else
        {
            cellHeight = CGSizeMake(414, imageCellHeight);
        }
    }
    
    
    CGRect textRect = [dataStr boundingRectWithSize:maximumLabelSize
                                            options:NSStringDrawingUsesLineFragmentOrigin
                                         attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]}
                                            context:nil];
    
    // NSLog(@"%f %@", textRect.size.height, dict);
    
    cellHeight = CGSizeMake(cellHeight.width, cellHeight.height + textRect.size.height+30);
    
    return cellHeight;
}


#pragma mark - Bold perticular string

- (NSMutableAttributedString *)setBoldString:(NSString *)fullstring boldString:(NSString *)boldString
{
    NSMutableAttributedString *yourAttributedString = [[NSMutableAttributedString alloc] initWithString:fullstring];
    
    NSRange boldRange = [fullstring rangeOfString:boldString];
    
    [yourAttributedString addAttribute: NSFontAttributeName value:[UIFont fontWithName:@"MuseoSansCyrl-500" size:14] range:boldRange];
    
    [yourAttributedString addAttribute: NSForegroundColorAttributeName value:[UIColor blackColor] range:boldRange];
    
    return  yourAttributedString;
}



#pragma mark - CollectionView Delegate

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (isGridView == YES)
    {
        if (collectionView.frame.size.width == 320)
        {
            return CGSizeMake(106, 106);
            
        }
        else if (collectionView.frame.size.width == 375)
        {
            return CGSizeMake(124, 124);
        }
        
        return  CGSizeMake(137, 137);
    }
    else
    {
        if ([postArr[indexPath.row] isKindOfClass:[GADNativeExpressAdView class]])
        {
            GADNativeExpressAdView *adView = postArr[indexPath.row];
            
            
            bool isLoaded = loadStateForAds[[self referenceKeyForAdView:adView]];
          
            if (isLoaded == YES)
            {
                if (collectionView.frame.size.width == 320)
                {
                    return CGSizeMake(320, 280);
                    
                }
                else if (collectionView.frame.size.width == 375)
                {
                    return CGSizeMake(375, 280);
                }
                
                return  CGSizeMake(414, 280);
                //return  CGSizeMake(280, 250);
            }
            else
            {
                if (collectionView.frame.size.width == 320)
                {
                    return CGSizeMake(320, 280);
                    
                }
                else if (collectionView.frame.size.width == 375)
                {
                    return CGSizeMake(375, 280);
                }
                
                return  CGSizeMake(414, 280);
                //return  CGSizeMake(280, 0);
            }
        }
        else if ([postArr[indexPath.row] isKindOfClass:[UIView class]])
        {
            if (collectionView.frame.size.width == 320)
            {
                return CGSizeMake(320, 185);
                
            }
            else if (collectionView.frame.size.width == 375)
            {
                return CGSizeMake(375, 185);
            }
            
            return  CGSizeMake(414, 185);
        }
        else
        {
            return [self setCollectionViewHeight:[postArr objectAtIndex:indexPath.row]indexPath:indexPath];
        }
    }
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return postArr.count;
}



- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView1 cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (isGridView == YES)
    {
        static NSString *gridCellIdentifier = @"userCell";
        
        UserCollectionViewCell *cell = (UserCollectionViewCell*)[collectionView1 dequeueReusableCellWithReuseIdentifier:gridCellIdentifier forIndexPath:indexPath];
        
        cell.contentView.autoresizingMask  = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        
        if ([[[postArr objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"image"])
        {
            [cell.userImg sd_setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [[postArr objectAtIndex:indexPath.row] valueForKey:@"media"]]] placeholderImage:[UIImage imageNamed:@""] options:SDWebImageRefreshCached];
            
            cell.videoIconImg.hidden = YES;
            
        }
        else
        {
            [cell.userImg sd_setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [[postArr objectAtIndex:indexPath.row] valueForKey:@"thumbnail"]]] placeholderImage:[UIImage imageNamed:@""] options:SDWebImageRefreshCached];
            
            
            cell.videoIconImg.hidden = NO;
        }
        
        return cell;
    }
    else
    {
        NSLog(@"%lu", (unsigned long)postArr.count);
        
        // NSLog(@"%@", postArr);
        
        
        if ([postArr[indexPath.row] isKindOfClass:[GADNativeExpressAdView class]])
        {
            UICollectionViewCell *cell = [userCollectionView dequeueReusableCellWithReuseIdentifier:@"GoogleAdds" forIndexPath:indexPath];
            
            for (UIView *subview in cell.contentView.subviews)
            {
                [subview removeFromSuperview];
            }
            
            GADNativeExpressAdView *adView = postArr[indexPath.row];
            
            adView.delegate = self;
            
            CGRect addViewFrame;
            
            if ([[UIScreen mainScreen] bounds].size.width == 320)
            {
                addViewFrame = CGRectMake(0, 0, 320, 280);
                
            }
            else if ([[UIScreen mainScreen] bounds].size.width == 375)
            {
                addViewFrame = CGRectMake(0, 0, 375, 280);
            }
            else
            {
                addViewFrame = CGRectMake(0, 0, 414, 280);
            }
            
            
            
            adView.frame = addViewFrame;
            
            adView.center = cell.contentView.center;
            
            [cell.contentView addSubview:adView];
            
            
            GADRequest *request = [[GADRequest alloc] init];
            
            request.testDevices = [NSArray arrayWithObjects:kDFPSimulatorID, nil];
            
            
            [adView loadRequest:request];
            
            
            
            return cell;
        }
        
        else if ([postArr[indexPath.row] isKindOfClass:[UIView class]])
        {
            UICollectionViewCell *rewardVideoCell = [userCollectionView dequeueReusableCellWithReuseIdentifier:@"RewardAdCell" forIndexPath:indexPath];
            
            UIView *rewardView = postArr[indexPath.row];
            
            if ([[UIScreen mainScreen] bounds].size.width == 320)
            {
                rewardView.frame = CGRectMake(0, 0, 320, 150);
                
            }
            else if ([[UIScreen mainScreen] bounds].size.width == 375)
            {
                rewardView.frame = CGRectMake(0, 0, 375, 150);
            }
            else
            {
                rewardView.frame = CGRectMake(0, 0, 414, 150);
            }
            
            rewardView.backgroundColor = [UIColor blackColor];
            
            
            UILabel *headingLbl = [[UILabel alloc] init];
            
            headingLbl.textColor = [UIColor whiteColor];
            
            headingLbl.frame = CGRectMake(10, 5, rewardView.frame.size.width - 20, 40);
            
            headingLbl.textAlignment = NSTextAlignmentCenter;
            
            headingLbl.numberOfLines = 2.0;
            
            headingLbl.font = [UIFont boldSystemFontOfSize: 15];
            
            headingLbl.text = @"Congratulations!\nYou found the Daily Double";
            
            [rewardView addSubview:headingLbl];
            
            
            
            UIImageView *imageView = [[UIImageView alloc] init];
            
            imageView.frame = CGRectMake((rewardView.frame.size.width/2) - 60, 55, 120, 100);
            
            imageView.image = [UIImage imageNamed:@"dailyDouble"];
            
            imageView.backgroundColor = [UIColor clearColor];
            
            [rewardView addSubview:imageView];
            
            [rewardVideoCell.contentView addSubview:rewardView];
            
            
            UILabel *bottomLbl = [[UILabel alloc] init];
            
            bottomLbl.textColor = [UIColor whiteColor];
            
            bottomLbl.frame = CGRectMake(10, 145, rewardView.frame.size.width - 20, 40);
            
            bottomLbl.textAlignment = NSTextAlignmentCenter;
            
            bottomLbl.numberOfLines = 2.0;
            
            bottomLbl.font = [UIFont boldSystemFontOfSize: 15];
            
            bottomLbl.text = @"Tap to activate double points";
            
            [rewardView addSubview:bottomLbl];
            
            
            
            
            //commented because temp view
            
            //rewardView.backgroundColor = [UIColor groupTableViewBackgroundColor];
            
            /*UIImageView *imageView = [[UIImageView alloc] init];
             
             imageView.frame = CGRectMake(30, 20, 100, 120);
             
             imageView.image = [UIImage imageNamed:@"rewardVideoImg"];
             
             imageView.backgroundColor = [UIColor clearColor];
             
             [rewardView addSubview:imageView];
             
             
             UILabel *headingLbl = [[UILabel alloc] init];
             
             headingLbl.textColor = [UIColor darkGrayColor];
             
             headingLbl.frame = CGRectMake(rewardView.frame.size.width - 170, 30, 170, 50);
             
             headingLbl.textAlignment = NSTextAlignmentCenter;
             
             headingLbl.numberOfLines = 2.0;
             
             headingLbl.font = [UIFont systemFontOfSize:13];
             
             headingLbl.text = @"WATCH THIS AD TO\nVIEW MORE PHOTOS";
             
             [rewardView addSubview:headingLbl];
             
             
             UILabel *subHeadingLbl = [[UILabel alloc] init];
             
             subHeadingLbl.textColor = [UIColor darkGrayColor];
             
             subHeadingLbl.frame = CGRectMake(rewardView.frame.size.width - 170, headingLbl.frame.origin.y + headingLbl.frame.size.height, 170, 20);
             
             subHeadingLbl.textAlignment = NSTextAlignmentCenter;
             
             subHeadingLbl.numberOfLines = 2.0;
             
             subHeadingLbl.font = [UIFont boldSystemFontOfSize:13];
             
             subHeadingLbl.text = @"TAP TO PLAY";
             
             [rewardView addSubview:subHeadingLbl];
             
             
             
             UIButton *playBtn = [UIButton buttonWithType: UIButtonTypeCustom];
             
             playBtn.frame = CGRectMake(rewardView.frame.size.width - 130, subHeadingLbl.frame.origin.y + subHeadingLbl.frame.size.height + 5, 100, 30);
             
             [playBtn setBackgroundImage:[UIImage imageNamed:@"rewardAdPlayBtnImg"] forState:UIControlStateNormal];
             
             playBtn.titleLabel.font = [UIFont boldSystemFontOfSize:13];
             
             [playBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
             
             [playBtn setTitle:@"PLAY" forState:UIControlStateNormal];
             
             [rewardView addSubview:playBtn];
             
             
             
             [rewardVideoCell.contentView addSubview:rewardView];*/
            
            return rewardVideoCell;
        }
        else
        {
            
            UICollectionViewCell *cell = [userCollectionView dequeueReusableCellWithReuseIdentifier:@"imageCell" forIndexPath:indexPath];
            
            if (cell != nil)
            {
                for (UIView* view in [cell.contentView subviews])
                {
                    [view removeFromSuperview];
                }
            }
            
            
            //Profile image
            
            UIImageView *profileImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 50, 50)];
            
            profileImage.layer.cornerRadius = 25.0;
            
            profileImage.clipsToBounds = YES;
            
            [profileImage sd_setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [[postArr objectAtIndex:indexPath.row] valueForKey:@"profilePic"] ] ] placeholderImage:[UIImage imageNamed:@"profile_demo"]];
            
            
            [cell.contentView addSubview:profileImage];
            
            
            //Profile Picture button
            
            UIButton *profilePicBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            
            profilePicBtn.tag = indexPath.row;
            
            [profilePicBtn addTarget:self action:@selector(tapOnProfileImage:) forControlEvents:UIControlEventTouchUpInside];
            
            profilePicBtn.frame = profileImage.frame;
            
            [cell.contentView addSubview:profilePicBtn];
            
            
            
            // Name;
            
            UILabel *nameLbl = [[UILabel alloc] initWithFrame:CGRectMake(profileImage.frame.origin.x + profileImage.frame.size.width+5, 20, [[UIScreen mainScreen] bounds].size.width-5, 20)];
            
            //nameLbl.font = [UIFont fontWithName:@"MuseoSansCyrl-700" size:13];
            
            nameLbl.font = [UIFont boldSystemFontOfSize:15];
            
            nameLbl.textColor = [UIColor whiteColor];
            
            nameLbl.text = [[[postArr objectAtIndex:indexPath.row] objectForKey:@"fullname"] lowercaseString];
            
            [cell.contentView addSubview:nameLbl];
            
            
            
            //location
            
            UILabel *locationLbl = [[UILabel alloc] initWithFrame:CGRectMake(profileImage.frame.origin.x + profileImage.frame.size.width+5, nameLbl.frame.origin.y + nameLbl.frame.size.height, [[UIScreen mainScreen] bounds].size.width-60, 20)];
            
            //locationLbl.font = [UIFont fontWithName:@"MuseoSansCyrl-500" size:13];
            
            locationLbl.font = [UIFont systemFontOfSize:13];
            
            locationLbl.textColor = [UIColor whiteColor];
            
            locationLbl.text = [[postArr objectAtIndex:indexPath.row] objectForKey:@"location"];
            
            
            [cell.contentView addSubview:locationLbl];
            
            
            
            //LocationTap
            
            UIButton *locationBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            
            locationBtn.backgroundColor = [UIColor clearColor];
            
            locationBtn.tag = indexPath.row;
            
            locationBtn.frame = locationLbl.frame;
            
            [locationBtn addTarget:self action:@selector(locationTap:) forControlEvents:UIControlEventTouchUpInside];
            
            if (locationLbl.text.length)
            {
                [cell.contentView addSubview:locationBtn];
            }
            
            
            
            //Follow button
            
            if ([[[postArr objectAtIndex:indexPath.row] valueForKey:@"userId"] intValue] != [[(APPDEL).userDetailDict objectForKey:@"userId"] intValue])
            {
                UIButton *followBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                
                followBtn.tag = indexPath.row;
                
                [followBtn addTarget:self action:@selector(followTap:) forControlEvents:UIControlEventTouchUpInside];
                
                
                if ([[[postArr objectAtIndex:indexPath.row] objectForKey:@"youfollowing"] intValue] == 0)
                {
                    [followBtn setTitle:@"Follow" forState:UIControlStateNormal];
                    
                    followBtn.backgroundColor = [UIColor whiteColor];
                    
                    [followBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                }
                else if ([[[postArr objectAtIndex:indexPath.row] objectForKey:@"youfollowing"] intValue] == 1)
                {
                    [followBtn setTitle:@"Following" forState:UIControlStateNormal];
                    
                    followBtn.backgroundColor = [UIColor blackColor];
                    
                    [followBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                }
                else
                {
                    [followBtn setTitle:@"Requested" forState:UIControlStateNormal];
                    
                    followBtn.backgroundColor = [UIColor blackColor];
                    
                    [followBtn setTitleColor: [UIColor whiteColor] forState:UIControlStateNormal];
                }
                
                //followBtn.titleLabel.font = [UIFont fontWithName:@"MuseoSansCyrl-500" size:12];
                
                followBtn.titleLabel.font = [UIFont systemFontOfSize:12];
                
                followBtn.frame = CGRectMake(([[UIScreen mainScreen] bounds].size.width - 65) -15, 23, 65, 25);
                
                followBtn.layer.cornerRadius = 2.0;
                
                followBtn.layer.borderWidth = 1.0;
                
                followBtn.layer.borderColor = [UIColor blackColor].CGColor;
                
                //[cell.contentView addSubview:followBtn];
            }
            
            
            //Media
            
            UIView *mediaView = [[UIView alloc] init];
            
            mediaView.backgroundColor = [UIColor groupTableViewBackgroundColor];
            
            
            if ([[[postArr objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"image"])
            {
                int backEndHeight = [[[postArr objectAtIndex:indexPath.row] valueForKey:@"height"] intValue];
                
                int backEndscreenWidth = [[[postArr objectAtIndex:indexPath.row] valueForKey:@"width"] intValue];
                
                
                float scaleFator = [[UIScreen mainScreen] bounds].size.width/backEndscreenWidth;
                
                
                float actualHeight = scaleFator * backEndHeight;
                
                mediaView.frame = CGRectMake(0, locationLbl.frame.origin.y + locationLbl.frame.size.height+5, [[UIScreen mainScreen] bounds].size.width, actualHeight);
                
                
                UIImageView *mediaImageView = [[UIImageView alloc] init];
                
                mediaImageView.backgroundColor = [UIColor clearColor];
                
                
                mediaImageView.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, actualHeight);
                
                
                [mediaView addSubview:mediaImageView];
                
                
                [mediaImageView setShowActivityIndicatorView:YES];
                
                [mediaImageView setIndicatorStyle:UIActivityIndicatorViewStyleGray];
                
                
                //[mediaImageView sd_setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [[postArr objectAtIndex:indexPath.row] valueForKey:@"media"] ] ] placeholderImage:nil options:SDWebImageRefreshCached];
                SDWebImageManager *manager = [SDWebImageManager sharedManager];
                
                [manager downloadImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [[postArr objectAtIndex:indexPath.row] valueForKey:@"media"] ] ] options:SDWebImageLowPriority progress:^(NSInteger receivedSize, NSInteger expectedSize)
                 {
                     //NSLog(@"received - %li,  expect - %li", (long)receivedSize, (long)expectedSize);
                     
                 } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                     if (image)
                     {
                         mediaImageView.image = image;
                     }
                 }];
                
                //                [manager downloadWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [[postArr objectAtIndex:indexPath.row] valueForKey:@"media"] ] ]
                //                                 options:0
                //                                progress:^(NSInteger receivedSize, NSInteger expectedSize)
                //                 {
                //                     // progression tracking code
                //                 }
                //                    completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished)
                //                 {
                //                     if (image)
                //                     {
                //                         mediaImageView.image = image;
                //                     }
                //                 }];
                
                
                [cell.contentView addSubview:mediaView];
                
                
                //Tag people Btn
                
                if ([(NSArray *)[[postArr objectAtIndex:indexPath.row] valueForKey:@"tagpeople"] count])
                {
                    UIButton *tagPeopleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                    
                    tagPeopleBtn.tag = indexPath.row;
                    
                    tagPeopleBtn.frame = CGRectMake(10, (mediaView.frame.origin.y + mediaView.frame.size.height) - 50, 25, 25);
                    
                    [tagPeopleBtn addTarget:self action:@selector(showTagPeopleView:) forControlEvents:UIControlEventTouchUpInside];
                    
                    
                    [tagPeopleBtn setImage:[UIImage imageNamed:@"tag_Pic"] forState:UIControlStateNormal];
                    
                    // [cell.contentView addSubview:tagPeopleBtn];
                }
            }
            else
            {
                
                mediaView.frame = CGRectMake(0, locationLbl.frame.origin.y + locationLbl.frame.size.height+5, [[UIScreen mainScreen] bounds].size.width, 350);
                
                InstagramVideoView *videoView = [[InstagramVideoView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 350)];
                
                
                videoView.backgroundColor = [UIColor clearColor];
                
                
                [mediaView addSubview:videoView];
                
                [cell.contentView addSubview:mediaView];
                
                dispatch_async(dispatch_get_main_queue(),
                               ^{
                                   if(isScrolling == NO)
                                   {
                                       [videoView resume];
                                       
                                       [videoView playVideoWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", ServiceURL, [[postArr objectAtIndex:indexPath.row] valueForKey:@"media"]]] ];
                                       
                                       [videoPlayerObj addObject:videoView];
                                   }
                                   else
                                   {
                                       [videoView stopVideoPlay];
                                   }
                               });
            }
            
            
            // like Buttom
            
            UIButton *likeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            
            likeBtn.tag = indexPath.row;
            
            [likeBtn addTarget:self action:@selector(likeTap:) forControlEvents:UIControlEventTouchUpInside];
            
            likeBtn.frame = CGRectMake(10, mediaView.frame.origin.y + mediaView.frame.size.height+10, 25, 25);
            
            
            if ([[[postArr objectAtIndex:indexPath.row] valueForKey:@"youlike"] intValue] == 0)
            {
                [likeBtn setImage:[UIImage imageNamed:@"like"] forState:UIControlStateNormal];
            }
            else
            {
                [likeBtn setImage:[UIImage imageNamed:@"like_h"] forState:UIControlStateNormal];
            }
            
            
            
            [cell.contentView addSubview:likeBtn];
            
            
            // Comment Buttom
            
            UIButton *commentBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            
            commentBtn.tag = indexPath.row;
            
            
            commentBtn.frame = CGRectMake(likeBtn.frame.origin.x + likeBtn.frame.size.width + 10, likeBtn.frame.origin.y, 25, 25);
            
            if ([[[postArr objectAtIndex:indexPath.row] objectForKey:@"commentflag"] intValue] == 0)
            {
                [commentBtn setImage:[UIImage imageNamed:@"comment_off"] forState: UIControlStateNormal];
            }
            else
            {
                [commentBtn setImage:[UIImage imageNamed:@"comment"] forState: UIControlStateNormal];
                
                [commentBtn addTarget:self action:@selector(commentTap:) forControlEvents:UIControlEventTouchUpInside];
            }
            
            [cell.contentView addSubview:commentBtn];
            
            
            // Favourite Buttom
            
            UIButton *favBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            
            favBtn.tag = indexPath.row;
            
            [favBtn addTarget:self action:@selector(favTap:) forControlEvents:UIControlEventTouchUpInside];
            
            favBtn.frame = CGRectMake(commentBtn.frame.origin.x + commentBtn.frame.size.width + 10, likeBtn.frame.origin.y, 25, 25);
            
            if ([[[postArr objectAtIndex:indexPath.row] valueForKey:@"favourite"] intValue] == 0)
            {
                [favBtn setImage:[UIImage imageNamed:@"star"] forState:UIControlStateNormal];
            }
            else
            {
                [favBtn setImage:[UIImage imageNamed:@"star_h"] forState:UIControlStateNormal];
            }
            
            [cell.contentView addSubview:favBtn];
            
            
            
            // Three Dot Button
            
            UIButton *dotBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            
            dotBtn.tag = indexPath.row;
            
            [dotBtn addTarget:self action:@selector(dotsTap:) forControlEvents:UIControlEventTouchUpInside];
            
            dotBtn.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width - 50, likeBtn.frame.origin.y, 40, 30);
            
            [dotBtn setImage:[UIImage imageNamed:@"three_dots"] forState:UIControlStateNormal];
            
            [cell.contentView addSubview:dotBtn];
            
            
            
            //Total LikesLabel
            
            UILabel *totalLikeLbl = [[UILabel alloc] init];
            
            totalLikeLbl.backgroundColor = [UIColor clearColor];
            
            //totalLikeLbl.font = [UIFont fontWithName:@"MuseoSansCyrl-500" size:14];
            
            totalLikeLbl.font = [UIFont boldSystemFontOfSize:14];
            
            totalLikeLbl.textAlignment = NSTextAlignmentLeft;
            
            totalLikeLbl.textColor = [UIColor whiteColor];
            
            
            // Total like Button
            
            UIButton *getTotlLikeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            
            getTotlLikeBtn.backgroundColor = [UIColor clearColor];
            
            getTotlLikeBtn.tag = indexPath.row;
            
            [getTotlLikeBtn addTarget:self action:@selector(getTotalLike:) forControlEvents:UIControlEventTouchUpInside];
            
            
            
            //Caption Label
            
            
            ResponsiveLabel *captionLbl = [[ResponsiveLabel alloc] init];
            
            captionLbl.numberOfLines = 999;
            
            //captionLbl.font = [UIFont fontWithName:@"MuseoSansCyrl-500" size:14];
            
            captionLbl.font = [UIFont systemFontOfSize:14];
            
            captionLbl.textColor = [UIColor whiteColor];
            
            // Get the Label height
            
            NSString *dataStr = @"";
            
            if (![[[postArr objectAtIndex:indexPath.row] objectForKey:@"caption"] isEqualToString:@""])
            {
                dataStr = [NSString stringWithFormat:@"%@ %@", [[[postArr objectAtIndex:indexPath.row] objectForKey:@"fullname"] lowercaseString], [[postArr objectAtIndex:indexPath.row] objectForKey:@"caption"]];
                
            }
            
            /*NSArray *commentArr = [[postArr objectAtIndex:indexPath.row] objectForKey:@"allComment"];
             
             NSString *totalComment = @"";
             
             if ([commentArr count])
             {
             NSMutableString *commentStr = [@"" mutableCopy];
             
             for (int i = 0; i < commentArr.count; i++)
             {
             [commentStr appendFormat:@"%@\n", [NSString stringWithFormat:@"%@ %@", [[commentArr objectAtIndex:i] objectForKey:@"fullname"], [[commentArr objectAtIndex:i] objectForKey:@"comment"]]];
             
             
             //string detection
             PatternTapResponder commentUser = ^(NSString *string)
             {
             NSLog(@"tapped = %@",string);
             };
             
             [captionLbl enableStringDetection:[[commentArr objectAtIndex:i] objectForKey:@"fullname"] withAttributes:@{NSFontAttributeName : [UIFont fontWithName:@"MuseoSansCyrl-700" size:14],NSForegroundColorAttributeName:[UIColor blackColor],RLTapResponderAttributeName: commentUser}];
             //
             }
             
             if ([[[postArr objectAtIndex:indexPath.row] objectForKey:@"totalcomment"] intValue] > 1)
             {
             totalComment = [[postArr objectAtIndex:indexPath.row] objectForKey:@"totalcomment"];
             
             dataStr = [NSString stringWithFormat:@"%@\nView all %@ comments\n\n%@", dataStr, totalComment, commentStr];
             
             }
             else
             {
             dataStr = [NSString stringWithFormat:@"%@\n%@", dataStr, commentStr];
             }
             }
             
             if ([[[postArr objectAtIndex:indexPath.row] objectForKey:@"totalcomment"] intValue] > 1)
             {
             if (dataStr.length)
             {
             dataStr = [NSString stringWithFormat:@"%@\nView all %@ comments", dataStr, [[postArr objectAtIndex:indexPath.row] objectForKey:@"totalcomment"]];
             }
             else
             {
             dataStr = [NSString stringWithFormat:@"View all %@ comments", [[postArr objectAtIndex:indexPath.row] objectForKey:@"totalcomment"]];
             }
             }
             else if ([[[postArr objectAtIndex:indexPath.row] objectForKey:@"totalcomment"] intValue] == 1)
             {
             if (dataStr.length)
             {
             dataStr = [NSString stringWithFormat:@"%@\nView all %@ comments", dataStr, [[postArr objectAtIndex:indexPath.row] objectForKey:@"totalcomment"]];
             }
             else
             {
             dataStr = [NSString stringWithFormat:@"View all %@ comments", [[postArr objectAtIndex:indexPath.row] objectForKey:@"totalcomment"]];
             }
             
             }
             else
             {
             if (dataStr.length)
             {
             dataStr = [NSString stringWithFormat:@"%@\nView all comments", dataStr];
             }
             else
             {
             dataStr = @"View all comments";
             }
             }*/
            
            captionLbl.text = dataStr;
            
            
            
            //Hastag detection
            
            captionLbl.userInteractionEnabled = YES;
            PatternTapResponder hashTagTapAction = ^(NSString *tappedString)
            {
                NSLog(@"HashTag Tapped = %@",tappedString);
                
                
                ViewPostBySearchViewController *vc = [[ViewPostBySearchViewController alloc] initWithNibName:@"ViewPostBySearchViewController" bundle:nil];
                
                vc.isSeacrhTagPeople = YES;
                
                vc.searchStr = [tappedString stringByReplacingOccurrencesOfString:@"#" withString:@""];
                
                [self.navigationController pushViewController:vc animated:YES];
            };
            
            [captionLbl enableHashTagDetectionWithAttributes:
             @{NSForegroundColorAttributeName:hashtag_color, RLTapResponderAttributeName:hashTagTapAction}];
            
            //
            
            
            //string detection
            PatternTapResponder tapResponder = ^(NSString *string)
            {
                NSLog(@"tapped = %@",string);
            };
            
            [captionLbl enableStringDetection:nameLbl.text withAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName : [UIFont boldSystemFontOfSize:14], RLTapResponderAttributeName: tapResponder}];
            //
            
            
            
            //Username detection
            
            captionLbl.userInteractionEnabled = YES;
            PatternTapResponder userNameDetection = ^(NSString *tappedString)
            {
                NSLog(@"HashTag Tapped = %@",tappedString);
                
                [self getUserIdFromUsername:[NSDictionary dictionaryWithObjectsAndKeys:tappedString, @"otherusername", [(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", nil]];
            };
            
            [captionLbl enableUserHandleDetectionWithAttributes:@{NSForegroundColorAttributeName:hashtag_color, RLTapResponderAttributeName:userNameDetection}];
            
            //
            
            
            
            CGRect textRect;
            
            if (dataStr.length)
            {
                textRect = [dataStr boundingRectWithSize:CGSizeMake([[UIScreen mainScreen] bounds].size.width-20, FLT_MAX)
                                                 options:NSStringDrawingUsesLineFragmentOrigin
                                              attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]}
                                                 context:nil];
            }
            
            
            
            int captionLblHeigtht = textRect.size.height;
            
            
            
            //View all comment label
            
            ResponsiveLabel *viewAllCommentLbl = [[ResponsiveLabel alloc] init];
            
            //viewAllCommentLbl.font = [UIFont fontWithName:@"MuseoSansCyrl-500" size:14];
            
            viewAllCommentLbl.font = [UIFont systemFontOfSize:14];
            
            viewAllCommentLbl.textColor = [UIColor lightGrayColor];
            
            
            if ([[[postArr objectAtIndex:indexPath.row] objectForKey:@"totalcomment"] intValue] > 1)
            {
                viewAllCommentLbl.text = [NSString stringWithFormat:@"View all %@ comments", [[postArr objectAtIndex:indexPath.row] objectForKey:@"totalcomment"]];
                
            }
            else if ([[[postArr objectAtIndex:indexPath.row] objectForKey:@"totalcomment"] intValue] == 1)
            {
                viewAllCommentLbl.text = [NSString stringWithFormat:@"View all %@ comments", [[postArr objectAtIndex:indexPath.row] objectForKey:@"totalcomment"]];
            }
            else
            {
                viewAllCommentLbl.text = @"View all comments";
            }
            
            
            //string detection
            PatternTapResponder viewAllCountComment = ^(NSString *string)
            {
                UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
                
                btn.tag = indexPath.row;
                
                [self commentTap:btn];
            };
            
            [viewAllCommentLbl enableStringDetection:[NSString stringWithFormat:@"View all %@ comments", [[postArr objectAtIndex:indexPath.row] objectForKey:@"totalcomment"]] withAttributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor],RLTapResponderAttributeName: viewAllCountComment}];
            //
            
            
            //string detection
            PatternTapResponder viewAllcomment = ^(NSString *string)
            {
                UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
                
                btn.tag = indexPath.row;
                
                [self commentTap:btn];
            };
            
            [viewAllCommentLbl enableStringDetection:@"View all comments" withAttributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor],RLTapResponderAttributeName: viewAllcomment}];
            //
            
            
            
            //Time Label
            
            UILabel *timeLbl = [[UILabel alloc] init];
            
            //timeLbl.font = [UIFont fontWithName:@"MuseoSansCyrl-500" size:13];
            
            timeLbl.font = [UIFont systemFontOfSize:13];
            
            timeLbl.textColor = [UIColor lightGrayColor];
            
            if ([[[postArr objectAtIndex: indexPath.row] objectForKey:@"totallike"] intValue] != 0)
            {
                NSString *singularPluralStr;
                
                if ([[[postArr objectAtIndex: indexPath.row] objectForKey:@"totallike"] intValue] <= 1 )
                {
                    singularPluralStr = @"like";
                }
                else
                {
                    singularPluralStr = @"likes";
                }
                
                totalLikeLbl.frame = CGRectMake(10, likeBtn.frame.origin.y + likeBtn.frame.size.height + 5, [[UIScreen mainScreen] bounds].size.width - 20, 15);
                
                getTotlLikeBtn.frame = totalLikeLbl.frame;
                
                
                totalLikeLbl.text = [NSString stringWithFormat:@"%@ %@", [[postArr objectAtIndex: indexPath.row] objectForKey:@"totallike"], singularPluralStr];
                
                
                [cell.contentView addSubview:totalLikeLbl];
                
                [cell.contentView addSubview:getTotlLikeBtn];
                
                
                if (dataStr.length)
                {
                    captionLbl.frame = CGRectMake(10, totalLikeLbl.frame.origin.y + totalLikeLbl.frame.size.height+5, [[UIScreen mainScreen] bounds].size.width - 20, captionLblHeigtht+10);
                    
                    [cell.contentView addSubview:captionLbl];
                    
                    
                    viewAllCommentLbl.frame = CGRectMake(10, captionLbl.frame.origin.y + captionLbl.frame.size.height, 200, 15);
                    
                    
                    timeLbl.frame = CGRectMake(10, viewAllCommentLbl.frame.origin.y + viewAllCommentLbl.frame.size.height+5, 200, 15);
                    
                    
                    
                    timeLbl.text = [NSString stringWithFormat:@"%@", [[postArr objectAtIndex:indexPath.row] objectForKey:@"posttime"]];
                    
                    
                    [cell.contentView addSubview:viewAllCommentLbl];
                    
                    [cell.contentView addSubview:timeLbl];
                }
                else
                {
                    viewAllCommentLbl.frame = CGRectMake(10, totalLikeLbl.frame.origin.y + totalLikeLbl.frame.size.height+5, 200, 15);
                    
                    timeLbl.frame = CGRectMake(10, viewAllCommentLbl.frame.origin.y + viewAllCommentLbl.frame.size.height+5, 100, 15);
                    
                    
                    
                    timeLbl.text = [NSString stringWithFormat:@"%@", [[postArr objectAtIndex:indexPath.row] objectForKey:@"posttime"]];
                    
                    [cell.contentView addSubview:viewAllCommentLbl];
                    
                    [cell.contentView addSubview:timeLbl];
                }
            }
            else
            {
                if (dataStr.length)
                {
                    captionLbl.frame = CGRectMake(10, likeBtn.frame.origin.y + likeBtn.frame.size.height, [[UIScreen mainScreen] bounds].size.width-20, captionLblHeigtht+10);
                    
                    [cell.contentView addSubview:captionLbl];
                    
                    
                    captionLbl.frame = CGRectMake(10, likeBtn.frame.origin.y + likeBtn.frame.size.height, [[UIScreen mainScreen] bounds].size.width-20, captionLblHeigtht+10);
                    
                    [cell.contentView addSubview:captionLbl];
                    
                    
                    viewAllCommentLbl.frame = CGRectMake(10, captionLbl.frame.origin.y + captionLbl.frame.size.height, 200, 15);
                    
                    timeLbl.frame = CGRectMake(10, viewAllCommentLbl.frame.origin.y + viewAllCommentLbl.frame.size.height+5, 100, 15);
                    
                    
                    
                    timeLbl.text = [NSString stringWithFormat:@"%@", [[postArr objectAtIndex:indexPath.row] objectForKey:@"posttime"]];
                    
                    [cell.contentView addSubview:viewAllCommentLbl];
                    
                    [cell.contentView addSubview:timeLbl];
                }
                else
                {
                    viewAllCommentLbl.frame = CGRectMake(10, likeBtn.frame.origin.y + likeBtn.frame.size.height, 200, 15);
                    
                    timeLbl.frame = CGRectMake(10, viewAllCommentLbl.frame.origin.y + viewAllCommentLbl.frame.size.height+5, 100, 15);
                    
                    
                    
                    timeLbl.text = [NSString stringWithFormat:@"%@", [[postArr objectAtIndex:indexPath.row] objectForKey:@"posttime"]];
                    
                    [cell.contentView addSubview:viewAllCommentLbl];
                    
                    [cell.contentView addSubview:timeLbl];
                }
            }
            
            return cell;
            
        }
    }
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (isGridView == YES)
    {
        ShowPostsViewController *vc = [[ShowPostsViewController alloc] initWithNibName:@"ShowPostsViewController" bundle:nil];
        
        vc.hidesBottomBarWhenPushed = YES;
        
        
        vc.postArr = postArr;
        
        vc.indexPath = indexPath;
        
        
        [self.navigationController pushViewController:vc animated:YES];
    }
    else
    {
        if ([postArr[indexPath.row] isKindOfClass:[UIView class]])
        {
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:@"Nice Job!"
                                                  message:
                                                  @"You have activated the Daily Double. All points you earn liking photos today will now be doubled."
                                                  preferredStyle:UIAlertControllerStyleAlert];
            
            
            
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(220, 10, 45, 35)];
            
            imageView.image = [UIImage imageNamed:@"dailyDouble"];
            
            
            UIAlertAction *oKBtn = [UIAlertAction
                                        actionWithTitle:@"OK"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction *action)
                                        {
                                            [self activeDailyDouble: indexPath.row];
                                            
                                            return;
                                        }];
            
            
            
            [alertController.view addSubview:imageView];
            
            [alertController addAction:oKBtn];
            
            
            
            [self presentViewController:alertController animated:YES completion:nil];
            
            
            /*if ([[GADRewardBasedVideoAd sharedInstance] isReady])
            {
                //wasPlayRewardVideo = YES;
             
                [floatButton removeFromSuperview];
             
                [[GADRewardBasedVideoAd sharedInstance] presentFromRootViewController:self];
            }
            else
            {
                [[ServiceHelper sharedManager] reloadRewardVideoAlert];
             
                [self loadAdd];
            }*/
        }
        else if (![postArr[indexPath.row] isKindOfClass:[GADNativeExpressAdView class]])
        {
            UICollectionViewCell *cell = (UICollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
            
            if ([[[postArr objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"image"])
            {
                [self addTagBtnOnCell:cell sender:[UIButton buttonWithType:UIButtonTypeCustom] personTagArr:[[postArr objectAtIndex:indexPath.row] valueForKey:@"tagpeople"]];
            }
        }
    }
}


- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (postArr.count > 1)
    {
        if (![postArr[indexPath.row] isKindOfClass:[GADNativeExpressAdView class]])
        {
            //if ([[[postArr objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"image"])
            //{
                // ListCollectionViewCell *imageCell = (ListCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
            //}
        }
    }
}




- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
  
    
       for (UIView *view in cell.subviews)
        {
            if ([view isKindOfClass:[JDTagView class]])
            {
                [view removeFromSuperview];
            }
        }
    
        //imageCell.tagBtn.alpha = 1;
    

   
    /*ListCollectionViewVideoCell *videoCell = (ListCollectionViewVideoCell *)cell;
    
    if (postArr.count > 1)
    {
     
        NSLog(@"%ld", indexPath.row);
     
        if (indexPath.row <= postArr.count-1)
        {
            if ([[[postArr objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"video"])
            {
                if ([videoCell isKindOfClass:[ListCollectionViewVideoCell class]])
                {
                    [videoCell.player.player pause];
     
                    [videoCell.videoView stopVideoPlay];
     
                    [videoCell.player removeObserver];
                }
            }
            else
            {
                ListCollectionViewCell *imageCell = (ListCollectionViewCell *)cell;
     
     
     
                if ([imageCell isKindOfClass:[ListCollectionViewCell class]])
                {
                    for (UIView *view in cell.subviews)
                    {
                        if ([view isKindOfClass:[JDTagView class]])
                        {
                            [view removeFromSuperview];
                        }
                    }
     
                    imageCell.tagBtn.alpha = 1;
                }
     
            }
        }
    }*/
}



- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
//    isScrolling = NO;
//    
//    [userCollectionView reloadData];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)aScrollView
{
//    [self clearvideoViews];
//    
//    isScrolling = YES;
//   
//    [userCollectionView reloadData];
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
//    NSLog(@"%i<><><> %i", (int)scrollView.contentSize.height, (int)scrollView.contentOffset.y + (int)scrollView.frame.size.height);
//    
//    if ((int)scrollView.contentSize.height == (int)scrollView.contentOffset.y + (int)scrollView.frame.size.height)
//    {
//        [self viewUsersPostsWebserviceService:YES];
//    }
}


- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    
}




#pragma mark - UISearchBar Delegate


- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    SearchViewController *vc = [[SearchViewController alloc] initWithNibName:@"SearchViewController" bundle:nil];
    
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:NO];
    
    return NO;
}


#pragma mark - JDTagView Delegate

- (void)selectedTag:(JDTagView *)View
{
    ListCollectionViewCell *cell = (ListCollectionViewCell *)View.superview;
    
    UICollectionView *collectionView = (UICollectionView *)View.superview.superview;
    
    NSIndexPath *indexPath = [collectionView indexPathForCell:cell];
    
    NSArray *taggArr = [[postArr objectAtIndex:indexPath.row] objectForKey:@"tagpeople"];
    
    
    ProfileViewController *vc = [[ProfileViewController alloc] initWithNibName:@"ProfileViewController" bundle:nil];
    
    vc.differentUserId = [[taggArr objectAtIndex:View.tag-1] objectForKey:@"userId"];
    
    [self.navigationController pushViewController:vc animated:YES];
}


- (void)deleteTagFromView:(JDTagView *)JDTagView
{
    
}


#pragma mark - ActionSheet

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"%ld", actionSheet.tag);
   
    if ([[[postArr objectAtIndex:actionSheet.tag] objectForKey:@"userId"] intValue] == [[(APPDEL).userDetailDict objectForKey:@"userId"] intValue])
    {
        if(buttonIndex == 0)
        {
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:nil
                                                  message:@"Delete Post?"
                                                  preferredStyle:UIAlertControllerStyleAlert];
            
            alertController.view.tag = actionSheet.tag;
            
            UIAlertAction *yesAction = [UIAlertAction
                                                  actionWithTitle:@"YES"
                                                  style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction *action)
                                                  {
                                                      
                                                      [self deletePost:actionSheet.tag postId:[[postArr objectAtIndex:actionSheet.tag] valueForKey:@"postId"]];
                                                  }];
            
            UIAlertAction *noAction = [UIAlertAction
                                         actionWithTitle:@"NO"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction *action)
                                         {
                                            
                                         }];
            
    
           
            [alertController addAction:yesAction];
            
            [alertController addAction:noAction];
            
            
            [self presentViewController:alertController animated:YES completion:nil];
        }
        
        else if(buttonIndex == 1)
        {
            [self turnOffCommenting:actionSheet.tag params:[NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", [[postArr objectAtIndex:actionSheet.tag] objectForKey:@"postId"], @"postId", [[[postArr objectAtIndex:actionSheet.tag] objectForKey:@"commentflag"] intValue] == 0 ? @"1" : @"0", @"flag", nil]];
        }
        else if(buttonIndex == 2)
        {
            EditPostViewController *vc = [[EditPostViewController alloc] initWithNibName:@"EditPostViewController" bundle:nil];
            
            vc.postDict = [postArr objectAtIndex:actionSheet.tag];
            
            vc.isEdit = YES;
            
            [self.navigationController pushViewController:vc animated:NO];
            
        }
        else if(buttonIndex == 3)
        {
            
        }
    }
    
    else
    {
        if(buttonIndex == 0)
        {
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:nil
                                                  message:nil
                                                  preferredStyle:UIAlertControllerStyleActionSheet];
            
            alertController.view.tag = actionSheet.tag;
            
            UIAlertAction *InappropriateAction = [UIAlertAction
                                           actionWithTitle:@"Inappropriate Post"
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction *action)
                                           {
                                               [self reportAbuse:[[postArr objectAtIndex:alertController.view.tag] valueForKey:@"postId"] secondryId:[[postArr objectAtIndex:alertController.view.tag] valueForKey:@"userId"] isInappropiate:@"1"];
                                               
                                               NSLog(@"Inappropriate, %li", (long)alertController.view.tag);
                                           }];
            
            UIAlertAction *SpamAction = [UIAlertAction
                                       actionWithTitle:@"Spam Account"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                           [self reportAbuse:[[postArr objectAtIndex:alertController.view.tag] valueForKey:@"postId"] secondryId:[[postArr objectAtIndex:alertController.view.tag] valueForKey:@"userId"] isInappropiate:@"2"];
                                           
                                           NSLog(@"Spam Account");
                                       }];
            
            UIAlertAction *resetAction = [UIAlertAction
                                          actionWithTitle:@"Cancel"
                                          style:UIAlertActionStyleCancel
                                          handler:^(UIAlertAction *action)
                                          {
                                              NSLog(@"Cancel, %li", (long)alertController.view.tag);
                                          }];
            
            
            [alertController addAction:resetAction];
            
            [alertController addAction:InappropriateAction];
           
            [alertController addAction:SpamAction];
            
            
            [self presentViewController:alertController animated:YES completion:nil];

        }
    }
    
}



/*#pragma mark - Google

// MARK: - GADRewardVideo delegate methods


- (void)rewardBasedVideoAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd
   didRewardUserWithReward:(GADAdReward *)reward
{
    NSString *rewardMessage = [NSString stringWithFormat:@"Reward received with currency %@ , amount %lf",
    
    reward.type,
     
    [reward.amount doubleValue]];
   
    NSLog(@"%@", rewardMessage);
    
    ifUserGetRewarded = YES;
    
    [refreshControlBottom endRefreshing];

    
//    [postArr removeLastObject];
//    
//    [latestDataArr removeLastObject];
    
    
    
    [userCollectionView reloadData];
    
   // [self removeFloatButton];
}

- (void)rewardBasedVideoAdDidReceiveAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd
{
    NSLog(@"Reward based video ad is received.");
    
   
    
    
//    floatButton.alpha = 1;
//    
//    floatButton.userInteractionEnabled = YES;
}

- (void)rewardBasedVideoAdDidOpen:(GADRewardBasedVideoAd *)rewardBasedVideoAd
{
    NSLog(@"Opened reward based video ad.");
    
    [refreshControlBottom endRefreshing];
}

- (void)rewardBasedVideoAdDidStartPlaying:(GADRewardBasedVideoAd *)rewardBasedVideoAd
{
    NSLog(@"Reward based video ad started playing.");
    
    [refreshControlBottom endRefreshing];
}

- (void)rewardBasedVideoAdDidClose:(GADRewardBasedVideoAd *)rewardBasedVideoAd
{
    NSLog(@"Reward based video ad is closed.");
    
    [refreshControlBottom endRefreshing];
}

- (void)rewardBasedVideoAdWillLeaveApplication:(GADRewardBasedVideoAd *)rewardBasedVideoAd
{
    NSLog(@"Reward based video ad will leave application.");
    
    [refreshControlBottom endRefreshing];
}

- (void)rewardBasedVideoAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd
    didFailToLoadWithError:(NSError *)error
{
    NSLog(@"Reward based video ad failed to load.");
    
    [refreshControlBottom endRefreshing];
}*/



@end
