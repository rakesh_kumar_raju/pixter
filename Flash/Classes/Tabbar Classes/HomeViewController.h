//
//  HomeViewController.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 1/2/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "UIImageView+WebCache.h"

#import "ZOWVideoView.h"
#import "InstagramVideoView.h"

#import <MediaPlayer/MediaPlayer.h>


@import GoogleMobileAds;


@interface HomeViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource, UISearchBarDelegate>
{
    IBOutlet UICollectionView *userCollectionView;
    
    IBOutlet UICollectionView *liveVideoCollectionView;
    
    UIRefreshControl *refreshControl;
    
   IBOutlet UISearchBar *searchBar;
    
    IBOutlet UILabel *messgeCountLbl, *rewardLbl;
    
    IBOutlet UIView *unfollowView, *popupView;
    
    IBOutlet UIImageView *unfollowUserImg;
    
    IBOutlet UILabel *unfollowUserNameLbl;
}

@property (nonatomic, retain) MPMoviePlayerViewController *moviewPlayer;

@property (nonatomic, strong) IBOutlet InstagramVideoView *videoView;


- (IBAction)changeLayoutTap:(UIButton *)sender;

- (IBAction)messageTap;

- (IBAction)unfollowCancelTap;

- (IBAction)unfollowTap;

- (IBAction)searchTap;

@end
