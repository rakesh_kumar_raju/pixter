//
//  TabbarViewController.m
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 1/2/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//


#import "TabbarViewController.h"
#import "HomeViewController.h"
#import "ProfileViewController.h"
#import "CameraViewController.h"
#import "NotificationViewController.h"
#import "RewardViewController.h"


@interface TabbarViewController ()
{
    UIViewController *previousController;
    BOOL check;
}

@end



@implementation TabbarViewController

#pragma mark - ViewCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)])
    {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    [self setUpView];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
}


#pragma mark - Void

- (void)setUpView
{
    [[UITabBar appearance] setTintColor:[UIColor whiteColor]];
    
    HomeViewController *vc1 = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];


    vc1.tabBarItem.image = [UIImage imageNamed:@"tab_1"];
    
   // vc1.tabBarItem.selectedImage = [[UIImage imageNamed:@"tab_h_1"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    vc1.tabBarItem.imageInsets = UIEdgeInsetsMake(5, 0, -5, 0);
    
    
    ProfileViewController *vc2 = [[ProfileViewController alloc] initWithNibName:@"ProfileViewController" bundle:nil];

    vc2.tabBarItem.image = [UIImage imageNamed:@"tab_2"];
    
   // vc2.tabBarItem.selectedImage = [[UIImage imageNamed:@"tab_h_2"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    vc2.tabBarItem.imageInsets = UIEdgeInsetsMake(5, 0, -5, 0);
    
    
    CameraViewController *vc3 = [[CameraViewController alloc] initWithNibName:@"CameraViewController" bundle:nil];

    vc3.tabBarItem.image = [UIImage imageNamed:@"tab_3"];
    
   
    //vc3.tabBarItem.selectedImage = [[UIImage imageNamed:@"tab_h_3"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    vc3.tabBarItem.imageInsets = UIEdgeInsetsMake(5, 0, -5, 0);
    
    
    NotificationViewController *vc4 = [[NotificationViewController alloc] initWithNibName:@"NotificationViewController" bundle:nil];

    vc4.tabBarItem.image = [UIImage imageNamed:@"tab_4"];
    
    //vc4.tabBarItem.selectedImage = [[UIImage imageNamed:@"tab_h_4"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    vc4.tabBarItem.imageInsets = UIEdgeInsetsMake(5, 0, -5, 0);
    
    
    RewardViewController *vc5 = [[RewardViewController alloc] initWithNibName:@"RewardViewController" bundle:nil];
    
    vc5.tabBarItem.image = [UIImage imageNamed:@"tab_5"];

    //vc5.tabBarItem.selectedImage = [[UIImage imageNamed:@"tab_h_5"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    vc5.tabBarItem.imageInsets = UIEdgeInsetsMake(5, 0, -5, 0);
    
    
    UINavigationController *nVc1 = [[UINavigationController alloc] initWithRootViewController:vc1];

    UINavigationController *nVc2 = [[UINavigationController alloc] initWithRootViewController:vc2];

    UINavigationController *nVc3 = [[UINavigationController alloc] initWithRootViewController:vc3];

    UINavigationController *nVc4 = [[UINavigationController alloc] initWithRootViewController:vc4];

    UINavigationController *nVc5 = [[UINavigationController alloc] initWithRootViewController:vc5];

    
    
    
    self.tabbarVc.viewControllers = @[nVc1, nVc2, nVc3, nVc4, nVc5];
    
   
    //self.tabbarVc.view.tintColor = [UIColor redColor];
    
    [self.view addSubview: self.tabbarVc.view];
}


- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
{
    NSLog(@"%ld", tabBarController.selectedIndex);
    if (tabBarController.selectedIndex != 0) {
        check = true;
    }else
    {
        check = false;
    }
    return YES;
}


- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    if (tabBarController.selectedIndex == 0)
    {
        UINavigationController *nvc = [tabBarController.viewControllers objectAtIndex:0];
        
        NSLog(@"%@", nvc.viewControllers);
        
        if (check == false) {
            if (previousController == viewController)
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"ScrollToTop" object:nil];
                
                previousController = nil;
            }
            else
            {
                previousController = viewController;
            }
        }else
        {
            previousController = nil;
        }
        
    }
    else if (tabBarController.selectedIndex == 2)
    {
        
       /* CameraViewController *cVc = [[CameraViewController alloc] initWithNibName:@"CameraViewController" bundle:nil];
        
        cVc.tabBarItem.image = [UIImage imageNamed:@"tab_3"];
        
        cVc.tabBarItem.imageInsets = UIEdgeInsetsMake(5, 0, -5, 0);
        
        UINavigationController *nVc = [[UINavigationController alloc] initWithRootViewController:cVc];
        
       
        NSMutableArray *allControllers = [[NSMutableArray alloc] initWithArray:[tabBarController viewControllers]];
        
        [allControllers removeObjectAtIndex:tabBarController.selectedIndex];
       
        [allControllers insertObject:nVc atIndex:tabBarController.selectedIndex ];
        
        [tabBarController setViewControllers:allControllers];*/
    }
}


@end
