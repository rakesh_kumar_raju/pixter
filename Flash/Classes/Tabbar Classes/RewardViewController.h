//
//  RewardViewController.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 1/2/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"


@interface RewardViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UITableView *rewardTbl;
    
    IBOutlet UIButton *sortBtn;
}

- (IBAction)showMyPointsTap;

- (IBAction)couponTap;

@end
