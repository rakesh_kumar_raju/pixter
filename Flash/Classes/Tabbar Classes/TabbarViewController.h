//
//  TabbarViewController.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 1/2/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TabbarViewController : UIViewController<UITabBarControllerDelegate>
{
    
}

@property (nonatomic, retain) IBOutlet UITabBarController *tabbarVc;

@end
