//
//  ProfileViewController.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 1/2/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "UIImageView+WebCache.h"


@interface ProfileViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource>
{
    IBOutlet UICollectionView *contentCollectionView;
    
    IBOutlet UIButton *gridBtn, *listBtn, *locationBtn, *taggedBtn, *savedBtn, *editProfileBtn, *switchProfileBtn, *backBtn;
    
    IBOutlet UIView *tabView, *headerView;
    
    UIRefreshControl *refreshControl;
    
    IBOutlet UILabel *totalPostLbl, *followerLbl, *followingLbl, *nameLbl, *titleLbl;
    
    IBOutlet UITextView *bioTv;
    
    IBOutlet UIImageView *userImg;
    
   
    IBOutlet UIView *unfollowView, *popupView;
    
    IBOutlet UIImageView *unfollowUserImg;
    
    IBOutlet UILabel *unfollowUserNameLbl;
    
    IBOutlet UIButton *settingBtn;
}

@property (nonatomic, retain) NSString *differentUserId;

- (IBAction)tabSelectionTap:(UIButton *)sender;

- (IBAction)editProfileTap;

- (IBAction)switchProfileTap;

- (IBAction)backTap;

- (IBAction)settingTap;

- (IBAction)unfollowCancelTap;

- (IBAction)unfollowTap;

@end
