//
//  RewardViewController.m
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 1/2/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//


#import "RewardViewController.h"
#import "BuyCouponViewController.h"
#import "ListCouponViewController.h"
#import "RewardTableViewCell.h"
#import "ServiceHelper.h"
#import "GPUImage.h"

@interface RewardViewController ()
{
    NSMutableArray *couponArr;
   
    NSString *totalPoints;
}

@end



@implementation RewardViewController


#pragma mark - ViewCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.hidden = YES;
    
    couponArr = [NSArray new];
    
    [self setUpView];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
     [self getAllCouponWebSevice];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    
    sortBtn.selected = NO;
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
}


#pragma mark - Void

- (void) setUpView
{
    rewardTbl.estimatedRowHeight = 105;
    
    rewardTbl.rowHeight = UITableViewAutomaticDimension;
}


#pragma mark - Table button Action


- (void)butNowTap:(UIButton *)sender
{
    NSString *couponPoints = [[[couponArr objectAtIndex:sender.tag] objectForKey:@"coupon_points"] stringByReplacingOccurrencesOfString:@"," withString:@""];
    
    if ([couponPoints intValue] <= totalPoints)
    {
        BuyCouponViewController *vc = [[BuyCouponViewController alloc] initWithNibName:@"BuyCouponViewController" bundle:nil];
        
        vc.hidesBottomBarWhenPushed = YES;
        
        vc.couponId = [[couponArr objectAtIndex:sender.tag] objectForKey:@"id"];
        
        vc.couponName = [[couponArr objectAtIndex:sender.tag] objectForKey:@"coupon_title"];
        
        [self.navigationController pushViewController:vc animated:YES];
    }
    
    else
    {
        [[[UIAlertView alloc] initWithTitle:@"Oops!" message:@"Sorry you don't have enough\npoints yet for this reward." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
}


#pragma mark - Action

- (IBAction)showMyPointsTap
{
    [[[UIAlertView alloc] initWithTitle:@"Available Rewards Points " message:totalPoints  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
}


- (IBAction)couponTap
{
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            [(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", nil];
    
    
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"listUsedCoupons" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 NSArray *userCouponArr = [result objectForKey:@"usedcoupon"];
                 
                 if (!userCouponArr.count)
                 {
                     [[[UIAlertView alloc] initWithTitle:nil message:@"No Rewards Puchased" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
                 }
                 else
                 {
                     ListCouponViewController *vc = [[ListCouponViewController alloc] initWithNibName:@"ListCouponViewController" bundle:nil];
                     
                     
                     [self.navigationController pushViewController:vc animated:YES];
                 }
                 
             }
         }
     }];
    
}


#pragma mark - Webservice

- (void)getAllCouponWebSevice
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            [(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", nil];
    

    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"allCoupons" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 totalPoints = [result objectForKey:@"points"];
                
                 NSLog(@"%@", totalPoints);
                 
                 couponArr = [[result objectForKey:@"coupons"] mutableCopy];
                 
                 [couponArr addObject:@{@"title" : @"All trademarks and registered trademarks appearing on Paracle are the property of their respective owners. Said owners do not endorse nor are they affiliated with Paracle or its promotions."}];
                 
                [rewardTbl reloadData];
             }
         }
     }];
}


#pragma mark - TableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return couponArr.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == couponArr.count-1)
    {
        NSString *cellIdentifier = @"CELLID";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CELLID"];
        }
        
        cell.textLabel.text = couponArr[indexPath.row][@"title"];
        
        cell.textLabel.font = [UIFont systemFontOfSize:10];
        
        cell.backgroundColor = [UIColor blackColor];
        
        cell.textLabel.numberOfLines = 5;

        cell.textLabel.textColor = [UIColor lightGrayColor];
        
        return cell;
    }
    
    else
    {
        NSString *cellIdentifier = @"CELL";
        
        RewardTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if (cell == nil)
        {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"RewardTableViewCell" owner:self options:nil] lastObject];
        }
        
        cell.buyNowBtn.tag = indexPath.row;
        
        [cell.buyNowBtn addTarget:self action:@selector(butNowTap:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.userInteractionEnabled = YES;
        
        [cell.coponImg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", [[[couponArr objectAtIndex:indexPath.row] objectForKey:@"coupon_image"] stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]]] placeholderImage:nil options:SDWebImageRefreshCached];
        
        cell.titleLbl.text = [[[couponArr objectAtIndex:indexPath.row] objectForKey:@"coupon_title"] isEqual:[NSNull null]] ? @"" : [[couponArr objectAtIndex:indexPath.row] objectForKey:@"coupon_title"];
        
        if (![[[couponArr objectAtIndex:indexPath.row] objectForKey:@"coupon_amount"] isEqualToString:@""])
        {
            cell.priceLbl.text = [NSString stringWithFormat:@"%@ $",[[[couponArr objectAtIndex:indexPath.row] objectForKey:@"coupon_amount"] isEqual:[NSNull null]] ? @"" : [[couponArr objectAtIndex:indexPath.row] objectForKey:@"coupon_amount"]];
        }
        
        else
        {
            cell.priceLbl.text = @"";
        }
        
        
        cell.pointsLbl.text = [NSString stringWithFormat:@"Points %@", [[[couponArr objectAtIndex:indexPath.row] objectForKey:@"coupon_points"] isEqual:[NSNull null]] ? @"" : [[couponArr objectAtIndex:indexPath.row] objectForKey:@"coupon_points"]];
        
        
        return cell;
    }
    
}


@end
