//
//  ProfileViewController.m
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 1/2/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#define RestOfElementSize_insted_of_imageSize  145

#define hashtag_color [UIColor colorWithRed:18.0/255.0 green:86.0/255.0 blue:136.0/255.0 alpha:1.0]


#import <MessageUI/MessageUI.h>

#import "ProfileViewController.h"
#import "EditProfileViewController.h"
#import "EditProfileViewController.h"
#import "AddCommentViewController.h"
#import "ViewLikesViewController.h"
#import "ShowPostsViewController.h"
#import "SettingViewController.h"
#import "UserCollectionJuggarCell.h"
#import "UserCollectionViewCell.h"
#import "ListCollectionViewCell.h"
#import "ListCollectionViewVideoCell.h"
#import "ProfileCollectionViewCell.h"
#import "ViewPostBySearchViewController.h"
#import "CreateBusinessViewController.h"
#import "EditPostViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "JDTagView.h"
#import "ServiceHelper.h"


@interface ProfileViewController () <UIActionSheetDelegate, MFMailComposeViewControllerDelegate, JDTagViewDelegate>
{
    NSMutableArray *userDataArr, *bussinesArr;
    
    NSDictionary *userDetailDict;
    
    BOOL isScrolling, fullvisible, gridSelected;
    
    NSMutableArray *videoPlayerObj;
   
    
    UIButton *layOutSelectionButton;
    
    NSIndexPath *selectedIndexPath;
    
    int index;
}

@end


@implementation ProfileViewController


#pragma mark - ViewCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    videoPlayerObj = [NSMutableArray new];
    
    fullvisible = YES;
    
    bussinesArr = [NSMutableArray new];
    
    refreshControl = [[UIRefreshControl alloc] init];
    
    refreshControl.tintColor = [UIColor whiteColor];
    
    [refreshControl addTarget:self action:@selector(pullToRefresh) forControlEvents:UIControlEventValueChanged];
    
    index = 1;
    
    [contentCollectionView addSubview:refreshControl];
    
    contentCollectionView.alwaysBounceVertical = YES;
    
    gridSelected = YES;
    
    self.navigationController.navigationBar.hidden = YES;
    
    [self setUpView];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    NSLog(@"%@",(APPDEL).userDetailDict);
    
    self.navigationController.navigationBar.hidden = YES;
    
    
    [self viewUsersProfileWebservice];
    
    [contentCollectionView setContentOffset:CGPointMake(0, 0) animated:NO];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    
    if ((APPDEL).AvplayerObj.count)
    {
        for (AVPlayer *player in (APPDEL).AvplayerObj)
        {
            NSLog(@"%@", player);
            
            [player pause];
        }
    }
    
    [self tabSelectionTap:layOutSelectionButton]; //jugaar
    
    [refreshControl endRefreshing];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
}


#pragma mark - Void

- (void) setUpView
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadTableAfterDelete:) name:@"reloadTableAfterDeletePost" object:nil];
    
   
    
    editProfileBtn.layer.cornerRadius = 2;
    
    editProfileBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    editProfileBtn.layer.borderWidth = 1.0;
    
    
    switchProfileBtn.layer.cornerRadius = 2;
    
    switchProfileBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    switchProfileBtn.layer.borderWidth = 1.0;
    
    if (self.differentUserId.length) //Different user Profile
    {
        if ([self.differentUserId intValue] == [[(APPDEL).userDetailDict objectForKey:@"userId"] intValue])
        {
            backBtn.hidden = YES;
            
            settingBtn.hidden = NO;
        }
        
        else
        {
            backBtn.hidden = YES;
            
            settingBtn.hidden = YES;
        }
       
    
    }
    
    else
    {
        //[backBtn setImage:[UIImage imageNamed:@"profile_addImage"] forState:UIControlStateNormal];
        
        backBtn.hidden = YES;
        
        settingBtn.hidden = NO;
    }
    
  
    
    
     [contentCollectionView registerNib:[UINib nibWithNibName:@"ProfileCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"profileCell"];
   
    //[contentCollectionView reloadData];
}


- (void)changeFollowStatus
{
    NSMutableDictionary *dict = [[userDataArr objectAtIndex:selectedIndexPath.row] mutableCopy];
    
    [userDataArr removeObjectAtIndex:selectedIndexPath.row];
    
    
    if ([[dict objectForKey:@"youfollowing"] intValue] == 0)
    {
        [dict setObject:@"1" forKey:@"youfollowing"];
        
        [userDataArr insertObject:dict atIndex:selectedIndexPath.row];
        
        
        [self followUser:[[userDataArr objectAtIndex:selectedIndexPath.row] valueForKey:@"userId"]];
    }
    else
    {
        [dict setObject:@"0" forKey:@"youfollowing"];
        
        [userDataArr insertObject:dict atIndex:selectedIndexPath.row];
        
       
        [self unfollowUser:[[userDataArr objectAtIndex:selectedIndexPath.row] valueForKey:@"userId"]];
    }
    
   
    
    
    [contentCollectionView performSelector:@selector(reloadItemsAtIndexPaths:) withObject:@[selectedIndexPath] afterDelay:0.2];
}



#pragma mark - ShowUnfolowView

- (void)showUnfolloView :(NSDictionary *)userDict
{
    unfollowView.frame = self.view.frame;
    
    [unfollowUserImg sd_setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [userDict valueForKey:@"profile_pic"]]] placeholderImage:nil];
    
    unfollowUserNameLbl.text = [NSString stringWithFormat:@"Unfollow %@?", [userDict valueForKey:@"username"]];
    
    
    [self.view addSubview:unfollowView];
    
    
    [UIView animateWithDuration:0.1 animations:^
     {
         popupView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
     }
                     completion:^(BOOL finished)
     {
         [UIView animateWithDuration:0.1 animations:^
          {
              popupView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
          }
                          completion:^(BOOL finished)
          {
              [UIView animateWithDuration:0.1 animations:^
               {
                   popupView.transform = CGAffineTransformIdentity;
               }];
          }];
     }];
}



#pragma mark - Clear all video viewFrom Cell

- (void)clearvideoViews
{
    for (InstagramVideoView *videoView in videoPlayerObj)
    {
        [[NSNotificationCenter defaultCenter] removeObserver:videoView];
        
        [videoView pause];
    }
}


#pragma mark - Get image size


- (UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width
{
    float oldWidth = sourceImage.size.width;
    
    float scaleFactor = i_width / oldWidth;
    
    
    float newHeight = sourceImage.size.height * scaleFactor;
    
    float newWidth = oldWidth * scaleFactor;
    
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return newImage;
}



#pragma mark - Switch layout selection buttons

- (void)setButtonsUI:(ProfileCollectionViewCell *)cell
{
    if ([[[userDataArr objectAtIndex:0] objectForKey:@"grid"] isEqualToString:@"0"])
    {
        cell.gridBtn.selected = YES;
        
        cell.listBtn.selected = NO;
        
        cell.taggedBtn.selected = NO;
        
        cell.savedBtn.selected = NO;
    }
    
    else if ([[[userDataArr objectAtIndex:0] objectForKey:@"list"] isEqualToString:@"1"])
    {
        cell.gridBtn.selected = NO;
        
        cell.listBtn.selected = YES;
        
        cell.taggedBtn.selected = NO;
        
        cell.savedBtn.selected = NO;
    }
    
    else if ([[[userDataArr objectAtIndex:0] objectForKey:@"tagged"] isEqualToString:@"1"])
    {
        cell.gridBtn.selected = NO;
        
        cell.listBtn.selected = NO;
        
        cell.taggedBtn.selected = YES;
        
        cell.savedBtn.selected = NO;
    }
    
    else if ([[[userDataArr objectAtIndex:0] objectForKey:@"saved"] isEqualToString:@"1"])
    {
        cell.gridBtn.selected = NO;
        
        cell.listBtn.selected = NO;
        
        cell.taggedBtn.selected = NO;
        
        cell.savedBtn.selected = YES;
    }
}


#pragma mark - Add tag button on Cell


/*- (void)addTagBtnOnCell: (ListCollectionViewCell *)cell personTagArr:(NSArray *)personTagArr
{
    if (personTagArr.count)
    {
        cell.tagBtn.hidden = NO;
        
        cell.tagBtn.alpha = 1;
        
        [self performSelector:@selector(hideTagBtn:) withObject:cell afterDelay:2];
        
        if (cell.subviews.count > 1)
        {
            for (UIView *view in cell.subviews)
            {
                if ([view isKindOfClass:[JDTagView class]])
                {
                    [view removeFromSuperview];
                }
            }
        }
        else
        {
            for (int i = 0 ; i < personTagArr.count; i++)
            {
                CGPoint location;
                
                location.x = [[[personTagArr objectAtIndex:i] objectForKey:@"tagX"] floatValue];
                
                location.y = [[[personTagArr objectAtIndex:i] objectForKey:@"tagY"] floatValue];
                
                
                JDTagView *tagview = [[JDTagView alloc] init];
                
                tagview.JDTagViewDelegate = self;
                
                tagview.tag = i+1;
                
                [tagview setUpTagViewForViewWithTagTitle:[[personTagArr objectAtIndex:i] objectForKey:@"userName"] WithViewPoints:location];
                
                [cell addSubview: tagview];
                
                tagview.delBtn.hidden = YES;
            }
        }
    }
    else
    {
        cell.tagBtn.hidden = YES;
    }
}*/


- (void)addTagBtnOnCell: (UICollectionViewCell *)cell sender:(UIButton *)sender personTagArr:(NSArray *)personTagArr
{
    if (personTagArr.count)
    {
        sender.hidden = NO;
        
        sender.alpha = 1;
        
        [self performSelector:@selector(hideTagBtn:) withObject:sender afterDelay:2];
        
        if (cell.subviews.count > 1)
        {
            for (UIView *view in cell.subviews)
            {
                if ([view isKindOfClass:[JDTagView class]])
                {
                    [view removeFromSuperview];
                }
            }
        }
        else
        {
            for (int i = 0 ; i < personTagArr.count; i++)
            {
                int backEndHeight = [[[userDataArr objectAtIndex:sender.tag] valueForKey:@"height"] intValue];
                
                int backEndscreenWidth = [[[userDataArr objectAtIndex:sender.tag] valueForKey:@"width"] intValue];
                
                
                float scaleFator = [[UIScreen mainScreen] bounds].size.width/backEndscreenWidth;
                
                
                float actualHeight = scaleFator * backEndHeight;
            
                
                
                CGPoint location;
                
                location.x =  [[[personTagArr objectAtIndex:i] objectForKey:@"tagX"] floatValue] * [[UIScreen mainScreen]bounds].size.width;
                
                location.y = [[[personTagArr objectAtIndex:i] objectForKey:@"tagY"] floatValue] * actualHeight;
                
                
                JDTagView *tagview = [[JDTagView alloc] init];
                
                tagview.JDTagViewDelegate = self;
                
                tagview.tag = i+1;
                
                [tagview setUpTagViewForViewWithTagTitle:[[personTagArr objectAtIndex:i] objectForKey:@"userName"] WithViewPoints:location];
                
                [cell addSubview: tagview];
                
                tagview.delBtn.hidden = YES;
            }
        }
    }
    else
    {
        sender.hidden = YES;
    }
}



#pragma mark - Set collectinView height

- (CGSize)setCollectionViewHeight :(NSIndexPath *)indexPath
{
    int backEndHeight = [[[userDataArr objectAtIndex:indexPath.row] valueForKey:@"height"] intValue];
    
    int backEndscreenWidth = [[[userDataArr objectAtIndex:indexPath.row] valueForKey:@"width"] intValue];
    
    
    float scaleFator = [[UIScreen mainScreen] bounds].size.width/backEndscreenWidth;
    
    
    float actualHeight = scaleFator * backEndHeight;
    
    
    
    int imageCellHeight = actualHeight + RestOfElementSize_insted_of_imageSize;
    
    
    if ([[[userDataArr objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"image"])
    {
        imageCellHeight = actualHeight + RestOfElementSize_insted_of_imageSize;
    }
    
    else
    {
        // 350 is video view height
        
        imageCellHeight = 350 + RestOfElementSize_insted_of_imageSize;
    }
    
    
    
    NSString *dataStr;
    
    CGSize cellHeight;
    
    CGSize maximumLabelSize;
    
    
    if (indexPath.row == 0)
    {
        if (![[[userDataArr objectAtIndex:indexPath.row] objectForKey:@"bio"] isEqualToString:@""])
        {
            
            dataStr = [NSString stringWithFormat:@"%@", [[userDataArr objectAtIndex:indexPath.row] objectForKey:@"bio"]];
        }
    }
    
    else
    {
        if (![[[userDataArr objectAtIndex:indexPath.row] objectForKey:@"caption"] isEqualToString:@""])
        {
            dataStr = [NSString stringWithFormat:@"%@ %@", [[userDataArr objectAtIndex:indexPath.row] objectForKey:@"fullname"], [[userDataArr objectAtIndex:indexPath.row] objectForKey:@"caption"]];
            
        }
    
       /* NSArray *commentArr = [[userDataArr objectAtIndex:indexPath.row] objectForKey:@"allComment"];
        
        if ([commentArr count])
        {
            NSMutableString *commentStr = [@"" mutableCopy];
            
            for (int i = 0; i < commentArr.count; i++)
            {
                [commentStr appendFormat:@"%@\n", [NSString stringWithFormat:@"%@ %@", [[commentArr objectAtIndex:i] objectForKey:@"fullname"], [[commentArr objectAtIndex:i] objectForKey:@"comment"]]];
            }
            
            dataStr = [NSString stringWithFormat:@"%@\nView all comments\n\n%@", dataStr, commentStr];
        }*/
    }
    
    
    
    //Set label width & cellHeight
    
    if ([[UIScreen mainScreen] bounds].size.width == 320)
    {
        maximumLabelSize = CGSizeMake(300, FLT_MAX);
       
        if (indexPath.row == 0)
        {
            cellHeight = CGSizeMake(320, 155);
        }
        else
        {
            if ([[[userDataArr objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"image"])
            {
                cellHeight = CGSizeMake(320, imageCellHeight);
            }
            else
            {
                cellHeight = CGSizeMake(320, 430);

            }
        }
        
    }
    else if ([[UIScreen mainScreen] bounds].size.width == 375)
    {
        maximumLabelSize = CGSizeMake(355, FLT_MAX);
        
        if (indexPath.row == 0)
        {
            cellHeight = CGSizeMake(375, 155);
        }
        else
        {
            if ([[[userDataArr objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"image"])
            {
                cellHeight = CGSizeMake(375, imageCellHeight);
            }
            
            else
            {
                cellHeight = CGSizeMake(375, 430);
            }
        }
    }
    else
    {
        maximumLabelSize = CGSizeMake(394, FLT_MAX);
        
        if (indexPath.row == 0)
        {
            cellHeight = CGSizeMake(414, 155);
        }
        else
        {
            if ([[[userDataArr objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"image"])
            {
                cellHeight = CGSizeMake(414, imageCellHeight);
            }
            
            else
            {
                cellHeight = CGSizeMake(414, 430);
            }
        }
    }
    
    
    CGRect textRect = [dataStr boundingRectWithSize:maximumLabelSize
                                            options:NSStringDrawingUsesLineFragmentOrigin
                                         attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]}
                                            context:nil];
    

    if (indexPath.row == 0)
    {
        cellHeight = CGSizeMake(cellHeight.width, cellHeight.height + textRect.size.height);
    }
    else
    {
        cellHeight = CGSizeMake(cellHeight.width, cellHeight.height + textRect.size.height + 25);
    }
    
    return cellHeight;
}


#pragma mark - Bold perticular string

- (NSMutableAttributedString *)setBoldString:(NSString *)fullstring boldString:(NSString *)boldString
{
    NSMutableAttributedString *yourAttributedString = [[NSMutableAttributedString alloc] initWithString:fullstring];
    
    NSRange boldRange = [fullstring rangeOfString:boldString];
    
    [yourAttributedString addAttribute: NSFontAttributeName value:[UIFont fontWithName:@"MuseoSansCyrl-500" size:14] range:boldRange];
    
    [yourAttributedString addAttribute: NSForegroundColorAttributeName value:[UIColor blackColor] range:boldRange];
    
    return  yourAttributedString;
}


#pragma mark - Reload collection view

- (void) reloadCollectionView: (ProfileCollectionViewCell *)cell button:(UIButton *)sender
{
    if (sender == cell.gridBtn)
    {
        if (userDataArr.count == 2)
        {
            [contentCollectionView registerNib:[UINib nibWithNibName:@"UserCollectionJuggarCell" bundle:nil] forCellWithReuseIdentifier:@"juggarGridCell"];
        }
        else
        {
            [contentCollectionView registerNib:[UINib nibWithNibName:@"UserCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"userCell"];
        }
    }
    
    else
    {
//        [contentCollectionView registerNib:[UINib nibWithNibName:@"ListCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"listCollection"];
//        
//         [contentCollectionView registerNib:[UINib nibWithNibName:@"ListCollectionViewVideoCell" bundle:nil] forCellWithReuseIdentifier:@"listCollectionVideo"];
        
        [contentCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"imageCell"];
    }
    
    [contentCollectionView reloadData];
}




#pragma mark - Animation on button

- (void)animateButton:(UIButton *)sender
{
    sender.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    
    [UIView animateWithDuration:0.3/1.5 animations:^{
        
        sender.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
    }
                     completion:^(BOOL finished)
     {
         [UIView animateWithDuration:0.3/2 animations:^{
             
             sender.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
         }
                          completion:^(BOOL finished)
          {
              [UIView animateWithDuration:0.3/2 animations:^{
                  
                  sender.transform = CGAffineTransformIdentity;
              }];
          }];
     }];
}

#pragma mark - Webservice

- (void)viewUsersProfileWebservice
{
    NSDictionary *params;
    
    NSString *methodName;
    
    if (self.differentUserId.length) //Different user Profile
    {
        params = [NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userid", self.differentUserId, @"otheruserid", nil];
        
        methodName = @"otherUserView";
    }
    
    else                            //self user Profile
    {
        params = [NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", nil];
        
         methodName = @"viewFullDetail";
    }
    
 
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:methodName setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         
         [refreshControl endRefreshing];
         
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 if (self.differentUserId.length) //Different user Profile
                 {
                     if ([[result objectForKey:@"youfollowing"] intValue] == 0)
                     {
                         [editProfileBtn setTitle:@"Follow" forState:UIControlStateNormal];
                     }
                     
                     else
                     {
                         [editProfileBtn setTitle:@"Following" forState:UIControlStateNormal];
                     }
                 }
                
                 if ([[result objectForKey:@"totalpost"] intValue] == 0)
                 {
                     totalPostLbl.textColor = [UIColor lightGrayColor];
                 }
                 
                 if ([[result objectForKey:@"followers"] intValue] == 0)
                 {
                     followerLbl.textColor = [UIColor lightGrayColor];
                 }
               
                 if ([[result objectForKey:@"following"] intValue] == 0)
                 {
                     followingLbl.textColor = [UIColor lightGrayColor];
                 }
                 
                 totalPostLbl.text = [NSString stringWithFormat:@"%@", [result objectForKey:@"totalpost"]];
                 
                 followerLbl.text = [NSString stringWithFormat:@"%@", [result objectForKey:@"followers"]];
                 
                 followingLbl.text = [NSString stringWithFormat:@"%@", [result objectForKey:@"following"]];
                 
                 nameLbl.text = [result objectForKey:@"fullname"]  ;
                 
                 titleLbl.text = [[result objectForKey:@"username"] lowercaseString];
                 
                 
                 bioTv.text = [result objectForKey:@""];
                 
                 [userImg sd_setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [result objectForKey:@"profile_pic"]] ] placeholderImage:nil];
                 
                 
                 userDataArr = [NSMutableArray new];
                 
                 NSString *userBioStr = @"";
                 
                 NSString *phoneNumberStr = @"";
                 
                 NSString *fullNameStr = @"";
                 
                 NSString *userNameStr = @"";
                 
                 NSString *userProfileUrlStr;
                 
                 NSString *multipleUser = @"";
                 
                 
                 userBioStr = [result objectForKey:@"quote"];
                 
                 phoneNumberStr = @"";
                 
                 fullNameStr = [result objectForKey:@"fullname"];
                 
                 userNameStr = [result objectForKey:@"username"];
                 
                 userProfileUrlStr = [result objectForKey:@"profile_pic"];
                 
                 multipleUser = [result objectForKey:@"multipleAccount"];
                 
                 
                /* if ([[result objectForKey:@"currentactive"] intValue] == 0)
                 {
                     userBioStr = [result objectForKey:@"quote"];
                     
                     phoneNumberStr = @"";
                     
                     fullNameStr = [result objectForKey:@"fullname"];
                     
                     userProfileUrlStr = [result objectForKey:@"profile_pic"];
                 }
                 
                 else
                 {
                     userBioStr = [NSString stringWithFormat:@"%@\n%@", [[result objectForKey:@"secondary"] objectForKey:@"emailId"], [result objectForKey:@"quote"] ];
                     
                     phoneNumberStr = [[result objectForKey:@"secondary"] objectForKey:@"phonenumber"];
                     
                     fullNameStr = [[result objectForKey:@"secondary"] objectForKey:@"fullName"];
                     
                     userProfileUrlStr = [[result objectForKey:@"secondary"] objectForKey:@"profile_pic"];
                 }*/
                 
                 if (self.differentUserId.length) //Different user Profile
                 {
                     if (index == 2)
                     {
                         [userDataArr addObject: [[NSDictionary dictionaryWithObjectsAndKeys:
                                                   @"0", @"grid",
                                                   @"1", @"list",
                                                   @"0", @"tagged",
                                                   @"0", @"saved",
                                                   phoneNumberStr, @"phonenumber",
                                                   userProfileUrlStr, @"profile_pic",
                                                   userNameStr, @"username",
                                                   fullNameStr, @"fullname",
                                                   [result objectForKey:@"totalpost"], @"totalpost",
                                                   [result objectForKey:@"followers"], @"followers",
                                                   [result objectForKey:@"following"], @"following",
                                                   [result objectForKey:@"youfollowing"], @"youfollowing",
                                                   [result objectForKey:@"privacy"], @"privacy",
                                                   userBioStr, @"bio",
                                                   multipleUser, @"multipleAccount",
                                                   nil] mutableCopy]];
                     }
                    else
                    {
                        [userDataArr addObject: [[NSDictionary dictionaryWithObjectsAndKeys:
                                                  @"1", @"grid",
                                                  @"0", @"list",
                                                  @"0", @"tagged",
                                                  @"0", @"saved",
                                                  phoneNumberStr, @"phonenumber",
                                                  userProfileUrlStr, @"profile_pic",
                                                  userNameStr, @"username",
                                                  fullNameStr, @"fullname",
                                                  [result objectForKey:@"totalpost"], @"totalpost",
                                                  [result objectForKey:@"followers"], @"followers",
                                                  [result objectForKey:@"following"], @"following",
                                                  [result objectForKey:@"youfollowing"], @"youfollowing",
                                                  [result objectForKey:@"privacy"], @"privacy",
                                                  userBioStr, @"bio",
                                                  multipleUser, @"multipleAccount",
                                                  nil] mutableCopy]];
                    }
                     
                     if ([[result objectForKey:@"youfollowing"] intValue] == 0 || [[result objectForKey:@"youfollowing"] intValue] == 2)
                     {
                         if ([[result objectForKey:@"privacy"] intValue] == 1)
                         {
                             [[[UIAlertView alloc] initWithTitle:nil message:@"Private Account tap follow to request access" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
                         }
                     }
                 
                 }
                 
                 else
                 {
                     if (index == 2)
                     {
                         [userDataArr addObject: [[NSDictionary dictionaryWithObjectsAndKeys:
                                                   @"0", @"grid",
                                                   @"1", @"list",
                                                   @"0", @"tagged",
                                                   @"0", @"saved",
                                                   phoneNumberStr, @"phonenumber",
                                                   userProfileUrlStr, @"profile_pic",
                                                   userNameStr, @"username",
                                                   fullNameStr, @"fullname",
                                                   [result objectForKey:@"totalpost"], @"totalpost",
                                                   [result objectForKey:@"followers"], @"followers",
                                                   [result objectForKey:@"following"], @"following",
                                                   userBioStr, @"bio",
                                                   multipleUser, @"multipleAccount",
                                                   nil] mutableCopy]];
                     }
                    else
                    {
                        [userDataArr addObject: [[NSDictionary dictionaryWithObjectsAndKeys:
                                                  @"1", @"grid",
                                                  @"0", @"list",
                                                  @"0", @"tagged",
                                                  @"0", @"saved",
                                                  phoneNumberStr, @"phonenumber",
                                                  userProfileUrlStr, @"profile_pic",
                                                  userNameStr, @"username",
                                                  fullNameStr, @"fullname",
                                                  [result objectForKey:@"totalpost"], @"totalpost",
                                                  [result objectForKey:@"followers"], @"followers",
                                                  [result objectForKey:@"following"], @"following",
                                                  userBioStr, @"bio",
                                                  multipleUser, @"multipleAccount",
                                                  nil] mutableCopy]];
                    }
                 }
                 
                 if ([(NSArray *)[result objectForKey:@"allpost"] count])
                 {
                     [userDataArr addObjectsFromArray:[result objectForKey:@"allpost"]] ;
                 }
                 
                
                 if (userDataArr.count == 2)
                 {
                     [contentCollectionView registerNib:[UINib nibWithNibName:@"UserCollectionJuggarCell" bundle:nil] forCellWithReuseIdentifier:@"juggarGridCell"];
                 }
                 else
                 {
                     [contentCollectionView registerNib:[UINib nibWithNibName:@"UserCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"userCell"];
                 }
               
                 [contentCollectionView reloadData];
                 
//                 if (userDataArr.count == 2)
//                 {
//                     [contentCollectionView performSelector:@selector(reloadData) withObject:nil afterDelay:1];
//                 }
            }
             
         }
         
     }];
}


- (void)likePost:(NSString *)postId index:(NSInteger)tag
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", postId, @"postId", [[userDataArr objectAtIndex:tag] valueForKey:@"userId"], @"secondaryId", nil];
    
    
    [[ServiceHelper sharedManager] setHudShow:NO];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"likePost" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
              
                 
             }
             
             else
             {
                 NSMutableDictionary *dict = [[userDataArr objectAtIndex:tag] mutableCopy];
                 
                 [userDataArr removeObjectAtIndex:tag];
                 
                 
                 [dict setObject:[NSString stringWithFormat:@"%i", [[result objectForKey:@"totallike"] intValue]-1] forKey:@"totallike"];
                 
                 
                 if ([[dict objectForKey:@"youlike"] intValue] == 0)
                 {
                     [dict setObject:@"1" forKey:@"youlike"];
                 }
                 else
                 {
                     [dict setObject:@"0" forKey:@"youlike"];
                 }
                 
                 [userDataArr insertObject:dict atIndex:(int)tag];
                 
                 [contentCollectionView reloadData];
             }
         }
         
     }];

}


- (void)favPost:(NSString *)postId index:(NSInteger)tag
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", postId, @"postId", nil];
    
    
    [[ServiceHelper sharedManager] setHudShow:NO];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"addFavourite" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 
                 
             }
             
             else
             {
                 NSMutableDictionary *dict = [[userDataArr objectAtIndex:tag] mutableCopy];
                 
                 [userDataArr removeObjectAtIndex:tag];
                 
                 
                 if ([[dict objectForKey:@"favourite"] intValue] == 0)
                 {
                     [dict setObject:@"1" forKey:@"favourite"];
                 }
                 else
                 {
                     [dict setObject:@"0" forKey:@"favourite"];
                 }
                 
                 [userDataArr insertObject:dict atIndex:(int)tag];
                 
                 [contentCollectionView reloadData];
             }
         }
         
     }];
    
}


- (void)getFavPostWebService
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", nil];
    
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"listFavourite" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
             [refreshControl endRefreshing];
             
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 NSMutableDictionary *userDict = [userDataArr objectAtIndex:0];
                 
                 [userDataArr removeAllObjects];
                 
                 
                 [userDataArr addObject:userDict];
                 
                 
                 if ([(NSArray *)[result objectForKey:@"list"] count])
                 {
                     [userDataArr addObjectsFromArray:[result objectForKey:@"list"]] ;
                 }
                
                 [contentCollectionView reloadData];
             }
     
         }
         
     }];
}


- (void)getTagPostWebService
{
    NSDictionary *params;
    
    if (self.differentUserId.length) //Different user Profile
    {
        params = [NSDictionary dictionaryWithObjectsAndKeys:self.differentUserId, @"userId", nil];
    }
    
    else
    {
        params = [NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", nil];
    }
    
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"getTagPost" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
             [refreshControl endRefreshing];
             
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 NSMutableDictionary *userDict = [userDataArr objectAtIndex:0];
                 
                 [userDataArr removeAllObjects];
                 
                 
                 [userDataArr addObject:userDict];
                 
                 
                 if ([(NSArray *)[result objectForKey:@"posts"] count])
                 {
                     [userDataArr addObjectsFromArray:[result objectForKey:@"posts"]] ;
                 }
                
                 [contentCollectionView reloadData];
             }
             
         }
         
     }];
}


- (void)getUserIdFromUsername:(NSDictionary *)params
{
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"getByUser" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 ProfileViewController *vc = [[ProfileViewController alloc] initWithNibName:@"ProfileViewController" bundle:nil];
                 
                 vc.differentUserId = [result objectForKey:@"id"] ;
                 
                 [self.navigationController pushViewController:vc animated:YES];
             }
             
             else
             {
                 
             }
         }
     }];
    
}


- (void)turnOffCommenting:(NSInteger) path params:(NSDictionary *)params
{
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"setCommenting" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 NSMutableDictionary *dict = [[userDataArr objectAtIndex:path] mutableCopy];
                 
                 [userDataArr removeObjectAtIndex:path];
                 
                
                 [dict setObject:[result objectForKey:@"commentflag"] forKey:@"commentflag"];
                 
                 [userDataArr insertObject:dict atIndex:path];
                 
                 
                 [contentCollectionView reloadData];
             }
             
             else
             {
                 
             }
         }
     }];
}


- (void)followUser:(NSString *)userId
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"primary_userid", userId, @"secondary_userid", nil];
    
    
    [[ServiceHelper sharedManager] setHudShow:NO];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"sendFollowingRequest" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 //                 if ([[result objectForKey:@"follow"] intValue] == 0)
                 //                 {
                 //                     [editProfileBtn setTitle:@"Follow" forState:UIControlStateNormal];
                 //                 }
                 //
                 //                 else
                 //                 {
                 //                     [editProfileBtn setTitle:@"Following" forState:UIControlStateNormal];
                 //                 }
             }
             
             else
             {
                 
             }
         }
         else
         {
             NSMutableDictionary *dict = [[userDataArr objectAtIndex:selectedIndexPath.row] mutableCopy];
             
             [userDataArr removeObjectAtIndex:selectedIndexPath.row];
             
             
             if ([[dict objectForKey:@"youfollowing"] intValue] == 0)
             {
                 [dict setObject:@"1" forKey:@"youfollowing"];
             }
             else
             {
                 [dict setObject:@"0" forKey:@"youfollowing"];
             }
             
             [userDataArr insertObject:dict atIndex:selectedIndexPath.row];
             
             
             [contentCollectionView performSelector:@selector(reloadItemsAtIndexPaths:) withObject:@[selectedIndexPath] afterDelay:0.2];
         }
     }];
    
    
}

- (void)unfollowUser:(NSString *)userId
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userid", userId, @"followerid", nil];
    
    
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"unFollowUser" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 
             }
             
             else
             {
                 
             }
         }
         else
         {
             NSMutableDictionary *dict = [[userDataArr objectAtIndex:selectedIndexPath.row] mutableCopy];
             
             [userDataArr removeObjectAtIndex:selectedIndexPath.row];
             
             
             if ([[dict objectForKey:@"youfollowing"] intValue] == 0)
             {
                 [dict setObject:@"1" forKey:@"youfollowing"];
             }
             else
             {
                 [dict setObject:@"0" forKey:@"youfollowing"];
             }
             
             [userDataArr insertObject:dict atIndex:selectedIndexPath.row];
             
             
             [contentCollectionView performSelector:@selector(reloadItemsAtIndexPaths:) withObject:@[selectedIndexPath] afterDelay:0.2];
         }
     }];
    
}


- (void)cancelFollowRequest:(NSString *)userId
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", userId, @"secondaryId", nil];
    
    
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"cancelFollowRequest" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
             [self pullToRefresh];
             
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 
             }
             
             else
             {
                 
             }
         }
         else
         {
             /*NSMutableDictionary *dict = [[postArr objectAtIndex:indexpath.row] mutableCopy];
              
              [postArr removeObjectAtIndex:indexpath.row];
              
              
              if ([[dict objectForKey:@"youfollowing"] intValue] == 0)
              {
              [dict setObject:@"1" forKey:@"youfollowing"];
              }
              else
              {
              [dict setObject:@"0" forKey:@"youfollowing"];
              }
              
              [postArr insertObject:dict atIndex:indexpath.row];
              
              
              [userCollectionView performSelector:@selector(reloadItemsAtIndexPaths:) withObject:@[indexpath] afterDelay:0.2];*/
         }
     }];
    
}



- (void)hitSwitchProfileWs:(NSDictionary *)params
{
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"switchProfile" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:[result objectForKey:@"detail"]] forKey:@"login_dict"];
                 
                 [[NSUserDefaults standardUserDefaults] synchronize];
                 
                 
                 (APPDEL).userDetailDict = [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"login_dict"]];
                 
                 NSLog(@"%@", (APPDEL).userDetailDict);
                 
                 [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:[NSDictionary new]] forKey:@"user_detail"];
                 
                 [[NSUserDefaults standardUserDefaults] synchronize];
                 
                 [self pullToRefresh];
             }
             
             else
             {
                 
             }
         }
     }];
}



#pragma mark - Pull to refresh

- (void)pullToRefresh
{
    if (index == 1)
    {
        [self viewUsersProfileWebservice];
    }
    
    else if (index == 2)
    {
        [self viewUsersProfileWebservice];

    }
    
    else if (index == 3)
    {
        [self getTagPostWebService];
    }
    
    else if (index == 4)
    {
        [self getFavPostWebService];

    }
}


#pragma mark - Action

/*- (IBAction)tabSelectionTap:(UIButton *)sender
{
    if (sender == gridBtn && sender.selected == NO)
    {
        gridBtn.selected = YES;

        listBtn.selected = NO;
        
        locationBtn.selected = NO;
        
        taggedBtn.selected = NO;
        
        savedBtn.selected = NO;
       
        contentCollectionView.backgroundColor = [UIColor blackColor];
        
        refreshControl.tintColor = [UIColor whiteColor];
        
        [self reloadCollectionView:gridBtn];
    }
    
    else if (sender == listBtn && sender.selected == NO)
    {
        gridBtn.selected = NO;
        
        listBtn.selected = YES;
        
        locationBtn.selected = NO;
        
        taggedBtn.selected = NO;
        
        savedBtn.selected = NO;
        
        
        contentCollectionView.backgroundColor = [UIColor whiteColor];
        
        refreshControl.tintColor = [UIColor blackColor];
        
        [self reloadCollectionView:listBtn];
    }
    
    else if (sender == taggedBtn && sender.selected == NO)
    {
        gridBtn.selected = NO;
        
        listBtn.selected = NO;
        
        locationBtn.selected = NO;
        
        taggedBtn.selected = YES;
        
        savedBtn.selected = NO;
    }
    
    else if (sender == savedBtn && sender.selected == NO)
    {
        gridBtn.selected = NO;
        
        listBtn.selected = NO;
        
        locationBtn.selected = NO;
        
        taggedBtn.selected = NO;
        
        savedBtn.selected = YES;
    }
}*/


/*- (IBAction)editProfileTap
{
   NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"primary_userid", self.differentUserId, @"secondary_userid", nil];
    
    if (self.differentUserId.length) //Different user Profile
    {
        if ([self.differentUserId isEqualToString:[(APPDEL).userDetailDict objectForKey:@"userId"]])
        {
            EditProfileViewController *vc = [[EditProfileViewController alloc] initWithNibName:@"EditProfileViewController" bundle:nil];
            
            [self.navigationController pushViewController:vc animated:YES];
        }
        else
        {
            [[ServiceHelper sharedManager] setHudShow:NO];
            
            [[ServiceHelper sharedManager] sendRequestWithMethodName:@"sendFollowingRequest" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
             {
                 
                 [refreshControl endRefreshing];
                 
                 if (result)
                 {
                     if ([[result objectForKey:@"status"] intValue] == 1)
                     {
                         if ([[result objectForKey:@"follow"] intValue] == 0)
                         {
                             [editProfileBtn setTitle:@"Follow" forState:UIControlStateNormal];
                         }
                         
                         else
                         {
                             [editProfileBtn setTitle:@"Following" forState:UIControlStateNormal];
                         }
                     }
                     
                 }
                 
             }];
        }
    }
    
    else
    {
        EditProfileViewController *vc = [[EditProfileViewController alloc] initWithNibName:@"EditProfileViewController" bundle:nil];
        
        [self.navigationController pushViewController:vc animated:YES];
    }
}


- (IBAction)switchProfileTap
{
    [[ServiceHelper sharedManager] setHudShow:NO];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"listBusiness" setHTTPMethod:@"POST" params:[NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", nil] completion:^(NSDictionary *result)
     {
         
         [refreshControl endRefreshing];
         
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 bussinesArr = [result objectForKey:@"business"];
                 
                 
                 UIActionSheet* switchProfileAction = [[UIActionSheet alloc] init];
                 
                 switchProfileAction.title = @"Switch your profile";
                 
                 switchProfileAction.delegate = self;
                 
                 for(int i = 0; i< [bussinesArr count];i++)
                 {
                     [switchProfileAction addButtonWithTitle:[[bussinesArr objectAtIndex:i] objectForKey:@"business_name"]];
                 }
                 
                 switchProfileAction.cancelButtonIndex = [switchProfileAction addButtonWithTitle:@"Cancel"];
                 
                 [switchProfileAction showInView:self.tabBarController.view];
             }
             
         }
         
     }];
}*/


- (IBAction)backTap
{
    if (self.differentUserId.length)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    else
    {
        CreateBusinessViewController *vc = [[CreateBusinessViewController alloc] initWithNibName:@"CreateBusinessViewController" bundle:nil];
        
         [self presentViewController:vc animated:YES completion:nil];
    }
}


- (IBAction)settingTap
{
    SettingViewController *vc = [[SettingViewController alloc] initWithNibName:@"SettingViewController" bundle:nil];
    
    [self.navigationController presentViewController:vc animated:YES completion:nil];
}


#pragma mark - Unfollow

- (IBAction)unfollowCancelTap
{
    [unfollowView removeFromSuperview];
}


- (IBAction)unfollowTap
{
   [unfollowView removeFromSuperview];
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userid", self.differentUserId, @"followerid", nil];
    
    
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"unFollowUser" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         NSMutableDictionary *dict = [[userDataArr objectAtIndex:0] mutableCopy];
         
         [userDataArr removeObjectAtIndex:0];
         
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 if ([[dict objectForKey:@"youfollowing"] intValue] == 1)
                 {
                     [dict setObject:@"0" forKey:@"youfollowing"];
                 }
                 else
                 {
                     [dict setObject:@"1" forKey:@"youfollowing"];
                 }
             }
             
             else
             {
                [dict setObject:@"1" forKey:@"youfollowing"];
             }
             
    
             [userDataArr insertObject:dict atIndex:0];
             
         }
         else
         {
             [[[UIAlertView alloc] initWithTitle:@"Error!" message:@"Something went wrong" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
         }
         
         [contentCollectionView reloadData];
        // [contentCollectionView performSelector:@selector(reloadItemsAtIndexPaths:) withObject:@[[NSIndexPath indexPathWithIndex:0]] afterDelay:0.2];
     }];

}


#pragma mark - Dot button action


- (void)postShareOnFacebookShare:(UIImage *)image
{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
        SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        [mySLComposerSheet addImage:image];
        
        
        [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result)
         {
             
             switch (result) {
                 case SLComposeViewControllerResultCancelled:
                     NSLog(@"Post Canceled");
                     break;
                 case SLComposeViewControllerResultDone:
                     NSLog(@"Post Sucessful");
                     break;
                     
                 default:
                     break;
             }
         }];
        
        [self presentViewController:mySLComposerSheet animated:YES completion:nil];
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:@"Oops!" message:@"Please login your Facebook account in your device" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
}


- (void)postShareOnMailShare:(NSString *)imagePath
{
    if ([MFMailComposeViewController canSendMail])
    {
        NSString *mimeType = @"image/jpeg";
        
        
        
        MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
        
        mail.mailComposeDelegate = self;
        
        [mail setSubject:@"Check out this"];
        
        [mail setMessageBody:@"Please download the Pister app for more activity." isHTML:NO];
        
        [mail addAttachmentData:[NSData dataWithContentsOfFile:imagePath] mimeType:mimeType fileName:@"Pixster"];
        
        
        
        [self presentViewController:mail animated:YES completion:NULL];
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:@"Oops!" message:@"This device cannot send email" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        
    }
}


- (void)reportAbuse:(NSString *)postId secondryId:(NSString *)secondryId isInappropiate: (NSString *)type
{
    NSDictionary *params;
    
    params = [NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", postId, @"postId", type, @"flag", secondryId, @"secondaryId", nil];
 
    
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"postReportAbuse" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 
                 
             }
             
             else
             {
                 
             }
         }
         
     }];
    
}


- (void)deletePost:(NSInteger)path postId:(NSString *)postId
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", postId, @"postId", nil];
    
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"deletePost" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 [userDataArr removeObjectAtIndex:path];
                 
                 [contentCollectionView reloadData];
             }
             
             else
             {
                 
             }
         }
         
     }];
}




#pragma mark - Notification

- (void)reloadTableAfterDelete:(NSNotification *)noti
{
//    NSMutableDictionary *dict = [userDataArr objectAtIndex:0];
//    
//    NSString *totalPost = [NSString stringWithFormat:@"%i", [[dict objectForKey:@"totalpost"] intValue] -1];
//    
//    [dict setObject:totalPost forKey:@"totalpost"];
//    
//    
//    [userDataArr removeObjectAtIndex:0];
//    
//    [userDataArr insertObject:dict atIndex:0];
//    
//   // [contentCollectionView performSelector:@selector(reloadItemsAtIndexPaths:) withObject:@[[NSIndexPath indexPathForRow:0 inSection:0]] afterDelay:0.2];
//    
//    [userDataArr removeObjectAtIndex:selectedIndexPath.row];
//    
//    // [contentCollectionView performSelector:@selector(reloadItemsAtIndexPaths:) withObject:@[selectedIndexPath] afterDelay:0.2];
//    
//    [contentCollectionView reloadData];
}


#pragma mark - Collection Action

- (void)likeTap:(UIButton *)sender
{
    [self animateButton:sender];
   
    
    NSMutableDictionary *dict = [[userDataArr objectAtIndex:sender.tag] mutableCopy];
    
    [userDataArr removeObjectAtIndex:sender.tag];
  
    
    if ([[dict objectForKey:@"youlike"] intValue] == 0)
    {
        [dict setObject:@"1" forKey:@"youlike"];
        
        [dict setObject:[NSString stringWithFormat:@"%i", [[dict objectForKey:@"totallike"] intValue]+1 ] forKey:@"totallike"];
    }
    else
    {
        [dict setObject:@"0" forKey:@"youlike"];
        
        [dict setObject:[NSString stringWithFormat:@"%i", [[dict objectForKey:@"totallike"] intValue]-1 ] forKey:@"totallike"];
    }
    
    [userDataArr insertObject:dict atIndex:sender.tag];
    
    
    NSIndexPath *path = [contentCollectionView indexPathForCell: (ListCollectionViewCell *)sender.superview.superview];
    

    [contentCollectionView performSelector:@selector(reloadItemsAtIndexPaths:) withObject:@[path] afterDelay:0.2];
    
    
    [self likePost:[[userDataArr objectAtIndex:sender.tag] valueForKey:@"userId"] index:sender.tag];

}



- (void)favTap:(UIButton *)sender
{
    [self animateButton:sender];
    
    NSMutableDictionary *dict = [[userDataArr objectAtIndex:sender.tag] mutableCopy];
    
    [userDataArr removeObjectAtIndex:sender.tag];
    
    
    if ([[dict objectForKey:@"favourite"] intValue] == 0)
    {
        [dict setObject:@"1" forKey:@"favourite"];
    }
    else
    {
        [dict setObject:@"0" forKey:@"favourite"];
    }
    
    [userDataArr insertObject:dict atIndex:sender.tag];
    
    
    NSIndexPath *path = [contentCollectionView indexPathForCell: (ListCollectionViewCell *)sender.superview.superview];
    
    
    [contentCollectionView performSelector:@selector(reloadItemsAtIndexPaths:) withObject:@[path] afterDelay:0.2];
    
   
    [self favPost:[[userDataArr objectAtIndex:sender.tag] valueForKey:@"postId"] index:sender.tag];
}


- (void)commentTap:(UIButton *)sender
{
    if ([[[userDataArr objectAtIndex:sender.tag] objectForKey:@"commentflag"] intValue] == 1)
    {
        AddCommentViewController *vc = [[AddCommentViewController alloc] initWithNibName:@"AddCommentViewController" bundle:nil];
        
        vc.hidesBottomBarWhenPushed = YES;
        
        vc.postId = [[userDataArr objectAtIndex:sender.tag] objectForKey:@"postId"];
        
        vc.secondryId = [[userDataArr objectAtIndex:sender.tag] objectForKey:@"userId"];
        
        [self.navigationController pushViewController:vc animated:YES];
    }
}



- (void)followTap:(UIButton *)sender
{
    selectedIndexPath = [contentCollectionView indexPathForCell: (ListCollectionViewCell *)sender.superview.superview];
    
    NSLog(@"%ld", sender.tag);
    
    if ([[[userDataArr objectAtIndex:sender.tag] objectForKey:@"youfollowing"] intValue] == 0)
    {
        [self changeFollowStatus];
    }
    else if ([[[userDataArr objectAtIndex:sender.tag] objectForKey:@"youfollowing"] intValue] == 1)
    {
        unfollowView.frame = self.view.frame;
        
        [unfollowUserImg sd_setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [[userDataArr objectAtIndex:sender.tag] valueForKey:@"profilePic"]]] placeholderImage:nil];
        
        unfollowUserNameLbl.text = [NSString stringWithFormat:@"Unfollow %@?", [[userDataArr objectAtIndex:sender.tag] valueForKey:@"fullname"]];
        
        
        [self.view addSubview:unfollowView];
        
        
        [UIView animateWithDuration:0.1 animations:^
         {
             popupView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
         }
                         completion:^(BOOL finished)
         {
             [UIView animateWithDuration:0.1 animations:^
              {
                  popupView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
              }
                              completion:^(BOOL finished)
              {
                  [UIView animateWithDuration:0.1 animations:^
                   {
                       popupView.transform = CGAffineTransformIdentity;
                   }];
              }];
         }];
    }
    else
    {
        NSMutableDictionary *dict = [[userDataArr objectAtIndex:sender.tag] mutableCopy];
        
        [userDataArr removeObjectAtIndex:selectedIndexPath.row];
        
        
        if ([[dict objectForKey:@"youfollowing"] intValue] == 2)
        {
            [dict setObject:@"0" forKey:@"youfollowing"];
            
            [userDataArr insertObject:dict atIndex:selectedIndexPath.row];
            
            [self cancelFollowRequest:[[userDataArr objectAtIndex:selectedIndexPath.row] valueForKey:@"userId"]];
        }
        
        [contentCollectionView performSelector:@selector(reloadItemsAtIndexPaths:) withObject:@[selectedIndexPath] afterDelay:0.2];
    }
}


- (void)getTotalLike:(UIButton *)sender
{
    ViewLikesViewController *vc = [[ViewLikesViewController alloc] initWithNibName:@"ViewLikesViewController" bundle:nil];
    
    vc.hidesBottomBarWhenPushed = YES;
    
    vc.switchTitleStr = @"like";
    
    vc.postId = [[userDataArr objectAtIndex:sender.tag] objectForKey:@"postId"];
    
    [self.navigationController pushViewController:vc animated:YES];
    
}


- (void)tabSelectionTap:(UIButton *)sender
{
    NSMutableDictionary *dict = [userDataArr objectAtIndex:sender.tag];
    
    ProfileCollectionViewCell *cell = (ProfileCollectionViewCell *)sender.superview.superview.superview;
    
    
    if ((sender == cell.gridBtn || sender == cell.differentUserGridBtn) && [[dict objectForKey:@"grid"] isEqualToString:@"0"])
    {
        index = 1;
        
        cell.gridBtn.selected = YES;
        
        cell.listBtn.selected = NO;
       
        cell.taggedBtn.selected = NO;
        
        cell.savedBtn.selected = NO;
    
        
        cell.differentUserGridBtn.selected = YES;
        
        cell.differentUserlistBtn.selected = NO;
        
        cell.differentUsertaggedBtn.selected = NO;
        
        
        cell.savedBtn.selected = NO;
        
        gridSelected = YES;

        [dict setObject:@"1" forKey:@"grid"];
        
        [dict setObject:@"0" forKey:@"list"];
        
        [dict setObject:@"0" forKey:@"tagged"];
        
        [dict setObject:@"0" forKey:@"saved"];
        
        
        [userDataArr removeObjectAtIndex:sender.tag];
        
        [userDataArr insertObject:dict atIndex:sender.tag];
        
        
        contentCollectionView.backgroundColor = [UIColor blackColor];
        
        refreshControl.tintColor = [UIColor whiteColor];
        
       
        [self reloadCollectionView:cell button:cell.gridBtn];
        
        [self viewUsersProfileWebservice];
    }
 
    else if ((sender == cell.listBtn || sender == cell.differentUserlistBtn) && [[dict objectForKey:@"list"] isEqualToString:@"0"])
    {
        index = 2;
        
        cell.gridBtn.selected = NO;
        
        cell.listBtn.selected = YES;
        
        cell.taggedBtn.selected = NO;
        
        cell.savedBtn.selected = NO;
        
        
        cell.differentUserGridBtn.selected = NO;
        
        cell.differentUserlistBtn.selected = YES;
        
        cell.differentUsertaggedBtn.selected = NO;
      
        
        gridSelected = NO;


        //contentCollectionView.backgroundColor = [UIColor whiteColor];
        
        refreshControl.tintColor = [UIColor blackColor];
      
        
        [dict setObject:@"0" forKey:@"grid"];
        
        [dict setObject:@"1" forKey:@"list"];
        
        [dict setObject:@"0" forKey:@"tagged"];
        
        [dict setObject:@"0" forKey:@"saved"];
        
        
        [userDataArr removeObjectAtIndex:sender.tag];
        
        [userDataArr insertObject:dict atIndex:sender.tag];
        
        
        [self reloadCollectionView:cell button:cell.listBtn];
        
         [self viewUsersProfileWebservice];
    }
   
    else if ((sender == cell.taggedBtn || sender == cell.differentUsertaggedBtn) && [[dict objectForKey:@"tagged"] isEqualToString:@"0"])
    {
        index = 3;
        
        cell.gridBtn.selected = NO;
        
        cell.listBtn.selected = NO;
        
        cell.taggedBtn.selected = YES;
        
        cell.savedBtn.selected = NO;
        
        
        cell.differentUserGridBtn.selected = NO;
        
        cell.differentUserlistBtn.selected = NO;
        
        cell.differentUsertaggedBtn.selected = YES;
       
        
        gridSelected = NO;
        
       
        //contentCollectionView.backgroundColor = [UIColor whiteColor];
        
        refreshControl.tintColor = [UIColor blackColor];
        
        
        [dict setObject:@"0" forKey:@"grid"];
        
        [dict setObject:@"0" forKey:@"list"];
        
        [dict setObject:@"1" forKey:@"tagged"];
        
        [dict setObject:@"0" forKey:@"saved"];
        
        
        [userDataArr removeObjectAtIndex:sender.tag];
        
        [userDataArr insertObject:dict atIndex:sender.tag];
        
       
        
        [self reloadCollectionView:cell button:cell.listBtn];
        
        
        [self getTagPostWebService];
    }
   
    else if (sender == cell.savedBtn && [[dict objectForKey:@"saved"] isEqualToString:@"0"])
    {
        index = 4;
        
        cell.gridBtn.selected = NO;
        
        cell.listBtn.selected = NO;
      
        cell.taggedBtn.selected = NO;
        
        cell.savedBtn.selected = YES;
        
        
        gridSelected = NO;
     
        
        //contentCollectionView.backgroundColor = [UIColor whiteColor];
        
        refreshControl.tintColor = [UIColor blackColor];

        
        [dict setObject:@"0" forKey:@"grid"];
        
        [dict setObject:@"0" forKey:@"list"];
        
        [dict setObject:@"0" forKey:@"tagged"];
        
        [dict setObject:@"1" forKey:@"saved"];
        
        [self reloadCollectionView:cell button:cell.savedBtn];
        
        [self getFavPostWebService];
    }
}


- (void)editProfileTap
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"primary_userid", self.differentUserId, @"secondary_userid", nil];
    
    if (self.differentUserId.length) //Different user Profile
    {
        if ([self.differentUserId isEqualToString:[(APPDEL).userDetailDict objectForKey:@"userId"]])
        {
            EditProfileViewController *vc = [[EditProfileViewController alloc] initWithNibName:@"EditProfileViewController" bundle:nil];
            
            [self presentViewController:vc animated:YES completion:nil];
        }
        else
        {
            NSMutableDictionary *dict = [userDataArr objectAtIndex:0];
            
            if ([[dict objectForKey:@"youfollowing"] intValue] == 1)
            {
                [self showUnfolloView:dict];
            }
            else  if ([[dict objectForKey:@"youfollowing"] intValue] == 0)
            {
                [[ServiceHelper sharedManager] setHudShow:YES];
                
                [[ServiceHelper sharedManager] sendRequestWithMethodName:@"sendFollowingRequest" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
                 {
                     
                     [refreshControl endRefreshing];
                     
                     if (result)
                     {
                         if ([[result objectForKey:@"status"] intValue] == 1)
                         {
                             //NSMutableDictionary *dict = [userDataArr objectAtIndex:0];
                             
                            /* [dict setObject:@"1" forKey:@"youfollowing"];
                             
                             [userDataArr removeObjectAtIndex:0];
                             
                             [userDataArr insertObject:dict atIndex:0];
                             
                             [contentCollectionView reloadData];*/
                             
                             
                             
                             if ([[result objectForKey:@"follow"] intValue] == 0)
                             {
                                 [editProfileBtn setTitle:@"Follow" forState:UIControlStateNormal];
                             }
                             
                             else
                             {
                                 [editProfileBtn setTitle:@"Following" forState:UIControlStateNormal];
                             }

                             [self pullToRefresh];
                         }
                         
                     }
                     
                 }];
            }
            else
            {
                [self cancelFollowRequest:self.differentUserId];
            }
        }
    }
    
    else
    {
        EditProfileViewController *vc = [[EditProfileViewController alloc] initWithNibName:@"EditProfileViewController" bundle:nil];
        
        [self.navigationController pushViewController:vc animated:YES];
    }
}


- (void)switchProfileTap
{
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"listProfile" setHTTPMethod:@"POST" params:[NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", nil] completion:^(NSDictionary *result)
     {
         
         [refreshControl endRefreshing];
         
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 NSPredicate *predicate;
                 
                 UIActionSheet* switchProfileAction;
                 
                 switchProfileAction = [[UIActionSheet alloc] init];
                 
                 switchProfileAction.tag = 100;
                 
                 switchProfileAction.delegate = self;
                 
                 
                 if ([(NSArray *)[result objectForKey:@"profile"] count])
                 {
                    /* if ([[(APPDEL).userDetailDict objectForKey:@"type"] isEqualToString:@""])
                     {
                         predicate = [NSPredicate predicateWithFormat:@"type ==[c] %@", @"business"];
                     }
                     
                     else
                     {
                         predicate = [NSPredicate predicateWithFormat:@"type ==[c] %@", @""];
                     }
                     
                     bussinesArr = [[(NSArray *)[result objectForKey:@"profile"] filteredArrayUsingPredicate:predicate] mutableCopy];
                     
                     
                     if ([[(APPDEL).userDetailDict objectForKey:@"type"] isEqualToString:@""])
                     {
                         predicate = [NSPredicate predicateWithFormat:@"type ==[c] %@", @"business"];
                     }
                     
                     else
                     {
                         predicate = [NSPredicate predicateWithFormat:@"type ==[c] %@", @""];
                     }
                     
                     bussinesArr = [[(NSArray *)[result objectForKey:@"profile"] filteredArrayUsingPredicate:predicate] mutableCopy];*/
                     
                     bussinesArr = [[result objectForKey:@"profile"] mutableCopy];
                     
                     switchProfileAction.title = @"Switch your profile";
                     
                     [switchProfileAction addButtonWithTitle:[[bussinesArr objectAtIndex:0] objectForKey:@"name"]];
                     
                 }
                else
                {
                    switchProfileAction.title = @"Create an additional profile for business or personal use";
                    
                    [switchProfileAction addButtonWithTitle:@"Create Profile"];
                }
        
                 switchProfileAction.cancelButtonIndex = [switchProfileAction addButtonWithTitle:@"Cancel"];
                 
                 [switchProfileAction showInView:self.tabBarController.view];
                
             }
             
             else
             {
                 [[[UIAlertView alloc] initWithTitle:[result objectForKey:@"message"] message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
             }
             
         }
         
     }];
}


- (void)dotsTap:(UIButton *)sender
{
//    if (self.differentUserId.length) //Different user Profile
//    {
        if ([[[userDataArr objectAtIndex:sender.tag] objectForKey:@"userId"] intValue] == [[(APPDEL).userDetailDict objectForKey:@"userId"] intValue])
        {
            NSString *swapStr;
            
            if ([[[userDataArr objectAtIndex:sender.tag] objectForKey:@"commentflag"] intValue] == 1)
            {
                swapStr = @"Turn Off Commenting";
            }
            
            else
            {
                swapStr = @"Turn On Commenting";
            }
        
            
            UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Delete" otherButtonTitles:swapStr, @"Edit", nil];
            
            action.tag = sender.tag;
            
            [action showInView:self.view];
        }
        else
        {
            UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Report", nil];
            
            action.tag = sender.tag;
            
            [action showInView:self.view];
        }
//    }
//    else
//    {
//        UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Delete" otherButtonTitles:@"Turn Off Commenting", @"Edit", @"Share", nil];
//        
//        action.tag = sender.tag;
//        
//        [action showInView:self.view];
//    }
}


- (void)viewFollowersTap:(UIButton *)sender
{
    ViewLikesViewController *vc = [[ViewLikesViewController alloc] initWithNibName:@"ViewLikesViewController" bundle:nil];
    
    vc.hidesBottomBarWhenPushed = YES;
    
    vc.switchTitleStr = @"FOLLOWERS";
    
    if (self.differentUserId.length) //Different user Profile
    {
        if ([self.differentUserId isEqualToString:[(APPDEL).userDetailDict objectForKey:@"userId"]])
        {
            vc.postId = [(APPDEL).userDetailDict objectForKey:@"userId"];
        }
        else
        {
            vc.postId = self.differentUserId;
        }
    }
    else
    {
        vc.postId = [(APPDEL).userDetailDict objectForKey:@"userId"];
    }
    
    
    [self.navigationController pushViewController:vc animated:YES];

}


- (void)viewFollowingTap:(UIButton *)sender
{
    ViewLikesViewController *vc = [[ViewLikesViewController alloc] initWithNibName:@"ViewLikesViewController" bundle:nil];
    
    vc.hidesBottomBarWhenPushed = YES;
    
    vc.switchTitleStr = @"FOLLOWING";
    
    if (self.differentUserId.length) //Different user Profile
    {
        if ([self.differentUserId isEqualToString:[(APPDEL).userDetailDict objectForKey:@"userId"]])
        {
            vc.postId = [(APPDEL).userDetailDict objectForKey:@"userId"];
        }
        else
        {
            vc.postId = self.differentUserId;
        }
    }
    else
    {
        vc.postId = [(APPDEL).userDetailDict objectForKey:@"userId"];
    }
    
    
    [self.navigationController pushViewController:vc animated:YES];
}


- (void)totalPostTap:(UIButton *)sender
{
    if (userDataArr.count > 1)
    {
         [contentCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0] atScrollPosition:UICollectionViewScrollPositionTop animated:YES];
    }
}


- (void)locationTap:(UIButton *)sender
{
    ViewPostBySearchViewController *vc = [[ViewPostBySearchViewController alloc] initWithNibName:@"ViewPostBySearchViewController" bundle:nil];
    
    vc.isSeacrhTagPeople = NO;
    
    vc.searchStr = [[userDataArr objectAtIndex:sender.tag] valueForKey:@"location"];
    
    [self.navigationController pushViewController:vc animated:YES];
}


- (void)hideTagBtn:(UIButton *)sender
{
    [UIView animateWithDuration:0.2 animations:^
     {
         sender.alpha = 0;
     }];
}


- (void)showTagBtn:(ListCollectionViewCell *)cell
{
    [UIView animateWithDuration:0.2 animations:^
     {
         cell.tagBtn.alpha = 1;
     }];
}


- (void)showTagPeopleView:(UIButton *)sender
{
    UICollectionViewCell *cell = (UICollectionViewCell *)sender.superview.superview;
    
    [self addTagBtnOnCell:cell sender:sender personTagArr:[[userDataArr objectAtIndex:sender.tag] valueForKey:@"tagpeople"]];
}




#pragma mark - CollectionView Delegate



- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (gridSelected == YES)
    {
        if (indexPath.row == 0)
        {
            return [self setCollectionViewHeight:indexPath];
        }
       else
       {
           /*if (userDataArr.count == 2)
           {
               UserCollectionViewCell *cell = (UserCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
             
               if (collectionView.frame.size.width == 320)
               {
                   cell.userImg.frame = CGRectMake(0, 0, 106, 106);
               }
               else if (collectionView.frame.size.width == 375)
               {
                   cell.userImg.frame = CGRectMake(0, 0, 124, 124);
               }
               else
               {
                   cell.userImg.frame = CGRectMake(0, 0, 137, 137);
              }
            
               return CGSizeMake([[UIScreen mainScreen] bounds].size.width, 106);
           }*/
           
           if (userDataArr.count == 2)
           {
               if (collectionView.frame.size.width == 320)
               {
                   return CGSizeMake([[UIScreen mainScreen] bounds].size.width, 106);
               }
               else if (collectionView.frame.size.width == 375)
               {
                   return CGSizeMake([[UIScreen mainScreen] bounds].size.width, 124);
               }
               
               return  CGSizeMake([[UIScreen mainScreen] bounds].size.width, 137);
           }
           else
           {
               if (collectionView.frame.size.width == 320)
               {
                   return CGSizeMake(106, 106);
               }
               else if (collectionView.frame.size.width == 375)
               {
                   return CGSizeMake(124, 124);
               }
               
               return  CGSizeMake(137, 137);
           }
       }
   }
    
    else
    {
        return [self setCollectionViewHeight:indexPath];
    }
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return userDataArr.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView1 cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        ProfileCollectionViewCell *cell = (ProfileCollectionViewCell*)[collectionView1 dequeueReusableCellWithReuseIdentifier:@"profileCell" forIndexPath:indexPath];
        
        cell.contentView.autoresizingMask  = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        [cell.profileImg sd_setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [[userDataArr objectAtIndex:indexPath.row] valueForKey:@"profile_pic"] ] ] placeholderImage:[UIImage imageNamed:@"profile_demo"]];

        
       
        layOutSelectionButton = cell.gridBtn;//juggar
        
        cell.nameLbl.text = [[userDataArr objectAtIndex:indexPath.row] valueForKey:@"fullname"];
        
        [cell.gridBtn addTarget:self action:@selector(tabSelectionTap:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.listBtn addTarget:self action:@selector(tabSelectionTap:) forControlEvents:UIControlEventTouchUpInside];
       
        [cell.taggedBtn addTarget:self action:@selector(tabSelectionTap:) forControlEvents:UIControlEventTouchUpInside];
      
        [cell.savedBtn addTarget:self action:@selector(tabSelectionTap:) forControlEvents:UIControlEventTouchUpInside];
      
        [cell.differentUserGridBtn addTarget:self action:@selector(tabSelectionTap:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.differentUserlistBtn addTarget:self action:@selector(tabSelectionTap:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.differentUsertaggedBtn addTarget:self action:@selector(tabSelectionTap:) forControlEvents:UIControlEventTouchUpInside];

        
        
        [cell.editProfileBtn addTarget:self action:@selector(editProfileTap) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.switchProfileBtn addTarget:self action:@selector(switchProfileTap) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.followerBtn addTarget:self action:@selector(viewFollowersTap:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.followingBtn addTarget:self action:@selector(viewFollowingTap:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.totalPostBtn addTarget:self action:@selector(totalPostTap:) forControlEvents:UIControlEventTouchUpInside];
        
        
        //Total Posts
        if ([[[userDataArr objectAtIndex:indexPath.row] valueForKey:@"totalpost"] intValue] == 0)
        {
            cell.totalPostLbl.textColor = [UIColor lightGrayColor];
        }
        else if([[[userDataArr objectAtIndex:indexPath.row] valueForKey:@"totalpost"] intValue] < 2)
        {
            cell.totalPostLbl.textColor = [UIColor whiteColor];
        }
        else
        {
            cell.totalPostLbl.textColor = [UIColor whiteColor];
        }
        
        cell.totalPostLbl.text = [NSString stringWithFormat:@"%i", [[[userDataArr objectAtIndex:indexPath.row] valueForKey:@"totalpost"] intValue]];
        
        
        //Followers
        
        if ([[[userDataArr objectAtIndex:indexPath.row] valueForKey:@"followers"] intValue] == 0)
        {
            cell.followerLbl.textColor = [UIColor lightGrayColor];
        }
        else if([[[userDataArr objectAtIndex:indexPath.row] valueForKey:@"followers"] intValue] < 2)
        {
            cell.followerLbl.textColor = [UIColor whiteColor];
        }
        else
        {
           cell.followerLbl.textColor = [UIColor whiteColor];
        }
        
        cell.followerLbl.text = [NSString stringWithFormat:@"%i", [[[userDataArr objectAtIndex:indexPath.row] valueForKey:@"followers"] intValue]];
        
        
        //Following
        
        if ([[[userDataArr objectAtIndex:indexPath.row] valueForKey:@"following"] intValue] == 0)
        {
            cell.followingLbl.textColor = [UIColor lightGrayColor];
        }
        else if([[[userDataArr objectAtIndex:indexPath.row] valueForKey:@"following"] intValue] < 2)
        {
            cell.followingLbl.textColor = [UIColor whiteColor];
        }
        else
        {
            cell.followingLbl.textColor = [UIColor whiteColor];
        }
        
        
        cell.followingLbl.text = [NSString stringWithFormat:@"%i", [[[userDataArr objectAtIndex:indexPath.row] valueForKey:@"following"] intValue]];
    
        
        cell.bioLbl.text = [[userDataArr objectAtIndex:indexPath.row] valueForKey:@"bio"];
        
        
        cell.editProfileBtn.layer.cornerRadius = 2;
        
        cell.editProfileBtn.layer.borderColor = [UIColor whiteColor].CGColor;
        
        cell.editProfileBtn.layer.borderWidth = 1.0;
        
        
        cell.switchProfileBtn.layer.cornerRadius = 2;
        
        cell.switchProfileBtn.layer.borderColor = [UIColor whiteColor].CGColor;
        
        cell.switchProfileBtn.layer.borderWidth = 1.0;
        
        
        if (self.differentUserId.length) //Different user Profile
        {
            if ([[[userDataArr objectAtIndex:indexPath.row] objectForKey:@"youfollowing"] intValue] == 0 || [[[userDataArr objectAtIndex:indexPath.row] objectForKey:@"youfollowing"] intValue] == 2)
            {
                if ([[[userDataArr objectAtIndex:indexPath.row] objectForKey:@"privacy"] intValue] == 1)
                {
                    cell.differentUserGridBtn.userInteractionEnabled = NO;
                    
                    cell.differentUserGridBtn.selected = NO;
                    
                    cell.differentUserlistBtn.userInteractionEnabled = NO;
                    
                    cell.differentUsertaggedBtn.userInteractionEnabled = NO;
                    
                    
                    cell.followerLbl.textColor = [UIColor lightGrayColor];
                    
                    cell.followingLbl.textColor = [UIColor lightGrayColor];
                    
                    cell.totalPostLbl.textColor = [UIColor lightGrayColor];
                    
                    
                    cell.followerBtn.userInteractionEnabled = NO;
                    
                    cell.followingBtn.userInteractionEnabled = NO;
                }
                
            }
            
            
            if ([self.differentUserId intValue] == [[(APPDEL).userDetailDict objectForKey:@"userId"] intValue])
            {
                [cell.editProfileBtn setTitle:@"Edit Profile" forState:UIControlStateNormal];
                
                cell.switchProfileBtn.hidden = NO;
                
                cell.differentUserView.hidden = YES;
                
                if ([[[userDataArr objectAtIndex: indexPath.row] objectForKey:@"multipleAccount"] intValue] == 1)
                {
                    [cell.switchProfileBtn setTitle:@"Switch Profile" forState:UIControlStateNormal];
                }
                else
                {
                    [cell.switchProfileBtn setTitle:@"Add Profile" forState:UIControlStateNormal];
                }
            }
            
            else
            {
                if ([[[userDataArr objectAtIndex: indexPath.row] objectForKey:@"youfollowing"] intValue] == 0)
                {
                    [cell.editProfileBtn setTitle:@"Follow" forState:UIControlStateNormal];
                    
                    cell.editProfileBtn.layer.cornerRadius = 5;
                    
                    cell.editProfileBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
                    
                    cell.editProfileBtn.layer.borderWidth = 1.0;
                    
                   
                    cell.editProfileBtn.backgroundColor = [UIColor whiteColor];
                    
                    [cell.editProfileBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                }
                else if ([[[userDataArr objectAtIndex: indexPath.row] objectForKey:@"youfollowing"] intValue] == 1)
                {
                    [cell.editProfileBtn setTitle:@"Following" forState:UIControlStateNormal];
                    
                    cell.editProfileBtn.layer.cornerRadius = 2;
                    
                    cell.editProfileBtn.backgroundColor = [UIColor blackColor];
                    
                    cell.editProfileBtn.layer.borderColor = [UIColor whiteColor].CGColor;

                    cell.editProfileBtn.layer.borderWidth = 1.0;
                    
                    [cell.editProfileBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                }
                
                else
                {
                    [cell.editProfileBtn setTitle:@"Requested" forState:UIControlStateNormal];
                    
                    cell.editProfileBtn.layer.cornerRadius = 2;
                    
                    cell.editProfileBtn.backgroundColor = [UIColor blackColor];
                    
                    cell.editProfileBtn.layer.borderColor = [UIColor whiteColor].CGColor;
                    
                    cell.editProfileBtn.layer.borderWidth = 1.0;
                    
                    [cell.editProfileBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                }
                
                
                if ([[UIScreen mainScreen] bounds].size.width == 320)
                {
                    cell.editProfileTrailing.constant = -200;
                }
                else if ([[UIScreen mainScreen] bounds].size.width == 375)
                {
                    cell.editProfileTrailing.constant = -250;
                }
                else
                {
                    cell.editProfileTrailing.constant = -300;
                }
                
                cell.switchProfileBtn.hidden = YES;
            }
            
            backBtn.hidden = NO;
        }
        
        else
        {
            [backBtn setImage:[UIImage imageNamed:@"profile_addImage"] forState:UIControlStateNormal];
            
            [cell.editProfileBtn setTitle:@"Edit Profile" forState:UIControlStateNormal];
            
            cell.differentUserView.hidden = YES;
            
            cell.switchProfileBtn.hidden = NO;
            
            if ([[[userDataArr objectAtIndex: indexPath.row] objectForKey:@"multipleAccount"] intValue] == 1)
            {
                [cell.switchProfileBtn setTitle:@"Switch Profile" forState:UIControlStateNormal];
            }
            else
            {
                [cell.switchProfileBtn setTitle:@"Add Profile" forState:UIControlStateNormal];
            }
        }
        
        
        return cell;
    }
    
    if (gridSelected == YES)
    {
       if (userDataArr.count == 2)
        {
            static NSString *gridCellIdentifier = @"juggarGridCell";
            
            UserCollectionJuggarCell *cell = (UserCollectionJuggarCell*)[collectionView1 dequeueReusableCellWithReuseIdentifier:gridCellIdentifier forIndexPath:indexPath];
            
            //cell.contentView.autoresizingMask  = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            
            
            if ([[[userDataArr objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"image"])
            {
                [cell.userImg sd_setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [[userDataArr objectAtIndex:indexPath.row] valueForKey:@"media"] ] ] placeholderImage:nil];
                
                cell.videoIconImg.hidden = YES;
            }
            else
            {
                [cell.userImg sd_setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [[userDataArr objectAtIndex:indexPath.row] valueForKey:@"thumbnail"] ] ] placeholderImage:nil];
                
                cell.videoIconImg.hidden = NO;
            }
            
            return cell;
        }
        else
        {
            static NSString *gridCellIdentifier = @"userCell";
            
            UserCollectionViewCell *cell = (UserCollectionViewCell*)[collectionView1 dequeueReusableCellWithReuseIdentifier:gridCellIdentifier forIndexPath:indexPath];
            
            cell.contentView.autoresizingMask  = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            
            
            if ([[[userDataArr objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"image"])
            {
                [cell.userImg sd_setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [[userDataArr objectAtIndex:indexPath.row] valueForKey:@"media"] ] ] placeholderImage:nil];
                
                cell.videoIconImg.hidden = YES;
            }
            else
            {
                [cell.userImg sd_setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [[userDataArr objectAtIndex:indexPath.row] valueForKey:@"thumbnail"] ] ] placeholderImage:nil];
                
                cell.videoIconImg.hidden = NO;
            }
            
            return cell;
        }
        
    }
    else
    {
        
        UICollectionViewCell *cell = [contentCollectionView dequeueReusableCellWithReuseIdentifier:@"imageCell" forIndexPath:indexPath];
        
        if (cell != nil)
        {
            for (UIView* view in [cell.contentView subviews])
            {
                [view removeFromSuperview];
            }
        }
        //Profile image
        
        UIImageView *profileImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 50, 50)];
        
        profileImage.layer.cornerRadius = 25.0;
        
        profileImage.clipsToBounds = YES;
        
        [profileImage sd_setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [[userDataArr objectAtIndex:indexPath.row] valueForKey:@"profilePic"] ] ] placeholderImage:[UIImage imageNamed:@"profile_demo"]];
        
        
        [cell.contentView addSubview:profileImage];
        
        
        //Profile Picture button
        
        UIButton *profilePicBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        
        profilePicBtn.tag = indexPath.row;
        
        //[profilePicBtn addTarget:self action:@selector(tapOnProfileImage:) forControlEvents:UIControlEventTouchUpInside];
        
        profilePicBtn.frame = profileImage.frame;
        
        [profilePicBtn setImage:[UIImage imageNamed:@"like"] forState:UIControlStateNormal];
        
        //[cell.contentView addSubview:profilePicBtn];
        
        
        
        // Name;
        
        UILabel *userNameLbl = [[UILabel alloc] initWithFrame:CGRectMake(profileImage.frame.origin.x + profileImage.frame.size.width+5, 20, [[UIScreen mainScreen] bounds].size.width-5, 20)];
        
        //userNameLbl.font = [UIFont fontWithName:@"MuseoSansCyrl-700" size:13];
        
        userNameLbl.font = [UIFont boldSystemFontOfSize:15];
        
        userNameLbl.textColor = [UIColor whiteColor];
        
        userNameLbl.text = [[[userDataArr objectAtIndex:indexPath.row] objectForKey:@"fullname"] lowercaseString];
        
        [cell.contentView addSubview:userNameLbl];
        
        
        
        //location
        
        UILabel *locationLbl = [[UILabel alloc] initWithFrame:CGRectMake(profileImage.frame.origin.x + profileImage.frame.size.width+5, userNameLbl.frame.origin.y + userNameLbl.frame.size.height, [[UIScreen mainScreen] bounds].size.width-5, 20)];
        
        //locationLbl.font = [UIFont fontWithName:@"MuseoSansCyrl-500" size:13];
        
        locationLbl.font = [UIFont systemFontOfSize:13];
        
        locationLbl.textColor = [UIColor whiteColor];
        
        locationLbl.text = [[userDataArr objectAtIndex:indexPath.row] objectForKey:@"location"];
        
        
        [cell.contentView addSubview:locationLbl];
        
        
        //LocationTap
        
        UIButton *location = [UIButton buttonWithType:UIButtonTypeCustom];
        
        location.backgroundColor = [UIColor clearColor];
        
        location.tag = indexPath.row;
        
        location.frame = locationLbl.frame;
        
        [location addTarget:self action:@selector(locationTap:) forControlEvents:UIControlEventTouchUpInside];
        
        if (locationLbl.text.length)
        {
            [cell.contentView addSubview:locationBtn];
        }
        
        
        //Follow button
        
        if ([[[userDataArr objectAtIndex:indexPath.row] valueForKey:@"userId"] intValue] != [[(APPDEL).userDetailDict objectForKey:@"userId"] intValue])
        {
            if (index == 3 || index == 4)
            {
                selectedIndexPath = indexPath;
                
                UIButton *followBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                
                followBtn.tag = indexPath.row;
                
                [followBtn addTarget:self action:@selector(followTap:) forControlEvents:UIControlEventTouchUpInside];
                
                
                if ([[[userDataArr objectAtIndex:indexPath.row] objectForKey:@"youfollowing"] intValue] == 0)
                {
                    [followBtn setTitle:@"Follow" forState:UIControlStateNormal];
                    
                    followBtn.backgroundColor = [UIColor whiteColor];
                    
                    [followBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                }
                else if ([[[userDataArr objectAtIndex:indexPath.row] objectForKey:@"youfollowing"] intValue] == 1)
                {
                    [followBtn setTitle:@"Following" forState:UIControlStateNormal];
                    
                    followBtn.backgroundColor = [UIColor blackColor];
                    
                    [followBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                }
                else
                {
                    [followBtn setTitle:@"Requested" forState:UIControlStateNormal];
                    
                    followBtn.backgroundColor = [UIColor blackColor];
                    
                    [followBtn setTitleColor: [UIColor whiteColor] forState:UIControlStateNormal];
                }
                
                followBtn.titleLabel.font = [UIFont fontWithName:@"MuseoSansCyrl-500" size:12];
                
                followBtn.frame = CGRectMake(([[UIScreen mainScreen] bounds].size.width - 65) -15, 23, 65, 25);
                
                followBtn.layer.cornerRadius = 2.0;
                
                followBtn.layer.borderWidth = 1.0;
                
                followBtn.layer.borderColor = [UIColor blackColor].CGColor;
                
                if ([[[userDataArr objectAtIndex:indexPath.row] valueForKey:@"userId"] intValue] != [self.differentUserId intValue])
                {
                    //[cell.contentView addSubview:followBtn];
                }
            }
        }
        
        
        //Media
        
        UIView *mediaView = [[UIView alloc] init];
        
        mediaView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        
        
        if ([[[userDataArr objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"image"])
        {
            int backEndHeight = [[[userDataArr objectAtIndex:indexPath.row] valueForKey:@"height"] intValue];
            
            int backEndscreenWidth = [[[userDataArr objectAtIndex:indexPath.row] valueForKey:@"width"] intValue];
            
            
            float scaleFator = [[UIScreen mainScreen] bounds].size.width/backEndscreenWidth;
            
            
            float actualHeight = scaleFator * backEndHeight;
            
            
        
            mediaView.frame = CGRectMake(0, locationLbl.frame.origin.y + locationLbl.frame.size.height+5, [[UIScreen mainScreen] bounds].size.width, actualHeight);
            
            
            UIImageView *mediaImageView = [[UIImageView alloc] init];
            
            mediaImageView.backgroundColor = [UIColor clearColor];
            
            //[mediaImageView sd_setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [[userDataArr objectAtIndex:indexPath.row] valueForKey:@"media"] ] ] placeholderImage:nil options:SDWebImageRefreshCached];
            
            SDWebImageManager *manager = [SDWebImageManager sharedManager];
            
            [manager downloadImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [[userDataArr objectAtIndex:indexPath.row] valueForKey:@"media"] ] ] options:SDWebImageLowPriority progress:^(NSInteger receivedSize, NSInteger expectedSize)
             {
                 NSLog(@"received - %li,  expect - %li", (long)receivedSize, (long)expectedSize);
                 
             } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                 if (image)
                 {
                     mediaImageView.image = image;
                 }
             }];
            
            mediaImageView.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, actualHeight);
            
            
            [mediaView addSubview:mediaImageView];
            
            [cell.contentView addSubview:mediaView];
            
            
            //Tag people Btn
            
            if ([(NSArray *)[[userDataArr objectAtIndex:indexPath.row] valueForKey:@"tagpeople"] count])
            {
                UIButton *tagPeopleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                
                tagPeopleBtn.tag = indexPath.row;
                
                tagPeopleBtn.frame = CGRectMake(10, (mediaView.frame.origin.y + mediaView.frame.size.height) - 30, 25, 25);
                
                [tagPeopleBtn addTarget:self action:@selector(showTagPeopleView:) forControlEvents:UIControlEventTouchUpInside];
                
                
                [tagPeopleBtn setImage:[UIImage imageNamed:@"tag_Pic"] forState:UIControlStateNormal];
                
               // [cell.contentView addSubview:tagPeopleBtn];
            }
        }
        else
        {
            
            mediaView.frame = CGRectMake(0, locationLbl.frame.origin.y + locationLbl.frame.size.height+5, [[UIScreen mainScreen] bounds].size.width, 350);
            
            InstagramVideoView *videoView = [[InstagramVideoView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 350)];
            
            
            videoView.backgroundColor = [UIColor clearColor];
            
            
            [mediaView addSubview:videoView];
            
            [cell.contentView addSubview:mediaView];
            
            dispatch_async(dispatch_get_main_queue(),
                           ^{
                               if(isScrolling == NO)
                               {
                                   [videoView resume];
                                   
                                   [videoView playVideoWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", ServiceURL, [[userDataArr objectAtIndex:indexPath.row] valueForKey:@"media"]]] ];
                                   
                                   [videoPlayerObj addObject:videoView];
                               }
                               else
                               {
                                   [videoView stopVideoPlay];
                               }
                           });
        }
        
        
        // like Buttom
        
        UIButton *likeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        
        likeBtn.tag = indexPath.row;
        
        [likeBtn addTarget:self action:@selector(likeTap:) forControlEvents:UIControlEventTouchUpInside];
        
        likeBtn.frame = CGRectMake(10, mediaView.frame.origin.y + mediaView.frame.size.height+10, 25, 25);
        
        
        if ([[[userDataArr objectAtIndex:indexPath.row] valueForKey:@"youlike"] intValue] == 0)
        {
            [likeBtn setImage:[UIImage imageNamed:@"like"] forState:UIControlStateNormal];
        }
        else
        {
            [likeBtn setImage:[UIImage imageNamed:@"like_h"] forState:UIControlStateNormal];
        }
        
        
        
        [cell.contentView addSubview:likeBtn];
        
        
        // Comment Buttom
        
        UIButton *commentBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        
        commentBtn.tag = indexPath.row;
        
        
        commentBtn.frame = CGRectMake(likeBtn.frame.origin.x + likeBtn.frame.size.width + 10, likeBtn.frame.origin.y, 25, 25);
        
        if ([[[userDataArr objectAtIndex:indexPath.row] objectForKey:@"commentflag"] intValue] == 0)
        {
            [commentBtn setImage:[UIImage imageNamed:@"comment_off"] forState: UIControlStateNormal];
        }
        else
        {
            [commentBtn setImage:[UIImage imageNamed:@"comment"] forState: UIControlStateNormal];
            
            [commentBtn addTarget:self action:@selector(commentTap:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        [cell.contentView addSubview:commentBtn];
        
        
        // Favourite Buttom
        
        UIButton *favBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        
        favBtn.tag = indexPath.row;
        
        [favBtn addTarget:self action:@selector(favTap:) forControlEvents:UIControlEventTouchUpInside];
        
        favBtn.frame = CGRectMake(commentBtn.frame.origin.x + commentBtn.frame.size.width + 10, likeBtn.frame.origin.y, 25, 25);
        
        if ([[[userDataArr objectAtIndex:indexPath.row] valueForKey:@"favourite"] intValue] == 0)
        {
            [favBtn setImage:[UIImage imageNamed:@"star"] forState:UIControlStateNormal];
        }
        else
        {
            [favBtn setImage:[UIImage imageNamed:@"star_h"] forState:UIControlStateNormal];
        }
        
        [cell.contentView addSubview:favBtn];
        
        
        
        // Three Dot Button
        
        UIButton *dotBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        
        dotBtn.tag = indexPath.row;
        
        [dotBtn addTarget:self action:@selector(dotsTap:) forControlEvents:UIControlEventTouchUpInside];
        
        dotBtn.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width - 50, likeBtn.frame.origin.y, 40, 30);
        
        [dotBtn setImage:[UIImage imageNamed:@"three_dots"] forState:UIControlStateNormal];
        
        [cell.contentView addSubview:dotBtn];
        
        
        
        //Total LikesLabel
        
        UILabel *totalLikeLbl = [[UILabel alloc] init];
        
        totalLikeLbl.backgroundColor = [UIColor clearColor];
        
        //totalLikeLbl.font = [UIFont fontWithName:@"MuseoSansCyrl-500" size:14];
        
        totalLikeLbl.font = [UIFont boldSystemFontOfSize:14];
       
        totalLikeLbl.textAlignment = NSTextAlignmentLeft;
        
        totalLikeLbl.textColor = [UIColor whiteColor];
        
        
        // Total like Button
        
        UIButton *getTotlLikeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        
        getTotlLikeBtn.backgroundColor = [UIColor clearColor];
        
        getTotlLikeBtn.tag = indexPath.row;
        
        [getTotlLikeBtn addTarget:self action:@selector(getTotalLike:) forControlEvents:UIControlEventTouchUpInside];
        
        
        
        //Caption Label
        
        
        ResponsiveLabel *captionLbl = [[ResponsiveLabel alloc] init];
        
        captionLbl.numberOfLines = 999;
        
        //captionLbl.font = [UIFont fontWithName:@"MuseoSansCyrl-500" size:14];
        
        captionLbl.font = [UIFont systemFontOfSize:14];
        
        captionLbl.textColor = [UIColor whiteColor];
        
        
        // Get the Label height
        
        NSString *dataStr = @"";
        
        if (![[[userDataArr objectAtIndex:indexPath.row] objectForKey:@"caption"] isEqualToString:@""])
        {
            dataStr = [NSString stringWithFormat:@"%@ %@", [[[userDataArr objectAtIndex:indexPath.row] objectForKey:@"fullname"] lowercaseString], [[userDataArr objectAtIndex:indexPath.row] objectForKey:@"caption"]];
            
        }
        
       /* NSArray *commentArr = [[userDataArr objectAtIndex:indexPath.row] objectForKey:@"allComment"];
        
        NSString *totalComment = @"";
        
        if ([commentArr count])
        {
            NSMutableString *commentStr = [@"" mutableCopy];
            
            for (int i = 0; i < commentArr.count; i++)
            {
                [commentStr appendFormat:@"%@\n", [NSString stringWithFormat:@"%@ %@", [[commentArr objectAtIndex:i] objectForKey:@"fullname"], [[commentArr objectAtIndex:i] objectForKey:@"comment"]]];
                
                
                //string detection
                PatternTapResponder commentUser = ^(NSString *string)
                {
                    NSLog(@"tapped = %@",string);
                };
                
                [captionLbl enableStringDetection:[[commentArr objectAtIndex:i] objectForKey:@"fullname"] withAttributes:@{NSFontAttributeName : [UIFont fontWithName:@"MuseoSansCyrl-700" size:14],NSForegroundColorAttributeName:[UIColor blackColor],RLTapResponderAttributeName: commentUser}];
                //
            }
            
            if ([[[userDataArr objectAtIndex:indexPath.row] objectForKey:@"totalcomment"] intValue] > 1)
            {
                totalComment = [[userDataArr objectAtIndex:indexPath.row] objectForKey:@"totalcomment"];
                
                dataStr = [NSString stringWithFormat:@"%@\nView all %@ comments\n\n%@", dataStr, totalComment, commentStr];
                
            }
            else
            {
                dataStr = [NSString stringWithFormat:@"%@\n%@", dataStr, commentStr];
            }
        }
        
        
        if ([[[userDataArr objectAtIndex:indexPath.row] objectForKey:@"totalcomment"] intValue] > 1)
        {
            if (dataStr.length)
            {
                dataStr = [NSString stringWithFormat:@"%@\n\nView all %@ comments", dataStr, [[userDataArr objectAtIndex:indexPath.row] objectForKey:@"totalcomment"]];
            }
            else
            {
                dataStr = [NSString stringWithFormat:@"View all %@ comments", [[userDataArr objectAtIndex:indexPath.row] objectForKey:@"totalcomment"]];
            }
        }
        else if ([[[userDataArr objectAtIndex:indexPath.row] objectForKey:@"totalcomment"] intValue] == 1)
        {
            if (dataStr.length)
            {
                dataStr = [NSString stringWithFormat:@"%@\n\nView all %@ comments", dataStr, [[userDataArr objectAtIndex:indexPath.row] objectForKey:@"totalcomment"]];
            }
            else
            {
                dataStr = [NSString stringWithFormat:@"View all %@ comments", [[userDataArr objectAtIndex:indexPath.row] objectForKey:@"totalcomment"]];
            }
            
        }
        else
        {
            if (dataStr.length)
            {
                dataStr = [NSString stringWithFormat:@"%@\nView all comments", dataStr];
            }
            else
            {
                dataStr = @"View all comments";
            }
        }*/
        
        
        captionLbl.text = dataStr;
        
        
        
        
        //Hastag detection
        
        captionLbl.userInteractionEnabled = YES;
        PatternTapResponder hashTagTapAction = ^(NSString *tappedString)
        {
            NSLog(@"HashTag Tapped = %@",tappedString);
            
            
            ViewPostBySearchViewController *vc = [[ViewPostBySearchViewController alloc] initWithNibName:@"ViewPostBySearchViewController" bundle:nil];
            
            vc.isSeacrhTagPeople = YES;
            
            vc.searchStr = [tappedString stringByReplacingOccurrencesOfString:@"#" withString:@""];
            
            [self.navigationController pushViewController:vc animated:YES];
        };
        
        [captionLbl enableHashTagDetectionWithAttributes:
         @{NSForegroundColorAttributeName:hashtag_color, RLTapResponderAttributeName:hashTagTapAction}];
        
        //
        
        
        //string detection
        PatternTapResponder tapResponder = ^(NSString *string)
        {
            NSLog(@"tapped = %@",string);
        };
        
        [captionLbl enableStringDetection:nameLbl.text withAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName : [UIFont boldSystemFontOfSize:14], RLTapResponderAttributeName: tapResponder}];
        //
        

        
        //Username detection
        
        captionLbl.userInteractionEnabled = YES;
        PatternTapResponder userNameDetection = ^(NSString *tappedString)
        {
            NSLog(@"HashTag Tapped = %@",tappedString);
            
            [self getUserIdFromUsername:[NSDictionary dictionaryWithObjectsAndKeys:tappedString, @"otherusername", [(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", nil]];
        };
        
        [captionLbl enableUserHandleDetectionWithAttributes:@{NSForegroundColorAttributeName:hashtag_color, RLTapResponderAttributeName:userNameDetection}];
        
        //
        
        
        
        CGRect textRect = [dataStr boundingRectWithSize:CGSizeMake([[UIScreen mainScreen] bounds].size.width-20, FLT_MAX)
                                                options:NSStringDrawingUsesLineFragmentOrigin
                                             attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]}
                                                context:nil];
        
        int captionLblHeigtht = textRect.size.height;
    
        
        //View all comment label
        
        ResponsiveLabel *viewAllCommentLbl = [[ResponsiveLabel alloc] init];
        
        //viewAllCommentLbl.font = [UIFont fontWithName:@"MuseoSansCyrl-500" size:14];
        
        viewAllCommentLbl.font = [UIFont systemFontOfSize:14];
        
        viewAllCommentLbl.textColor = [UIColor lightGrayColor];
        
        
        if ([[[userDataArr objectAtIndex:indexPath.row] objectForKey:@"totalcomment"] intValue] > 1)
        {
            viewAllCommentLbl.text = [NSString stringWithFormat:@"View all %@ comments", [[userDataArr objectAtIndex:indexPath.row] objectForKey:@"totalcomment"]];
            
        }
        else if ([[[userDataArr objectAtIndex:indexPath.row] objectForKey:@"totalcomment"] intValue] == 1)
        {
            viewAllCommentLbl.text = [NSString stringWithFormat:@"View all %@ comments", [[userDataArr objectAtIndex:indexPath.row] objectForKey:@"totalcomment"]];
        }
        else
        {
            viewAllCommentLbl.text = @"View all comments";
        }
        
        
        //string detection
        PatternTapResponder viewAllCountComment = ^(NSString *string)
        {
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            
            btn.tag = indexPath.row;
            
            [self commentTap:btn];
        };
        
        [viewAllCommentLbl enableStringDetection:[NSString stringWithFormat:@"View all %@ comments", [[userDataArr objectAtIndex:indexPath.row] objectForKey:@"totalcomment"]] withAttributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor],RLTapResponderAttributeName: viewAllCountComment}];
        //
        
        
        //string detection
        PatternTapResponder viewAllcomment = ^(NSString *string)
        {
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            
            btn.tag = indexPath.row;
            
            [self commentTap:btn];
        };
        
        [viewAllCommentLbl enableStringDetection:@"View all comments" withAttributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor],RLTapResponderAttributeName: viewAllcomment}];
        //
        
        
        
        //Time Label
        
        UILabel *timeLbl = [[UILabel alloc] init];
        
        //timeLbl.font = [UIFont fontWithName:@"MuseoSansCyrl-500" size:14];
        
        timeLbl.font = [UIFont systemFontOfSize: 14];
        
        timeLbl.textColor = [UIColor lightGrayColor];
        
        
        
        if ([[[userDataArr objectAtIndex: indexPath.row] objectForKey:@"totallike"] intValue] != 0)
        {
            NSString *singularPluralStr;
            
            if ([[[userDataArr objectAtIndex: indexPath.row] objectForKey:@"totallike"] intValue] <= 1 )
            {
                singularPluralStr = @"Like";
            }
            else
            {
                singularPluralStr = @"Likes";
            }
            
            totalLikeLbl.frame = CGRectMake(10, likeBtn.frame.origin.y + likeBtn.frame.size.height + 5, [[UIScreen mainScreen] bounds].size.width - 20, 15);
            
            getTotlLikeBtn.frame = totalLikeLbl.frame;
            
            
            totalLikeLbl.text = [NSString stringWithFormat:@"%@ %@", [[userDataArr objectAtIndex: indexPath.row] objectForKey:@"totallike"], singularPluralStr];
            
            
            [cell.contentView addSubview:totalLikeLbl];
            
            [cell.contentView addSubview:getTotlLikeBtn];
            
            
            if (dataStr.length)
            {
                captionLbl.frame = CGRectMake(10, totalLikeLbl.frame.origin.y + totalLikeLbl.frame.size.height+5, [[UIScreen mainScreen] bounds].size.width - 20, captionLblHeigtht+10);
                
                [cell.contentView addSubview:captionLbl];
                
                
                viewAllCommentLbl.frame = CGRectMake(10, captionLbl.frame.origin.y + captionLbl.frame.size.height, 200, 15);
                
                
                timeLbl.frame = CGRectMake(10, viewAllCommentLbl.frame.origin.y + viewAllCommentLbl.frame.size.height+5, 100, 15);
                
                
                
                timeLbl.text = [NSString stringWithFormat:@"%@", [[userDataArr objectAtIndex:indexPath.row] objectForKey:@"posttime"]];
               
                [cell.contentView addSubview:viewAllCommentLbl];
                
                [cell.contentView addSubview:timeLbl];
            }
            else
            {
                viewAllCommentLbl.frame = CGRectMake(10, totalLikeLbl.frame.origin.y + totalLikeLbl.frame.size.height+5, 200, 15);
                
                timeLbl.frame = CGRectMake(10, viewAllCommentLbl.frame.origin.y + viewAllCommentLbl.frame.size.height+5, 100, 15);
                
                
                
                timeLbl.text = [NSString stringWithFormat:@"%@", [[userDataArr objectAtIndex:indexPath.row] objectForKey:@"posttime"]];
                
                [cell.contentView addSubview:viewAllCommentLbl];
                
                [cell.contentView addSubview:timeLbl];
            }
        }
        else
        {
        
            if (dataStr.length)
            {
                captionLbl.frame = CGRectMake(10, likeBtn.frame.origin.y + likeBtn.frame.size.height, [[UIScreen mainScreen] bounds].size.width-20, captionLblHeigtht+10);
                
                [cell.contentView addSubview:captionLbl];
                
                
                captionLbl.frame = CGRectMake(10, likeBtn.frame.origin.y + likeBtn.frame.size.height, [[UIScreen mainScreen] bounds].size.width-20, captionLblHeigtht+10);
                
                [cell.contentView addSubview:captionLbl];
                
                
                viewAllCommentLbl.frame = CGRectMake(10, captionLbl.frame.origin.y + captionLbl.frame.size.height, 200, 15);
                
                timeLbl.frame = CGRectMake(10, viewAllCommentLbl.frame.origin.y + viewAllCommentLbl.frame.size.height+5, 100, 15);
                
                
                
                timeLbl.text = [NSString stringWithFormat:@"%@", [[userDataArr objectAtIndex:indexPath.row] objectForKey:@"posttime"]];
                
                [cell.contentView addSubview:viewAllCommentLbl];
                
                [cell.contentView addSubview:timeLbl];
            }
            else
            {
                viewAllCommentLbl.frame = CGRectMake(10, likeBtn.frame.origin.y + likeBtn.frame.size.height, 200, 15);
                
                timeLbl.frame = CGRectMake(10, viewAllCommentLbl.frame.origin.y + viewAllCommentLbl.frame.size.height+5, 100, 15);
                
                
                
                timeLbl.text = [NSString stringWithFormat:@"%@", [[userDataArr objectAtIndex:indexPath.row] objectForKey:@"posttime"]];
                
                [cell.contentView addSubview:viewAllCommentLbl];
                
                [cell.contentView addSubview:timeLbl];
            }
        }
        
        return cell;
        
        
       /* static NSString *gridCellIdentifier;
        
        if ([[[userDataArr objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"image"])
        {
            gridCellIdentifier = @"listCollection";
            
           ListCollectionViewCell *cell = (ListCollectionViewCell*)[collectionView1 dequeueReusableCellWithReuseIdentifier:gridCellIdentifier forIndexPath:indexPath];
            
            cell.contentView.autoresizingMask  = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            
            
            cell.followBtn.hidden = YES;
            
            //set button tag
            cell.likeBtn.tag = indexPath.row;
            
            cell.favBtn.tag = indexPath.row;
            
            cell.commentBtn.tag = indexPath.row;
            
            cell.shareBtn.tag = indexPath.row;
            
            cell.getTotlLike.tag = indexPath.row;
            
            cell.dotBtn.tag = indexPath.row;
            
            cell.tagBtn.tag = indexPath.row;
            
            
            [self performSelector:@selector(hideTagBtn:) withObject:cell afterDelay:1];

            //set Action on buttons
            [cell.likeBtn addTarget:self action:@selector(likeTap:) forControlEvents:UIControlEventTouchUpInside];
            
            [cell.commentBtn addTarget:self action:@selector(commentTap:) forControlEvents:UIControlEventTouchUpInside];
            
            [cell.favBtn addTarget:self action:@selector(favTap:) forControlEvents:UIControlEventTouchUpInside];
            
            [cell.shareBtn addTarget:self action:@selector(shareTap:) forControlEvents:UIControlEventTouchUpInside];
            
            [cell.getTotlLike addTarget:self action:@selector(getTotalLike:) forControlEvents:UIControlEventTouchUpInside];
            
            [cell.dotBtn addTarget:self action:@selector(dotsTap:) forControlEvents:UIControlEventTouchUpInside];
            
            [cell.tagBtn addTarget:self action:@selector(showTagPeopleView:) forControlEvents:UIControlEventTouchUpInside];
           
            //
            [cell.profileImg sd_setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [[userDataArr objectAtIndex:indexPath.row] valueForKey:@"profilePic"] ] ] placeholderImage:nil];
            
            cell.userNameLbl.text = [[userDataArr objectAtIndex:indexPath.row] objectForKey:@"fullname"];
            
            
            //Hastag detection
            
            cell.captionLbl.userInteractionEnabled = YES;
            PatternTapResponder hashTagTapAction = ^(NSString *tappedString)
            {
                NSLog(@"HashTag Tapped = %@",tappedString);
            };
            
            [cell.captionLbl enableHashTagDetectionWithAttributes:
             @{NSForegroundColorAttributeName:[UIColor blueColor], RLTapResponderAttributeName:hashTagTapAction}];
            
            //
            
            
            if (![[[userDataArr objectAtIndex:indexPath.row] objectForKey:@"caption"] isEqualToString:@""])
            {
                cell.captionLbl.attributedText = [self setBoldString:[NSString stringWithFormat:@"%@ %@", cell.userNameLbl.text, [[userDataArr objectAtIndex:indexPath.row] objectForKey:@"caption"]] boldString:cell.userNameLbl.text];
            }
            else
            {
                cell.captionLbl.attributedText = [[NSMutableAttributedString alloc] initWithString:@""];
            }
            
            
            //string detection
            PatternTapResponder tapResponder = ^(NSString *string)
            {
                NSLog(@"tapped = %@",string);
            };
            
            [cell.captionLbl enableStringDetection:cell.userNameLbl.text withAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor],RLTapResponderAttributeName: tapResponder}];
            //
            
            
            NSArray *commentArr = [[userDataArr objectAtIndex:indexPath.row] objectForKey:@"allComment"];
            
            if ([commentArr count])
            {
                NSMutableString *commentStr = [@"" mutableCopy];
                
                for (int i = 0; i < commentArr.count; i++)
                {
                    [commentStr appendFormat:@"%@\n", [NSString stringWithFormat:@"%@ %@", [[commentArr objectAtIndex:i] objectForKey:@"fullname"], [[commentArr objectAtIndex:i] objectForKey:@"comment"]]];
                    
                    //string detection
                    PatternTapResponder tapResponder = ^(NSString *string)
                    {
                        NSLog(@"tapped = %@",string);
                    };
                    
                    [cell.captionLbl enableStringDetection:[[commentArr objectAtIndex:i] objectForKey:@"fullname"] withAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor],
                                                                                                                                    RLTapResponderAttributeName: tapResponder}];
                    //
                }
                
                //string detection
                PatternTapResponder tapResponder = ^(NSString *string)
                {
                    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
                    
                    btn.tag = indexPath.row;
                    
                    [self commentTap:btn];
                };
                
                [cell.captionLbl enableStringDetection:@"View all comments" withAttributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor],RLTapResponderAttributeName: tapResponder}];
                //
                
                NSMutableAttributedString *yourAttributedString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\nView all comments\n\n%@", cell.captionLbl.text, commentStr]];
                
                cell.captionLbl.attributedText = yourAttributedString;
                
            }
            
            
            
            cell.timeLbl.text = [[userDataArr objectAtIndex:indexPath.row] valueForKey:@"posttime"];
            
             cell.locationLbl.text = [[userDataArr objectAtIndex:indexPath.row] valueForKey:@"location"];
            
            
            cell.totalLikesLbl.text = [[userDataArr objectAtIndex:indexPath.row] valueForKey:@"totallike"];
            
            if ([[[userDataArr objectAtIndex:indexPath.row] valueForKey:@"totallike"] intValue] >= 1)
            {
                cell.totalLikesLbl.hidden = NO;
                
                cell.totalLikeImg.hidden = NO;
                
                cell.getTotlLike.hidden = NO;
            }
            else
            {
                cell.totalLikesLbl.hidden = YES;
                
                cell.totalLikeImg.hidden = YES;
                
                cell.getTotlLike.hidden = YES;
            }
            
            if ([[[userDataArr objectAtIndex:indexPath.row] objectForKey:@"commentflag"] intValue] == 0)
            {
                [cell.commentBtn setImage:[UIImage imageNamed:@"comment_off"] forState: UIControlStateNormal];
            }
            else
            {
                [cell.commentBtn setImage:[UIImage imageNamed:@"comment"] forState: UIControlStateNormal];
            }

            if ([(NSArray *)[[userDataArr objectAtIndex:indexPath.row] valueForKey:@"tagpeople"] count])
            {
                [self showTagBtn:cell];
            }
            else
            {
                [self hideTagBtn:cell];
            }
            
            
            //like
            if ([[[userDataArr objectAtIndex:indexPath.row] valueForKey:@"youlike"] intValue] == 0)
            {
                [cell.likeBtn setImage:[UIImage imageNamed:@"like"] forState:UIControlStateNormal];
            }
            else
            {
                [cell.likeBtn setImage:[UIImage imageNamed:@"like_h"] forState:UIControlStateNormal];
            }
            
            //Favourite
            if ([[[userDataArr objectAtIndex:indexPath.row] valueForKey:@"favourite"] intValue] == 0)
            {
                [cell.favBtn setImage:[UIImage imageNamed:@"star"] forState:UIControlStateNormal];
            }
            else
            {
                [cell.favBtn setImage:[UIImage imageNamed:@"star_h"] forState:UIControlStateNormal];
            }
            
            [cell.userImg sd_setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [[userDataArr objectAtIndex:indexPath.row] valueForKey:@"media"] ] ] placeholderImage:nil];
            
            
            UIImageView *testImage = [[UIImageView alloc] init];
            
            [testImage sd_setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [[userDataArr objectAtIndex:indexPath.row] valueForKey:@"media"] ] ] placeholderImage:nil];
            
            UIImage *image = [self imageWithImage:testImage.image scaledToWidth:[[UIScreen mainScreen] bounds].size.width];
            
            
            cell.imageHeight.constant = image.size.height;
            
            
            return cell;
            
        }
        
        else
        {
            gridCellIdentifier = @"listCollectionVideo";
            
            ListCollectionViewVideoCell *cell = (ListCollectionViewVideoCell*)[collectionView1 dequeueReusableCellWithReuseIdentifier:gridCellIdentifier forIndexPath:indexPath];
            
            cell.contentView.autoresizingMask  = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            
            cell.followBtn.hidden = YES;
            
            //set button tag
            cell.likeBtn.tag = indexPath.row;
            
            cell.favBtn.tag = indexPath.row;
            
            cell.commentBtn.tag = indexPath.row;
            
            cell.shareBtn.tag = indexPath.row;
            
            cell.dotBtn.tag = indexPath.row;
            
            //set Action on buttons
            [cell.likeBtn addTarget:self action:@selector(likeTap:) forControlEvents:UIControlEventTouchUpInside];
            
            [cell.commentBtn addTarget:self action:@selector(commentTap:) forControlEvents:UIControlEventTouchUpInside];
            
            [cell.favBtn addTarget:self action:@selector(favTap:) forControlEvents:UIControlEventTouchUpInside];
            
            [cell.shareBtn addTarget:self action:@selector(shareTap:) forControlEvents:UIControlEventTouchUpInside];
            
            [cell.getTotlLike addTarget:self action:@selector(getTotalLike:) forControlEvents:UIControlEventTouchUpInside];
            
            [cell.dotBtn addTarget:self action:@selector(dotsTap:) forControlEvents:UIControlEventTouchUpInside];
            
            //
            [cell.profileImg sd_setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [[userDataArr objectAtIndex:indexPath.row] valueForKey:@"profilePic"] ] ] placeholderImage:nil];
            
            cell.userNameLbl.text = [[userDataArr objectAtIndex:indexPath.row] objectForKey:@"fullname"];
            
            if (![[[userDataArr objectAtIndex:indexPath.row] objectForKey:@"caption"] isEqualToString:@""])
            {
                cell.captionLbl.attributedText = [self setBoldString:[NSString stringWithFormat:@"%@ %@", cell.userNameLbl.text, [[userDataArr objectAtIndex:indexPath.row] objectForKey:@"caption"]] boldString:cell.userNameLbl.text];
            }
            else
            {
                cell.captionLbl.attributedText = [[NSMutableAttributedString alloc] initWithString:@""];
            }
            cell.timeLbl.text = [[userDataArr objectAtIndex:indexPath.row] valueForKey:@"posttime"];
            
            cell.totalLikesLbl.text = [[userDataArr objectAtIndex:indexPath.row] valueForKey:@"totallike"];
            
            if ([[[userDataArr objectAtIndex:indexPath.row] valueForKey:@"totallike"] intValue] >= 1)
            {
                cell.totalLikesLbl.hidden = NO;
                
                cell.totalLikeImg.hidden = NO;
                
                cell.getTotlLike.hidden = NO;
            }
            else
            {
                cell.totalLikesLbl.hidden = YES;
                
                cell.totalLikeImg.hidden = YES;
                
                cell.getTotlLike.hidden = YES;
            }
            
            
            if ([[[userDataArr objectAtIndex:indexPath.row] objectForKey:@"commentflag"] intValue] == 0)
            {
                [cell.commentBtn setImage:[UIImage imageNamed:@"comment_off"] forState: UIControlStateNormal];
            }
            else
            {
                [cell.commentBtn setImage:[UIImage imageNamed:@"comment"] forState: UIControlStateNormal];
            }
            
            //like
            if ([[[userDataArr objectAtIndex:indexPath.row] valueForKey:@"youlike"] intValue] == 0)
            {
                [cell.likeBtn setImage:[UIImage imageNamed:@"like"] forState:UIControlStateNormal];
            }
            else
            {
                [cell.likeBtn setImage:[UIImage imageNamed:@"like_h"] forState:UIControlStateNormal];
            }
            
            //Favourite
            if ([[[userDataArr objectAtIndex:indexPath.row] valueForKey:@"favourite"] intValue] == 0)
            {
                [cell.favBtn setImage:[UIImage imageNamed:@"star"] forState:UIControlStateNormal];
            }
            else
            {
                [cell.favBtn setImage:[UIImage imageNamed:@"star_h"] forState:UIControlStateNormal];
            }
            
            if ([self isIndexPathVisible:indexPath])
            {
                cell.videoView = [[InstagramVideoView alloc] initWithFrame:cell.player.frame];
                
                cell.videoView.backgroundColor = [UIColor lightTextColor];
                
                [cell addSubview:cell.videoView];
                
                [cell.videoView playVideoWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", ServiceURL, [[userDataArr objectAtIndex:indexPath.row] valueForKey:@"media"]]] ];
                
               // [cell.player videoUrl:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", ServiceURL, [[userDataArr objectAtIndex:indexPath.row] valueForKey:@"media"]]] frame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width - 16, 245)];
                
            }
            
            
            
            return cell;
        }*/
    }
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (gridSelected == YES)
    {
        if (indexPath.row == 0)
        {
            return;
        }
        
        
        ShowPostsViewController *vc = [[ShowPostsViewController alloc] initWithNibName:@"ShowPostsViewController" bundle:nil];
        
        vc.hidesBottomBarWhenPushed = YES;
        
        vc.postArr = [[NSMutableArray alloc] initWithObjects:[[userDataArr objectAtIndex:indexPath.row] mutableCopy], nil];
        
        //vc.indexPath = indexPath;
        
        [self.navigationController pushViewController:vc animated:YES];

        
        
//        ViewSinglePostViewController *vc = [[ViewSinglePostViewController alloc] initWithNibName:@"ViewSinglePostViewController" bundle:nil];
//        
//        vc.postDict = [[userDataArr objectAtIndex:indexPath.row] mutableCopy];
//        
//        vc.userNameStr = nameLbl.text;
//        
//        vc.userImage =  userImg.image;
//        
//        [self.navigationController pushViewController:vc animated:YES];
    }
    else
    {
        UICollectionViewCell *cell = (UICollectionViewCell *)[contentCollectionView cellForItemAtIndexPath:indexPath];
        
        if ([[[userDataArr objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"image"])
        {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            
            button.tag = indexPath.row;
            
            [self addTagBtnOnCell:cell sender:button personTagArr:[[userDataArr objectAtIndex:indexPath.row] valueForKey:@"tagpeople"]];
        }
//        ListCollectionViewCell *cell = (ListCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
//        
//        if ([[[userDataArr objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"image"])
//        {
//            [self addTagBtnOnCell:cell personTagArr:[[userDataArr objectAtIndex:indexPath.row] valueForKey:@"tagpeople"]];
//        }
    }
    
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
//    if (userDataArr.count > 1)
//    {
//        if ([[[userDataArr objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"image"])
//        {
//            ListCollectionViewCell *imageCell = (ListCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
//          
//            [self showTagBtn:imageCell];
//        }
//    }
}


- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    for (UIView *view in cell.subviews)
    {
        if ([view isKindOfClass:[JDTagView class]])
        {
            [view removeFromSuperview];
        }
    }
    
   /* ListCollectionViewVideoCell *videoCell = (ListCollectionViewVideoCell *)cell;
   
    if (userDataArr.count > 1)
    {
      
        NSLog(@"%ld", indexPath.row);
        
        if (indexPath.row <= userDataArr.count-1)
        {
            if ([[[userDataArr objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"video"])
            {
                if ([videoCell isKindOfClass:[ListCollectionViewVideoCell class]])
                {
                    [videoCell.player.player pause];
                    
                    [videoCell.videoView stopVideoPlay];
                    
                    [videoCell.player removeObserver];
                }
            }
            else
            {
                ListCollectionViewCell *imageCell = (ListCollectionViewCell *)cell;
                
                
                
                if ([imageCell isKindOfClass:[ListCollectionViewCell class]])
                {
                    for (UIView *view in cell.subviews)
                    {
                        if ([view isKindOfClass:[JDTagView class]])
                        {
                            [view removeFromSuperview];
                        }
                    }
                    
                    imageCell.tagBtn.alpha = 1;
                }
                
            }
        }
    }*/
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    isScrolling = NO;
    
    [contentCollectionView reloadData];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)aScrollView
{
    [self clearvideoViews];
    
    isScrolling = YES;
    
    [contentCollectionView reloadData];
}




- (BOOL)isIndexPathVisible:(NSIndexPath*)indexPath
{
    NSArray *visiblePaths = [contentCollectionView indexPathsForVisibleItems];
    
    for (NSIndexPath *currentIndex in visiblePaths)
    {
        NSComparisonResult result = [currentIndex compare:currentIndex];
        
        if(result == NSOrderedSame)
        {
            NSLog(@"Visible");
           
            return YES;
        }
    }
    
    return NO;
}


#pragma mark - ActionSheet

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"%ld", actionSheet.tag);

    if (actionSheet.tag == 100)
    {
        if(buttonIndex != actionSheet.cancelButtonIndex)
        {
            if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Create Profile"])
            {
                [self backTap];
            }
            else
            {
                [self hitSwitchProfileWs:[NSDictionary dictionaryWithObjectsAndKeys: [[bussinesArr objectAtIndex:0] objectForKey:@"id"],@"userId", @"0", @"flag", nil]];
            }
            
        }
    }
    else
    {
        if ([[[userDataArr objectAtIndex:actionSheet.tag] objectForKey:@"userId"] intValue] == [[(APPDEL).userDetailDict objectForKey:@"userId"] intValue])
        {
            if(buttonIndex == 0)
            {
                UIAlertController *alertController = [UIAlertController
                                                      alertControllerWithTitle:nil
                                                      message:@"Delete Post?"
                                                      preferredStyle:UIAlertControllerStyleAlert];
                
                alertController.view.tag = actionSheet.tag;
                
                UIAlertAction *yesAction = [UIAlertAction
                                            actionWithTitle:@"YES"
                                            style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction *action)
                                            {
                                                
                                                 [self deletePost:actionSheet.tag postId:[[userDataArr objectAtIndex:actionSheet.tag] valueForKey:@"postId"]];
                                            }];
                
                UIAlertAction *noAction = [UIAlertAction
                                           actionWithTitle:@"NO"
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction *action)
                                           {
                                               
                                           }];
                
                
                
                [alertController addAction:yesAction];
                
                [alertController addAction:noAction];
                
                
                [self presentViewController:alertController animated:YES completion:nil];
            }
           
            else if(buttonIndex == 1)
            {
                [self turnOffCommenting:actionSheet.tag params:[NSDictionary dictionaryWithObjectsAndKeys:[(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", [[userDataArr objectAtIndex:actionSheet.tag] objectForKey:@"postId"], @"postId", [[[userDataArr objectAtIndex:actionSheet.tag] objectForKey:@"commentflag"] intValue] == 0 ? @"1" : @"0", @"flag", nil]];
            }
            else if(buttonIndex == 2)
            {
                EditPostViewController *vc = [[EditPostViewController alloc] initWithNibName:@"EditPostViewController" bundle:nil];
                
                vc.postDict = [userDataArr objectAtIndex:actionSheet.tag];
                
                [self.navigationController pushViewController:vc animated:NO];
                
            }
            else if(buttonIndex == 3)
            {
                
            }
        }
        
        else
        {
            if(buttonIndex == 0)
            {
                UIAlertController *alertController = [UIAlertController
                                                      alertControllerWithTitle:nil
                                                      message:nil
                                                      preferredStyle:UIAlertControllerStyleActionSheet];
                
                alertController.view.tag = actionSheet.tag;
                
                UIAlertAction *InappropriateAction = [UIAlertAction
                                                      actionWithTitle:@"Inappropriate Post"
                                                      style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action)
                                                      {
                                                          [self reportAbuse:[[userDataArr objectAtIndex:alertController.view.tag] valueForKey:@"postId"] secondryId:[[userDataArr objectAtIndex:alertController.view.tag] valueForKey:@"userId"] isInappropiate:@"1"];
                                                      }];
                
                UIAlertAction *SpamAction = [UIAlertAction
                                             actionWithTitle:@"Spam Account"
                                             style:UIAlertActionStyleDefault
                                             handler:^(UIAlertAction *action)
                                             {
                                                 [self reportAbuse:[[userDataArr objectAtIndex:alertController.view.tag] valueForKey:@"postId"] secondryId:[[userDataArr objectAtIndex:alertController.view.tag] valueForKey:@"userId"] isInappropiate:@"2"];
                                             }];
                
                UIAlertAction *resetAction = [UIAlertAction
                                              actionWithTitle:@"Cancel"
                                              style:UIAlertActionStyleCancel
                                              handler:^(UIAlertAction *action)
                                              {
                                                  NSLog(@"Reset action");
                                              }];
                
                
                [alertController addAction:resetAction];
                
                [alertController addAction:InappropriateAction];
                
                [alertController addAction:SpamAction];
                
                
                [self presentViewController:alertController animated:YES completion:nil];
            }
           /* if ([[[userDataArr objectAtIndex:actionSheet.tag] valueForKey:@"type"] isEqualToString:@"image"])
            {
                UIImageView *localImg = [[UIImageView alloc] init];
                
                [localImg sd_setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [[userDataArr objectAtIndex:actionSheet.tag] valueForKey:@"media"] ] ] placeholderImage:nil];
                
                if(buttonIndex == 0)
                {
                    [self postShareOnFacebookShare:localImg.image];
                }
                
                else if (buttonIndex == 1)
                {
                    [self postShareOnMailShare:[NSString stringWithFormat:@"%@%@", ServiceURL, [[userDataArr objectAtIndex:actionSheet.tag] valueForKey:@"media"]]];
                }
                
                else if (buttonIndex == 2)
                {
                    [self reportAbuse:[[userDataArr objectAtIndex:actionSheet.tag] valueForKey:@"postId"]];
                }
            }
            else
            {
                UIImageView *localImg = [[UIImageView alloc] init];
                
                [localImg sd_setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [[userDataArr objectAtIndex:actionSheet.tag] valueForKey:@"thumbnail"] ] ] placeholderImage:nil];
                
                
                if(buttonIndex == 0)
                {
                    [self postShareOnFacebookShare:localImg.image];
                }
                
                else if (buttonIndex == 1)
                {
                    [self postShareOnMailShare:[NSString stringWithFormat:@"%@%@", ServiceURL, [[userDataArr objectAtIndex:actionSheet.tag] valueForKey:@"thumbnail"]]];
                }
                
                else if (buttonIndex == 2)
                {
                    [self reportAbuse:[[userDataArr objectAtIndex:actionSheet.tag] valueForKey:@"postId"]];
                }
            }*/
        }
    }
}


#pragma mark - Mail Delegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}


#pragma mark - JDTagView Delegate

- (void)selectedTag:(JDTagView *)View
{
    ListCollectionViewCell *cell = (ListCollectionViewCell *)View.superview;
    
    UICollectionView *collectionView = (UICollectionView *)View.superview.superview;
    
    NSIndexPath *indexPath = [collectionView indexPathForCell:cell];
    
    NSArray *taggArr = [[userDataArr objectAtIndex:indexPath.row] objectForKey:@"tagpeople"];
    
    
    ProfileViewController *vc = [[ProfileViewController alloc] initWithNibName:@"ProfileViewController" bundle:nil];
    
    vc.differentUserId = [[taggArr objectAtIndex:View.tag-1] objectForKey:@"userId"];
    
    [self.navigationController pushViewController:vc animated:YES];
}


- (void)deleteTagFromView:(JDTagView *)JDTagView
{
    
}



@end
