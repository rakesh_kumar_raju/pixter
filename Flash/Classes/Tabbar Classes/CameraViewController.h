//
//  CameraViewController.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 1/2/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#define CAPTURE_FRAMES_PER_SECOND		20


#import <UIKit/UIKit.h>
#import "GetPhoneLibrary.h"
#import "AvplayerLibrary.h"
#import "CameraControl.h"

#import <CoreMedia/CoreMedia.h>
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <ImageIO/ImageIO.h>

@interface CameraViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, AVCaptureFileOutputRecordingDelegate, AVCaptureVideoDataOutputSampleBufferDelegate,AVCaptureVideoDataOutputSampleBufferDelegate, AVCaptureAudioDataOutputSampleBufferDelegate, GetPhoneLibraryDelegate>
{
    IBOutlet UIScrollView *contentSv, *selctedImgSv;
    
    IBOutlet UIView *contentView;
    
    IBOutlet UITableView *assetGroupTbl;
    
    IBOutlet UIView *assetGroupView;
    
    UIView *cameraView;
    
    IBOutlet UIButton *titleBtn, *cameraSwapIcon;
    
    IBOutlet UIButton *nextBtn;

    BOOL WeAreRecording;
    
    CALayer *rootLayer;
    
    AVCaptureMovieFileOutput *MovieFileOutput;
    
    AVCaptureSession *session;
    
    AVCaptureStillImageOutput *stillImageOutput;
}

- (void)addCameraViewOnSuperView;


- (IBAction)selectPhotoGroup;

- (IBAction)nextTap;

@end
