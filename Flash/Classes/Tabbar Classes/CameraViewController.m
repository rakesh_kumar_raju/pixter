//
//  CameraViewController.m
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 1/2/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#define SCREEN_Width [[UIScreen mainScreen] bounds].size.width

#define SCREEN_height [[UIScreen mainScreen] bounds].size.height

#import "CameraViewController.h"
#import "AssetGroupTableViewCell.h"

#import "FilterViewController.h"
#import "ShareViewController.h"
#import "AppDelegate.h"
#import "GallaryData.h"

@interface CameraViewController ()
{
    BOOL isCapturePic, isGallaryVideo, ifImageZoom;
    
    
    ALAsset *selectedAsset;
    
    AVCaptureDeviceInput *micDeviceInput;
    
    AVCaptureDeviceInput *videoInputDevice;
    
    
    UIButton *cameraBtn, *libraryBtn;
    
    NSMutableArray *albumArr;
    
    UIImageView *selectedImg;
    
    NSURL *savedVideoUrl;
    
    
    GetPhoneLibrary *photolibObj;

    AvplayerLibrary *playerLibObj;
    
    CameraControl *cameraCtrlObj;
    
    UILabel *timerLbl;
    
    NSTimer *timer;
    
    int videoTime;
}



@end



@implementation CameraViewController


#pragma mark - ViewCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    videoTime = 30;
    
    self.navigationController.navigationBar.hidden = YES;

    ifImageZoom = NO;
    
    [self setUpView];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    [contentSv setContentOffset:CGPointMake(0, 0) animated:YES];
    
    [titleBtn setTitle:@"Camera" forState:UIControlStateNormal];
    
    
    if(libraryBtn.selected)
    {
        libraryBtn.selected = NO;
        
        cameraBtn.selected = YES;
    }
    
    //add camera control
    
    [contentSv addSubview:cameraCtrlObj];
    
    //add collection view
    [contentSv addSubview:photolibObj];
    
    
    //add selection video
    [selctedImgSv addSubview:playerLibObj];
    
    
    //Add Camera
    [self performSelector:@selector(addCameraViewOnSuperView) withObject:nil afterDelay:0.5];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    [playerLibObj removeVideoPreviewLayer];
    
    [playerLibObj removeObserver];
    
    
    [self removeCameraView];
}


#pragma mark - Void

- (void) setUpView
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(camcoderToCameraToggle:) name:@"camcorderToCamera" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(recordingOrCapture:) name:@"record_capture" object:nil];
  
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(captureImage:) name:@"capture" object:nil];
    //
    
    
    
    cameraBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    cameraBtn.selected = YES;
    
    cameraBtn.frame = CGRectMake(0, 0, SCREEN_Width/2, 40);
    
    [cameraBtn setTitle:@"Camera" forState:UIControlStateNormal];
    
    [cameraBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    
    [cameraBtn setTitleColor:[UIColor colorWithRed:124.0/255.0 green:124.0/255.0 blue:124.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    
    [cameraBtn addTarget:self action:@selector(switchBtn:) forControlEvents:UIControlEventTouchUpInside];
    
    //
    libraryBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    libraryBtn.selected = NO;
    
    libraryBtn.frame = CGRectMake(cameraBtn.frame.size.width, 0, SCREEN_Width/2, 40);
    
    [libraryBtn setTitle:@"Library" forState:UIControlStateNormal];
    
    [libraryBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    
    [libraryBtn setTitleColor:[UIColor colorWithRed:124.0/255.0 green:124.0/255.0 blue:124.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    
    [libraryBtn addTarget:self action:@selector(switchBtn:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [contentView addSubview:cameraBtn];
    
    [contentView addSubview:libraryBtn];
    
    
    
    selectedImg = [[UIImageView alloc] init];
    
    selectedImg.contentMode  = UIViewContentModeScaleAspectFit;
    
    selectedImg.backgroundColor = [UIColor blackColor];
    
   // selectedImg.clipsToBounds = YES;
    
    
   
    CGRect sizeRect;
    
    if (SCREEN_Width == 320)
    {
        sizeRect = CGRectMake(0, 0, 320, [[UIScreen mainScreen] bounds].size.height-230-40);
    }
    
    else if (SCREEN_Width == 375)
    {
        sizeRect = CGRectMake(0, 0, 375, [[UIScreen mainScreen] bounds].size.height - 230-40);
    }
    
    else if (SCREEN_Width == 414)
    {
        sizeRect = CGRectMake(0, 0, 414, [[UIScreen mainScreen] bounds].size.height - 230-40);
    }
 
    selectedImg.frame = sizeRect;
   
   
   
    //
    photolibObj = [[[NSBundle mainBundle] loadNibNamed:@"GetPhoneLibrary" owner:self options:nil] lastObject];
    
    photolibObj.delegate = self;
    
    photolibObj.frame = CGRectMake(SCREEN_Width, 0, SCREEN_Width, contentSv.frame.size.height);
    
    
    [photolibObj loadView];
    
    
    //
    playerLibObj = [[[NSBundle mainBundle] loadNibNamed:@"AvplayerLibrary" owner:self options:nil] lastObject];
    
    CGRect playerLibObjFrame;
    
    if (SCREEN_Width == 320)
    {
        playerLibObjFrame = CGRectMake(0, 0, 320, [[UIScreen mainScreen] bounds].size.height);
    }
    
    else if (SCREEN_Width == 375)
    {
        playerLibObjFrame = CGRectMake(0, 0, 375, [[UIScreen mainScreen] bounds].size.height - 230);
    }
    
    else if (SCREEN_Width == 414)
    {
        playerLibObjFrame = CGRectMake(0, 0, 414, [[UIScreen mainScreen] bounds].size.height - 230);
    }
    
    playerLibObj.frame = playerLibObjFrame;
    

    //
    cameraCtrlObj = [[[NSBundle mainBundle] loadNibNamed:@"CameraControl" owner:self options:nil] objectAtIndex:0];
    
    cameraCtrlObj.frame = CGRectMake(0, 0, SCREEN_Width, contentSv.frame.size.height);
   
    
    //
    contentSv.contentSize = CGSizeMake(SCREEN_Width * 2, contentSv.frame.size.height);
}


- (void)addCameraViewOnSuperView
{
    if(![cameraView isDescendantOfView:selctedImgSv])
    {
         [self hideNextBtn];
        
        CGRect cameraFrame;
        
        if (SCREEN_Width == 320)
        {
            cameraFrame = CGRectMake(0, 0, 320, [[UIScreen mainScreen] bounds].size.height-230);
        }
        
        else if (SCREEN_Width == 375)
        {
            cameraFrame = CGRectMake(0, 0, 375, [[UIScreen mainScreen] bounds].size.height - 230);
        }
        
        else if (SCREEN_Width == 414)
        {
            cameraFrame = CGRectMake(0, 0, 414, [[UIScreen mainScreen] bounds].size.height - 230);
        }
        
        cameraView.frame = cameraFrame;
        
        cameraView.tag = 999;
        
        [selctedImgSv addSubview:cameraView];
        
    
        cameraSwapIcon = [UIButton buttonWithType:UIButtonTypeCustom];
        
        cameraSwapIcon.frame = CGRectMake(10, selctedImgSv.frame.size.height- 50, 40, 40);
        
        [cameraSwapIcon setImage:[UIImage imageNamed:@"camera_swap"] forState:UIControlStateNormal];
        
        
        [cameraSwapIcon addTarget:self action:@selector(cameraToggleButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        
        [selctedImgSv addSubview:cameraSwapIcon];
        
       
        [self cameraModeToStillImage];
    }
}


- (void)removeCameraView
{
//    [UILabel beginAnimations:NULL context:nil];
//  
//    [UILabel setAnimationDuration:0.5];
//    
//    [cameraView setAlpha:0];
//    
//    [UILabel commitAnimations];
//    
    [cameraView removeFromSuperview];
    
    [session stopRunning];
}


- (void)addSelectedImgView
{
    [selctedImgSv addSubview:selectedImg];
}


- (void)addFirstAssetFromGallaryCollection
{
    [self setAssetTypeOnScreen:[[[GallaryData sharedManager] gallaryDataArr] objectAtIndex:0]];
}


- (void)saveFileToDocumentDirectory:(NSString *)fileName image:(UIImage *)image isMirror:(BOOL)isMirror
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:fileName];
    
    NSData *imageData;
    
    if (isMirror == YES)
    {
        imageData = UIImagePNGRepresentation(image);
        
    }
    
    else
    {
        imageData = UIImagePNGRepresentation(image);
    }
    
    [imageData writeToFile:savedImagePath atomically:NO];
}



/*- (UIImage*)imageWithImage:(UIImage*)image
              scaledToSize:(CGSize)newSize;
{
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}*/


- (void)addTimerLbl
{
    timerLbl = [[UILabel alloc] initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width-70, 20, 50, 50)];
    
    timerLbl.text = @"";
    
    timerLbl.font = [UIFont systemFontOfSize: 18];
    
    timerLbl.textColor = [UIColor yellowColor];
    
    [selctedImgSv addSubview:timerLbl];
}

- (void)removeTimerLbl
{
    [timerLbl removeFromSuperview];
}


#pragma mark - NSTimer For video

- (void)timerFired:(NSTimer *)timer1
{
    if (videoTime == 0)
    {
        [timer invalidate];
    }
    else
    {
        videoTime--;
        
        timerLbl.text = [NSString stringWithFormat:@"0:%i", videoTime];
    }
}


#pragma mark - Save video to Document directory

- (void)saveVideoToDocumentDirectory:(NSURL *)pathUrl
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *videoPath = [documentsDirectory stringByAppendingPathComponent:@"output.mov"];
    
    
    ALAssetsLibrary *assetLibrary=[[ALAssetsLibrary alloc] init];
    
    __block NSData *data;
    
    [assetLibrary assetForURL: pathUrl resultBlock:^(ALAsset *asset)
     {
         ALAssetRepresentation *rep = [asset defaultRepresentation];
         
         Byte *buffer = (Byte*)malloc(rep.size);
         
         NSUInteger buffered = [rep getBytes:buffer fromOffset:0.0 length:rep.size error:nil];
         
         data = [NSData dataWithBytesNoCopy:buffer length:buffered freeWhenDone:YES];
         
         [data writeToFile:videoPath atomically:YES];
     }
        failureBlock:^(NSError *err)
     {
         NSLog(@"Error: %@",[err localizedDescription]);
         
     }];
    
    NSLog(@"video path --> %@",videoPath);
}



#pragma mark - Next Button

- (void)hideNextBtn
{
    nextBtn.alpha = 0.7;
    
    nextBtn.userInteractionEnabled = NO;
}

- (void)showNextBtn
{
    nextBtn.alpha = 1;
    
    nextBtn.userInteractionEnabled = YES;
}


- (void)cameraToggleButtonPressed
{
    [UIView animateWithDuration:0.1 animations:^{
        cameraView.alpha = 0;
        
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.1 animations:^{
             cameraView.alpha = 1;
            
            if ([[AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo] count] > 1)
            {
                NSLog(@"Toggle camera");
                
                NSError *error;
                
                AVCaptureDeviceInput *NewVideoInput;
                
                AVCaptureDevicePosition position = [[videoInputDevice device] position];
                
                if (position == AVCaptureDevicePositionBack)
                {
                    NewVideoInput = [[AVCaptureDeviceInput alloc] initWithDevice:[self CameraWithPosition:AVCaptureDevicePositionFront] error:&error];
                    
                    (APPDEL).isFrontCamera = YES;
                }
                
                else if (position == AVCaptureDevicePositionFront)
                {
                    NewVideoInput = [[AVCaptureDeviceInput alloc] initWithDevice:[self CameraWithPosition:AVCaptureDevicePositionBack] error:&error];
                    
                    (APPDEL).isFrontCamera = NO;
                    
                }
                
                if (NewVideoInput != nil)
                {
                    [session beginConfiguration];
                    
                    NSMutableArray *arr = [session.inputs mutableCopy];
                    
                    for (int i = 0; i < arr.count; i++ )
                    {
                        AVCaptureDeviceInput *input1 =  arr[i];
                        
                        if (input1.device.position == AVCaptureDevicePositionFront)
                        {
                            [arr removeObjectAtIndex: i];
                            
                            [session removeInput:input1];
                        }
                        
                        else if (input1.device.position == AVCaptureDevicePositionBack)
                        {
                            [arr removeObjectAtIndex: i];
                            
                            [session removeInput:input1];
                        }
                        
                    }
                    
                    if ([session canAddInput:NewVideoInput])
                    {
                        [session addInput:NewVideoInput];
                        
                        videoInputDevice = NewVideoInput;
                    }
                    
                    else
                    {
                        [session addInput:videoInputDevice];
                    }
                    
                    //[self CameraSetOutputProperties];
                    
                    
                    [session commitConfiguration];
                    
                }
            }
        }];
    }];
}


#pragma mark - Set Asset for screen

- (void)setAssetTypeOnScreen:(ALAsset *)asset
{
    selectedAsset = asset;
    
    ALAssetRepresentation *defaultRep = [asset defaultRepresentation];
    
    if ([[asset valueForProperty:ALAssetPropertyType] isEqualToString:ALAssetTypeVideo])
    {
        isGallaryVideo = YES;
        
        (APPDEL).isVideo = YES;
        
        [selctedImgSv setZoomScale:1.0f];
        
        selctedImgSv.scrollEnabled = NO;
        
        selctedImgSv.delegate = nil;
        
       
        CGRect playerFrame;
        
        if (SCREEN_Width == 320)
        {
            playerFrame = CGRectMake(0, 0, 320, [[UIScreen mainScreen] bounds].size.height-230);
        }
        
        else if (SCREEN_Width == 375)
        {
            playerFrame = CGRectMake(0, 0, 375, [[UIScreen mainScreen] bounds].size.height - 230);
        }
        
        else if (SCREEN_Width == 414)
        {
            playerFrame = CGRectMake(0, 0, 414, [[UIScreen mainScreen] bounds].size.height - 230);
        }

        
        [playerLibObj videoUrl:[asset defaultRepresentation].url frame:playerFrame];
        
        [selectedImg removeFromSuperview];
        
        
        [self performSelector:@selector(saveVideoToDocumentDirectory:) withObject:[asset defaultRepresentation].url afterDelay:0.5];
       
    }
    
    else
    {
        isGallaryVideo = NO;
        
        (APPDEL).isVideo = NO;
        
        [playerLibObj removeVideoPreviewLayer];
        
        
        selectedImg.image = [UIImage imageWithCGImage:defaultRep.fullResolutionImage
                                                scale:[defaultRep scale]
                                          orientation:(UIImageOrientation)[defaultRep orientation]];/*[self setImageSize:[UIImage imageWithCGImage:[defaultRep fullScreenImage] scale:[defaultRep scale] orientation:0]]*/;
        
     
        [self addSelectedImgView];
        
        [selctedImgSv setZoomScale:1.0f];
        
        selctedImgSv.minimumZoomScale = 1;
        
        selctedImgSv.maximumZoomScale = 6;
        
        selctedImgSv.scrollEnabled = YES;
    }
}


#pragma mark - Set Image Size


- (UIImage *)setImageSize:(UIImage *)image
{
    UIImage *originalImage = image;
    
    // Create rect with height and width equal to originalImages height
    
    CGSize size = CGSizeMake(originalImage.size.width, originalImage.size.height);
    
    UIGraphicsBeginImageContextWithOptions(size, YES, 0);
    
    // Fill the background with some color
    [[UIColor lightGrayColor] setFill];
    
    UIRectFill(CGRectMake(0, 0, size.width, size.height));
    
    // Create image for what we just did
    UIImage *bgImage = UIGraphicsGetImageFromCurrentImageContext();
   
    UIGraphicsEndImageContext();
    
    UIGraphicsBeginImageContext(bgImage.size);
    // Take originalImage and center it in bgImage
    // We get the center of bgImage by (bgImage.size.width / 2)
    // But the coords of the originalImage start at the top left (0,0)
    // So we need to subtract half of our orginalImages width so that it centers (bgImage.size.width / 2) - (originalImage.size.width / 2)
    [originalImage drawInRect:CGRectMake((bgImage.size.width / 2) - (originalImage.size.width / 2), 0, originalImage.size.width, originalImage.size.height)];
    // Create image for what we just did
   
    UIImage *finalImage = UIGraphicsGetImageFromCurrentImageContext();
  
    UIGraphicsEndImageContext();
    
    return finalImage;
}


#pragma mark - Notification

- (void)camcoderToCameraToggle:(NSNotification *)noti
{
    NSString *str = (NSString *)noti.object;
    
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       
                       if ([str isEqualToString:@"1"])
                       {
                           (APPDEL).isVideo = YES;
                           
                           
                           MovieFileOutput = [[AVCaptureMovieFileOutput alloc] init];
                           
                           //Set video time Limit
                           
                           Float64 TotalSeconds = 30;
                           
                           int32_t preferredTimeScale = 30;
                           
                           CMTime maxDuration = CMTimeMakeWithSeconds(TotalSeconds, preferredTimeScale);
                          
                           MovieFileOutput.maxRecordedDuration = maxDuration;
                           
                           MovieFileOutput.minFreeDiskSpaceLimit = 1024 * 1024;
                        
                           [session setSessionPreset:AVCaptureSessionPresetMedium];
                          
                           
                           if ([session canAddOutput:MovieFileOutput])
                           {
                               [session addOutput:MovieFileOutput];
                           }
                           
                           AVCaptureDevice *micInput = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeAudio];
                           
                           micDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:micInput error:nil];
                          
                           if ([session canAddInput:micDeviceInput])
                           {
                               [session addInput:micDeviceInput];
                           }
                           
                           [session removeOutput:stillImageOutput];
                           
                           [self addTimerLbl];
                       }
                       else
                       {
                           (APPDEL).isVideo = NO;
                           
                           [session removeOutput: MovieFileOutput];
                           
                           [session removeInput:micDeviceInput];
                           
                           
                           stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
                           
                           NSDictionary *outputSettings = [[NSDictionary alloc] initWithObjectsAndKeys: AVVideoCodecJPEG, AVVideoCodecKey, nil];
                           
                           [stillImageOutput setOutputSettings:outputSettings];
                           
                           
                           if ([session canAddOutput:stillImageOutput])
                           {
                               [session addOutput:stillImageOutput];
                           }
                          
                           [self removeTimerLbl];
                       }
                       
                   });

}


- (void)recordingOrCapture:(NSNotification *)noti
{
    NSString *str = (NSString *)noti.object;
  
    if ([str isEqualToString:@"0"])
    {
        nextBtn.alpha = 0.7;
        
        nextBtn.userInteractionEnabled = NO;
        
        NSLog(@"START RECORDING");
        
        NSString *DestFilename = @"output.mov";
        
        NSLog(@"Starting recording to file: %@", DestFilename);
        
        NSString *DestPath;
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        
        DestPath = [paths objectAtIndex:0];
        
        DestPath = [DestPath stringByAppendingPathComponent:DestFilename];
        
        
        NSURL* saveLocationURL = [[NSURL alloc] initFileURLWithPath:DestPath];
        
        if([[NSFileManager defaultManager] fileExistsAtPath:DestPath])
        {
            [[NSFileManager defaultManager] removeItemAtPath:DestPath error:nil];
        }
        
        cameraCtrlObj.isRecording = NO;
        
        AVCaptureConnection *videoConnection = nil;
        
        for ( AVCaptureConnection *connection in [MovieFileOutput connections] )
        {
            for ( AVCaptureInputPort *port in [connection inputPorts] )
            {
                if ( [[port mediaType] isEqual:AVMediaTypeVideo] )
                {
                    videoConnection = connection;
                    
                    if(videoConnection.supportsVideoMirroring)
                    {
                        if ((APPDEL).isFrontCamera == YES)
                        {
                             videoConnection.videoMirrored = YES;
                        }
                       else
                       {
                            videoConnection.videoMirrored = NO;
                       }
                    }
                        
                }
            }
        }
        

        [MovieFileOutput startRecordingToOutputFileURL:saveLocationURL recordingDelegate:self];
        
        timer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(timerFired:)
                                                userInfo:nil repeats:YES];
    }
    else
    {
        cameraCtrlObj.isRecording = YES;
        
        [MovieFileOutput stopRecording];
    }
}


- (void)captureImage:(NSNotification *)noti
{
    AVCaptureConnection *videoConnection = nil;
    
    for (AVCaptureConnection *connection in stillImageOutput.connections)
    {
        for (AVCaptureInputPort *port in [connection inputPorts])
        {
            if ([[port mediaType] isEqual:AVMediaTypeVideo])
            {
                videoConnection = connection;
                
                break;
            }
        }
        if (videoConnection)
        {
            break;
        }
    }
    
    NSLog(@"about to request a capture from: %@", stillImageOutput);
    
    [stillImageOutput captureStillImageAsynchronouslyFromConnection:videoConnection completionHandler: ^(CMSampleBufferRef imageSampleBuffer, NSError *error)
     {
         CFDictionaryRef exifAttachments = CMGetAttachment( imageSampleBuffer, kCGImagePropertyExifDictionary, NULL);
         
         if (exifAttachments)
         {
             NSLog(@"attachements: %@", exifAttachments);
             
             NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageSampleBuffer];
             
             UIImage *image = [[UIImage alloc] initWithData:imageData];
            
             
             FilterViewController *vc = [[FilterViewController alloc] initWithNibName:@"FilterViewController" bundle:nil];
             
             if ((APPDEL).isFrontCamera == YES)
             {
                 vc.captureImg = [UIImage imageWithCGImage:[image CGImage]
                                                     scale:[image scale]
                                               orientation: UIImageOrientationLeftMirrored];
             }
             else
             {
                 vc.captureImg = image;
             }
         
             [self.navigationController pushViewController:vc animated:NO];
         }
         else
         {
             NSLog(@"no attachments");
         }
         
        /* NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageSampleBuffer];
         
         UIImage *image = [[UIImage alloc] initWithData:imageData];
         
         //NSLog(@"%ld", (long)image.imageOrientation);
        
         
         
        
         
         FilterViewController *vc = [[FilterViewController alloc] initWithNibName:@"FilterViewController" bundle:nil];
         
         if ((APPDEL).isFrontCamera == YES)
         {
             vc.captureImg = [UIImage imageWithCGImage:[image CGImage]
                                                 scale:[image scale]
                                           orientation: UIImageOrientationLeftMirrored];
         }
       else
       {
           vc.captureImg = image;
       }
//          vc.captureImg = [UIImage imageWithCGImage:[image CGImage]
//                                                 scale:[image scale]
//                                           orientation: UIImageOrientationLeftMirrored];/*[UIImage imageWithCGImage:image.CGImage scale:image.scale orientation:UIImageOrientationLeftMirrored];;*/
//
         //[self.navigationController pushViewController:vc animated:NO];*/
     }];
    
}


#pragma mark - add camera method

- (void)cameraModeToStillImage
{
    if (!session)
    {
        session = [[AVCaptureSession alloc] init];
        
        [session setSessionPreset:AVCaptureSessionPresetPhoto];
    }
    
   
    
    AVCaptureDevice *videoDevice;
    
  
    
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    
    
    for (AVCaptureDevice *device in devices)
    {
        if ((APPDEL).isFrontCamera == YES)
        {
            if ([device position] == AVCaptureDevicePositionFront)
            {
                videoDevice = device;
                
                (APPDEL).isFrontCamera = YES;
                
            }
        }
        else
        {
            if ([device position] == AVCaptureDevicePositionBack)
            {
                videoDevice = device;
                
                (APPDEL).isFrontCamera = NO;
                
            }
        }
        
    }
    
    //Add Video input
    
    NSError *err;
   
    videoInputDevice = [AVCaptureDeviceInput deviceInputWithDevice:videoDevice error:&err];
    
    if([session canAddInput:videoInputDevice])
    {
        [session addInput:videoInputDevice];
    }
    
    
    AVCaptureVideoPreviewLayer *previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:session];
    
    previewLayer.connection.videoOrientation = AVCaptureVideoOrientationPortrait;
    
    [previewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    
    
    rootLayer = [cameraView  layer];
    
    [rootLayer setMasksToBounds:YES];
    
    CGRect cameraFrame;
    
    if (SCREEN_Width == 320)
    {
        cameraFrame = CGRectMake(0, 0, 320, [[UIScreen mainScreen] bounds].size.height-230);
    }
    
    else if (SCREEN_Width == 375)
    {
        cameraFrame = CGRectMake(0, 0, 375, [[UIScreen mainScreen] bounds].size.height - 230);
    }
    
    else if (SCREEN_Width == 414)
    {
        cameraFrame = CGRectMake(0, 0, 414, [[UIScreen mainScreen] bounds].size.height - 230);
    }
    
    
    
    [previewLayer setFrame:cameraFrame];
    
    [rootLayer insertSublayer:previewLayer atIndex:0];
    

    if (stillImageOutput == nil)
    {
        stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
        
        NSDictionary *outputSettings = [[NSDictionary alloc] initWithObjectsAndKeys: AVVideoCodecJPEG, AVVideoCodecKey, nil];
        
        [stillImageOutput setOutputSettings:outputSettings];
        
        
        if ([session canAddOutput:stillImageOutput])
        {
            [session addOutput:stillImageOutput];
        }
    }
 
    [session startRunning];
}


- (AVCaptureDevice *) CameraWithPosition:(AVCaptureDevicePosition) Position
{
    NSArray *Devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    
    for (AVCaptureDevice *Device in Devices)
    {
        if ([Device position] == Position)
        {
            return Device;
        }
    }
    return nil;
}


#pragma mark - Action

- (void)switchBtn:(UIButton *)sender
{
    if (sender == cameraBtn)
    {
        if (sender.selected == NO)
        {
            sender.selected = YES;
            
            libraryBtn.selected = NO;
            
            [contentSv setContentOffset:CGPointMake(0, 0) animated:YES];
        }
       
    }
    else
    {
        if (sender.selected == NO)
        {
            sender.selected = YES;
            
            cameraBtn.selected = NO;
            
            [contentSv setContentOffset:CGPointMake(SCREEN_Width, 0) animated:YES];
        }
    }
}


- (IBAction)selectPhotoGroup
{
    nextBtn.hidden = YES;
    
    [UIView animateWithDuration:0.2 animations:
     ^{
        assetGroupView.frame = CGRectMake(0, 60, SCREEN_Width, [[UIScreen mainScreen] bounds].size.height-60);
         
         [self.tabBarController.view addSubview: assetGroupView];
         
         [assetGroupTbl reloadData];
    }];
}


- (IBAction)nextTap
{
    FilterViewController *vc = [[FilterViewController alloc] initWithNibName:@"FilterViewController" bundle:nil];
    
    ShareViewController *sVc = [[ShareViewController alloc] initWithNibName:@"ShareViewController" bundle:nil];
  
    if ((APPDEL).isVideo == YES)
    {
        NSString *filename = @"output.mov";
        
        NSArray *pathArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES);
        
        NSString *documentsDirectory = [pathArray objectAtIndex:0];
        
        NSString *videoPath = [documentsDirectory stringByAppendingPathComponent:filename];
        
        if (isGallaryVideo == YES)
        {
            ALAssetRepresentation *defaultRep = [selectedAsset defaultRepresentation];
            
            sVc.isVideo = YES;
            
//            vc.videoUrl = [selectedAsset defaultRepresentation].url;
//            
//            vc.isGallaryVideo = YES;
            
            //[self saveFileToDocumentDirectory:@"filterImage.jpg" image:[UIImage imageWithCGImage:[defaultRep fullScreenImage] scale:[defaultRep scale] orientation:0] isMirror:NO];
            
           // vc.captureImg = [UIImage imageWithCGImage:[defaultRep fullScreenImage] scale:[defaultRep scale] orientation:0];
        }
        else
        {
            [cameraCtrlObj cameraModeChangeToVideo:NO]; //Camcoder to Still image
            
            if ([[NSFileManager defaultManager] fileExistsAtPath:videoPath])
            {
                //Get thumbnail
                
                AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:[NSURL fileURLWithPath:videoPath isDirectory:NO] options:nil];
                
                
                
                AVAssetImageGenerator *generate = [[AVAssetImageGenerator alloc] initWithAsset:asset];
                
                NSError *err = NULL;
                
                CMTime time = CMTimeMake(1, 2);
                
                CGImageRef imgRef = [generate copyCGImageAtTime:time actualTime:NULL error:&err];
               
                UIImage *thumbnail = [UIImage imageWithCGImage:imgRef];
                
                
                
                
//                vc.videoUrl = savedVideoUrl;/*[NSURL fileURLWithPath:videoPath isDirectory:NO];*/
//                
//                vc.isGallaryVideo = NO;
                
                sVc.isVideo = YES;
                
//                [self saveFileToDocumentDirectory:@"filterImage.jpg" image:[UIImage imageWithCGImage:[thumbnail CGImage]scale:[thumbnail scale]orientation: UIImageOrientationLeftMirrored] isMirror:YES];
//                
//                vc.captureImg = [UIImage imageWithCGImage:[thumbnail CGImage]
//                                                    scale:[thumbnail scale]
//                                              orientation: UIImageOrientationLeftMirrored];
                
            }
        }
        
        [self.navigationController pushViewController:sVc animated:NO];
    }
    else
    {
        /*UIGraphicsBeginImageContextWithOptions(selctedImgSv.bounds.size,
                                               YES,
                                               selctedImgSv.window.screen.scale);
  
        CGPoint offset = selctedImgSv.contentOffset;
      
        CGContextTranslateCTM(UIGraphicsGetCurrentContext(), -offset.x, -offset.y);
        
        [selctedImgSv.layer renderInContext:UIGraphicsGetCurrentContext()];
       
        UIImage *visibleScrollViewImage = UIGraphicsGetImageFromCurrentImageContext();
       
        UIGraphicsEndImageContext();
        
       // [self saveFileToDocumentDirectory:@"filterImage.jpg" image:visibleScrollViewImage isMirror:NO];
       
        //vc.captureImg = selectedImg.image;
        
        if (ifImageZoom == YES)
        {
            vc.captureImg = visibleScrollViewImage;
        }
        else
        {
            vc.captureImg = selectedImg.image;
        }*/
        
        vc.captureImg = selectedImg.image;
      
        [self.navigationController pushViewController:vc animated:NO];
    }

    
   // vc.hidesBottomBarWhenPushed = YES;
    
    
}


#pragma mark - TableView Delegate


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return albumArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CELL";
   
    AssetGroupTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"AssetGroupTableViewCell" owner:self options:nil] lastObject];
    }
    
    ALAssetsGroup *g = (ALAssetsGroup*)[albumArr objectAtIndex: indexPath.row];
    
     NSInteger gCount = [g numberOfAssets];
    
    cell.posterImg.image = [UIImage imageWithCGImage:[g posterImage]];

    cell.titleLbl.text = [g valueForProperty:ALAssetsGroupPropertyName];
   
    cell.detailLbl.text = [NSString stringWithFormat:@"%ld", gCount];
   
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    nextBtn.hidden = NO;
    
    [UIView animateWithDuration:0.2 animations:
     ^{
         assetGroupView.frame = CGRectMake(0, [[UIScreen mainScreen] bounds].size.height, SCREEN_Width, [[UIScreen mainScreen] bounds].size.height);
     }];
    
    [photolibObj choosePhotoAlbum:(ALAssetsGroup*)[albumArr objectAtIndex: indexPath.row]];
  
    ALAssetsGroup *g = (ALAssetsGroup*)[albumArr objectAtIndex: indexPath.row];
    
    //[titleBtn setTitle:[[g valueForProperty:ALAssetsGroupPropertyName] uppercaseString] forState:UIControlStateNormal];
    
    [titleBtn setTitle:@"Library " forState:UIControlStateNormal];
}


#pragma mark - PhotoLibrary Delagte

- (void)getPhotoAlbum:(NSMutableArray *)groupArr
{
    albumArr = [NSMutableArray new];
    
    albumArr = groupArr;
}


- (void)getImage:(ALAsset *)asset
{
    
    [self setAssetTypeOnScreen:asset];
}


#pragma mark - ScrolViewDelegate

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return selectedImg;
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.tag == 99)
    {
        if (scrollView.contentOffset.x == 0)
        {
            cameraSwapIcon.hidden = NO;
            
            [self hideNextBtn];
            
            [titleBtn setTitle:@"Camera" forState:UIControlStateNormal];
        
            titleBtn.userInteractionEnabled = NO;
            
            
            cameraBtn.selected = YES;
            
            libraryBtn.selected = NO;
            
            
            [selctedImgSv setZoomScale:1.0f];
            
            selctedImgSv.scrollEnabled = NO;
            
            selctedImgSv.delegate = nil;
            
            
            [playerLibObj removeVideoPreviewLayer];
            
            
            [selectedImg removeFromSuperview];
            
            
            [self addCameraViewOnSuperView];
            
            if (cameraCtrlObj.toggleBtn.selected == YES)
            {
                [session removeOutput: MovieFileOutput];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"camcorderToCamera" object:@"1"];
            }
            
            else
            {
                
            }
       }
        
        else if (scrollView.contentOffset.x == SCREEN_Width)
        {
            [self showNextBtn];
            
            cameraSwapIcon.hidden = YES;
            
            [titleBtn setTitle:@"Library" forState:UIControlStateNormal];
            
            titleBtn.userInteractionEnabled = YES;
            
            cameraBtn.selected = NO;
            
            libraryBtn.selected = YES;
            
            selctedImgSv.delegate = self;
            
           
            [self addFirstAssetFromGallaryCollection];
            
            [self removeCameraView];
            
            //[cameraCtrlObj cameraModeChangeToVideo:NO];
        }
    }
    else
    {
        ifImageZoom = YES;
    }
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
   
}


- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
  
}


#pragma mark - AvCaptureFileOutPutDelegate

- (void)captureOutput:(AVCaptureFileOutput *)captureOutput didFinishRecordingToOutputFileAtURL:(NSURL *)outputFileURL fromConnections:(NSArray *)connections error:(NSError *)error
{
    ALAssetsLibrary* library = [[ALAssetsLibrary alloc] init];
   
    [library writeVideoAtPathToSavedPhotosAlbum:outputFileURL
                                completionBlock:^(NSURL *assetURL, NSError *error)
    {
        savedVideoUrl = assetURL;
    }];
    
    
    cameraCtrlObj.isRecording = YES;
    
    isGallaryVideo = NO;
    
    [self showNextBtn];
    
    
    [cameraCtrlObj removePulseAnimation];
    
    [timer invalidate];
    
    videoTime = 30;
    
    [self nextTap];
}

@end
