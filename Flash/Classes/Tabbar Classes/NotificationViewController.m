//
//  NotificationViewController.m
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 1/2/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//



#import "NotificationViewController.h"
#import "YouNotificationView.h"
#import "FollowerNotificationView.h"
#import "ServiceHelper.h"


@interface NotificationViewController ()
{
    YouNotificationView *youNotiView;
    
    FollowerNotificationView *followNotiView;
}
@end


@implementation NotificationViewController


#pragma mark - ViewCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.hidden = YES;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
     [self setUpView];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
}


#pragma mark - Action

- (IBAction)switchTap:(UIButton *)sender
{
    if (sender == youBtn)
    {
        [contentSv setContentOffset:CGPointMake(0, 0) animated:YES];
    }
    
    else
    {
        [contentSv setContentOffset:CGPointMake(self.view.frame.size.width, 0) animated:YES];
    }
}




#pragma mark - Void

- (void) setUpView
{
    contentSv.contentSize = CGSizeMake([[UIScreen mainScreen] bounds].size.width * 2, contentSv.frame.size.height);
    
    
    youNotiView = [[[NSBundle mainBundle] loadNibNamed:@"YouNotificationView" owner:self options:nil] lastObject];
    
    youNotiView.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, contentSv.frame.size.height);
    
    youNotiView.controller = self.navigationController;
    
    [youNotiView reloadTableView];
    
   
    
    followNotiView = [[[NSBundle mainBundle] loadNibNamed:@"FollowerNotificationView" owner:self options:nil] lastObject];
    
    followNotiView.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width, 0, [[UIScreen mainScreen] bounds].size.width, contentSv.frame.size.height);
    
    followNotiView.controller = self.navigationController;
    
    [followNotiView reloadTableView];
   
    
    
    [contentSv addSubview:youNotiView];
    
    [contentSv addSubview:followNotiView];
    
}





#pragma mark - ScrolView Delegate


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
   animateImg.frame = CGRectMake(scrollView.contentOffset.x/2, 0, animateImg.frame.size.width, animateImg.frame.size.height);
    
    if (scrollView.contentOffset.x == 0)
    {
        [youBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [followBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    }
    else if (scrollView.contentOffset.x == [[UIScreen mainScreen] bounds].size.width)
    {
        [followBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [youBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        
      
        [followNotiView getMyFollowingNotifications];
    }
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.x == 0)
    {
        [youBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [followBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];

    }
    
    else
    {
       [followBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
       [youBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    }
}

@end
