//
//  NotificationViewController.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 1/2/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationViewController : UIViewController <UIScrollViewDelegate>
{
    IBOutlet UIScrollView *contentSv;
    
    IBOutlet UIImageView *animateImg;
    
    IBOutlet UIButton *youBtn, *followBtn;
}

- (IBAction)switchTap:(UIButton *)sender;

@end
