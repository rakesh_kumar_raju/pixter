//
//  PhoneNumberViewController.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 1/2/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <DigitsKit/DigitsKit.h>

@interface PhoneNumberViewController : UIViewController <UITextFieldDelegate, UIAlertViewDelegate>
{
    IBOutlet UITextField *phoneTf;
    
    IBOutlet UILabel *countryCodeLbl;
}

- (IBAction)countryCodeTap;

- (IBAction)confirmTap;

- (IBAction)backTap;

- (IBAction)hideKey;

@end
