//
//  MyAccountViewController.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 1/2/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ResponsiveLabel.h"


@interface MyAccountViewController : UIViewController
{
    IBOutlet UIImageView *profileImg;
    
    IBOutlet UITextField *fullNameTf, *emailTf, *userNameTf;
    
    IBOutlet UIView *contentView;
    
    ResponsiveLabel *tosLbl;
    
    IBOutlet UIButton *createAccBtn;
}

@property (nonatomic, retain) NSString *userId;

- (IBAction)createAccountTap;

- (IBAction)backTap;

- (IBAction)tapGestOnUserPic:(UITapGestureRecognizer *)sender;


@end
