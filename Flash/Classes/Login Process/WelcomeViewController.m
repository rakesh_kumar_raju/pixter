//
//  WelcomeViewController.m
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 12/30/16.
//  Copyright © 2016 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import "WelcomeViewController.h"
#import "PhoneNumberViewController.h"
#import "TabbarViewController.h"
#import "MyAccountViewController.h"
#import "TermOfServiceViewController.h"
#import "ServiceHelper.h"

@interface WelcomeViewController ()

@end


@implementation WelcomeViewController


#pragma mark - ViewCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
   
    [[Digits sharedInstance] logOut];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    [self setUpView];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
}


#pragma mark - Void

- (void) setUpView
{
    tosLbl = [[ResponsiveLabel alloc] initWithFrame:CGRectMake(0, 15, [[UIScreen mainScreen] bounds].size.width, 20)];
 
    
    tosLbl.textColor = [UIColor blackColor];
    
    tosLbl.font = [UIFont systemFontOfSize:10];
    
    tosLbl.textAlignment = NSTextAlignmentCenter;
    
    
    [bottomView addSubview:tosLbl];
    
    
    tosLbl.text = @"By signing up you agree to our ToS & Privacy Policy";
    
    PatternTapResponder TOStapResponder = ^(NSString *string)
    {
        TermOfServiceViewController *vc = [[TermOfServiceViewController alloc] initWithNibName:@"TermOfServiceViewController" bundle:nil];
        
        vc.isTermOfService = YES;
        
        [self presentViewController:vc animated:YES completion:nil];
    };
    
    [tosLbl enableStringDetection:@"ToS" withAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName : [UIFont boldSystemFontOfSize:12], RLTapResponderAttributeName: TOStapResponder}];
    
    
    
    PatternTapResponder PrivacyResponder = ^(NSString *string)
    {
        TermOfServiceViewController *vc = [[TermOfServiceViewController alloc] initWithNibName:@"TermOfServiceViewController" bundle:nil];
        
        vc.isTermOfService = NO;
        
        [self presentViewController:vc animated:YES completion:nil];
    };
    
    [tosLbl enableStringDetection:@"Privacy Policy" withAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName : [UIFont boldSystemFontOfSize:12], RLTapResponderAttributeName: PrivacyResponder}];
    
    
    
    
   /* DGTAuthenticateButton *authButton;
   
    authButton = [DGTAuthenticateButton buttonWithAuthenticationCompletion:^(DGTSession *session, NSError *error) {
        if (session.userID)
        {
            // TODO: associate the session userID with your user model
            NSString *msg = [NSString stringWithFormat:@"Phone number: %@", session.phoneNumber];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"You are logged in!"
                                                            message:msg
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
        else if (error)
        {
            NSLog(@"Authentication error: %@", error.localizedDescription);
        }
    }];
  
    authButton.frame = phoneBtn.frame;
    
    authButton.layer.cornerRadius = 24.0;
    
    authButton.digitsAppearance = [self makeTheme];

    
    //[contentView addSubview:authButton];*/
}



#pragma mark - Action

- (IBAction)phoneTap
{

    Digits *digits = [Digits sharedInstance];
  
    DGTAuthenticationConfiguration *configuration = [[DGTAuthenticationConfiguration alloc] initWithAccountFields:DGTAccountFieldsDefaultOptionMask];
   
    configuration.appearance = [self makeTheme];
    
    [digits authenticateWithViewController:self configuration:configuration completion:^(DGTSession *session, NSError *error)
     {
         NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                 
                                 session.phoneNumber, @"phonenumber",
                                 [[NSUserDefaults standardUserDefaults] objectForKey:@"device_token"], @"device_token", nil];
         
         [[ServiceHelper sharedManager] setHudShow:YES];
         
         [[ServiceHelper sharedManager] sendRequestWithMethodName:@"signup" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
          {
              if (result)
              {
                  if ([[result objectForKey:@"status"] intValue] == 1)
                  {
                      if ([[result objectForKey:@"alreadyregistered"] intValue] == 1)
                      {
                          [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isUserLogin"];
                          
                          [[NSUserDefaults standardUserDefaults] synchronize];
                          
                          
                          [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:[result objectForKey:@"detail"]] forKey:@"login_dict"];
                          
                          [[NSUserDefaults standardUserDefaults] synchronize];
                          
                          
                           (APPDEL).userDetailDict = [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"login_dict"]];
                          
                          TabbarViewController *vc = [[TabbarViewController alloc] initWithNibName:@"TabbarViewController" bundle:nil];
                          
                          [self.navigationController pushViewController:vc animated:YES];
                      }
                      else
                      {
                          MyAccountViewController *vc = [[MyAccountViewController alloc] initWithNibName:@"MyAccountViewController" bundle:nil];
                          
                          vc.userId = [[result objectForKey:@"detail"] objectForKey:@"userId"];
                          
                          [self.navigationController pushViewController:vc animated:YES];
                      }
                  }
              }
              
          }];

         
    }];

//    PhoneNumberViewController *vc = [[PhoneNumberViewController alloc] initWithNibName:@"PhoneNumberViewController" bundle:nil];
//    
//    [self.navigationController pushViewController:vc animated:YES];
}


- (DGTAppearance *)makeTheme
{
    DGTAppearance *theme = [[DGTAppearance alloc] init];
  
    theme.bodyFont = [UIFont fontWithName:@"MuseoSansCyrl-500" size:16];
    
    theme.labelFont = [UIFont fontWithName:@"MuseoSansCyrl-500" size:17];
    
    theme.accentColor = [UIColor blackColor];
    
    theme.backgroundColor = [UIColor whiteColor];
   
    //theme.logoImage = [UIImage imageNamed:@"h_tab"];
    
    return theme;
}

- (IBAction)termsOfService;
{
   TermOfServiceViewController *vc = [[TermOfServiceViewController alloc] initWithNibName:@"TermOfServiceViewController" bundle:nil];
    
    vc.isTermOfService = YES;
    
    [self presentViewController:vc animated:YES completion:nil];
    
}

- (IBAction)privacyPolicy
{
    TermOfServiceViewController *vc = [[TermOfServiceViewController alloc] initWithNibName:@"TermOfServiceViewController" bundle:nil];
    
    vc.isTermOfService = NO;
    
    [self presentViewController:vc animated:YES completion:nil];
}


@end
