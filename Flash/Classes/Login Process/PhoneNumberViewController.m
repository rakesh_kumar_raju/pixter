//
//  PhoneNumberViewController.m
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 1/2/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import "PhoneNumberViewController.h"
#import "OTPViewController.h"
#import "CountryViewController.h"

#import "ServiceHelper.h"


@interface PhoneNumberViewController () <CountryViewControllerDelegate>
{
    NSDictionary *codeDict;
}

@end



@implementation PhoneNumberViewController



#pragma mark - ViewCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    codeDict = [NSDictionary new];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)])
    {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
     [self setUpView];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
}


#pragma mark - Void

- (void) setUpView
{
    CALayer *bottomBorder = [CALayer layer];
    
    bottomBorder.frame = CGRectMake(0, phoneTf.frame.size.height - 8, phoneTf.frame.size.width-10, 1.0f);
    
    bottomBorder.backgroundColor = [UIColor colorWithRed:184.0/255.0 green:184.0/255.0 blue:190.0/255.0 alpha:1.0].CGColor;
    
    [phoneTf.layer addSublayer: bottomBorder];
    
    }


#pragma mark - WebService

- (void)sendUserPhoneNumberWebService
{
    NSString *code = @"";
    
    if ([codeDict objectForKey:@"dial_code"])
    {
        code = [codeDict objectForKey:@"dial_code"];
    }
    
    else
    {
        code = @"+1";
    }
  
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            phoneTf.text, @"phonenumber",
                            code, @"countrycode", nil];
    
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"signup" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 [[[UIAlertView alloc] initWithTitle:@"Success" message:[result objectForKey:@"message"]  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
                 
               
                 OTPViewController *vc = [[OTPViewController alloc] initWithNibName:@"OTPViewController" bundle:nil];
                 
                 vc.phoneStr = phoneTf.text;
                 
                 vc.codeStr = code;
                 
                 vc.userIdStr = [[result objectForKey:@"detail"] valueForKey:@"userId"];
                 
                 [self.navigationController pushViewController:vc animated:YES];
             }
         }
         
    }];
}



#pragma mark - Action

- (IBAction)countryCodeTap
{
    CountryViewController *vc = [[CountryViewController alloc] initWithNibName:@"CountryViewController" bundle:nil];
    
    vc.delegate = self;
    
    [self presentViewController:vc animated:YES completion:nil];
}


- (IBAction)confirmTap
{
    [self.view endEditing:YES];
    
    if (!phoneTf.text.length)
    {
        [[[UIAlertView alloc] initWithTitle:@"Error!" message:[NSString emptyPhoneNmberField] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        
        return;
    }
   
    
    [self sendUserPhoneNumberWebService];
}


- (IBAction)backTap
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)hideKey
{
    [self.view endEditing:YES];
}


#pragma mark - CountryViewControllerDelgate

- (void)didSelectCountry:(NSDictionary *)country
{
    if (country)
    {
        countryCodeLbl.text = [NSString stringWithFormat:@"%@ (%@)",[country objectForKey:@"name"], [country objectForKey:@"dial_code"]];
    }
    
    codeDict = country;
    
    NSLog(@"%@", country);
}


#pragma mark - TextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    
    return YES;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    int length = (int)textField.text.length;
    
    if(length == 10)
    {
        if(range.length == 0)
        {
            return NO;
        }
    }
    
    return YES;
}


#pragma mark - AlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == alertView.cancelButtonIndex)
    {
        [phoneTf becomeFirstResponder];
    }
}

@end

