//
//  MyAccountViewController.m
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 1/2/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#define ACCEPTABLE_CHARACTERS @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_."


#import "MyAccountViewController.h"
#import "TabbarViewController.h"
#import "TermOfServiceViewController.h"

#import "ServiceHelper.h"


@interface MyAccountViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate, UITextFieldDelegate, UIAlertViewDelegate>
{
    NSString *base64ImgStr;
}

@end


@implementation MyAccountViewController


#pragma mark - ViewCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    base64ImgStr = @"";
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)])
    {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [self setUpView];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    
    /*tosLbl = [[ResponsiveLabel alloc] initWithFrame:CGRectMake(0, userNameTf.frame.origin.y + userNameTf.frame.size.height+30 , [[UIScreen mainScreen] bounds].size.width, 20)];
    
    
    tosLbl.textColor = [UIColor whiteColor];
    
    tosLbl.textAlignment = NSTextAlignmentCenter;
    
    tosLbl.font = [UIFont systemFontOfSize:10];
    
   
    tosLbl.text = @"By signing up you agree to our ToS and Privacy Policy";
    
    
    [contentView addSubview:tosLbl];
    
    PatternTapResponder TOStapResponder = ^(NSString *string)
    {
        TermOfServiceViewController *vc = [[TermOfServiceViewController alloc] initWithNibName:@"TermOfServiceViewController" bundle:nil];
        
        vc.isTermOfService = YES;
        
        [self presentViewController:vc animated:YES completion:nil];
    };
    
    [tosLbl enableStringDetection:@"ToS" withAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName : [UIFont boldSystemFontOfSize:12], RLTapResponderAttributeName: TOStapResponder}];
    
    
    
    PatternTapResponder PrivacyResponder = ^(NSString *string)
    {
        TermOfServiceViewController *vc = [[TermOfServiceViewController alloc] initWithNibName:@"TermOfServiceViewController" bundle:nil];
        
        vc.isTermOfService = NO;
        
        [self presentViewController:vc animated:YES completion:nil];
    };
    
    [tosLbl enableStringDetection:@"Privacy Policy" withAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName : [UIFont boldSystemFontOfSize:12], RLTapResponderAttributeName: PrivacyResponder}];*/
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
}


#pragma mark - Void

- (void) setUpView
{
     [self addBaseLineBelowTextField:@"Full Name" textfield:fullNameTf leftViewImageName:@"edit_name"];
    
     [self addBaseLineBelowTextField:@"Email" textfield:emailTf leftViewImageName:@"edit_email"];
    
     [self addBaseLineBelowTextField:@"Username" textfield:userNameTf leftViewImageName:@"edit_username"];
    
    
    createAccBtn.layer.cornerRadius = 25.0;
    
    createAccBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    
    createAccBtn.layer.borderWidth = 1.0;

}


- (void)addBaseLineBelowTextField :(NSString *)placeholder textfield:(UITextField *)textfield leftViewImageName:(NSString *)name
{
    UIColor *color = [UIColor lightGrayColor];
    
    textfield.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeholder attributes:@{NSForegroundColorAttributeName: color, NSFontAttributeName: [UIFont boldSystemFontOfSize:17]}];
    
    CALayer *bottomBorder = [CALayer layer];
    
    bottomBorder.frame = CGRectMake(30.0f, textfield.frame.size.height - 1, [[UIScreen mainScreen] bounds].size.width -10, 1.0f);
    
    bottomBorder.backgroundColor = [UIColor whiteColor].CGColor;
    
    [textfield.layer addSublayer: bottomBorder];
    
    
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    
    UIImageView *fillImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    
    fillImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    fillImageView.image = [UIImage imageNamed:name];
    
    
    [leftView addSubview: fillImageView];
    
    
    textfield.leftViewMode = UITextFieldViewModeAlways;
    
    textfield.leftView = leftView;
}



#pragma mark - myAccountWebservice

- (void)myAccWebService
{
    if(fullNameTf.text.length == 0 || emailTf.text.length == 0 || userNameTf.text.length == 0)
    {
        [[[UIAlertView alloc] initWithTitle:@"Error!" message:[NSString allEmptyTxtFields] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        
        return;
    }
    else if(![NSString stringIsValidEmail:emailTf.text])
    {
        [[[UIAlertView alloc] initWithTitle:nil message:[NSString emailFormatNotCorrect] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        
        return;
    }
    else
    {
        [self.view endEditing:YES];
        
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                self.userId, @"userId",
                                fullNameTf.text, @"fullName",
                                emailTf.text, @"emailId",
                                userNameTf.text, @"userName",
                                [base64ImgStr stringByReplacingOccurrencesOfString:@"\n" withString:@""], @"profilePic",nil];
        
        
        [[ServiceHelper sharedManager] setHudShow:YES];
        
        [[ServiceHelper sharedManager] sendRequestWithMethodName:@"completeProfile" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
         {
             
             if (result)
             {
                 if ([[result objectForKey:@"status"] intValue] == 1)
                 {
                     [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isUserLogin"];
                     
                     [[NSUserDefaults standardUserDefaults] synchronize];
                     
                     
                     [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:[result objectForKey:@"detail"]] forKey:@"login_dict"];
                     
                     [[NSUserDefaults standardUserDefaults] synchronize];
                     
                     
                     (APPDEL).userDetailDict = [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"login_dict"]];
                     
                     
                     TabbarViewController *vc = [[TabbarViewController alloc] initWithNibName:@"TabbarViewController" bundle:nil];
                     
                     [self.navigationController pushViewController:vc animated:YES];
                 }
                 else
                 {
                     [[[UIAlertView alloc] initWithTitle:@"Error!" message:[result objectForKey:@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
                 }
             }
         }];
        
    }
}



#pragma mark - Reduce Image quality

- (UIImage *)resizeImage:(UIImage *)image
{
    float actualHeight = image.size.height;
    
    float actualWidth = image.size.width;
    
    float maxHeight = 300.0;
    
    float maxWidth = 400.0;
    
    float imgRatio = actualWidth/actualHeight;
    
    float maxRatio = maxWidth/maxHeight;
    
    float compressionQuality = 0.5;//50 percent compression
    
    
    if (actualHeight > maxHeight || actualWidth > maxWidth)
    {
        if(imgRatio < maxRatio)
        {
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            
            actualWidth = imgRatio * actualWidth;
            
            actualHeight = maxHeight;
        }
        
        else if(imgRatio > maxRatio)
        {
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            
            actualHeight = imgRatio * actualHeight;
            
            actualWidth = maxWidth;
        }
        
        else
        {
            actualHeight = maxHeight;
            
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    
    
    UIGraphicsBeginImageContext(rect.size);
    
    
    [image drawInRect:rect];
    
    
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    
    
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    
    
    UIGraphicsEndImageContext();
    
    
    return [UIImage imageWithData:imageData];
}

#pragma mark - Camera

- (void)takePhotoFromCamera
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        
        picker.delegate = self;
        
        picker.allowsEditing = YES;
        
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        
        [self presentViewController:picker animated:YES completion:NULL];
    }
}


- (void)takePotoFromGallary
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    
    picker.delegate = self;
    
    picker.allowsEditing = YES;
    
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
}


#pragma mark - Action

- (IBAction)createAccountTap
{
    
    [self myAccWebService];
}


- (IBAction)backTap
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)tapGestOnUserPic:(UITapGestureRecognizer *)sender
{
     [[[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take photo", @"Choose existing photo", nil] showInView:self.view];
}


#pragma mark - ActionSheet

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0)
    {
        [self takePhotoFromCamera];
    }
    
    else if(buttonIndex == 1)
    {
        [self takePotoFromGallary];
    }
}



#pragma mark - ImgaePicker Delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    
    
    profileImg.image = [self resizeImage:chosenImage];
    
    
    NSData *data = UIImagePNGRepresentation(profileImg.image);
    
    
    base64ImgStr = [data base64EncodedStringWithOptions:
                              NSDataBase64Encoding64CharacterLineLength];
    
   
    [picker dismissViewControllerAnimated:YES completion:NULL];
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}


#pragma mark - AlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == alertView.cancelButtonIndex)
    {
        [fullNameTf becomeFirstResponder];
    }
}


#pragma mark - Textfield


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == userNameTf)
    {
        [self myAccWebService];
    }
    
    return YES;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == userNameTf)
    {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        if ([string isEqualToString:filtered])
        {
            if(range.length + range.location > textField.text.length)
            {
                return NO;
            }
            
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            
            return newLength <= 30;
        }
        else
        {
            return NO;
        }
    }
    else
    {
        return YES;
    }
}


@end

