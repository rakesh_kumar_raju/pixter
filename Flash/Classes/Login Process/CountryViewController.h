//
//  CountryViewController.h
//  FastFriends
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 11/8/16.
//  Copyright © 2016 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CountryViewControllerDelegate <NSObject>

- (void)didSelectCountry:(NSDictionary *)country;

@end


@interface CountryViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UITableView *countryTbl;
}

- (IBAction)doneTap:(id)sender;


@property (nonatomic, assign) id<CountryViewControllerDelegate>delegate;


@end
