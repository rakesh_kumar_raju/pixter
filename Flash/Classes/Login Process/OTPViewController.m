//
//  OTPViewController.m
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 1/2/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//


#import "OTPViewController.h"
#import "MyAccountViewController.h"
#import "TabbarViewController.h"
#import "ServiceHelper.h"


@interface OTPViewController ()

@end


@implementation OTPViewController



#pragma mark - ViewCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    [self setUpView];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
}


#pragma mark - Void

- (void) setUpView
{
    phoneLbl.text = [NSString stringWithFormat:@"%@ %@", self.codeStr, self.phoneStr];
    
    for (UIView *view in self.view.subviews)
    {
        if ([view isKindOfClass:[UITextField class]])
        {
            CALayer *bottomBorder = [CALayer layer];
            
            bottomBorder.frame = CGRectMake(5.0f, view.frame.size.height - 8, view.frame.size.width-10, 2.0f);
            
            bottomBorder.backgroundColor = [UIColor colorWithRed:184.0/255.0 green:184.0/255.0 blue:190.0/255.0 alpha:1.0].CGColor;
            
            [view.layer addSublayer: bottomBorder];
        }
    }
}


- (IBAction)tfDidChange:(UITextField *)tf
{
    //Add
    if (tf == tf1 && tf1.text.length > 0)
    {
        [tf2 becomeFirstResponder];
    }
    
    if (tf == tf2 && tf2.text.length > 0)
    {
        [tf3 becomeFirstResponder];
    }
    
    if (tf == tf3 && tf3.text.length > 0)
    {
        [tf4 becomeFirstResponder];
    }
    
    if (tf == tf4 && tf4.text.length > 0)
    {
        [tf5 becomeFirstResponder];
    }
    
    if (tf == tf5 && tf5.text.length > 0)
    {
        [tf6 becomeFirstResponder];
    }
    
    if (tf == tf6 && tf6.text.length > 0)
    {
        [tf6 resignFirstResponder];
    }
    
    
    //Delete
    
    if (tf == tf1 && tf1.text.length == 0)
    {
        [tf1 resignFirstResponder];
    }
    
    if (tf == tf2 && tf2.text.length == 0)
    {
        [tf1 becomeFirstResponder];
    }
    
    if (tf == tf3 && tf3.text.length == 0)
    {
        [tf2 becomeFirstResponder];
    }
    
    if (tf == tf4 && tf4.text.length == 0)
    {
        [tf3 becomeFirstResponder];
    }
    
    if (tf == tf5 && tf5.text.length == 0)
    {
        [tf4 becomeFirstResponder];
    }
    
    if (tf == tf6 && tf6.text.length == 0)
    {
        [tf5 becomeFirstResponder];
    }
    
    
    if (tf1.text.length && tf2.text.length && tf3.text.length && tf4.text.length && tf5.text.length && tf6.text.length)
    {
        [self sendOTPWebService];
    }
}


#pragma mark - WebService

- (void)sendOTPWebService
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            [NSString stringWithFormat:@"%@%@%@%@%@%@", tf1.text, tf2.text, tf3.text, tf4.text, tf5.text, tf6.text], @"otp",
                            self.userIdStr, @"userId", nil];
    
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"confirmOtp" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 if ([[result objectForKey:@"alreadyregistered"] intValue] == 1)
                 {
                     [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isUserLogin"];
                     
                     [[NSUserDefaults standardUserDefaults] synchronize];
                     
                     
                     [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:[result objectForKey:@"detail"]] forKey:@"login_dict"];
                     
                     [[NSUserDefaults standardUserDefaults] synchronize];
                     
                     
                       (APPDEL).userDetailDict = [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"login_dict"]];
                     
                     
                     TabbarViewController *vc = [[TabbarViewController alloc] initWithNibName:@"TabbarViewController" bundle:nil];
                     
                     [self.navigationController pushViewController:vc animated:YES];
                 }
                 else
                 {
                     MyAccountViewController *vc = [[MyAccountViewController alloc] initWithNibName:@"MyAccountViewController" bundle:nil];
                     
                     vc.userId = self.userIdStr;
                     
                     [self.navigationController pushViewController:vc animated:YES];
                 }
             }
             else
             {
                 [[[UIAlertView alloc] initWithTitle:[result objectForKey:@"message"] message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
             }
         }
         
     }];
}




#pragma mark - Action

- (IBAction)backTap
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)submitCodeTap
{
    if (tf1.text.length == 0 || tf2.text.length == 0 || tf3.text.length == 0 || tf4.text.length == 0 || tf5.text.length == 0 || tf6.text.length == 0)
    {
        [[[UIAlertView alloc] initWithTitle:@"Error!" message:[NSString emptyOTPField] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        
        return;
    }
  
    
    [self sendOTPWebService];
}


- (IBAction)hideKey
{
    [self.view endEditing:YES];
}


#pragma mark - TextField Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ((textField.text.length >= 1) && (string.length > 0))
    {
        return NO;
    }
    
    return YES;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == tf1 && tf1.text.length > 0)
    {
        [tf2 becomeFirstResponder];
    }
    
    if (textField == tf2 && tf2.text.length > 0)
    {
        [tf3 becomeFirstResponder];
    }
    
    if (textField == tf3 && tf3.text.length > 0)
    {
        [tf4 becomeFirstResponder];
    }
    
    if (textField == tf4 && tf4.text.length > 0)
    {
        [tf5 becomeFirstResponder];
    }
    
    if (textField == tf5 && tf5.text.length > 0)
    {
        [tf6 becomeFirstResponder];
    }
    
    if (textField == tf6 && tf6.text.length > 0)
    {
        [tf6 resignFirstResponder];
    }

    return YES;
}


#pragma mark - AlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == alertView.cancelButtonIndex)
    {
        [tf1 becomeFirstResponder];
    }
}


@end
