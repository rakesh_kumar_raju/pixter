//
//  OTPViewController.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 1/2/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"


@interface OTPViewController : UIViewController <UITextFieldDelegate, UIAlertViewDelegate>
{
    IBOutlet UITextField *tf1, *tf2, *tf3, *tf4, *tf5, *tf6;
    
    IBOutlet UILabel *phoneLbl;
}

@property (nonatomic, retain) NSString *codeStr, *phoneStr, *userIdStr;


- (IBAction)backTap;

- (IBAction)submitCodeTap;

- (IBAction)tfDidChange:(UITextField *)tf;

- (IBAction)hideKey;

@end
