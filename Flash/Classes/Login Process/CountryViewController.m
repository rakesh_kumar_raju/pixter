//
//  CountryViewController.m
//  FastFriends
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 11/8/16.
//  Copyright © 2016 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import "CountryViewController.h"
#import "CountryCell.h"


@interface CountryViewController ()
{
    NSArray *countryArr, *sectionTitlesArr;
    
    NSMutableDictionary *countryDict;
    
    NSDictionary *selectDict;
}

@end


@implementation CountryViewController


#pragma mark - VieCycle



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupView];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
}


#pragma mark - Void

- (void)setupView
{
    countryDict = [NSMutableDictionary new];
    
    
    self.navigationController.navigationBar.hidden = YES;
    
    NSData *data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"countries" ofType:@"json"]];
    
    NSError *localError = nil;
   
    NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
    
    if (localError != nil)
    {
        NSLog(@"%@", [localError userInfo]);
    }
    
    
    NSSortDescriptor * brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
   
    NSArray * sortDescriptors = [NSArray arrayWithObject:brandDescriptor];
    
    NSArray * sortedArray = [(NSArray *)parsedObject sortedArrayUsingDescriptors:sortDescriptors];
    
    countryArr = sortedArray;
    
    
    NSMutableArray *alphaArr = [NSMutableArray new];
    
    for (char a = 'A'; a <= 'Z'; a++)
    {
        [alphaArr addObject:[NSString stringWithFormat:@"%c", a]];
    }
    
    
    for (NSString *chr in alphaArr)
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name beginswith[c] %@",chr];
        
        NSArray *filteredArray = [[NSMutableArray alloc] initWithArray: [[countryArr filteredArrayUsingPredicate:predicate] copy]];
        
        if (filteredArray.count)
        {
            [countryDict setObject:filteredArray forKey:chr];
        }
        
    }
    
     sectionTitlesArr = [[countryDict allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    NSLog(@"%@", [countryDict valueForKey:@"v"]);
}


#pragma mark - UITableView Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [countryDict allKeys].count;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [sectionTitlesArr objectAtIndex:section];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *sectionTitle = [sectionTitlesArr objectAtIndex:section];
    
    NSArray *sectionAnimals = [countryDict objectForKey:sectionTitle];
    
    return [sectionAnimals count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    
    CountryCell *cell = (CountryCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(cell == nil)
    {
        cell = [[CountryCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
    }
    
    NSString *sectionTitle = [sectionTitlesArr objectAtIndex:indexPath.section];
   
    NSArray *sectionAnimals = [countryDict objectForKey:sectionTitle];
    
    cell.textLabel.text = [[sectionAnimals objectAtIndex:indexPath.row] valueForKey:@"name"];
   
    cell.detailTextLabel.text = [[sectionAnimals objectAtIndex:indexPath.row] valueForKey:@"dial_code"];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *sectionTitle = [sectionTitlesArr objectAtIndex:indexPath.section];
    
    NSArray *sectionAnimals = [countryDict objectForKey:sectionTitle];
    
    selectDict = [sectionAnimals objectAtIndex:indexPath.row];
}


- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return sectionTitlesArr;
}


- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    return [sectionTitlesArr indexOfObject:title];
}


#pragma mark Actions

- (IBAction)doneTap:(id)sender
{
    if ([_delegate respondsToSelector:@selector(didSelectCountry:)])
    {
       
        [self.delegate didSelectCountry:selectDict];
        
        [self dismissViewControllerAnimated:YES completion:NULL];
    }
    else
    {
        NSLog(@"CountryListView Delegate : didSelectCountry not implemented");
    }
}

@end
