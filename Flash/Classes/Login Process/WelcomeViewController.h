//
//  WelcomeViewController.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 12/30/16.
//  Copyright © 2016 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <DigitsKit/DigitsKit.h>

#import "ResponsiveLabel.h"


@interface WelcomeViewController : UIViewController
{
    IBOutlet UIButton *phoneBtn;
    
    IBOutlet UIView *contentView, *bottomView;
    
    ResponsiveLabel *tosLbl;
}

- (IBAction)phoneTap;

- (IBAction)termsOfService;

- (IBAction)privacyPolicy;

@end
