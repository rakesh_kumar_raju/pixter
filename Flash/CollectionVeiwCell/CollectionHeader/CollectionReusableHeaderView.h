//
//  CollectionReusableHeaderView.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 2/16/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionReusableHeaderView : UICollectionReusableView


@property (nonatomic, retain) IBOutlet UIImageView *profileImg;

@property (nonatomic, retain) IBOutlet UILabel *totalPostLbl, *followerLbl, *followingLbl, *nameLbl;

@property (nonatomic, retain) IBOutlet UIButton *gridBtn, *listBtn, *locationBtn, *taggedBtn;

@property (nonatomic, retain) IBOutlet IBOutlet UITextView *bioTv;

@end
