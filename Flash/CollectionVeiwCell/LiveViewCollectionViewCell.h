//
//  LiveViewCollectionViewCell.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 1/3/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LiveViewCollectionViewCell : UICollectionViewCell
{
    
}

@property (nonatomic, retain) IBOutlet UIImageView *userImg;

@property (nonatomic, retain) IBOutlet UILabel *titleLbl;
@end
