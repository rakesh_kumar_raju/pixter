//
//  PhotoLibraryCollectionViewCell.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 1/25/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoLibraryCollectionViewCell : UICollectionViewCell

@property (nonatomic, retain) IBOutlet UIImageView *libImg;

@property (nonatomic, retain) IBOutlet UILabel *durationLbl;

@property (nonatomic, retain) IBOutlet UIImageView *hideImgV;

@end
