//
//  ListCollectionViewVideoCell.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 2/16/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AvplayerLibrary.h"

#import "ZOWVideoView.h"
#import "InstagramVideoView.h"
#import "ResponsiveLabel.h"


@interface ListCollectionViewVideoCell : UICollectionViewCell

@property (nonatomic, retain) IBOutlet UIImageView *profileImg, *totalLikeImg;

@property (nonatomic, retain) IBOutlet UILabel *locationLbl, *userNameLbl, *totalLikesLbl, *timeLbl;

@property (nonatomic, retain) IBOutlet ResponsiveLabel *captionLbl;

@property (nonatomic, retain) IBOutlet UIButton *likeBtn, *commentBtn, *favBtn, *shareBtn, *getTotlLike, *followBtn, *dotBtn, *userImageTap;

@property (nonatomic, retain) IBOutlet AvplayerLibrary *player;

@property (nonatomic, strong) InstagramVideoView *videoView;

@end
