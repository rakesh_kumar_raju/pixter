//
//  ListCollectionImageCell.m
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 4/17/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import "ListCollectionImageCell.h"

@implementation ListCollectionImageCell

@synthesize profileImage, nameLbl, locationLbl, followBtn, mediaImage;


- (id)initWithFrame:(CGRect)aRect
{
    if (self = [super initWithFrame:aRect])
    {
        //Profile image
        
        profileImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 50, 50)];
        
        profileImage.layer.cornerRadius = 25.0;
        
        profileImage.clipsToBounds = YES;
        
     
        [self addSubview:profileImage];
        
        
        
        // Name;
        
        nameLbl = [[UILabel alloc] initWithFrame:CGRectMake(profileImage.frame.origin.x + profileImage.frame.size.width+5, 20, 170, 20)];
        
        nameLbl.font = [UIFont fontWithName:@"MuseoSansCyrl-700" size:13];
        
        nameLbl.textColor = [UIColor blackColor];
        
        [self addSubview:nameLbl];
        
        
        
        //location
        
        locationLbl = [[UILabel alloc] initWithFrame:CGRectMake(profileImage.frame.origin.x + profileImage.frame.size.width+5, nameLbl.frame.origin.y + nameLbl.frame.size.height, 170, 20)];
        
        locationLbl.font = [UIFont fontWithName:@"MuseoSansCyrl-500" size:13];
        
        locationLbl.textColor = [UIColor blackColor];
        
       
        [self addSubview:locationLbl];
        
        
        //Folow button
        
        followBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [followBtn setTitle:@"Follow" forState:UIControlStateNormal];
        
        followBtn.titleLabel.font = [UIFont fontWithName:@"MuseoSansCyrl-500" size:12];
        
        followBtn.frame = CGRectMake(([[UIScreen mainScreen] bounds].size.width - 65) -15, 23, 65, 25);
        
        followBtn.layer.cornerRadius = 2.0;
        
        followBtn.layer.borderWidth = 1.0;
        
        followBtn.layer.borderColor = [UIColor blackColor].CGColor;
        
        
        [self addSubview:followBtn];
        
        
      
        //Media
        
        mediaImage = [[UIImageView alloc] init];
        
        mediaImage.contentMode = UIViewContentModeScaleAspectFill;
        
        mediaImage.clipsToBounds = YES;
        
       
        UIImage *testImage = [[UIImage alloc] init];
        
        testImage = mediaImage.image;

        
        UIImage *image = [self imageWithImage: testImage scaledToWidth:[[UIScreen mainScreen] bounds].size.width];
        
        mediaImage.frame = CGRectMake(0, locationLbl.frame.origin.y + locationLbl.frame.size.height+5, image.size.width, image.size.height);
        
        [self addSubview: mediaImage];
    }
   
    return self;
}




#pragma mark - Get image size


- (UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width
{
    float oldWidth = sourceImage.size.width;
    
    float scaleFactor = i_width / oldWidth;
    
    
    float newHeight = sourceImage.size.height * scaleFactor;
    
    float newWidth = oldWidth * scaleFactor;
    
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return newImage;
}




- (void)awakeFromNib
{
    [super awakeFromNib];
    
}

@end
