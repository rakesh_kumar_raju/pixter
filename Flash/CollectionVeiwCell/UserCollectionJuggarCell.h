//
//  UserCollectionJuggarCell.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 4/21/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserCollectionJuggarCell : UICollectionViewCell

@property (nonatomic, retain) IBOutlet UIImageView *userImg, *videoIconImg;

@end
