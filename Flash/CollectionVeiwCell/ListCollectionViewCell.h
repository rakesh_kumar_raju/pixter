//
//  ListCollectionViewCell.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 1/9/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ResponsiveLabel.h"

@interface ListCollectionViewCell : UICollectionViewCell


@property (nonatomic, retain) IBOutlet UIImageView *userImg, *profileImg, *totalLikeImg;

@property (nonatomic, retain) IBOutlet UILabel *locationLbl, *timeLbl, *userNameLbl, *totalLikesLbl;

@property (nonatomic, retain) IBOutlet ResponsiveLabel *captionLbl;

@property (nonatomic, retain) IBOutlet UIButton *likeBtn, *commentBtn, *favBtn, *shareBtn, *getTotlLike, *followBtn, *dotBtn, *tagBtn, *userImageTap;

@property (nonatomic, retain) IBOutlet NSLayoutConstraint *imageHeight;

@end
