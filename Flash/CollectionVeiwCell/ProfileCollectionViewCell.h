//
//  ProfileCollectionViewCell.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 3/20/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileCollectionViewCell : UICollectionViewCell

@property (nonatomic, retain) IBOutlet UIImageView *profileImg;

@property (nonatomic, retain) IBOutlet UILabel *nameLbl, *totalPostLbl, *followerLbl, *followingLbl;

@property (nonatomic, retain) IBOutlet UILabel *bioLbl;

@property (nonatomic, retain) IBOutlet UIButton *gridBtn, *listBtn, *locationBtn, *taggedBtn, *savedBtn, *editProfileBtn, *switchProfileBtn, *followerBtn, *followingBtn, *totalPostBtn;

@property (nonatomic, retain) IBOutlet UIButton *differentUserGridBtn, *differentUserlistBtn, *differentUsertaggedBtn;


@property (nonatomic, retain) IBOutlet UIView *selfUserView, *differentUserView;

@property (nonatomic, retain) IBOutlet NSLayoutConstraint *editProfileTrailing, *callEmailBtnViewHeight;


@end
