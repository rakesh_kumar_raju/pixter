//
//  ListCollectionImageCell.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 4/17/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListCollectionImageCell : UICollectionViewCell

@property (nonatomic, retain)  UIImageView *profileImage, *mediaImage;

@property (nonatomic, retain) UILabel *nameLbl, *locationLbl;

@property (nonatomic, retain) UIButton *followBtn;

@end
