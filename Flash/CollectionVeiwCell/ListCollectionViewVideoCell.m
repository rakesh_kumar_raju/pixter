//
//  ListCollectionViewVideoCell.m
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 2/16/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import "ListCollectionViewVideoCell.h"

@implementation ListCollectionViewVideoCell

- (void)awakeFromNib
{
    
    _followBtn.layer.cornerRadius = 2.0;
    
    _followBtn.layer.borderWidth = 1.0;
    
    _followBtn.layer.borderColor = [UIColor darkGrayColor].CGColor;
    
    
   
    
    [super awakeFromNib];
}

@end
