//
//  ServiceHelper.h
//  XPRESSOR
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 10/10/16.
//  Copyright © 2016 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//


//@"http://52.43.13.44/flashws/ws/"

// testing http://www.seraphicdemos.com/pixster/flashws/ws/

// Live http://52.43.13.44/flashws/ws/

#define ServiceURL  @"http://52.43.13.44/flashws/ws/"

#import "UIImageView+WebCache.h"



#import <Foundation/Foundation.h>
#import "SVProgressHUD.h"

#import "NSString+JDString.h"


@interface ServiceHelper : NSObject

@property (nonatomic, assign) BOOL isHudShow;


+ (id)sharedManager;


- (void)sendRequestWithMethodName:(NSString *)methodName setHTTPMethod:(NSString *)setHTTPMethod params:(NSDictionary *)params completion:(void (^)(NSDictionary *result))completionBlock;

- (void)showHud;

- (void)dismisHud;

- (void)showHudWithProgress:(CGFloat)value withStatus:(NSString *)status;

- (void)setHudShow:(BOOL)show;

- (void)reloadRewardVideoAlert;

@end
