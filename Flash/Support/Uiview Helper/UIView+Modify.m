//
//  UIView+Modify.m
//  LocallyGreen
//
//  Created by mahesh on 13/05/17.
//  Copyright (c) 2017. All rights reserved.
//

#import "UIView+Modify.h"

@implementation UIView (Modify)


@dynamic borderColor,borderWidth,cornerRadius,topBorderColor,bottomBorderColor;

-(void)makeBorderWithWidth:(float)width andColor:(UIColor *)color
{
    [self.layer setBorderWidth:width];
    [self.layer setBorderColor:color.CGColor];
}

-(void)addTopBottomBorderWithColor:(UIColor *)color andWidth:(float)width
{
    self.clipsToBounds = YES;
    CALayer *rightBorder = [CALayer layer];
    rightBorder.borderColor = color.CGColor;
    rightBorder.borderWidth = width;
    rightBorder.frame = CGRectMake(-1, 0, CGRectGetWidth(self.frame)+2, CGRectGetHeight(self.frame));
    [self.layer addSublayer:rightBorder];
}

-(void)addBottomBorderWithColor:(UIColor *)color andWidth:(float)width
{
    self.clipsToBounds = YES;
    CALayer *rightBorder = [CALayer layer];
    rightBorder.borderColor = color.CGColor;
    rightBorder.borderWidth = width;
    rightBorder.frame = CGRectMake(-1, -1, CGRectGetWidth(self.frame)+2, CGRectGetHeight(self.frame)+1);
    [self.layer addSublayer:rightBorder];
}

-(void)addTopBorderWithWidth:(float)width andColor:(UIColor *)color
{
    CALayer *border = [CALayer layer];
    border.backgroundColor = color.CGColor;
    border.frame = CGRectMake(0, 0, self.frame.size.width, width);
    [self.layer addSublayer:border];
}

-(void)addBottomBorderWithWidth:(float)width andColor:(UIColor *)color
{
    CALayer *border = [CALayer layer];
    border.backgroundColor = color.CGColor;
    border.frame = CGRectMake(0, self.frame.size.height-width, self.frame.size.width, width);
    [self.layer addSublayer:border];
}

-(void)addLeftPadding:(float)width
{
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, 20)];
    UITextField *view=(UITextField *)self;
    view.leftView = paddingView;
    view.leftViewMode = UITextFieldViewModeAlways;
    
}

-(void)addRightPadding:(float)width
{
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, 20)];
    UITextField *view=(UITextField *)self;
    view.rightView = paddingView;
    view.rightViewMode = UITextFieldViewModeAlways;
    
}

#pragma mark - IBInspectable Items -



-(void)setBorderColor:(UIColor *)borderColor
{
    [self.layer setBorderColor:borderColor.CGColor];
}

-(void)setBorderWidth:(CGFloat)borderWidth
{
    [self.layer setBorderWidth:borderWidth];
}

-(void)setCornerRadius:(CGFloat)cornerRadius
{
    [self.layer setCornerRadius:cornerRadius];
}

-(void)setTopBorderColor:(UIColor *)topBorderColor
{
    [self addTopBorderWithWidth:1.0f andColor:topBorderColor];
}

-(void)setBottomBorderColor:(UIColor *)bottomBorderColor
{
    [self addBottomBorderWithWidth:1.0f andColor:bottomBorderColor];
}

-(void)addLeftSideImageToTextFieldWithImage:(UIImage *)image
{
    UITextField *textfield=(UITextField *)self;
    
    UIImageView *searchIconView = [[UIImageView alloc]initWithImage:image];
    
    [searchIconView setFrame:CGRectMake(0, 0, CGRectGetHeight(textfield.frame), CGRectGetHeight(textfield.frame))];
    
    [searchIconView setContentMode:UIViewContentModeCenter];
    
    [textfield  setLeftView:searchIconView];
    
    [textfield  setLeftViewMode:UITextFieldViewModeAlways];
}

-(void)addRightSideImageToTextFieldWithImage:(UIImage *)image
{
    UITextField *textfield=(UITextField *)self;
    
    UIImageView *searchIconView = [[UIImageView alloc]initWithImage:image];
    
    [searchIconView setFrame:CGRectMake(0, 0, CGRectGetHeight(textfield.frame), CGRectGetHeight(textfield.frame))];
    
    [searchIconView setContentMode:UIViewContentModeCenter];
    
    [textfield  setRightView:searchIconView];
    
    [textfield  setRightViewMode:UITextFieldViewModeAlways];
}
@end
