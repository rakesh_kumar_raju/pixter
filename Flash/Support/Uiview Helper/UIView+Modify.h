//
//  UIView+Modify.h
//  LocallyGreen
//
//  Created by Mahesh on 13/05/17.
//  Copyright (c) 2017. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Modify)

@property (nonatomic) IBInspectable UIColor *borderColor;
@property (nonatomic) IBInspectable UIColor *topBorderColor;
@property (nonatomic) IBInspectable UIColor *bottomBorderColor;

@property (nonatomic) IBInspectable CGFloat borderWidth;
@property (nonatomic) IBInspectable CGFloat cornerRadius;


/**
 Make Border of current view.
 
 @param width The Width of the border.
 @param color The Color of the border.
 
 */
-(void)makeBorderWithWidth:(float)width andColor:(UIColor *)color;

/**
 Add Top Border on current view.
 
 @param width The Width of the border.
 @param color The Color of the border.
 
 */
-(void)addTopBorderWithWidth:(float)width andColor:(UIColor *)color;

/**
 Add Top and bottom Border on current view.
 
 @param width The Width of the border.
 @param color The Color of the border.
 
 */

-(void)addTopBottomBorderWithColor:(UIColor *)color andWidth:(float)width;

/**
 Add bottom Border on current view.
 
 @param width The Width of the border.
 @param color The Color of the border.
 
 */

-(void)addBottomBorderWithColor:(UIColor *)color andWidth:(float)width;

/**
 Add Bottom Border on current view.
 
 @param width The Width of the border.
 @param color The Color of the border.
 
 */
-(void)addBottomBorderWithWidth:(float)width andColor:(UIColor *)color;

/**
 Add Left Padding to UITextField.
 @param: padding width
 */
-(void)addLeftPadding:(float)width;

/**
 Add Right Padding to UITextField.
 @param: padding width
 */
-(void)addRightPadding:(float)width;

/**
 Add Left Image on TextField.
 
 @param image Image to put left side on textfield.
 
 */
-(void)addLeftSideImageToTextFieldWithImage:(UIImage *)image;

/**
 Add Left Image on TextField.
 
 @param image Image to put right side on textfield.
 
 */

-(void)addRightSideImageToTextFieldWithImage:(UIImage *)image;

@end
