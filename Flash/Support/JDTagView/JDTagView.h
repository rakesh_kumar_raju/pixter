//
//  TagView.h
//  NewProject
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 3/30/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JDTagView;


@protocol JDTagViewDelegate <NSObject>

@required


- (void)deleteTagFromView:(JDTagView *)JDTagView;

- (void)selectedTag:(JDTagView *)View;


@end


@interface JDTagView : UIView
{
    UIImageView *backgroundImage;
}

@property (nonatomic, assign) id <JDTagViewDelegate> JDTagViewDelegate;


@property (nonatomic, retain) UIButton *tagAction, *delBtn;


- (void)setUpTagViewForViewWithTagTitle:(NSString *)tagTitle WithViewPoints:(CGPoint)tagPoint;

- (void)setTagActionTitle:(NSString *)str;

@end
