//
//  TagView.m
//  NewProject
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 3/30/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import "JDTagView.h"

@implementation JDTagView


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
   
    if (self)
    {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}



- (void)setUpTagViewForViewWithTagTitle:(NSString *)tagTitle WithViewPoints:(CGPoint)tagPoint
{
    //Background image
    backgroundImage = [[UIImageView alloc] init];
    
    backgroundImage.backgroundColor = [UIColor clearColor];
    
    backgroundImage.image = [UIImage imageNamed:@"callout"];
    
    
    //Title Btn
    self.tagAction = [UIButton buttonWithType:UIButtonTypeCustom];
    
    self.tagAction.titleLabel.font = [UIFont systemFontOfSize:12];
    
    self.tagAction.backgroundColor = [UIColor clearColor];
    
    [self.tagAction setTitle: tagTitle forState:UIControlStateNormal];
    
    [self.tagAction setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    

    [self.tagAction addTarget:self action:@selector(selectedView:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    //Delete Btn
    self.delBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [self.delBtn setImage:[UIImage imageNamed:@"delBtn"] forState:UIControlStateNormal];
    
    [self.delBtn addTarget:self action:@selector(deleteSelf) forControlEvents:UIControlEventTouchUpInside];
    
    self.delBtn.backgroundColor = [UIColor clearColor];
    
   
    
    
    
    CGSize stringsize = [tagTitle  sizeWithAttributes:@{NSFontAttributeName :[UIFont systemFontOfSize:12]}];
    
    
    
    self.frame = CGRectMake(tagPoint.x - ((stringsize.width + 20) /2),tagPoint.y,stringsize.width+20, 40);
    
    
    [backgroundImage setFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    
    
    [self addSubview:backgroundImage];
    
    
    [self.tagAction setFrame:CGRectMake(0,10,stringsize.width+20, 30)];
    
    [self.delBtn setFrame:CGRectMake(self.frame.size.width - 13,5,13, 13)];
    
    
    [self addSubview:self.tagAction];
    
    [self addSubview:self.delBtn];
    
    
    self.transform = CGAffineTransformMakeScale(0.3, 0.3);
    
    
     [UIView animateWithDuration:0.1 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^
     {
         self.transform = CGAffineTransformMakeScale(1,1);
     }
     completion:nil];;
    
}


- (void)deleteSelf
{
    [self.JDTagViewDelegate deleteTagFromView:self];
   
    [self removeFromSuperview];
}


- (void)selectedView:(UIButton *)sender
{
    [self.JDTagViewDelegate selectedTag:(JDTagView *)sender.superview];
}


- (void)setTagActionTitle:(NSString *)str
{
    [self.tagAction setTitle:str forState:UIControlStateNormal];
    
    CGSize stringsize = [str sizeWithAttributes:@{NSFontAttributeName :[UIFont systemFontOfSize:12]}];

    self.frame = CGRectMake(self.frame.origin.x,self.frame.origin.y,stringsize.width+20, 40);
    
    
    [backgroundImage setFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    
    [self.tagAction setFrame:CGRectMake(0,10,stringsize.width+20, 30)];
    
    [self.delBtn setFrame:CGRectMake(self.frame.size.width - 13,5,13, 13)];
}


@end
