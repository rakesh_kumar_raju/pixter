//
//  NSString+JDString.h
//  Round the Corner
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 6/16/16.
//  Copyright © 2016 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (JDString)

@property (nonatomic, readonly) NSString *allEmptyTxtFields, *emailFormatNotCorrect, *notMatchPassword;


+ (NSString *)allEmptyTxtFields;

+ (BOOL)stringIsValidEmail:(NSString *)checkString;

+ (NSString *)emailFormatNotCorrect;

+ (NSString *)notMatchPassword;

+ (NSString *)emptyPhoneNmberField;

+ (NSString *)emptyOTPField;

+ (BOOL)validatePhone:(NSString *)phoneNumber;


@end
