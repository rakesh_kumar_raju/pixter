//
//  NSString+JDString.m
//  Round the Corner
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 6/16/16.
//  Copyright © 2016 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//


#import "NSString+JDString.h"


@implementation NSString (JDString)

@dynamic allEmptyTxtFields, emailFormatNotCorrect, notMatchPassword;



+ (NSString *)allEmptyTxtFields
{
    return @"Kindly fill your required fields.";
}


+ (NSString *)emailFormatNotCorrect
{
    return @"Your email is not correct. Please try again!";
}


+ (NSString *)notMatchPassword
{
    return @"Your passwords don't match. Try again!";
}


+ (NSString *)emptyOTPField
{
    return @"Kindly enter your OTP number.";
}


+ (BOOL) stringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:checkString];
}


+ (BOOL)validatePhone:(NSString *)phoneNumber
{
    NSString *phoneRegex = @"^(\\([0-9]{3})\\) [0-9]{3}-[0-9]{4}$";
 
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    
    return [phoneTest evaluateWithObject:phoneNumber];
}


+ (NSString *)emptyPhoneNmberField
{
     return @"Kindly enter your phone number.";
}

@end
