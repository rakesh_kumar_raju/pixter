//
//  YouNotificationView.m
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 1/4/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import "YouNotificationView.h"
#import "NotificationTableViewCell.h"
#import "ShowPostsViewController.h"
#import "ProfileViewController.h"
#import "ServiceHelper.h"

@implementation YouNotificationView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void) awakeFromNib
{
    myNotificationArr = [NSMutableArray new];

//    if (!mynotificationArr.count)
//    {
//        [self getMyNotofications];
//    }
    [self getMyNotofications];
    
//    youTable.estimatedRowHeight = 70;
//    
//    youTable.rowHeight = UITableViewAutomaticDimension;
    
    [super awakeFromNib];
}


- (void)reloadTableView
{
    [youTable reloadData];
}


#pragma mark - Webservice

- (void)getMyNotofications
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            [(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", nil];
    
    
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"youNotification" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 myNotificationArr = [[result objectForKey:@"notification"] mutableCopy];
                 
                 [self reloadTableView];
             }
         }
     }];
}


#pragma mark - Action

- (void)cancelTap:(UIButton *)sender
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            [(APPDEL).userDetailDict objectForKey:@"userId"], @"userid", [[myNotificationArr objectAtIndex:sender.tag] objectForKey:@"userId"], @"followerid", @"2", @"flag", nil];
    
    
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"responseFollowingRequest" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 [myNotificationArr removeObjectAtIndex:sender.tag];
                 
                 [self reloadTableView];
             }
         }
     }];
}


- (void)acceptTap:(UIButton *)sender
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            [(APPDEL).userDetailDict objectForKey:@"userId"], @"userid", [[myNotificationArr objectAtIndex:sender.tag] objectForKey:@"userId"], @"followerid", @"1", @"flag", nil];
    
    
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"responseFollowingRequest" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 [myNotificationArr removeObjectAtIndex:sender.tag];
                 
                 [self reloadTableView];
             }
         }
     }];
}


- (void)tapOnPostBtnTap:(UIButton *)sender
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            [(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", [[myNotificationArr objectAtIndex:sender.tag] objectForKey:@"postId"], @"postId", nil];
    
    
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"getSinglePost" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 ShowPostsViewController *vc = [[ShowPostsViewController alloc] initWithNibName:@"ShowPostsViewController" bundle:nil];
                 
                 vc.hidesBottomBarWhenPushed = YES;
                 
                 
                 vc.postArr = [NSMutableArray arrayWithObject:[result objectForKey:@"post"]];
                 
                 //vc.indexPath = [NSIndexPath indexPathWithIndex:0];
                 
                 [self.controller pushViewController:vc animated:YES];
             }
         }
     }];
}


- (void)tapOnUserImageTap:(UIButton *)sender
{
    if (![[[myNotificationArr objectAtIndex:sender.tag] objectForKey:@"action"] isEqualToString:@"Double"] &&
        ![[[myNotificationArr objectAtIndex:sender.tag] objectForKey:@"action"] isEqualToString:@"reference"])
    {
        ProfileViewController *vc = [[ProfileViewController alloc] initWithNibName:@"ProfileViewController" bundle:nil];
        
        vc.differentUserId = [[myNotificationArr objectAtIndex: sender.tag] objectForKey:@"userId"] ;
        
        [self.controller pushViewController:vc animated:YES];

    }
}


#pragma mark - TableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return myNotificationArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"CELL";
    
    NotificationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        if ([[[myNotificationArr objectAtIndex:indexPath.row] objectForKey:@"action"] isEqualToString:@"wantfollow"])
        {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"NotificationTableViewCell" owner:self options:nil] objectAtIndex:1];
           
            cell.acceptBtn.tag = indexPath.row;
            
            cell.cancelBtn.tag = indexPath.row;
            
            [cell.userImg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", ServiceURL, [[myNotificationArr objectAtIndex:indexPath.row] objectForKey:@"profile_pic"]] ] placeholderImage:[UIImage imageNamed:@"profile_demo"] options:SDWebImageProgressiveDownload];
            
            
            [cell.acceptBtn addTarget:self action:@selector(acceptTap:) forControlEvents:UIControlEventTouchUpInside];
            
             [cell.cancelBtn addTarget:self action:@selector(cancelTap:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        else
        {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"NotificationTableViewCell" owner:self options:nil] objectAtIndex:0];
            
            [cell.userImg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", ServiceURL, [[myNotificationArr objectAtIndex:indexPath.row] objectForKey:@"profile_pic"]] ] placeholderImage:[UIImage imageNamed:@"profile_demo"] options:SDWebImageProgressiveDownload];
            
            cell.tapOnPost.tag = indexPath.row;
            
            cell.tapOnUserImg.tag = indexPath.row;
            
            
            [cell.tapOnPost addTarget:self action:@selector(tapOnPostBtnTap:) forControlEvents:UIControlEventTouchUpInside];
            
            [cell.tapOnUserImg addTarget:self action:@selector(tapOnUserImageTap:) forControlEvents:UIControlEventTouchUpInside];
            
            
            if ([[[myNotificationArr objectAtIndex:indexPath.row] objectForKey:@"action"] isEqualToString:@"like"] || [[[myNotificationArr objectAtIndex:indexPath.row] objectForKey:@"action"] isEqualToString:@"comment"] || [[[myNotificationArr objectAtIndex:indexPath.row] objectForKey:@"action"] isEqualToString:@"tag"])
            {
                if ([[[myNotificationArr objectAtIndex:indexPath.row] objectForKey:@"type"] isEqualToString:@"image"])
                {
                    [cell.mediaImg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", ServiceURL, [[myNotificationArr objectAtIndex:indexPath.row] objectForKey:@"postimage"]] ] placeholderImage:[UIImage imageNamed:@""] options:SDWebImageProgressiveDownload];
                }
                else
                {
                    [cell.mediaImg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", ServiceURL, [[myNotificationArr objectAtIndex:indexPath.row] objectForKey:@"screen_shot"]] ] placeholderImage:[UIImage imageNamed:@""] options:SDWebImageProgressiveDownload];
                }
            }
        }
        
         [cell.userImg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", ServiceURL, [[myNotificationArr objectAtIndex:indexPath.row] objectForKey:@"profile_pic"]] ] placeholderImage:[UIImage imageNamed:@""] options:SDWebImageProgressiveDownload];
        
        cell.descLbl.text = [[[myNotificationArr objectAtIndex:indexPath.row] objectForKey:@"title"] lowercaseString];
        
        cell.timeLbl.text = [[myNotificationArr objectAtIndex:indexPath.row] objectForKey:@"posttime"];
    }
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
