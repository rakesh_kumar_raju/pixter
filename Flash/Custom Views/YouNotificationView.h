//
//  YouNotificationView.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 1/4/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YouNotificationView : UIView <UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UITableView *youTable;
    
    NSMutableArray *myNotificationArr;
}

@property (nonatomic, retain) UINavigationController *controller;


- (void)reloadTableView;

- (void)getMyNotofications;


@end
