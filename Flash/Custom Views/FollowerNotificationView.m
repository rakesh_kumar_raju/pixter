//
//  FollowerNotificationView.m
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 1/4/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import "FollowerNotificationView.h"
#import "NotificationTableViewCell.h"
#import "ShowPostsViewController.h"
#import "ProfileViewController.h"

@implementation FollowerNotificationView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)awakeFromNib
{
    followingArr = [NSMutableArray new];
    
   
    [self getMyFollowingNotifications];
    
    [super awakeFromNib];
}


- (void)reloadTableView
{
    [followingTbl reloadData];
}


#pragma mark - Webservice

- (void)getMyFollowingNotifications
{
    if (followingArr.count)
    {
        [followingTbl reloadData];
    }
    
    else
    {
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                [(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", nil];
        
        
        [[ServiceHelper sharedManager] setHudShow:YES];
        
        [[ServiceHelper sharedManager] sendRequestWithMethodName:@"followingNotifications" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
         {
             if (result)
             {
                 if ([[result objectForKey:@"status"] intValue] == 1)
                 {
                     followingArr  = [[result objectForKey:@"notification"] mutableCopy];
                     
                     [self reloadTableView];
                 }
             }
         }];
    }
}


- (void)tapOnPostBtnTap:(UIButton *)sender
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            [(APPDEL).userDetailDict objectForKey:@"userId"], @"userId", [[followingArr objectAtIndex:sender.tag] objectForKey:@"postId"], @"postId", nil];
    
    
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"getSinglePost" setHTTPMethod:@"POST" params:params completion:^(NSDictionary *result)
     {
         if (result)
         {
             if ([[result objectForKey:@"status"] intValue] == 1)
             {
                 ShowPostsViewController *vc = [[ShowPostsViewController alloc] initWithNibName:@"ShowPostsViewController" bundle:nil];
                 
                 vc.hidesBottomBarWhenPushed = YES;
                 
                 
                 vc.postArr = [NSMutableArray arrayWithObject:[result objectForKey:@"post"]];
                 
                 //vc.indexPath = [NSIndexPath indexPathWithIndex:0];
                 
                 [self.controller pushViewController:vc animated:YES];
             }
         }
     }];
}


- (void)tapOnUserImageTap:(UIButton *)sender
{
    ProfileViewController *vc = [[ProfileViewController alloc] initWithNibName:@"ProfileViewController" bundle:nil];
    
    vc.differentUserId = [[followingArr objectAtIndex: sender.tag] objectForKey:@"userId"] ;
    
    [self.controller pushViewController:vc animated:YES];
    
}


#pragma mark - TableView Delegate


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return followingArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"CELL";
    
    NotificationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"NotificationTableViewCell" owner:self options:nil] objectAtIndex:0];
        
        cell.tapOnPost.tag = indexPath.row;
        
        cell.tapOnUserImg.tag = indexPath.row;
        
        
        [cell.tapOnPost addTarget:self action:@selector(tapOnPostBtnTap:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.tapOnUserImg addTarget:self action:@selector(tapOnUserImageTap:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.userImg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", ServiceURL, [[followingArr objectAtIndex:indexPath.row] objectForKey:@"profile_pic"]] ] placeholderImage:[UIImage imageNamed:@""] options:SDWebImageProgressiveDownload];
        
        if ([[[followingArr objectAtIndex:indexPath.row] objectForKey:@"action"] isEqualToString:@"like"] || [[[followingArr objectAtIndex:indexPath.row] objectForKey:@"action"] isEqualToString:@"comment"] || [[[followingArr objectAtIndex:indexPath.row] objectForKey:@"action"] isEqualToString:@"tag"])
        {
            if ([[[followingArr objectAtIndex:indexPath.row] objectForKey:@"type"] isEqualToString:@"image"])
            {
                [cell.mediaImg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", ServiceURL, [[followingArr objectAtIndex:indexPath.row] objectForKey:@"postimage"]] ] placeholderImage:[UIImage imageNamed:@""] options:SDWebImageProgressiveDownload];
            }
            else
            {
                [cell.mediaImg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", ServiceURL, [[followingArr objectAtIndex:indexPath.row] objectForKey:@"screen_shot"]] ] placeholderImage:[UIImage imageNamed:@""] options:SDWebImageProgressiveDownload];
            }
        }
        cell.descLbl.text = [[[followingArr objectAtIndex:indexPath.row] objectForKey:@"title"] lowercaseString];
        
        cell.timeLbl.text = [[followingArr objectAtIndex:indexPath.row] objectForKey:@"posttime"];
    }
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


@end
