//
//  SearchView.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 3/14/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchView : UIView
{
    IBOutlet UITableView *searchTbl;
}

@property (nonatomic, retain) NSMutableArray *searchArr;

@property (nonatomic, retain) UINavigationController *controller;

@property (nonatomic, assign) int searchType;


- (void)setArr:(NSMutableArray *)dataArr;

- (void)reloadTableView;


@end
