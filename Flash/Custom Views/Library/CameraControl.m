//
//  CameraControl.m
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 1/27/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import "CameraControl.h"
#import "AppDelegate.h"

@interface CameraControl ()
{
    CABasicAnimation *theAnimation;
}


@end


@implementation CameraControl




- (IBAction)cameraModeChangeToVideo:(BOOL)value
{
    self.toggleBtn.selected = !self.toggleBtn.selected;
    
    if (self.toggleBtn.selected == YES)
    {
        WeAreRecording = YES;
        
        self.isRecording = YES;
        
        [self.recordingBtn setImage:[UIImage imageNamed:@"videoIcon"] forState:UIControlStateNormal];
        
        [self.toggleBtn setImage:[UIImage imageNamed:@"camera_small"] forState:UIControlStateNormal];
    }
    
    else
    {
        WeAreRecording = NO;
        
        [self.recordingBtn setImage:[UIImage imageNamed:@"cameraIcon"] forState:UIControlStateNormal];
        
        [self.toggleBtn setImage:[UIImage imageNamed:@"video_small"] forState:UIControlStateNormal];
    }
    
    NSString *state = self.toggleBtn.selected == YES ? @"1" : @"0";
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"camcorderToCamera" object:state];
}


- (IBAction)recordOrCAptureTap
{
    if (WeAreRecording == YES)
    {
        NSString *state;
        
        if (self.isRecording == YES)
        {
            state = @"0";
        
            [self addPulseAnimation];
        }
        else
        {
            state = @"1";
            
            [self removePulseAnimation];
        }
       
        [[NSNotificationCenter defaultCenter] postNotificationName:@"record_capture" object:state];

    }
    else
    {
        (APPDEL).isVideo = NO;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"capture" object:nil];
    }
}

- (IBAction)CameraToggleButtonPressed
{
    
}



- (void)addPulseAnimation
{
    theAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    
    theAnimation.duration = 0.7;
    
    theAnimation.repeatCount = HUGE_VALF;
    
    theAnimation.autoreverses = YES;
    
    theAnimation.fromValue = [NSNumber numberWithFloat:1.0];
    
    theAnimation.toValue = [NSNumber numberWithFloat:0.0];
    
    [self.recordingBtn.layer addAnimation:theAnimation forKey:@"animateOpacity"];
}


- (void)removePulseAnimation
{
    [self.recordingBtn.layer removeAnimationForKey:@"animateOpacity"];
}


@end
