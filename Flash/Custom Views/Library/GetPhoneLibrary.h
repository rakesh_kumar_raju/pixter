//
//  GetPhoneLibrary.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 2/2/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <Foundation/Foundation.h>
#include <AssetsLibrary/AssetsLibrary.h>
#import <MobileCoreServices/UTCoreTypes.h>

#import "GallaryData.h"


@protocol GetPhoneLibraryDelegate <NSObject>

@required

- (void)getPhotoAlbum:(NSMutableArray *)groupArr;

- (void)getImage:(ALAsset *)asset;


@end


@interface GetPhoneLibrary : UIView <UICollectionViewDelegate, UICollectionViewDataSource>
{
    IBOutlet UICollectionView *photoCollection;
    
    id <GetPhoneLibraryDelegate> delegate;
    
    int minute, second;
}

@property (nonatomic,strong) id delegate;


@property (nonatomic, strong) NSMutableArray *assetGroupArr;

@property (nonatomic, strong) NSMutableArray *imagesArr;


@property (nonatomic, strong) NSArray *mediaTypes;

@property (nonatomic, strong) ALAssetsLibrary *library;

@property (nonatomic, strong) ALAssetsGroup *assetGroup;


- (void) loadView;

- (void)choosePhotoAlbum:(ALAssetsGroup *)assGroup;


@end
