//
//  CameraControl.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 1/27/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#define CAPTURE_FRAMES_PER_SECOND		20

#import <UIKit/UIKit.h>
#import <CoreMedia/CoreMedia.h>
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>	




@interface CameraControl : UIView 
{    
    BOOL WeAreRecording;
    
   IBOutlet UIView *cameraView;
    
    IBOutlet UIButton *cameraBtn;

}

@property (nonatomic, retain) IBOutlet UIButton *recordingBtn, *toggleBtn;

@property (nonatomic, retain) AVCaptureMovieFileOutput *MovieFileOutput;

@property (nonatomic, assign) BOOL isRecording;


- (IBAction)cameraModeChangeToVideo:(BOOL)value;

- (IBAction)CameraToggleButtonPressed;

- (IBAction)recordOrCAptureTap;


- (void)removePulseAnimation;


@end
