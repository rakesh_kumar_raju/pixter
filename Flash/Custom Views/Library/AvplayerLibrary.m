//
//  AvplayerLibrary.m
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 1/27/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import "AvplayerLibrary.h"

@implementation AvplayerLibrary

@synthesize player, playerLayer;



- (void)addGestureOnPlayer
{
    UITapGestureRecognizer *tapGest = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addScreenTouchEvent:)];
    
    [self addGestureRecognizer:tapGest];
}


- (void)videoUrl:(NSURL *)url frame:(CGRect)frame
{
    [self removeVideoPreviewLayer];
    
    
    player = [[AVPlayer alloc] initWithURL:url];
    
    player.volume = 0.0;
    
    player.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    
    playerLayer = [AVPlayerLayer playerLayerWithPlayer:player];
    
    [playerLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    
    playerLayer.frame = frame;
    
    NSLog(@"%@", playerLayer);
    
    [(APPDEL).AvplayerObj addObject:player];
    
    [self.layer addSublayer: playerLayer];
    
    
  //  [player addObserver:self forKeyPath:@"status" options:0 context:nil];
    
  
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[player currentItem]];

    
    [player play];
    
    
    [self addGestureOnPlayer];
}



- (void)removeObserver
{
    [[NSNotificationCenter defaultCenter] removeObserver:AVPlayerItemDidPlayToEndTimeNotification];
    
    //[player removeObserver:self forKeyPath:@"status"];
}


- (void)removeVideoPreviewLayer
{
    [playerLayer removeFromSuperlayer];
    
    [player replaceCurrentItemWithPlayerItem:nil];
}


- (void)addScreenTouchEvent:(UITapGestureRecognizer *)sender;
{
    if (player.volume == 0.0)
    {
        player.volume = 1.0;
    }
    else
    {
        player.volume = 0.0;
    }
    
   /* if (player.rate == 0)
    {
        [player play];
    }
    else
    {
        [player pause];
    }*/
}



- (void)playerItemDidReachEnd:(NSNotification *)notification
{
    if (notification.name == AVPlayerItemDidPlayToEndTimeNotification)
    {
        AVPlayerItem *p = [notification object];
        
        [p seekToTime:kCMTimeZero];
    }

}



- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object  change:(NSDictionary *)change context:(void *)context
{
    if (object == player && [keyPath isEqualToString:@"status"])
    {
        if (player.status == AVPlayerStatusReadyToPlay)
        {
            NSLog(@"Ready to play");
        }
        else if (player.status == AVPlayerStatusFailed)
        {
        NSLog(@"Faileddddd");
        }
    }
}
@end
