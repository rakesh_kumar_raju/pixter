//
//  GallaryData.m
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 2/6/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import "GallaryData.h"

@implementation GallaryData


+ (id)sharedManager
{
    static GallaryData *sharedMyManager = nil;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        sharedMyManager = [[self alloc] init];
        
    });
    
    return sharedMyManager;
}


- (void) initalize
{
    _gallaryDataArr = [NSMutableArray new];
}


- (void)setGalaryData:(NSMutableArray *)array
{
    _gallaryDataArr = array;
}


- (NSMutableArray *) gallaryDataArr
{
    return _gallaryDataArr;
}


@end

