//
//  GallaryData.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 2/6/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GallaryData : NSObject
{
    
}

@property (nonatomic, readwrite) NSMutableArray *gallaryDataArr;


+ (id)sharedManager;

- (void) initalize;

- (void)setGalaryData:(NSMutableArray *)array;

- (NSMutableArray *) gallaryDataArr;

@end
