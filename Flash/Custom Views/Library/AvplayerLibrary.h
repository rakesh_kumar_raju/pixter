//
//  AvplayerLibrary.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 1/27/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "AppDelegate.h"


@interface AvplayerLibrary : UIView
{
    AVPlayer *player;
    
    AVPlayerLayer  *playerLayer;
}

@property (nonatomic, strong) AVPlayer *player;

@property (nonatomic, strong) AVPlayerLayer  *playerLayer;


- (void)addGestureOnPlayer;

- (void)videoUrl:(NSURL *)url frame:(CGRect)frame;

- (void)removeVideoPreviewLayer;

- (void)removeObserver;

@end
