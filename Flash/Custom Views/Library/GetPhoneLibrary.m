//
//  GetPhoneLibrary.m
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 2/2/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import "GetPhoneLibrary.h"
#import "PhotoLibraryCollectionViewCell.h"

@implementation GetPhoneLibrary
{
    
}
@synthesize delegate;


- (void) loadView
{
    [photoCollection registerNib:[UINib nibWithNibName:@"PhotoLibraryCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"photoCell"];
    
    self.assetGroupArr = [[NSMutableArray alloc] init];
    
    self.imagesArr = [[NSMutableArray alloc] init];
    
    
    ALAssetsLibrary *assetLibrary = [[ALAssetsLibrary alloc] init];
    
    self.library = assetLibrary;
    
    // Load Albums into assetGroups
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       @autoreleasepool {
                           
                           // Group enumerator Block
                           void (^assetGroupEnumerator)(ALAssetsGroup *, BOOL *) = ^(ALAssetsGroup *group, BOOL *stop)
                           {
                               if (group == nil)
                               {
                                   [self reloadTableView];
                                   
                                   return;
                               }
                               
                               // added fix for camera albums order
                               NSString *sGroupPropertyName = (NSString *)[group valueForProperty:ALAssetsGroupPropertyName];
                               
                               NSUInteger nType = [[group valueForProperty:ALAssetsGroupPropertyType] intValue];
                               
                               if ([[sGroupPropertyName lowercaseString] isEqualToString:@"camera roll"] && nType == ALAssetsGroupSavedPhotos)
                               {
                                   [self.assetGroupArr insertObject:group atIndex:0];
                               }
                               else
                               {
                                   if (![self.assetGroupArr containsObject:group])
                                   {
                                       [self.assetGroupArr addObject:group];
                                   }
                                   
                               }
                           };
                           
                           // Group Enumerator Failure Block
                           void (^assetGroupEnumberatorFailure)(NSError *) = ^(NSError *error)
                           {
                               
                               if ([ALAssetsLibrary authorizationStatus] == ALAuthorizationStatusDenied)
                               {
                                   NSString *errorMessage = NSLocalizedString(@"This app does not have access to your photos or videos. You can enable access in Privacy Settings.", nil);
                                   [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Access Denied", nil) message:errorMessage delegate:nil cancelButtonTitle:NSLocalizedString(@"Ok", nil) otherButtonTitles:nil] show];
                                   
                               }
                               else
                               {
                                   NSString *errorMessage = [NSString stringWithFormat:@"Album Error: %@ - %@", [error localizedDescription], [error localizedRecoverySuggestion]];
                                   
                                   [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:errorMessage delegate:nil cancelButtonTitle:NSLocalizedString(@"Ok", nil) otherButtonTitles:nil] show];
                               }
                               
                               
                               NSLog(@"A problem occured %@", [error description]);
                           };
                           
                           // Enumerate Albums
                           [self.library enumerateGroupsWithTypes:ALAssetsGroupAll
                                                       usingBlock:assetGroupEnumerator
                                                     failureBlock:assetGroupEnumberatorFailure];
                           
                       }
                   });
    
}


- (void)reloadTableView
{
    self.assetGroup = [self.assetGroupArr objectAtIndex:0];
    
    [self.assetGroup setAssetsFilter:[self assetFilter]];
    
    
    [self prepareAssets:self.assetGroup];
    
    
   
    
    
    
    [self.delegate getPhotoAlbum:self.assetGroupArr];
}


- (ALAssetsFilter *)assetFilter
{
    //return [ALAssetsFilter allAssets];
    
    //    if([self.mediaTypes containsObject:(NSString *)kUTTypeImage] && [self.mediaTypes containsObject:(NSString *)kUTTypeMovie])
    //    {
    //        return [ALAssetsFilter allAssets];
    //    }
    //     if([self.mediaTypes containsObject:(NSString *)kUTTypeMovie])
    //    {
    //        return [ALAssetsFilter allVideos];
    //    }
    //    else
    //    {
    //        return [ALAssetsFilter allPhotos];
    //    }
    
    return [ALAssetsFilter allPhotos];
}


#pragma mark - Void

- (void)choosePhotoAlbum:(ALAssetsGroup *)assGroup
{
    [assGroup setAssetsFilter:[self assetFilter]];
    
    [self prepareAssets:assGroup];
}


#pragma mark - Get Photos

- (void)prepareAssets :(ALAssetsGroup *)assetGroup
{
    @autoreleasepool
    {
        [self.imagesArr removeAllObjects];
        
        [assetGroup enumerateAssetsUsingBlock:^(ALAsset *result, NSUInteger index, BOOL *stop) {
            
            if (result == nil)
            {
                //Array Reverse
                NSArray* reversedArray = [[self.imagesArr reverseObjectEnumerator] allObjects];
                
                [self.imagesArr removeAllObjects];
                
                self.imagesArr = [NSMutableArray arrayWithArray:reversedArray];
                
                
                [[GallaryData sharedManager] initalize];
                
                [[GallaryData sharedManager] setGalaryData:[reversedArray mutableCopy]];
                
    
                [photoCollection reloadData];
                
                return;
            }
            
            [self.imagesArr addObject:result];
        }];
    }
}


#pragma mark - CollectionView Delegate

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[UIScreen mainScreen] bounds].size.width == 320)
    {
        return  CGSizeMake(75, 75);
    }
    
    else if ([[UIScreen mainScreen] bounds].size.width == 375)
    {
        return  CGSizeMake(87, 87);
    }
    
    else
    {
        return  CGSizeMake(97, 97);
    }
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.imagesArr.count;
}



- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView1 cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *gridCellIdentifier = @"photoCell";
    
    PhotoLibraryCollectionViewCell *cell = (PhotoLibraryCollectionViewCell*)[collectionView1 dequeueReusableCellWithReuseIdentifier:gridCellIdentifier forIndexPath:indexPath];
    
    cell.contentView.autoresizingMask  = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    ALAsset *asset = [self.imagesArr objectAtIndex: indexPath.row];
    
    if ([[asset valueForProperty:ALAssetPropertyType] isEqualToString:ALAssetTypeVideo])
    {
        double value = [[asset valueForProperty:ALAssetPropertyDuration] doubleValue];
        
        cell.durationLbl.text =  [self timeFormatted:value];
        
        cell.durationLbl.hidden = NO;
        
        if (minute == 0 && second <= 30)
        {
           cell.hideImgV.hidden = YES;
        }
        
        else
        {
            cell.hideImgV.hidden = NO;
        }
    }
    
    else
    {
        cell.durationLbl.hidden = YES;
        
        cell.hideImgV.hidden = YES;
    }
    
    
    cell.libImg.image = [UIImage imageWithCGImage:asset.thumbnail];
    
    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    ALAsset *asset = [self.imagesArr objectAtIndex: indexPath.row];
    
    double value = [[asset valueForProperty:ALAssetPropertyDuration] doubleValue];
    
    [self timeFormatted:value];
    
    if ([[asset valueForProperty:ALAssetPropertyType] isEqualToString:ALAssetTypeVideo])
    {
        if (minute == 0 && second <= 30)
        {
            [self.delegate getImage:[self.imagesArr objectAtIndex: indexPath.row]];
        }
    }
    else
    {
        [self.delegate getImage:[self.imagesArr objectAtIndex: indexPath.row]];
    }
}


#pragma mark - Video Duration

- (NSString *)timeFormatted:(double)totalSeconds

{
    NSTimeInterval timeInterval = totalSeconds;
    
    long seconds = lroundf(timeInterval); // Modulo (%) operator below needs int or long
    
    int hour = 0;
    
    minute = seconds/60.0f;
    
    second = seconds % 60;
    
    if (minute > 59)
    {
        hour = minute/60;
        
        minute = minute%60;
        
        
        return [NSString stringWithFormat:@"%02d:%02d:%02d", hour, minute, second];
    }
    else
    {
        
        return [NSString stringWithFormat:@"%02d:%02d", minute, second];
    }
}


@end
