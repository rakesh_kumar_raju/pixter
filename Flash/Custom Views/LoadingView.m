//
//  LoadingView.m
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 2/8/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import "LoadingView.h"

@implementation LoadingView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (void)startAnimating
{
    theAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    
    theAnimation.duration = 0.7;
    
    theAnimation.repeatCount = HUGE_VALF;
    
    theAnimation.autoreverses = YES;
    
    theAnimation.fromValue = [NSNumber numberWithFloat:1.0];
    
    theAnimation.toValue = [NSNumber numberWithFloat:0.0];
    
    [animatingView.layer addAnimation:theAnimation forKey:@"animateOpacity"];
}


- (void)stopAnimating
{
    [animatingView.layer removeAnimationForKey:@"animateOpacity"];
}


@end
