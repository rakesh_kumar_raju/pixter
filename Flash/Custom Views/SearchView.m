//
//  SearchView.m
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 3/14/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import "SearchView.h"
#import "SerachTableViewCell.h"
#import "ProfileViewController.h"
#import "ViewPostBySearchViewController.h"

#import "ServiceHelper.h"

@implementation SearchView


- (void)reloadTableView
{
    [searchTbl reloadData];
}


- (void)setArr:(NSMutableArray *)dataArr
{
    self.searchArr = dataArr;
}


#pragma mark - TableView Delegate


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.searchArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"CELL";
  
    SerachTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];

    
    if (cell == nil)
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"SerachTableViewCell" owner:self options:nil] lastObject];
    }
    
    
    if (self.searchType == 1 || self.searchType == 2)
    {
        if ([[[self.searchArr objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"location"])
        {
            cell.nameLbl.text = [NSString stringWithFormat:@"#%@", [[self.searchArr objectAtIndex: indexPath.row] objectForKey:@"hashtag"]];
            
            cell.totalPostLbl.text = [[self.searchArr objectAtIndex: indexPath.row] objectForKey:@"totalCount"];
            
            cell.userImg.image = [UIImage imageNamed:@"profile_location"];
            
            cell.userImg.contentMode = UIViewContentModeScaleAspectFit;
        }
        else if ([[[self.searchArr objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"hashtag"])
        {
            cell.nameLbl.text = [NSString stringWithFormat:@"#%@", [[self.searchArr objectAtIndex: indexPath.row] objectForKey:@"hashtag"]];
            
            cell.totalPostLbl.text = [[self.searchArr objectAtIndex: indexPath.row] objectForKey:@"totalCount"];
            
            cell.userImg.image = [UIImage imageNamed:@"hash"];
            
            cell.userImg.contentMode = UIViewContentModeScaleToFill;
        }
        else
        {
            cell.nameLbl.text = [NSString stringWithFormat:@"#%@", [[self.searchArr objectAtIndex: indexPath.row] objectForKey:@"hashtag"]];
            
            cell.totalPostLbl.text = [[self.searchArr objectAtIndex: indexPath.row] objectForKey:@"totalCount"];
            
            [cell.userImg sd_setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", ServiceURL, [[self.searchArr objectAtIndex:indexPath.row] valueForKey:@"profile_pic"] ] ] placeholderImage:nil];
            
            cell.userImg.contentMode = UIViewContentModeScaleToFill;

        }
     }
   
    else if (self.searchType == 3)
    {
        cell.userImg.image = [UIImage imageNamed:@"hash"];
        
        cell.nameLbl.text = [NSString stringWithFormat:@"#%@", [[self.searchArr objectAtIndex: indexPath.row] objectForKey:@"hashtag"] ];
        
        cell.totalPostLbl.text = [[self.searchArr objectAtIndex: indexPath.row] objectForKey:@"totalCount"];
    }
  
    else
    {
        cell.nameLbl.text = [[[self.searchArr objectAtIndex:indexPath.row] valueForKey:@"full_name"] lowercaseString];
        

        cell.totalPostLbl.text = [[self.searchArr objectAtIndex: indexPath.row] objectForKey:@"totalCount"];
    }

    return cell;
}


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (![[[self.searchArr objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"location"])
    {
        ProfileViewController *vc = [[ProfileViewController alloc] initWithNibName:@"ProfileViewController" bundle:nil];
        
        vc.differentUserId = [[self.searchArr objectAtIndex: indexPath.row] objectForKey:@"id"] ;
        
        [self.controller pushViewController:vc animated:YES];
    }
}

@end
