//
//  LoadingView.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 2/8/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>


@interface LoadingView : UIView
{
    IBOutlet UIView *animatingView;
    
    CABasicAnimation *theAnimation;
}

- (void)startAnimating;

- (void)stopAnimating;

@end
