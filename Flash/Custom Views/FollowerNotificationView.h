//
//  FollowerNotificationView.h
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 1/4/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NotificationTableViewCell.h"
#import "ServiceHelper.h"

@interface FollowerNotificationView : UIView
{
    IBOutlet UITableView *followingTbl;
    
    NSMutableArray *followingArr;
}

@property (nonatomic, retain) UINavigationController *controller;


- (void)reloadTableView;

- (void)getMyFollowingNotifications;


@end
