//
//  AppDelegate.m
//  Flash
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 12/30/16.
//  Copyright © 2016 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import "AppDelegate.h"
#import "WelcomeViewController.h"
#import "TabbarViewController.h"
#import "AddCommentViewController.h"
#import "PhoneNumberViewController.h"
#import "MessageViewController.h"
#import "MyAccountViewController.h"


@import GoogleMobileAds;

/*#include <arpa/inet.h>
#include <net/if.h>
#include <ifaddrs.h>
#include <net/if_dl.h>*/


@interface AppDelegate () <CLLocationManagerDelegate>
{
    
}

@property (nonatomic ,retain) WelcomeViewController *vc;

@end


@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [Fabric with:@[[Digits class], [Crashlytics class]]];

    
    locationManager = [[CLLocationManager alloc] init];
    
    locationManager.delegate = self;
    
    locationManager.distanceFilter = kCLDistanceFilterNone;
    
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0 &&
        [CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedWhenInUse
        //[CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedAlways
        )
    {
        [locationManager requestWhenInUseAuthorization];
    }
    else
    {
        [locationManager startUpdatingLocation]; //Will update location immediately
    }
    
    [locationManager startUpdatingLocation];

    
    
    [self logUser];

    
    self.AvplayerObj = [NSMutableArray new];
    
    self.tagPeopleArr = [NSMutableArray new];
    
    self.locationStr = @"";
    
    
    WelcomeViewController *vc = [[WelcomeViewController alloc] initWithNibName:@"WelcomeViewController" bundle:nil];
    
    TabbarViewController *tVc = [[TabbarViewController alloc] initWithNibName:@"TabbarViewController" bundle:nil];
    
  
    PhoneNumberViewController *pVc = [[PhoneNumberViewController alloc] initWithNibName:@"PhoneNumberViewController" bundle:nil];
    
    AddCommentViewController *aVc = [[AddCommentViewController alloc] initWithNibName:@"AddCommentViewController" bundle:nil];
    
     MessageViewController *mVc = [[MessageViewController alloc] initWithNibName:@"MessageViewController" bundle:nil];

    MyAccountViewController *mAvc = [[MyAccountViewController alloc] initWithNibName:@"MyAccountViewController" bundle:nil];
    
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"isUserLogin"])
    {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isUserLogin"];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    
    if (![[NSUserDefaults standardUserDefaults] objectForKey: @"message_count"])
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"message_count"];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"first_time_installedApp"])
    {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"first_time_installedApp"];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"not_open_message_Box_first_time"])
    {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"not_open_message_Box_first_time"];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"login_dict"])
    {
   
        self.userDetailDict = [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"login_dict"]];
    }
    
    
    UINavigationController *nVc;
   // nVc = [[UINavigationController alloc] initWithRootViewController:tVc];
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"isUserLogin"] == YES)
    {
        nVc = [[UINavigationController alloc] initWithRootViewController:tVc];
        
       // nVc = [[UINavigationController alloc] initWithRootViewController:pVc];
    }
    else
    {
         nVc = [[UINavigationController alloc] initWithRootViewController:vc];
        
       // nVc = [[UINavigationController alloc] initWithRootViewController:mVc];
    }
    
    //nVc = [[UINavigationController alloc] initWithRootViewController:mAvc];
    
    
    nVc.navigationBar.hidden = YES;
    
    self.window.rootViewController = nVc;
    
    [self.window makeKeyAndVisible];
    
    
    [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleLightContent];


   // [GADMobileAds configureWithApplicationID:@"ca-app-pub-8270141017539622~7623500397"];

     [GADMobileAds configureWithApplicationID:ADMob_APPID];
    
    [[GADRewardBasedVideoAd sharedInstance] loadRequest:[GADRequest request]
                                           withAdUnitID:ADMob_UnitID_For_Reward_Video];
    
    //Push register
    [[UIApplication sharedApplication] registerUserNotificationSettings:
     [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
    
  
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


#pragma mark - Push Notification

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    [application registerForRemoteNotifications];
}


- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    NSLog(@"deviceToken: %@", deviceToken);
    
    NSString * token = [NSString stringWithFormat:@"%@", deviceToken];
    
    //Format token
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    token = [token stringByReplacingOccurrencesOfString:@">" withString:@""];
    
    token = [token stringByReplacingOccurrencesOfString:@"<" withString:@""];
    
    
    [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"device_token"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    if (userInfo)
    {
        if ([[[userInfo objectForKey:@"custom"] objectForKey:@"type"] isEqualToString:@"message"])
        {
            NSString *msgStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"message_count"];
            
            if (msgStr.length)
            {
                [[NSUserDefaults standardUserDefaults] setObject: [NSString stringWithFormat:@"%i", [msgStr intValue] +1] forKey:@"message_count"];
                
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            else
            {
                [[NSUserDefaults standardUserDefaults] setObject: @"1" forKey:@"message_count"];
                
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"new_chat_message" object:[userInfo objectForKey:@"custom"]];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"messageCounter" object:[[NSUserDefaults standardUserDefaults] objectForKey:@"message_count"]];
        }
    }
    
}


#pragma mark - CrashLytic

- (void) logUser
{
    [CrashlyticsKit setUserIdentifier:@"12345"];

    [CrashlyticsKit setUserEmail:@"user@fabric.io"];
    
    [CrashlyticsKit setUserName:@"Test User"];
}


#pragma mark - Get Current Location


- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    self.currentLocation = [[NSDictionary alloc] initWithObjectsAndKeys:[NSString stringWithFormat:@"%f", manager.location.coordinate.latitude], @"lattitude", [NSString stringWithFormat:@"%f", manager.location.coordinate.longitude], @"longitude", nil];
    
   
    [locationManager stopUpdatingLocation];
}


/*static NSString *const DataCounterKeyWWANSent = @"WWANSent";
static NSString *const DataCounterKeyWWANReceived = @"WWANReceived";
static NSString *const DataCounterKeyWiFiSent = @"WiFiSent";
static NSString *const DataCounterKeyWiFiReceived = @"WiFiReceived";


NSDictionary *DataCounters()
{
    
    struct ifaddrs *addrs;
    const struct ifaddrs *cursor;
    
    u_int32_t WiFiSent = 0;
    u_int32_t WiFiReceived = 0;
    u_int32_t WWANSent = 0;
    u_int32_t WWANReceived = 0;
    
    if (getifaddrs(&addrs) == 0)
    {
        cursor = addrs;
        while (cursor != NULL)
        {
            if (cursor->ifa_addr->sa_family == AF_LINK)
            {
#ifdef DEBUG
                const struct if_data *ifa_data = (struct if_data *)cursor->ifa_data;
                if(ifa_data != NULL)
                {
                    NSLog(@"Interface name %s: sent %tu received %tu",cursor->ifa_name,ifa_data->ifi_obytes,ifa_data->ifi_ibytes);
                }
#endif
                
                // name of interfaces:
                // en0 is WiFi
                // pdp_ip0 is WWAN
                NSString *name = [NSString stringWithFormat:@"%s",cursor->ifa_name];
                if ([name hasPrefix:@"en"])
                {
                    const struct if_data *ifa_data = (struct if_data *)cursor->ifa_data;
                    if(ifa_data != NULL)
                    {
                        WiFiSent += ifa_data->ifi_obytes;
                        WiFiReceived += ifa_data->ifi_ibytes;
                    }
                }
                
                if ([name hasPrefix:@"pdp_ip"])
                {
                    const struct if_data *ifa_data = (struct if_data *)cursor->ifa_data;
                    if(ifa_data != NULL)
                    {
                        WWANSent += ifa_data->ifi_obytes;
                        WWANReceived += ifa_data->ifi_ibytes;
                    }
                }
            }
            
            cursor = cursor->ifa_next;
        }
        
        freeifaddrs(addrs);
    }
    
    return @{DataCounterKeyWiFiSent:[NSNumber numberWithUnsignedInt:WiFiSent],
             DataCounterKeyWiFiReceived:[NSNumber numberWithUnsignedInt:WiFiReceived],
             DataCounterKeyWWANSent:[NSNumber numberWithUnsignedInt:WWANSent],
             DataCounterKeyWWANReceived:[NSNumber numberWithUnsignedInt:WWANReceived]};
}*/

@end
